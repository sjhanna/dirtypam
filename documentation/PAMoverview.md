# Overview of the PLA Aerosol Model (PAM)  {#PAMoverview}

\n 
 Models such as atmospheric general circulation models (AGCMs) and air quality
 models often lack the ability to predict aerosol size
 distributions mainly because of the large computational costs that are
 associated with a prognostic treatment of aerosol dynamical processes.
 Instead, parameterizations of aerosol chemical and physical processes
 are commonly based on highly idealized size distributions, for example, with
 constant size parameters. However, aerosol size distributions are
 considerably variable over a wide range of spatial and temporal scales
 in the atmosphere \cite Gilliani1995 \cite vonSalzen2005 \cite Birmili2001. Unaccounted for
 variability in size distributions is a primary source of uncertainty
 in studies of aerosol effects on clouds and climate
 \cite Feingold2006.
\n 
 Consequently, accurate and computationally efficient numerical methods
 are required for the representation of aerosol size distributions
 in aerosol models.
\n\n 
 Observed aerosol number size distributions can often be well
 approximated by a series of overlapping modes, with the size
 distribution for each mode represented by the log-normal distribution,
 i.e.,

\f{equation*} n (R_{p}) = \frac{d N}{d \ln \left( R_{p}/R_{0} \right)} \\
 = \frac{N}{\sqrt{2 \pi} \ln \sigma} \exp \left\{ -
 \frac{\left[ \ln \left( R_{p}/R_{0} \right)
 - \ln \left( R_{g}/R_{0} \right)
 \right]^{2}}{2 \ln^{2} \sigma} \right\}
\f}
 
 with particle radius \f$R_{p}\f$, mode mean particle radius \f$R_{g}\f$,
 total number of particles per unit
 volume (or mass) \f$N\f$, and geometric standard deviation \f$\sigma\f$
 \cite Whitby1978 \cite Seinfeld1998 \cite Remer1999. \f$R_{0}\f$ is
 an arbitrary reference radius (e.g. 1~\f$\mu\f$m) that is required for a
 dimensionally correct formulation.
\n\n
 The so-called modal approach in aerosol modelling is based on the idea
 that aerosol size distributions can be approximated using
 statistical moments with idealized properties
 \cite Whitby1981 \cite Binkowski1995 \cite Whitby1997 \cite Wilck1997 \cite Ackermann1998 \cite Harrington1998 \cite Wilson2001.
 Typically, a number of overlapping log-normally distributed modes
 (e.g. for nucleation, accumulation and coarse mode) are assumed to
 account for different types of aerosols and processes in the
 atmosphere. An important variant of the modal
 approach is the quadrature method of moments, QMOM
 \cite McGraw1997 \cite Wright2001 \cite Yu2003, which is not necessarily constrained to
 log-normally distributed modes.
\n\n 
 In practice, the log-normal distribution often yields good approximations of size
 distributions with clearly distinct modes under ideal conditions. However,
 it has been recognized that transport and non-linear dynamical processes, such
 as aerosol activation, condensation, and chemical production
 can lead to substantially skewed or even discontinuous size distributions
 [e.g. \cite Kerminen1995 \cite Gilliani1995 \cite vonSalzen1999b \cite Zhang2002. Skewed
 size distributions are often related to cloud processes so that an
 incomplete accounting of skewness in aerosol size distributions
 is a substantial concern for studies of the indirect effects of aerosols on climate.
\n\n 
 Size distributions with non-ideal features can be well
 approximated with the bin approach, also often called ''sectional'' or
 ''discrete'' approach \cite Gelbard1980 \cite Warren1985 \cite Gelbard1990 \cite Seigneur1982
 \cite Jacobson1997 \cite Lurmann1997 \cite Russell1998 \cite Meng1998 \cite vonSalzen1999a \cite Pilinis2000 \cite Gong2003.
 According to the bin approach, a range of particle sizes is
 subdivided into a number of bins, or sections, with discrete values
 of the size distribution in each bin.
\n\n 
 It can be generally distinguished between single- and multi-moment bin
 schemes, depending on the number of statistical moments of the size
 distribution that are predicted.
\n\n 
 In its original form with size-independent sub-bin particle
 size distributions [e.g. \cite Gelbard1980], the single-moment approach is
 subject to significant numerical diffusion when applied to
 simulations of aerosol dynamical processes
 [e.g. \cite Seigneur1986]. However, an even more substantial disadvantage of the
 method is that the application of the method to the continuity
 equation for a single moment (e.g. particle mass) is not sufficient to
 ensure conservation of other moments (e.g. particle number). Other
 moments of the size distribution can be obtained from diagnostic
 relationships that are based on the predicted mass size distribution.
 Owing to the lack of a prognostic treatment, the diagnosed quantities
 can violate fundamental mathematical constraints, such as
 inequality relations and positive definiteness \cite Feller1971. These can be
 significant problems if an insufficient number of bins is
 used. Despite these limitations, single-moment schemes are still
 widely used in aerosol models.
\n\n 
 Numerous multi-moment schemes have been suggested as improvements of
 the basic single-moment bin approach. For example, \cite Tzivion1987 used
 bin-integrated aerosol number and mass concentrations to obtain
 sub-bin aerosol number and size distributions as linear functions of
 particle mass. In another example, \cite Jacobson1997 assigned the
 bin-integrated aerosol number and mass concentrations to a single particle size in
 each bin. According to this approach, parameterizations of aerosol
 dynamical processes are applied to the single-particle representation
 of the size distribution in each bin. Transfer of particles between different bins
 is accomplished by combining particles with different sizes to produce a
 new distribution for a single particle size in each bin.
\n\n 
 Several studies compared results of the modal and bin approaches based
 on a limited number of simulations of aerosol dynamical processes
 [e.g. \cite Seigneur1986 \cite Zhang1999 \cite Wilson2001 \cite Zhang2002 \cite Korhonen2003.
 According to the studies, the modal approach performed well in terms
 of computationally efficiency and is able to reproduce important
 features of the size distributions. However, results with
 this approach were noticeably biased in simulations of coagulation and
 condensational growth in some of the studies.
 On the other hand, the bin approach produced acceptable results for a
 relatively large number of bins. Computational efficiencies were
 generally lower for this approach in comparison to the modal approach.
\n\n 
 Both approaches have certain constraints that are problematic for the
 development of parameterizations in models.
\n\n 
 For the bin method, the need to use a relatively large number of bins for
 sufficiently accurate solutions potentially
 leads to considerable computational costs. For example, simulations
 of advection and horizontal diffusion cause an overhead of about 4\%
 in CPU time for each additional prognostic tracer in the current
 operational version of the Canadian Centre for Climate Modelling and
 Analysis (CCCma) AGCM3. Parameterizations
 of aerosol dynamical processes may lead to further and disproportional
 increases in CPU time.
\n\n 
 A disadvantage for application of the modal approach in general atmospheric
 models is the need to account for varying degrees of overlap between
 different modes. As already pointed out, transport and aerosol dynamical processes
 often lead to changes in overlap and shape of modes. Relatively
 complicated treatments of aerosol dynamical processes have
 been proposed to allow distinct modes to merge into a single mode, for
 example [e.g. \cite Harrington1998 \cite Whitby2002.
\n\n 
 Similar to the modal- and single-moment bin approaches, the mathematical
 representation of aerosol size distributions in PAM
 is based on a compromise between the need for accuracy and flexibility
 of the approach on one hand and numerical efficiency on the other hand.
 Certain advantages offered by the  modal approach (e.g. observed aerosols are
 often nearly log-normally distributed in parts of the size spectrum) and the 
 single-moment bin approach (e.g. flexibility and simplicity) can be exploited for the
 development of a hybrid method with considerably improved performance.
\n\n 
 The mathematical framework of PAM is described in @ref PAMtheory.

---

This material is adapted from "von Salzen, K.: Piecewise log-normal approximation of size distributions for aerosol modelling, Atmos. Chem. Phys., 6, 1351–1372, https://doi.org/10.5194/acp-6-1351-2006, 2006." \cite vonSalzen2006


