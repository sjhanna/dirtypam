program driver_pam
  !-----------------------------------------------------------------------
  !     purpose:
  !     --------
  !     simple single column modelling framework based on idealized
  !     atmospheric conditions for driving pam.
  !
  !     history:
  !     --------
  !     * sep 26/2017 - k.vonsalzen   addition of aerosol code
  !     * jul 19/2017 - k.vonsalzen   completely rewritten. aerosol code was
  !     *                             removed and the code reconfigured to
  !     *                             better support use of aircraft data
  !     * may 7/2013  - k.vonsalzen   updated for i/o from netcdf and code
  !     *                             updates from canam4.3
  !     * feb 10/2010 - k.vonsalzen   updated for gcm15h; included
  !     *                             remaining parameterizations.
  !     * jan 15/2009 - k.vonsalzen   updated for gcm15g and new wet
  !     *                             deposition parameterization.
  !     * nov 27/2007 - k.vonsalzen   new, based on adiabatic parcel model.
  !
  !-----------------------------------------------------------------------
  !
  use fpdef            ! floating point precision information
  use runinfo          ! atmospheric properties and other information
  ! for general setup of scm simulation
  use psizes           ! contains dimensional parameters for
  ! driving atmospheric model
  use iodat            ! for input/output of model data
  use iodatif          ! contains i/o subroutine interface defitions
  use sdphys           ! physical constants
  use trcind, only : iso2,ihpo,igs6,igsp,idms ! gas-phase tracer indices
  !
  !> >>> * AEROSOL * >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
  use compar
  use sdparm
  use rdmod
  !< <<< * AEROSOL * <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
  !
  implicit none
  !
  real :: aits
  real :: am3loads
  real :: co2_ppm
  real :: dsloads
  real :: dt
  real :: dts
  real :: fac
  real :: fdphi0s
  real :: fphis
  real :: fr3
  real :: frtime
  integer :: i
  integer :: ichk
  integer :: il
  integer :: il1
  integer :: il2
  integer :: ilev1
  integer :: inits
  integer :: irfrc
  integer :: is
  integer :: isaintti
  integer :: isdiagi
  integer :: isdusti
  integer :: isub
  integer :: isvdust
  integer :: it
  integer :: itx
  integer :: ity
  integer :: iz
  integer :: j
  integer :: k
  integer :: kextti
  integer :: kintti
  integer :: l
  integer :: leva
  integer :: mynode
  integer :: n
  integer :: nd
  integer :: nday
  integer :: ndtim
  integer :: nhr
  integer :: nlvl
  integer :: nlvl1
  integer :: nmin
  integer :: nrmfldi
  integer :: nsec
  integer :: nsiz
  integer :: nstp
  integer :: nstpo
  integer :: ntr
  integer :: nuact
  integer :: nucoa
  integer :: nupar
  integer :: nurad
  real :: saverad
  real :: sicn_crt
  real :: ssloads
  real :: taul
  real :: taur
  real :: wso2
  real :: x55
  real :: x86
  real :: zdayl
  real :: zero
  real :: zref
  !
  !     * physical and chemical fields for driving atmospheric model.
  !
  real, allocatable, dimension(:,:)   :: dp !<
  real, allocatable, dimension(:,:)   :: zmratep !<
  real, allocatable, dimension(:,:)   :: zmlwc !<
  real, allocatable, dimension(:,:)   :: zfsnow !<
  real, allocatable, dimension(:,:)   :: zfrain !<
  real, allocatable, dimension(:,:)   :: zclf !<
  real, allocatable, dimension(:,:)   :: clrfr !<
  real, allocatable, dimension(:,:)   :: clrfs !<
  real, allocatable, dimension(:,:)   :: zfevap !<
  real, allocatable, dimension(:,:)   :: zfsubl !<
  real, allocatable, dimension(:,:)   :: zh !<
  real, allocatable, dimension(:,:)   :: zf !<
  real, allocatable, dimension(:,:)   :: ph !<
  real, allocatable, dimension(:,:)   :: pf !<
  real, allocatable, dimension(:,:)   :: throw !<
  real, allocatable, dimension(:,:)   :: qrow !<
  real, allocatable, dimension(:,:)   :: qtn !<
  real, allocatable, dimension(:,:)   :: hmn !<
  real, allocatable, dimension(:,:)   :: rhc !<
  real, allocatable, dimension(:,:)   :: wg !<
  real, allocatable, dimension(:,:)   :: wsub !<
  real, allocatable, dimension(:,:)   :: rhoc !<
  real, allocatable, dimension(:,:)   :: tfrow !<
  real, allocatable, dimension(:,:)   :: shtj !<
  real, allocatable, dimension(:,:)   :: shj !<
  real, allocatable, dimension(:,:)   :: dshj !<
  real, allocatable, dimension(:,:)   :: bghpo !<
  real, allocatable, dimension(:,:)   :: taus !<
  real, allocatable, dimension(:,:)   :: tauop !<
  real, allocatable, dimension(:,:)   :: xburd !<
  real, allocatable, dimension(:,:)   :: tmpscl !<
  real, allocatable, dimension(:,:)   :: dprad !<
  real, allocatable, dimension(:,:)   :: taut !<
  real, allocatable, dimension(:,:)   :: tauq !<
  real, allocatable, dimension(:,:)   :: throwr !<
  real, allocatable, dimension(:,:)   :: qrowr !<
  real, allocatable, dimension(:,:)   :: rh !<
  real, allocatable, dimension(:,:)   :: qlwc !<
  real, allocatable, dimension(:,:)   :: qlwcr !<
  real, allocatable, dimension(:,:)   :: almx !<
  real, allocatable, dimension(:,:)   :: almc !<
  real, allocatable, dimension(:,:)   :: dtdt !<
  real, allocatable, dimension(:,:)   :: dqdt !<
  real, allocatable, dimension(:,:)   :: dqldt !<
  real, allocatable, dimension(:,:)   :: dudt !<
  real, allocatable, dimension(:,:)   :: urow !<
  real, allocatable, dimension(:,:)   :: vrow !<
  real, allocatable, dimension(:,:)   :: urowr !<
  real, allocatable, dimension(:,:)   :: taum !<
  real, allocatable, dimension(:,:)   :: rkm !<
  real, allocatable, dimension(:,:)   :: rkh !<
  real, allocatable, dimension(:,:)   :: ri !<
  real, allocatable, dimension(:,:)   :: dqndg !<
  real, allocatable, dimension(:,:)   :: dtndg !<
  real, allocatable, dimension(:,:)   :: dqvdf !<
  real, allocatable, dimension(:,:)   :: dtvdf !<
  real, allocatable, dimension(:,:)   :: dqadv !<
  real, allocatable, dimension(:,:)   :: dtadv !<
  real, allocatable, dimension(:,:)   :: perta !<
  real, allocatable, dimension(:,:)   :: perrv !<
  real, allocatable, dimension(:,:)   :: tvp !<
  real, allocatable, dimension(:,:)   :: beta !<
  real, allocatable, dimension(:,:)   :: radeqv !<
  real, allocatable, dimension(:,:)   :: tautr !<
  real, allocatable, dimension(:,:)   :: tauqr !<
  real, allocatable, dimension(:,:)   :: taumr !<
  real, allocatable, dimension(:,:)   :: qcwa !<
  real, allocatable, dimension(:,:)   :: radeqva !<
  real, allocatable, dimension(:,:)   :: zcdnrow !<
  real, allocatable, dimension(:,:)   :: cdnrow !<
  real, allocatable, dimension(:,:)   :: rhoa !< Air density \f$[kg/m^3]\f$ 
  real, allocatable, dimension(:,:)   :: dqphs !<
  real, allocatable, dimension(:,:)   :: dtphs !<
  real, allocatable, dimension(:,:)   :: zcdnpam !<
  real, allocatable, dimension(:)   :: ft !<
  real, allocatable, dimension(:)   :: ft1 !<
  real, allocatable, dimension(:,:) :: flvt !<
  real, allocatable, dimension(:,:) :: flvt1 !<
  real, allocatable, dimension(:,:) :: flvt2 !<
  real, allocatable, dimension(:,:) :: flv1t !<
  real, allocatable, dimension(:,:) :: flv1t1 !<
  real, allocatable, dimension(:,:,:)   :: flvtn !<
  real, allocatable, dimension(:,:,:)   :: flvt1n !<
  real, allocatable, dimension(:,:,:)   :: flvt1s !<
  real, allocatable, dimension(:) :: pcp !<
  real, allocatable, dimension(:) :: cdm !<
  real, allocatable, dimension(:) :: ustar !<
  real, allocatable, dimension(:) :: tg !<
  real, allocatable, dimension(:) :: thliqg !<
  real, allocatable, dimension(:) :: zo !<
  real, allocatable, dimension(:) :: zspda !<
  real, allocatable, dimension(:) :: qsens !<
  real, allocatable, dimension(:) :: qlat !<
  real, allocatable, dimension(:) :: zspd !<
  real, allocatable, dimension(:) :: pbltrow !<
  real, allocatable, dimension(:) :: pft !<
  real, allocatable, dimension(:) :: gtrow !<
  real, allocatable, dimension(:) :: pressg !<
  real, allocatable, dimension(:) :: atime !<
  real, allocatable, dimension(:) :: wgt !<
  real, allocatable, dimension(:,:,:) :: xrow !<
  real, allocatable, dimension(:,:,:) :: dxdt !<
  real, allocatable, dimension(:,:,:) :: taux !<
  real, allocatable, dimension(:,:,:) :: tauxr !<
  !
  !     * other stuff.
  !
  integer*4 :: leng !<
  logical :: kmetout !<
  logical :: ktime1 !<
  character(len=50) :: cfile !<
  character(len=50) :: cfilb !<
  character(len=50) :: cfilr !<
  integer, parameter :: yspday=24*60*60 !< seconds per day
  integer, parameter :: ysphr=60*60 !< seconds per hour
  integer, parameter :: yspmin=60 !< seconds per minute
  real, parameter :: zcthr=.99 !<
  real, parameter :: spv=99999. !<
  real, parameter :: pi=3.1415926 !< pi
  real :: dec !< declination of the earth
  real :: doy !< current day of year
  real :: hr_angle !< hour angle
  real, allocatable, dimension(:) :: radj !< latitude in radians
  !
  !     * input/output file unit numbers.
  !
  data nupar /5 /
  !
  !> >>> * AEROSOL * >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
  !
  real, allocatable, dimension(:,:,:) :: sfrcrol !<
  real, allocatable, dimension(:,:,:) :: xarow !<
  real, allocatable, dimension(:,:,:) :: xcrow !<
  real, allocatable, dimension(:,:,:) :: dxadt !<
  real, allocatable, dimension(:,:,:) :: dxcdt !<
  real, allocatable, dimension(:,:,:) :: xrowr !<
  real, allocatable, dimension(:,:,:) :: sfrcr !<
  real, allocatable, dimension(:) :: dmso !<
  real, allocatable, dimension(:) :: ddmsdt !<
  real, allocatable, dimension(:) :: cszrow !<
  real, allocatable, dimension(:) :: dox4row !<
  real, allocatable, dimension(:) :: doxdrow !<
  real, allocatable, dimension(:) :: noxdrow !<
  real, allocatable, dimension(:) :: dd4row !<
  real, allocatable, dimension(:) :: snorow !<
  real, allocatable, dimension(:) :: hmfnrow !<
  real, allocatable, dimension(:) :: zfor !<
  real, allocatable, dimension(:,:)   :: sgpp !<
  real, allocatable, dimension(:,:)   :: ogpp !<
  real, allocatable, dimension(:,:)   :: docem1 !<
  real, allocatable, dimension(:,:)   :: docem2 !<
  real, allocatable, dimension(:,:)   :: docem3 !<
  real, allocatable, dimension(:,:)   :: docem4 !<
  real, allocatable, dimension(:,:)   :: dbcem1 !<
  real, allocatable, dimension(:,:)   :: dbcem2 !<
  real, allocatable, dimension(:,:)   :: dbcem3 !<
  real, allocatable, dimension(:,:)   :: dbcem4 !<
  real, allocatable, dimension(:,:)   :: dsuem1 !<
  real, allocatable, dimension(:,:)   :: dsuem2 !<
  real, allocatable, dimension(:,:)   :: dsuem3 !<
  real, allocatable, dimension(:,:)   :: dsuem4 !<
  real, allocatable, dimension(:,:)   :: so2em !<
  real, allocatable, dimension(:,:)   :: orgem !<
  real, allocatable, dimension(:,:)   :: o3row !<
  real, allocatable, dimension(:,:)   :: hno3row !<
  real, allocatable, dimension(:,:)   :: nh3row !<
  real, allocatable, dimension(:,:)   :: nh4row !<
  real, allocatable, dimension(:,:)   :: no3row !<
  real, allocatable, dimension(:,:)   :: ohrow !<
  real, allocatable, dimension(:,:)   :: ssldrow !<
  real, allocatable, dimension(:,:)   :: ressrow !<
  real, allocatable, dimension(:,:)   :: vessrow !<
  real, allocatable, dimension(:,:)   :: dsldrow !<
  real, allocatable, dimension(:,:)   :: vedsrow !<
  real, allocatable, dimension(:,:)   :: redsrow !<
  real, allocatable, dimension(:,:)   :: bcldrow !<
  real, allocatable, dimension(:,:)   :: vebcrow !<
  real, allocatable, dimension(:,:)   :: rebcrow !<
  real, allocatable, dimension(:,:)   :: ocldrow !<
  real, allocatable, dimension(:,:)   :: veocrow !<
  real, allocatable, dimension(:,:)   :: reocrow !<
  real, allocatable, dimension(:,:)   :: amldrow !<
  real, allocatable, dimension(:,:)   :: veamrow !<
  real, allocatable, dimension(:,:)   :: reamrow !<
  real, allocatable, dimension(:,:)   :: fr1row !<
  real, allocatable, dimension(:,:)   :: fr2row !<
  real, allocatable, dimension(:,:) :: cornrow !<
  real, allocatable, dimension(:,:) :: cormrow !<
  real, allocatable, dimension(:,:) :: rsn1row !<
  real, allocatable, dimension(:,:) :: rsm1row !<
  real, allocatable, dimension(:,:) :: rsn2row !<
  real, allocatable, dimension(:,:) :: rsm2row !<
  real, allocatable, dimension(:,:) :: rsn3row !<
  real, allocatable, dimension(:,:) :: rsm3row !<
  real, allocatable, dimension(:,:) :: so4load !<
  real, allocatable, dimension(:,:) :: bcyload !<
  real, allocatable, dimension(:,:) :: ocyload !<
  real, allocatable, dimension(:,:) :: bcoload !<
  real, allocatable, dimension(:,:) :: ocoload !<
  real, allocatable, dimension(:,:) :: reso4 !<
  real, allocatable, dimension(:,:) :: rebcy !<
  real, allocatable, dimension(:,:) :: reocy !<
  real, allocatable, dimension(:,:) :: veso4 !<
  real, allocatable, dimension(:,:) :: vebcy !<
  real, allocatable, dimension(:,:) :: veocy !<
  real, allocatable, dimension(:,:) :: aods !<
  real, allocatable, dimension(:,:) :: aod86 !<
  real, allocatable, dimension(:,:) :: mass !<
  real, allocatable, dimension(:,:) :: abss !<
  real, allocatable, dimension(:,:) :: exts !<
  real, allocatable, dimension(:,:) :: angstr !<
  real, allocatable, dimension(:) :: vrn1row !<
  real, allocatable, dimension(:) :: vrm1row !<
  real, allocatable, dimension(:) :: vrn2row !<
  real, allocatable, dimension(:) :: vrm2row !<
  real, allocatable, dimension(:) :: vrn3row !<
  real, allocatable, dimension(:) :: vrm3row !<
  real, allocatable, dimension(:) :: vncnrow !<
  real, allocatable, dimension(:) :: vasnrow !<
  real, allocatable, dimension(:) :: vscdrow !<
  real, allocatable, dimension(:) :: vgsprow !<
  real, allocatable, dimension(:,:) :: defxrow !<
  real, allocatable, dimension(:,:) :: defnrow !<
  real, allocatable, dimension(:,:) :: sdvlrow !<
  real, allocatable, dimension(:,:) :: sncnrow !<
  real, allocatable, dimension(:,:) :: ssunrow !<
  real, allocatable, dimension(:,:) :: scndrow !<
  real, allocatable, dimension(:,:) :: sgsprow !<
  real, allocatable, dimension(:,:) :: ccnrow !<
  real, allocatable, dimension(:,:) :: ccnerow !<
  real, allocatable, dimension(:,:) :: cc02row !<
  real, allocatable, dimension(:,:) :: cc04row !<
  real, allocatable, dimension(:,:) :: cc08row !<
  real, allocatable, dimension(:,:) :: cc16row !<
  real, allocatable, dimension(:,:) :: rcrirow !<
  real, allocatable, dimension(:,:) :: supsrow !<
  real, allocatable, dimension(:,:) :: henrrow !<
  real, allocatable, dimension(:,:) :: o3frrow !<
  real, allocatable, dimension(:,:) :: wparrow !<
  real, allocatable, dimension(:,:) :: ntrow !<
  real, allocatable, dimension(:,:) :: n20row !<
  real, allocatable, dimension(:,:) :: n50row !<
  real, allocatable, dimension(:,:) :: n100row !<
  real, allocatable, dimension(:,:) :: n200row !<
  real, allocatable, dimension(:,:) :: wtrow !<
  real, allocatable, dimension(:,:) :: w20row !<
  real, allocatable, dimension(:,:) :: w50row !<
  real, allocatable, dimension(:,:) :: w100row !<
  real, allocatable, dimension(:,:) :: w200row !<
  real, allocatable, dimension(:,:) :: acasrow !<
  real, allocatable, dimension(:,:) :: acoarow !<
  real, allocatable, dimension(:,:) :: acbcrow !<
  real, allocatable, dimension(:,:) :: acssrow !<
  real, allocatable, dimension(:,:) :: acmdrow !<
  real, allocatable, dimension(:,:) :: h2o2frrow !<
  real, allocatable, dimension(:,:)   :: fcanrow !<
  real, allocatable, dimension(:) :: smfrac !<
  real, allocatable, dimension(:) :: fcs !<
  real, allocatable, dimension(:) :: fgs !<
  real, allocatable, dimension(:) :: fc !<
  real, allocatable, dimension(:) :: fg !<
  real, allocatable, dimension(:) :: zspdso !<
  real, allocatable, dimension(:) :: zspdsbs !<
  real, allocatable, dimension(:) :: cdml !<
  real, allocatable, dimension(:) :: cdmnl !<
  real, allocatable, dimension(:) :: flnd !<
  real, allocatable, dimension(:) :: focn !<
  real, allocatable, dimension(:) :: sicn !<
  real, allocatable, dimension(:) :: fcant !<
  real, allocatable, dimension(:) :: bsfrac !<
  real, allocatable, dimension(:) :: pdsfrow !<
  real, allocatable, dimension(:) :: atau !<
  real, allocatable, dimension(:) :: ustarbs !<
  real, allocatable, dimension(:) :: fnrol !<
  real, allocatable, dimension(:) :: zfsh !<
  real, allocatable, dimension(:) :: suz0row !<
  real, allocatable, dimension(:) :: spotrow !<
  real, allocatable, dimension(:) :: st02row !<
  real, allocatable, dimension(:) :: st03row !<
  real, allocatable, dimension(:) :: st04row !<
  real, allocatable, dimension(:) :: st06row !<
  real, allocatable, dimension(:) :: st13row !<
  real, allocatable, dimension(:) :: st14row !<
  real, allocatable, dimension(:) :: st15row !<
  real, allocatable, dimension(:) :: st16row !<
  real, allocatable, dimension(:) :: st17row !<
  real, allocatable, dimension(:,:,:) :: sdnurow !<
  real, allocatable, dimension(:,:,:) :: sdmarow !<
  real, allocatable, dimension(:,:,:) :: sdacrow !<
  real, allocatable, dimension(:,:,:) :: sdcorow !<
  real, allocatable, dimension(:,:,:) :: sssnrow !<
  real, allocatable, dimension(:,:,:) :: smdnrow !<
  real, allocatable, dimension(:,:,:) :: sianrow !<
  real, allocatable, dimension(:,:,:) :: sssmrow !<
  real, allocatable, dimension(:,:,:) :: smdmrow !<
  real, allocatable, dimension(:,:,:) :: sewmrow !<
  real, allocatable, dimension(:,:,:) :: ssumrow !<
  real, allocatable, dimension(:,:,:) :: socmrow !<
  real, allocatable, dimension(:,:,:) :: sbcmrow !<
  real, allocatable, dimension(:,:,:) :: siwmrow !<
  real, allocatable, dimension(:) :: tnssrow !<
  real, allocatable, dimension(:) :: tnmdrow !<
  real, allocatable, dimension(:) :: tniarow !<
  real, allocatable, dimension(:) :: tmssrow !<
  real, allocatable, dimension(:) :: tmmdrow !<
  real, allocatable, dimension(:) :: tmocrow !<
  real, allocatable, dimension(:) :: tmbcrow !<
  real, allocatable, dimension(:) :: tmsprow !<
  real, allocatable, dimension(:,:) :: snssrow !<
  real, allocatable, dimension(:,:) :: snmdrow !<
  real, allocatable, dimension(:,:) :: sniarow !<
  real, allocatable, dimension(:,:) :: smssrow !<
  real, allocatable, dimension(:,:) :: smmdrow !<
  real, allocatable, dimension(:,:) :: smocrow !<
  real, allocatable, dimension(:,:) :: smbcrow !<
  real, allocatable, dimension(:,:) :: smsprow !<
  real, allocatable, dimension(:,:) :: siwhrow !<
  real, allocatable, dimension(:,:) :: sewhrow !<
  real, allocatable, dimension(:,:) :: pm25row !<
  real, allocatable, dimension(:,:) :: pm10row !<
  real, allocatable, dimension(:,:) :: dm25row !<
  real, allocatable, dimension(:,:) :: dm10row !<
  real, allocatable, dimension(:) :: voaerow !<
  real, allocatable, dimension(:) :: vbcerow !<
  real, allocatable, dimension(:) :: vaserow !<
  real, allocatable, dimension(:) :: vmderow !<
  real, allocatable, dimension(:) :: vsserow !<
  real, allocatable, dimension(:) :: voawrow !<
  real, allocatable, dimension(:) :: vbcwrow !<
  real, allocatable, dimension(:) :: vaswrow !<
  real, allocatable, dimension(:) :: vmdwrow !<
  real, allocatable, dimension(:) :: vsswrow !<
  real, allocatable, dimension(:) :: voadrow !<
  real, allocatable, dimension(:) :: vbcdrow !<
  real, allocatable, dimension(:) :: vasdrow !<
  real, allocatable, dimension(:) :: vmddrow !<
  real, allocatable, dimension(:) :: vssdrow !<
  real, allocatable, dimension(:) :: voagrow !<
  real, allocatable, dimension(:) :: vbcgrow !<
  real, allocatable, dimension(:) :: vasgrow !<
  real, allocatable, dimension(:) :: vmdgrow !<
  real, allocatable, dimension(:) :: vssgrow !<
  real, allocatable, dimension(:) :: vasirow !<
  real, allocatable, dimension(:) :: vas1row !<
  real, allocatable, dimension(:) :: vas2row !<
  real, allocatable, dimension(:) :: vas3row !<
  real, allocatable, dimension(:) :: vccnrow !<
  real, allocatable, dimension(:) :: vcnerow !<
  real, allocatable, dimension(:,:) :: dmacrow !<
  real, allocatable, dimension(:,:) :: dmcorow !<
  real, allocatable, dimension(:,:) :: dnacrow !<
  real, allocatable, dimension(:,:) :: dncorow !<
  real, allocatable, dimension(:) :: duwdrow !<
  real, allocatable, dimension(:) :: dustrow !<
  real, allocatable, dimension(:) :: duthrow !<
  real, allocatable, dimension(:) :: fallrow !<
  real, allocatable, dimension(:) :: fa10row !<
  real, allocatable, dimension(:) :: fa2row !<
  real, allocatable, dimension(:) :: fa1row !<
  real, allocatable, dimension(:) :: usmkrow !<
  real, allocatable, dimension(:) :: defarow !<
  real, allocatable, dimension(:) :: defcrow !<
  real, allocatable, dimension(:,:) :: bcicrow !<
  real, allocatable, dimension(:) :: depbrol !<
  real, allocatable, dimension(:,:) :: sulifrc !<
  real, allocatable, dimension(:,:) :: rsuifrc !<
  real, allocatable, dimension(:,:) :: vsuifrc !<
  real, allocatable, dimension(:,:) :: f1sufrc !<
  real, allocatable, dimension(:,:) :: f2sufrc !<
  real, allocatable, dimension(:,:) :: bclifrc !<
  real, allocatable, dimension(:,:) :: rbcifrc !<
  real, allocatable, dimension(:,:) :: vbcifrc !<
  real, allocatable, dimension(:,:) :: f1bcfrc !<
  real, allocatable, dimension(:,:) :: f2bcfrc !<
  real, allocatable, dimension(:,:) :: oclifrc !<
  real, allocatable, dimension(:,:) :: rocifrc !<
  real, allocatable, dimension(:,:) :: vocifrc !<
  real, allocatable, dimension(:,:) :: f1ocfrc !<
  real, allocatable, dimension(:,:) :: f2ocfrc !<
  real, allocatable, dimension(:) :: voabrd !<
  real, allocatable, dimension(:) :: vbcbrd !<
  real, allocatable, dimension(:) :: vasbrd !<
  real, allocatable, dimension(:) :: vmdbrd !<
  real, allocatable, dimension(:) :: vssbrd !<
  real, allocatable, dimension(:) :: vmdn !<
  real, allocatable, dimension(:) :: vssn !<
  real, allocatable, dimension(:) :: vinn !<
  real, allocatable, dimension(:) :: cldt !<
  real, allocatable, dimension(:) :: rmu !<
  real, allocatable, dimension(:,:) :: coacnc !<
  real, allocatable, dimension(:,:) :: cbccnc !<
  real, allocatable, dimension(:,:) :: cascnc !<
  real, allocatable, dimension(:,:) :: cmdcnc !<
  real, allocatable, dimension(:,:) :: csscnc !<
  real, allocatable, dimension(:,:) :: cmdn !<
  real, allocatable, dimension(:,:) :: cssn !<
  real, allocatable, dimension(:,:) :: cinn !<
  real, allocatable, dimension(:) :: tmin !<
  !
  !     * fields used for i/o for pla.
  !
  real, dimension(isdnum) :: radc !<
  real, allocatable, dimension(:,:,:) :: oednrow !<
  real, allocatable, dimension(:,:,:) :: oercrow !<
  real, allocatable, dimension(:,:) :: oidnrow !<
  real, allocatable, dimension(:,:) :: oircrow !<
  real, allocatable, dimension(:,:) :: svvbrow !<
  real, allocatable, dimension(:,:) :: psvvrow !<
  real, allocatable, dimension(:,:,:) :: svmbrow !<
  real, allocatable, dimension(:,:,:) :: svcbrow !<
  real, allocatable, dimension(:,:,:) :: qnvbrow !<
  real, allocatable, dimension(:,:,:,:) :: qnmbrow !<
  real, allocatable, dimension(:,:,:,:) :: qncbrow !<
  real, allocatable, dimension(:,:,:,:) :: qsvbrow !<
  real, allocatable, dimension(:,:,:,:,:) :: qsmbrow !<
  real, allocatable, dimension(:,:,:,:,:) :: qscbrow !<
  real, allocatable, dimension(:,:) :: qgvbrow !<
  real, allocatable, dimension(:,:,:) :: qgmbrow !<
  real, allocatable, dimension(:,:,:) :: qgcbrow !<
  real, allocatable, dimension(:,:) :: qdvbrow !<
  real, allocatable, dimension(:,:,:) :: qdmbrow !<
  real, allocatable, dimension(:,:,:) :: qdcbrow !<
  real, allocatable, dimension(:,:,:) :: devbrow !<
  real, allocatable, dimension(:,:,:) :: pdevrow !<
  real, allocatable, dimension(:,:,:,:) :: dembrow !<
  real, allocatable, dimension(:,:,:,:) :: decbrow !<
  real, allocatable, dimension(:,:) :: divbrow !<
  real, allocatable, dimension(:,:) :: pdivrow !<
  real, allocatable, dimension(:,:,:) :: dimbrow !<
  real, allocatable, dimension(:,:,:) :: dicbrow !<
  real, allocatable, dimension(:,:,:) :: revbrow !<
  real, allocatable, dimension(:,:,:) :: prevrow !<
  real, allocatable, dimension(:,:,:,:) :: rembrow !<
  real, allocatable, dimension(:,:,:,:) :: recbrow !<
  real, allocatable, dimension(:,:) :: rivbrow !<
  real, allocatable, dimension(:,:) :: privrow !<
  real, allocatable, dimension(:,:,:) :: rimbrow !<
  real, allocatable, dimension(:,:,:) :: ricbrow !<
  !
  !     * radiation-related fields.
  !
  real, allocatable, dimension(:,:,:) :: exta !<
  real, allocatable, dimension(:,:,:) :: exoma !<
  real, allocatable, dimension(:,:,:) :: exomga !<
  real, allocatable, dimension(:,:,:) :: fa !<
  real, allocatable, dimension(:,:,:) :: absa !<
  real, allocatable, dimension(:,:,:) :: taua055 !<
  real, allocatable, dimension(:,:,:) :: taua086 !<
  real, allocatable, dimension(:,:,:) :: absa055 !<
  real, allocatable, dimension(:,:) :: aext055 !<
  real, allocatable, dimension(:,:) :: aext086 !<
  real, allocatable, dimension(:,:) :: sext055 !<
  real, allocatable, dimension(:,:) :: sext086 !<
  real, allocatable, dimension(:,:) :: sssa055 !<
  real, allocatable, dimension(:,:) :: dext055 !<
  real, allocatable, dimension(:,:) :: dext086 !<
  real, allocatable, dimension(:,:) :: dssa055 !<
  !
  !     * parameters.
  !
  logical :: kcoagd !<
  logical :: kactd !<
  logical :: krad !<
  logical, parameter :: kcdnc = .true. !<
  integer :: ntracp !< Number of aerosol tracers
  integer :: iaindt !< First index for PLA/PAM tracers 
  integer, allocatable, dimension(:) :: iainda !< Tracer index array
  integer, allocatable, dimension(:) :: iaind !< Aerosol tracer index array
  integer, allocatable, dimension(:) :: itrwet !< Flags to enable wet deposition

  !
  !     * input/output file unit numbers.
  !
  data nucoa /77/
  data nuact /78/
  data nurad /79/
  !
  !< <<< * AEROSOL * <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
  !-----------------------------------------------------------------------
  !     * open file that contains basic information about
  !     * air properties.
  !
  open(nupar,file='PARAM')
  !
  !-----------------------------------------------------------------------
  !     * read in basic information about simulation.
  !
  modl%pltfq=1
  modl%taux=0.
  modl%tauq=0.
  modl%taut=0.
  modl%taum=0.
  modl%trax=0
  modl%traq=0
  modl%trat=0
  modl%trup=0
  modl%nugvd=spv
  modl%cdn=-spv
  modl%cdnn=-spv
  modl%zref=-spv
  modl%case=2   ! default autoconversion parameterization is wood
    modl%lat=0.   ! latitude, default is equator, units are  degrees from -90 (south pole) to 90 (north pole)
    modl%sdoy=1.  ! day of year
    read(nupar,nml=modnml)
    rewind(nupar)
    !
    !-----------------------------------------------------------------------
    !     * read files and field dimensions for boundary conditions and
    !     * restart fields, including meteorology, emissions and initial
    !     * tracer mixing ratios.
    !
    cfile=trim(modl%runname)
    cfilb=trim(trim(cfile)//'_bnd')
    cfilr=trim(trim(cfile)//'_tini')
    !
    !     * read and set field dimensions.
    !
    if (modl%pltfq /= 1) then
      ktime1=.true.
    else
      ktime1=.false.
    end if
    ntrac=1
    !
    !> >>> * AEROSOL * >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    !
    call rdncdima(cfilb,cfilr,nlvl,nlvl1,nstp,isdiagi,ntr, &
                    kextti,kintti,isaintti,nrmfldi,isdusti, &
                    ktime1)
    ntrac=ntr
    !
    !< <<< * AEROSOL * <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
    !
    nstpo=0
    do it=1,nstp
      if (mod(it+modl%pltfq-1,modl%pltfq) == 0) nstpo=nstpo+1
    end do
    if (ktime1) then
      allocate(atime(nstpo))
      call setncdim(nstpo,ndim)
    end if
    !
    !     * check dimensions.
    !
    if (nstp <= 2) call xit('MAIN',-1)
    if (modl%pltfq < 1 .or. modl%pltfq > nstp) call wrn('MAIN',-2)
    !
    !     * initialize netcdf i/o fields to save the model data.
    !
    call initio(ktime1,.true.)
    !
    !     * read boundary conditions and restart data from the files
    !     * and save in netcdf i/o fields as specified in the io lists.
    !
    call rdncdat(cfilb)
    call rdncdatrs(cfilr)
    !
    !-----------------------------------------------------------------------
    !     * grid dimensions and other model parameters.
    !
    ilev=nlvl        ! number of vertical grid cells for atmosphere
    ilg=1            ! number of horizontal grid cells for atmosphere
    !
    ilev1=nlvl1
    ilevp1=ilev
    levs=ilev
    msgt=ilev-levs
    msgp1=msgt+1
    msgp2=msgp1
    il1=1
    il2=1
    !
    !-----------------------------------------------------------------------
    !     * get scalar input fields supplied in file with boundary conditions.
    !
    call r0dfld('delt',dt)
    if (nint(dt-yna) == 0) call xit('MAIN',-3)
    !
    !     * switches for diagnostic output.
    !
    kmetout=.true.
    !
    !-----------------------------------------------------------------------
    !     * allocate memory.
    !
    allocate(zh     (ilg,ilev))
    allocate(zf     (ilg,ilev))
    allocate(rhc    (ilg,ilev))
    allocate(rh     (ilg,ilev))
    allocate(rhoc   (ilg,ilev))
    allocate(wsub   (ilg,ilev))
    allocate(wg     (ilg,ilev))
    allocate(ph     (ilg,ilev))
    allocate(pf     (ilg,ilev))
    allocate(dp     (ilg,ilev))
    allocate(dprad  (ilg,ilev))
    allocate(throw  (ilg,ilevp1))
    allocate(throwr (ilg,ilevp1))
    allocate(urow   (ilg,ilev))
    allocate(vrow   (ilg,ilev))
    allocate(urowr  (ilg,ilev))
    allocate(tfrow  (ilg,ilev))
    allocate(qrow   (ilg,ilevp1))
    allocate(qrowr  (ilg,ilevp1))
    allocate(qlwc   (ilg,ilev))
    allocate(qlwcr  (ilg,ilev))
    allocate(qtn    (ilg,ilev))
    allocate(hmn    (ilg,ilev))
    allocate(zmratep(ilg,ilev))
    allocate(zmlwc  (ilg,ilev))
    allocate(zfsnow (ilg,ilev))
    allocate(zfrain (ilg,ilev))
    allocate(zclf   (ilg,ilev))
    allocate(beta   (ilg,ilev))
    allocate(radeqv (ilg,ilev))
    allocate(radeqva(ilg,ilev))
    allocate(clrfr  (ilg,ilev))
    allocate(clrfs  (ilg,ilev))
    allocate(zfevap (ilg,ilev))
    allocate(zfsubl (ilg,ilev))
    allocate(shtj   (ilg,ilevp1))
    allocate(shj    (ilg,ilev))
    allocate(dshj   (ilg,ilev))
    allocate(taut   (ilg,ilevp1))
    allocate(tauq   (ilg,ilevp1))
    allocate(taum   (ilg,ilev))
    allocate(tautr  (ilg,ilevp1))
    allocate(tauqr  (ilg,ilevp1))
    allocate(taumr  (ilg,ilev))
    allocate(almx   (ilg,ilev))
    allocate(almc   (ilg,ilev))
    allocate(tvp    (ilg,ilev))
    allocate(dtdt   (ilg,ilevp1))
    allocate(dqdt   (ilg,ilevp1))
    allocate(dqldt  (ilg,ilev))
    allocate(dudt   (ilg,ilevp1))
    allocate(rkh    (ilg,ilev))
    allocate(rkm    (ilg,ilev))
    allocate(ri     (ilg,ilev))
    allocate(dqndg  (ilg,ilev))
    allocate(dtndg  (ilg,ilev))
    allocate(dqvdf  (ilg,ilev))
    allocate(dtvdf  (ilg,ilev))
    allocate(dqadv  (ilg,ilev))
    allocate(dtadv  (ilg,ilev))
    allocate(dqphs  (ilg,ilev))
    allocate(dtphs  (ilg,ilev))
    allocate(pcp    (ilg))
    allocate(cdm    (ilg))
    allocate(ustar  (ilg))
    allocate(tg     (ilg))
    allocate(thliqg (ilg))
    allocate(zo     (ilg))
    allocate(qsens  (ilg))
    allocate(qlat   (ilg))
    allocate(perta  (ilg,ilev))
    allocate(perrv  (ilg,ilev))
    allocate(wgt    (ilg))
    !
    allocate(zcdnrow(ilg,ilev))
    allocate(cdnrow(ilg,ilev))
    allocate(rhoa(ilg,ilev))
    allocate(qcwa(ilg,ilev))
    !
    allocate(ft(nstp))
    allocate(ft1(nstpo))
    allocate(flv1t(ilev1,nstp))
    allocate(flv1t1(ilev1,nstpo))
    allocate(flvt(ilev,nstp))
    allocate(flvt1(ilev,nstpo))
    allocate(flvt2(ilev+1,nstpo))
    allocate(flvtn(ilev,nstp,ntrac))
    allocate(flvt1n(ilev,nstpo,ntrac))
    !
    allocate(zspda  (ilg))
    allocate(zspd   (ilg))
    allocate(pbltrow(ilg))
    allocate(pft    (ilg))
    !
    allocate(gtrow  (ilg))
    allocate(pressg (ilg))
    !
    !     * tracers.
    !
    allocate(xrow   (ilg,ilevp1,ntrac))
    allocate(dxdt   (ilg,ilevp1,ntrac))
    !
    !-----------------------------------------------------------------------
    !     * height of vertical levels, grid cell interfaces, and surface
    !     * from input data (assumed to be time-invariant here).
    !
    ichk=0
    do nd=1,ndim
      if (dim(nd)%name(1:6)=='level ' ) then
        ichk=ichk+1
        do iz=1,ilev
          zh(:,iz)=dim(nd)%val(iz)
        end do
      else if (dim(nd)%name(1:7)=='level1 ' ) then
        ichk=ichk+1
        do iz=1,ilev
          zf(:,iz)=dim(nd)%val(iz+1)
        end do
      end if
    end do
    if (ichk /= 2) call xit('MAIN',-4)
    !
    !-----------------------------------------------------------------------
    !     * read initial prognostic fields from restart file.
    !
    call r0dfldrs('tstep',aits)
    inits=aits
    call r0dfldrs('ustar',ustar(1))
    call r1dfldrs('ua',urow(1,1),ilev)
    call r1dfldrs('ta',throw(1,1),ilev)
    call r1dfldrs('rv',qrow(1,1),ilev)
    call r1dfldrs('rl',qlwc(1,1),ilev)
    call r1dfldrs('clf',zclf(1,1),ilev)
    call r1dfldrs('cldnc',zcdnrow(1,1),ilev)
    call r1dfldrs('ri',ri(1,1),ilev)
    call r0dfldrs('indpbl',pbltrow(1))
    !
    !-----------------------------------------------------------------------
    !     * lower bound on cloud droplet number concentration.
    !
    zcdnrow=max(zcdnrow,1.e+06)
    !
    !     * nudging time scales for tracers, temperature, and moisture.
    !
    allocate(taux (ilg,ilevp1,ntrac))
    allocate(tauxr(ilg,ilevp1,ntrac))
    taur=60.*60.*24.
    tauxr=0.
    tautr=0.
    tauqr=0.
    taumr=0.
    !
    zref=modl%nugalt
    !      do iz=1,ilev
    !        if ( zh(1,iz) <= zref) then
    !          fact=taur*erf((zref-zh(1,iz))/modl%nugvd)
    !! !!
    !          tauxr(:,iz,:)=modl%taux*fact
    !          tautr(:,iz)=modl%taut*fact
    !          tauqr(:,iz)=modl%tauq*fact
    !          taumr(:,iz)=modl%taum*fact
    !        end if
    !      end do
    do iz=1,ilev
      if (zh(1,iz) <= modl%nugalt &
          .and. zh(1,iz) > modl%nugalt-modl%nugvd) then
        tauxr(:,iz,:)=modl%taux
        tautr(:,iz)=modl%taut
        tauqr(:,iz)=modl%tauq
        taumr(:,iz)=modl%taum
      end if
    end do
    !
    !     * prognostic results below lowest level of aircraft flight profile.
    !
    if (abs(modl%zref + spv) > 0.001) then
      zref=modl%zref
    else
      zref=0.
    end if
    taul=1.e+20
    do iz=1,ilev
      if (zh(1,iz) < zref) then
        tauxr(:,iz,:)=taul
        tautr(:,iz)=taul
        tauqr(:,iz)=taul
        taumr(:,iz)=taul
      end if
    end do
    !
    !> >>> * AEROSOL * >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    !     * check dimensions.
    !
    print *, isdust, isdusti
    if (kextt   /= kextti) call xit('MAIN',-5)
    if (kintt   /= kintti) call xit('MAIN',-6)
    if (nrmfld  /= nrmfldi) call xit('MAIN',-7)
    if (isaintt /= isaintti) call xit('MAIN',-8)
    if (isdiag  /= isdiagi) call xit('MAIN',-9)
    if (isdust  /= isdusti) call xit('MAIN',-10)
    !
    !     * numerical and other constants in aerosol parameterizations.
    !
    isvdust=0                   ! flag for turning on/off extra diagnostic fields for dust
    irfrc=0                     ! flag for calculation of radiative forcings
    saverad=1./real(modl%pltfq) ! saving interval for accumulated output variables
    !
    !     * define critical threshold of sea-ice concentration above which
    !     * grid point is considered to be sea-ice.
    !
    sicn_crt=0.15
    !
    !     * lower bound on tracer mixing ratios.
    !
    allocate(tmin(ntrac))
    tmin=0.
    !
    !     * get scalar input fields supplied in file with boundary conditions.
    !
    call r0dfld('rvco2',co2_ppm)
    !
    !-----------------------------------------------------------------------
    !     * tracer information for driving model.
    !
    allocate(iainda(ntrac))
    allocate(itrwet(ntrac))
    call trinfo(ntracp,iainda,iaindt,itrwet,ntrac)
    !
    !     * allocate and initialize array for aerosol tracer indices.
    !
    if ( .not.allocated(iaind) ) allocate(iaind(ntracp))
    do n=1,ntracp
      iaind(n)=iainda(n)
    end do
    !
    !     * check if the number of fields for averaging of cloud
    !     * droplet results is sufficient (increase nrmfld, if necessary).
    !
    if (ceiling(real(iavgprd)/real(iupdatp)) > nrmfld) then
      call xit('MAIN',-12)
    end if
    !
    !     * initialize basic aerosol properties and set up pla calculations.
    !
    inquire(file="COAGDAT",exist=kcoagd)
    if ( .not.kcoagd) then
      call xit('MAIN',-13)
    end if
    open(nucoa,file='COAGDAT')
    inquire(file="ACTDAT1",exist=kactd)
    if ( .not.kactd) then
      call xit('MAIN',-14)
    end if
    open(nuact,file='ACTDAT1',recl=4000)
    write(6,'(A41)') 'INITIALIZATION OF BASIC PLA PARAMETERS...'
    call sdconf(nupar,nucoa,mynode)
    call sdpemi
    write(6,'(A39)') 'INITIALIZATION OF CLOUD MICROPHYSICS...'
    call scconf
    call cnconf(nuact)
    write(6,'(A30)') 'INITIALIZATION OF RADIATION...'
    inquire(file="SSDAT",exist=krad)
    if ( .not.krad) then
      call xit('MAIN',-16)
    end if
    open(nurad,file='SSDAT')
    call rdconf(nurad)
    call aeromx3data
    !
    !     * check if the number of fields to save results for externally
    !     * mixed types of aerosol are sufficient.
    !
    if (kextt < kext .or. kintt < kint .or. isaintt < isaint) then
      call xit('MAIN',-17)
    end if
    !
    !     * switches for diagnostic output.
    !
    kxtra1=.true.
    kxtra2=.true.
    !
    !-----------------------------------------------------------------------
    !     * dimensions and arrays for output of aerosol size distributions.
    !
    if (kxtra2) then
      nsiz=isdnum
      call diagsdw(fphis,fdphi0s)
      do is=1,isdnum
        radc(is)=fphis+fdphi0s*(.5+real(is-1))
      end do
      call w1ddim('raddry',radc,isdnum)
    end if
    !
    !-----------------------------------------------------------------------
    !     * allocate memory.
    !
    allocate(xrowr  (ilg,ilevp1,ntrac))
    allocate(flvt1s (ilev,nstpo,isdnum))
    allocate(xarow  (ilg,ilevp1,ntrac))
    allocate(xcrow  (ilg,ilevp1,ntrac))
    allocate(dxadt  (ilg,ilevp1,ntrac))
    allocate(dxcdt  (ilg,ilevp1,ntrac))
    allocate(sfrcrol(ilg,ilev,ntrac))
    allocate(sfrcr  (ilg,ilev,ntrac))
    allocate(zcdnpam(ilg,ilev))
    allocate(tmpscl (ilg,ilev))
    allocate(sgpp   (ilg,ilev))
    allocate(ogpp   (ilg,ilev))
    allocate(docem1 (ilg,ilev))
    allocate(docem2 (ilg,ilev))
    allocate(docem3 (ilg,ilev))
    allocate(docem4 (ilg,ilev))
    allocate(dbcem1 (ilg,ilev))
    allocate(dbcem2 (ilg,ilev))
    allocate(dbcem3 (ilg,ilev))
    allocate(dbcem4 (ilg,ilev))
    allocate(dsuem1 (ilg,ilev))
    allocate(dsuem2 (ilg,ilev))
    allocate(dsuem3 (ilg,ilev))
    allocate(dsuem4 (ilg,ilev))
    allocate(so2em  (ilg,ilev))
    allocate(orgem  (ilg,ilev))
    allocate(o3row  (ilg,ilev))
    allocate(ohrow  (ilg,ilev))
    allocate(no3row (ilg,ilev))
    allocate(hno3row(ilg,ilev))
    allocate(nh3row (ilg,ilev))
    allocate(nh4row (ilg,ilev))
    allocate(bghpo  (ilg,ilev))
    allocate(ssldrow(ilg,ilev))
    allocate(ressrow(ilg,ilev))
    allocate(vessrow(ilg,ilev))
    allocate(dsldrow(ilg,ilev))
    allocate(redsrow(ilg,ilev))
    allocate(vedsrow(ilg,ilev))
    allocate(bcldrow(ilg,ilev))
    allocate(rebcrow(ilg,ilev))
    allocate(vebcrow(ilg,ilev))
    allocate(ocldrow(ilg,ilev))
    allocate(reocrow(ilg,ilev))
    allocate(veocrow(ilg,ilev))
    allocate(amldrow(ilg,ilev))
    allocate(reamrow(ilg,ilev))
    allocate(veamrow(ilg,ilev))
    allocate(fr1row (ilg,ilev))
    allocate(fr2row (ilg,ilev))
    allocate(sulifrc(ilg,ilev))
    allocate(rsuifrc(ilg,ilev))
    allocate(vsuifrc(ilg,ilev))
    allocate(f1sufrc(ilg,ilev))
    allocate(f2sufrc(ilg,ilev))
    allocate(bclifrc(ilg,ilev))
    allocate(rbcifrc(ilg,ilev))
    allocate(vbcifrc(ilg,ilev))
    allocate(f1bcfrc(ilg,ilev))
    allocate(f2bcfrc(ilg,ilev))
    allocate(oclifrc(ilg,ilev))
    allocate(rocifrc(ilg,ilev))
    allocate(vocifrc(ilg,ilev))
    allocate(f1ocfrc(ilg,ilev))
    allocate(f2ocfrc(ilg,ilev))
    allocate(bcicrow(ilg,ilev))
    allocate(cbccnc (ilg,ilev))
    allocate(coacnc (ilg,ilev))
    allocate(cascnc (ilg,ilev))
    allocate(csscnc (ilg,ilev))
    allocate(cmdcnc (ilg,ilev))
    allocate(cmdn   (ilg,ilev))
    allocate(cssn   (ilg,ilev))
    allocate(cinn   (ilg,ilev))
    allocate(xburd  (ilg,ntrac))
    allocate(mass   (ilg,6))
    allocate(abss   (ilg,6))
    allocate(exts   (ilg,6))
    allocate(angstr (ilg,6))
    allocate(dmso   (ilg))
    allocate(ddmsdt (ilg))
    allocate(cszrow (ilg))
    allocate(radj   (ilg))
    allocate(dd4row (ilg))
    allocate(dox4row(ilg))
    allocate(doxdrow(ilg))
    allocate(noxdrow(ilg))
    allocate(snorow (ilg))
    allocate(hmfnrow(ilg))
    allocate(zfor   (ilg))
    allocate(depbrol(ilg))
    allocate(vbcbrd (ilg))
    allocate(voabrd (ilg))
    allocate(vasbrd (ilg))
    allocate(vssbrd (ilg))
    allocate(vmdbrd (ilg))
    allocate(vmdn   (ilg))
    allocate(vssn   (ilg))
    allocate(vinn   (ilg))
    allocate(cldt   (ilg))
    allocate(rmu    (ilg))
    !
    if (kxtra1) then
      allocate(sncnrow(ilg,ilev))
      allocate(ssunrow(ilg,ilev))
      allocate(scndrow(ilg,ilev))
      allocate(sgsprow(ilg,ilev))
      allocate(acasrow(ilg,ilev))
      allocate(acoarow(ilg,ilev))
      allocate(acbcrow(ilg,ilev))
      allocate(acssrow(ilg,ilev))
      allocate(acmdrow(ilg,ilev))
      allocate(ntrow  (ilg,ilev))
      allocate(n20row (ilg,ilev))
      allocate(n50row (ilg,ilev))
      allocate(n100row(ilg,ilev))
      allocate(n200row(ilg,ilev))
      allocate(wtrow  (ilg,ilev))
      allocate(w20row (ilg,ilev))
      allocate(w50row (ilg,ilev))
      allocate(w100row(ilg,ilev))
      allocate(w200row(ilg,ilev))
      allocate(ccnrow (ilg,ilev))
      allocate(cc02row(ilg,ilev))
      allocate(cc04row(ilg,ilev))
      allocate(cc08row(ilg,ilev))
      allocate(cc16row(ilg,ilev))
      allocate(ccnerow(ilg,ilev))
      allocate(rcrirow(ilg,ilev))
      allocate(supsrow(ilg,ilev))
      allocate(henrrow(ilg,ilev))
      allocate(o3frrow(ilg,ilev))
      allocate(h2o2frrow(ilg,ilev))
      allocate(wparrow(ilg,ilev))
      allocate(vncnrow(ilg))
      allocate(vasnrow(ilg))
      allocate(vscdrow(ilg))
      allocate(vgsprow(ilg))
      allocate(voaerow(ilg))
      allocate(vbcerow(ilg))
      allocate(vaserow(ilg))
      allocate(vmderow(ilg))
      allocate(vsserow(ilg))
      allocate(voawrow(ilg))
      allocate(vbcwrow(ilg))
      allocate(vaswrow(ilg))
      allocate(vmdwrow(ilg))
      allocate(vsswrow(ilg))
      allocate(voadrow(ilg))
      allocate(vbcdrow(ilg))
      allocate(vasdrow(ilg))
      allocate(vmddrow(ilg))
      allocate(vssdrow(ilg))
      allocate(voagrow(ilg))
      allocate(vbcgrow(ilg))
      allocate(vasgrow(ilg))
      allocate(vmdgrow(ilg))
      allocate(vssgrow(ilg))
      allocate(vasirow(ilg))
      allocate(vas1row(ilg))
      allocate(vas2row(ilg))
      allocate(vas3row(ilg))
      allocate(vccnrow(ilg))
      allocate(vcnerow(ilg))
    end if
    if (kxtra2) then
      allocate(cornrow(ilg,ilev))
      allocate(cormrow(ilg,ilev))
      allocate(rsn1row(ilg,ilev))
      allocate(rsm1row(ilg,ilev))
      allocate(rsn2row(ilg,ilev))
      allocate(rsm2row(ilg,ilev))
      allocate(rsn3row(ilg,ilev))
      allocate(rsm3row(ilg,ilev))
      allocate(sdnurow(ilg,ilev,isdnum))
      allocate(sdmarow(ilg,ilev,isdnum))
      allocate(sdacrow(ilg,ilev,isdnum))
      allocate(sdcorow(ilg,ilev,isdnum))
      allocate(sssnrow(ilg,ilev,isdnum))
      allocate(smdnrow(ilg,ilev,isdnum))
      allocate(sianrow(ilg,ilev,isdnum))
      allocate(sssmrow(ilg,ilev,isdnum))
      allocate(smdmrow(ilg,ilev,isdnum))
      allocate(sewmrow(ilg,ilev,isdnum))
      allocate(ssumrow(ilg,ilev,isdnum))
      allocate(socmrow(ilg,ilev,isdnum))
      allocate(sbcmrow(ilg,ilev,isdnum))
      allocate(siwmrow(ilg,ilev,isdnum))
      allocate(sdvlrow(ilg,isdiag))
      allocate(vrn1row(ilg))
      allocate(vrm1row(ilg))
      allocate(vrn2row(ilg))
      allocate(vrm2row(ilg))
      allocate(vrn3row(ilg))
      allocate(vrm3row(ilg))
      allocate(duwdrow(ilg))
      allocate(dustrow(ilg))
      allocate(duthrow(ilg))
      allocate(fallrow(ilg))
      allocate(fa10row(ilg))
      allocate(fa2row (ilg))
      allocate(fa1row (ilg))
      allocate(usmkrow(ilg))
      allocate(defarow(ilg))
      allocate(defcrow(ilg))
      allocate(dmacrow(ilg,isdust))
      allocate(dmcorow(ilg,isdust))
      allocate(dnacrow(ilg,isdust))
      allocate(dncorow(ilg,isdust))
      allocate(defxrow(ilg,isdiag))
      allocate(defnrow(ilg,isdiag))
      allocate(tnssrow(ilg))
      allocate(tnmdrow(ilg))
      allocate(tniarow(ilg))
      allocate(tmssrow(ilg))
      allocate(tmmdrow(ilg))
      allocate(tmocrow(ilg))
      allocate(tmbcrow(ilg))
      allocate(tmsprow(ilg))
      allocate(snssrow(ilg,isdnum))
      allocate(snmdrow(ilg,isdnum))
      allocate(sniarow(ilg,isdnum))
      allocate(smssrow(ilg,isdnum))
      allocate(smmdrow(ilg,isdnum))
      allocate(smocrow(ilg,isdnum))
      allocate(smbcrow(ilg,isdnum))
      allocate(smsprow(ilg,isdnum))
      allocate(siwhrow(ilg,isdnum))
      allocate(sewhrow(ilg,isdnum))
      allocate(pm25row(ilg,ilev))
      allocate(pm10row(ilg,ilev))
      allocate(dm25row(ilg,ilev))
      allocate(dm10row(ilg,ilev))
    end if
    !
    allocate(fcanrow(ilg,ican+1))
    allocate(smfrac (ilg))
    allocate(fcs    (ilg))
    allocate(fgs    (ilg))
    allocate(fc     (ilg))
    allocate(fg     (ilg))
    allocate(zspdso (ilg))
    allocate(zspdsbs(ilg))
    allocate(cdml   (ilg))
    allocate(cdmnl  (ilg))
    allocate(flnd   (ilg))
    allocate(focn   (ilg))
    allocate(sicn   (ilg))
    allocate(bsfrac (ilg))
    allocate(pdsfrow(ilg))
    allocate(atau   (ilg))
    allocate(fcant  (ilg))
    allocate(ustarbs(ilg))
    allocate(fnrol  (ilg))
    allocate(zfsh   (ilg))
    allocate(spotrow(ilg))
    allocate(st02row(ilg))
    allocate(st03row(ilg))
    allocate(st04row(ilg))
    allocate(st06row(ilg))
    allocate(st13row(ilg))
    allocate(st14row(ilg))
    allocate(st15row(ilg))
    allocate(st16row(ilg))
    allocate(st17row(ilg))
    allocate(suz0row(ilg))
    !
    allocate(oednrow(ilg,ilev,kextt))
    allocate(oercrow(ilg,ilev,kextt))
    allocate(oidnrow(ilg,ilev))
    allocate(oircrow(ilg,ilev))
    allocate(svvbrow(ilg,ilev))
    allocate(psvvrow(ilg,ilev))
    allocate(svmbrow(ilg,ilev,nrmfld))
    allocate(svcbrow(ilg,ilev,nrmfld))
    allocate(qnvbrow(ilg,ilev,isaintt))
    allocate(qnmbrow(ilg,ilev,isaintt,nrmfld))
    allocate(qncbrow(ilg,ilev,isaintt,nrmfld))
    allocate(qsvbrow(ilg,ilev,isaintt,kintt))
    allocate(qsmbrow(ilg,ilev,isaintt,kintt,nrmfld))
    allocate(qscbrow(ilg,ilev,isaintt,kintt,nrmfld))
    allocate(qgvbrow(ilg,ilev))
    allocate(qgmbrow(ilg,ilev,nrmfld))
    allocate(qgcbrow(ilg,ilev,nrmfld))
    allocate(qdvbrow(ilg,ilev))
    allocate(qdmbrow(ilg,ilev,nrmfld))
    allocate(qdcbrow(ilg,ilev,nrmfld))
    allocate(devbrow(ilg,ilev,kextt))
    allocate(pdevrow(ilg,ilev,kextt))
    allocate(dembrow(ilg,ilev,kextt,nrmfld))
    allocate(decbrow(ilg,ilev,kextt,nrmfld))
    allocate(divbrow(ilg,ilev))
    allocate(pdivrow(ilg,ilev))
    allocate(dimbrow(ilg,ilev,nrmfld))
    allocate(dicbrow(ilg,ilev,nrmfld))
    allocate(revbrow(ilg,ilev,kextt))
    allocate(prevrow(ilg,ilev,kextt))
    allocate(rembrow(ilg,ilev,kextt,nrmfld))
    allocate(recbrow(ilg,ilev,kextt,nrmfld))
    allocate(rivbrow(ilg,ilev))
    allocate(privrow(ilg,ilev))
    allocate(rimbrow(ilg,ilev,nrmfld))
    allocate(ricbrow(ilg,ilev,nrmfld))
    !
    allocate(exta   (ilg,ilev,nbs))
    allocate(exoma  (ilg,ilev,nbs))
    allocate(exomga (ilg,ilev,nbs))
    allocate(fa     (ilg,ilev,nbs))
    allocate(absa   (ilg,ilev,nbl))
    allocate(taua055(ilg,ilev,4))
    allocate(taua086(ilg,ilev,4))
    allocate(absa055(ilg,ilev,4))
    allocate(aext055(ilg,ilev))
    allocate(aext086(ilg,ilev))
    allocate(sext055(ilg,ilev))
    allocate(sext086(ilg,ilev))
    allocate(sssa055(ilg,ilev))
    allocate(dext055(ilg,ilev))
    allocate(dext086(ilg,ilev))
    allocate(dssa055(ilg,ilev))
    allocate(so4load(ilg,ilev))
    allocate(bcyload(ilg,ilev))
    allocate(bcoload(ilg,ilev))
    allocate(ocyload(ilg,ilev))
    allocate(ocoload(ilg,ilev))
    allocate(reso4  (ilg,ilev))
    allocate(rebcy  (ilg,ilev))
    allocate(reocy  (ilg,ilev))
    allocate(veso4  (ilg,ilev))
    allocate(vebcy  (ilg,ilev))
    allocate(veocy  (ilg,ilev))
    allocate(aods   (ilg,6))
    allocate(aod86  (ilg,6))
    !
    !-----------------------------------------------------------------------
    !     * initialization of various pla-related diagnostic arrays.
    !
    if (kxtra1) then
      ccnrow=0.
      ccnerow=0.
      rcrirow=0.
      supsrow=0.
      henrrow=0.
      o3frrow=0.
      h2o2frrow=0.
      wparrow=0.
      vccnrow=0.
      vcnerow=0.
    end if
    if (kxtra2) then
      sdnurow=0.
      sdmarow=0.
      sdacrow=0.
      sdcorow=0.
      sssnrow=0.
      smdnrow=0.
      sianrow=0.
      sssmrow=0.
      smdmrow=0.
      sewmrow=0.
      ssumrow=0.
      socmrow=0.
      sbcmrow=0.
      siwmrow=0.
      tnssrow=0.
      tnmdrow=0.
      tniarow=0.
      tmssrow=0.
      tmmdrow=0.
      tmocrow=0.
      tmbcrow=0.
      tmsprow=0.
      snssrow=0.
      snmdrow=0.
      sniarow=0.
      smssrow=0.
      smmdrow=0.
      smocrow=0.
      smbcrow=0.
      smsprow=0.
      siwhrow=0.
      sewhrow=0.
      pm25row=0.
      pm10row=0.
      dm25row=0.
      dm10row=0.
    end if
    !
    !     * read initial prognostic tracer fields from restart file.
    !
    call r2dfldrs('rtrac',xrow(1,1,1),ilev,ntrac)
    call r2dfldrs('strac',sfrcrol(1,1,1),ilev,ntrac)
    !
    !     * pla work fields.
    !
    call r1dfldrs('oidn',oidnrow(1,1),ilev)
    call r1dfldrs('oirc',oircrow(1,1),ilev)
    call r1dfldrs('svvb',svvbrow(1,1),ilev)
    call r1dfldrs('psvv',psvvrow(1,1),ilev)
    call r1dfldrs('qgvb',qgvbrow(1,1),ilev)
    call r1dfldrs('qdvb',qdvbrow(1,1),ilev)
    call r1dfldrs('divb',divbrow(1,1),ilev)
    call r1dfldrs('pdiv',pdivrow(1,1),ilev)
    call r1dfldrs('rivb',rivbrow(1,1),ilev)
    call r1dfldrs('priv',privrow(1,1),ilev)
    call r2dfldrs('oedn',oednrow(1,1,1),ilev,kextt)
    call r2dfldrs('oerc',oercrow(1,1,1),ilev,kextt)
    call r2dfldrs('devb',devbrow(1,1,1),ilev,kextt)
    call r2dfldrs('pdev',pdevrow(1,1,1),ilev,kextt)
    call r2dfldrs('revb',revbrow(1,1,1),ilev,kextt)
    call r2dfldrs('prev',prevrow(1,1,1),ilev,kextt)
    call r2dfldrs('qnvb',qnvbrow(1,1,1),ilev,isaintt)
    call r2dfldrs('svmb',svmbrow(1,1,1),ilev,nrmfld)
    call r2dfldrs('svcb',svcbrow(1,1,1),ilev,nrmfld)
    call r2dfldrs('qgmb',qgmbrow(1,1,1),ilev,nrmfld)
    call r2dfldrs('qgcb',qgcbrow(1,1,1),ilev,nrmfld)
    call r2dfldrs('qdmb',qdmbrow(1,1,1),ilev,nrmfld)
    call r2dfldrs('qdcb',qdcbrow(1,1,1),ilev,nrmfld)
    call r2dfldrs('dimb',dimbrow(1,1,1),ilev,nrmfld)
    call r2dfldrs('dicb',dicbrow(1,1,1),ilev,nrmfld)
    call r2dfldrs('rimb',rimbrow(1,1,1),ilev,nrmfld)
    call r2dfldrs('ricb',ricbrow(1,1,1),ilev,nrmfld)
    call r3dfldrs('qsvb',qsvbrow(1,1,1,1),ilev,isaintt,kintt)
    call r3dfldrs('qnmb',qnmbrow(1,1,1,1),ilev,isaintt,nrmfld)
    call r3dfldrs('qncb',qncbrow(1,1,1,1),ilev,isaintt,nrmfld)
    call r3dfldrs('demb',dembrow(1,1,1,1),ilev,kextt,nrmfld)
    call r3dfldrs('decb',decbrow(1,1,1,1),ilev,kextt,nrmfld)
    call r3dfldrs('remb',rembrow(1,1,1,1),ilev,kextt,nrmfld)
    call r3dfldrs('recb',recbrow(1,1,1,1),ilev,kextt,nrmfld)
    call r4dfldrs('qsmb',qsmbrow(1,1,1,1,1),ilev,isaintt,kintt,nrmfld)
    call r4dfldrs('qscb',qscbrow(1,1,1,1,1),ilev,isaintt,kintt,nrmfld)
    !
    !     * pla extra diagnostic fields.
    !
    if (kxtra1) then
      call r0dfldrs('emibc',vbcerow(1))
      call r0dfldrs('emioa',voaerow(1))
      call r0dfldrs('emias',vaserow(1))
      call r0dfldrs('emiss',vsserow(1))
      call r0dfldrs('emimd',vmderow(1))
      call r0dfldrs('wetbcl',vbcwrow(1))
      call r0dfldrs('wetoal',voawrow(1))
      call r0dfldrs('wetasl',vaswrow(1))
      call r0dfldrs('wetssl',vsswrow(1))
      call r0dfldrs('wetmdl',vmdwrow(1))
      call r0dfldrs('drybc',vbcdrow(1))
      call r0dfldrs('dryoa',voadrow(1))
      call r0dfldrs('dryas',vasdrow(1))
      call r0dfldrs('dryss',vssdrow(1))
      call r0dfldrs('drymd',vmddrow(1))
      call r0dfldrs('vgtpbc',vbcgrow(1))
      call r0dfldrs('vgtpoa',voagrow(1))
      call r0dfldrs('vgtpas',vasgrow(1))
      call r0dfldrs('vgtpss',vssgrow(1))
      call r0dfldrs('vgtpmd',vmdgrow(1))
      call r0dfldrs('wetasl1',vas1row(1))
      call r0dfldrs('wetasl2',vas2row(1))
      call r0dfldrs('wetasl3',vas3row(1))
      call r0dfldrs('incldasl',vasirow(1))
      call r0dfldrs('vnuclas',vasnrow(1))
      call r0dfldrs('vnucln',vncnrow(1))
      call r0dfldrs('vcondas',vscdrow(1))
      call r0dfldrs('vgppsa',vgsprow(1))
      call r0dfldrs('emimdacc',defarow(1))
      call r0dfldrs('emimdcoa',defcrow(1))
      call r1dfldrs('nuclas',ssunrow(1,1),ilev)
      call r1dfldrs('nucln',sncnrow(1,1),ilev)
      call r1dfldrs('condas',scndrow(1,1),ilev)
      call r1dfldrs('gppsa',sgsprow(1,1),ilev)
      call r1dfldrs('concasd',acasrow(1,1),ilev)
      call r1dfldrs('concoad',acoarow(1,1),ilev)
      call r1dfldrs('concbcd',acbcrow(1,1),ilev)
      call r1dfldrs('concssd',acssrow(1,1),ilev)
      call r1dfldrs('concmdd',acmdrow(1,1),ilev)
      call r1dfldrs('cn0',ntrow(1,1),ilev)
      call r1dfldrs('cn20',n20row(1,1),ilev)
      call r1dfldrs('cn50',n50row(1,1),ilev)
      call r1dfldrs('cn100',n100row(1,1),ilev)
      call r1dfldrs('cn200',n200row(1,1),ilev)
      call r1dfldrs('wcn0',wtrow(1,1),ilev)
      call r1dfldrs('wcn20',w20row(1,1),ilev)
      call r1dfldrs('wcn50',w50row(1,1),ilev)
      call r1dfldrs('wcn100',w100row(1,1),ilev)
      call r1dfldrs('wcn200',w200row(1,1),ilev)
      call r1dfldrs('ccnc02',cc02row(1,1),ilev)
      call r1dfldrs('ccnc04',cc04row(1,1),ilev)
      call r1dfldrs('ccnc08',cc08row(1,1),ilev)
      call r1dfldrs('ccnc16',cc16row(1,1),ilev)
    end if
    if (kxtra2) then
      call r0dfldrs('vresn1',vrn1row(1))
      call r0dfldrs('vresn2',vrn2row(1))
      call r0dfldrs('vresn3',vrn3row(1))
      call r0dfldrs('vresm1',vrm1row(1))
      call r0dfldrs('vresm2',vrm2row(1))
      call r0dfldrs('vresm3',vrm3row(1))
      call r1dfldrs('resn',cornrow(1,1),ilev)
      call r1dfldrs('resm',cormrow(1,1),ilev)
      call r1dfldrs('resn1',rsn1row(1,1),ilev)
      call r1dfldrs('resn2',rsn2row(1,1),ilev)
      call r1dfldrs('resn3',rsn3row(1,1),ilev)
      call r1dfldrs('resm1',rsm1row(1,1),ilev)
      call r1dfldrs('resm2',rsm2row(1,1),ilev)
      call r1dfldrs('resm3',rsm3row(1,1),ilev)
      call r1dfldrs('vsdvl',sdvlrow(1,1),isdiag)
      call r1dfldrs('defxmd',defxrow(1,1),isdiag)
      call r1dfldrs('defnmd',defnrow(1,1),isdiag)
      call r1dfldrs('diagmd1',dmacrow(1,1),isdust)
      call r1dfldrs('diagmd2',dmcorow(1,1),isdust)
      call r1dfldrs('diagmdn1',dnacrow(1,1),isdust)
      call r1dfldrs('diagmdn2',dncorow(1,1),isdust)
    end if
    if (isvdust > 0) then
      call r0dfldrs('umd',duwdrow(1))
      call r0dfldrs('ustarmd',dustrow(1))
      call r0dfldrs('ustarmdthr',duthrow(1))
      call r0dfldrs('fallmd',fallrow(1))
      call r0dfldrs('fa10md',fa10row(1))
      call r0dfldrs('fa2md',fa2row(1))
      call r0dfldrs('fa1md',fa1row(1))
      call r0dfldrs('ustarmdthtfr',usmkrow(1))
    end if
    !
    !< <<< * AEROSOL * <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
    !
    !-----------------------------------------------------------------------
    !     * prognostic growth calculations and diagnosis of properties of
    !     * the size distribution. loop over all time steps as specified
    !     * in model boundary conditions.
    !
    write(6,'(A19)') 'TIME INTEGRATION...'
    write(6,'(1X)')
    ichk=0
    do nd=1,ndim
      if (dim(nd)%name(1:5)=='time ' ) then
        ichk=ichk+1
        ndtim=nd
      end if
    end do
    if (ichk /= 1) call xit('MAIN',-18)
    ity=0
    do it=1,nstp
      !
      !----------------------------------------------------------------------
      !       time information.
      !----------------------------------------------------------------------
      !
      itx=it+inits
      nday=int(nint(dim(ndtim)%val(itx))/yspday)
      nhr=int((nint(dim(ndtim)%val(itx))-nday*yspday)/ysphr)
      nmin=int((nint(dim(ndtim)%val(itx))-nday*yspday-nhr*ysphr) &
                 /yspmin)
      nsec=nint(dim(ndtim)%val(itx))-nday*yspday-nhr*ysphr &
                 -nmin*yspmin
      write(6,'(A,I4,A,I3,A2,I0.2,A1,I0.2,A1,I0.2,A1)') '================== Step:',it, &
        ' Time(DHMS):',nday+1,' (',nhr,':',nmin,':',nsec,') =================='
      !         WRITE(6,'(A21,I3,A2,I2,A1,I2,A1,I2,A1)') &
      !           'DAY (HOUR:MIN:SEC) = ',NDAY+1,' (',NHR,':',NMIN,':',NSEC,')'
      !
      !----------------------------------------------------------------------
      !       load current boundary conditions for pam and other routines.
      !----------------------------------------------------------------------
      !
      call r2dfld('paf',flv1t,ilev1,nstp,1,ilev1,it,it)  ! Air pressure at grid cell interfaces (Pa)
      pft(:)=flv1t(1,it)
      do iz=1,ilev
        pf(1,iz)=flv1t(iz+1,it)
      end do
      pressg(1)=flv1t(ilev1,it)                           ! surface pressure (pa)
      call r2dfld('pa',flvt,ilev,nstp,1,ilev,it,it)       ! Air pressure (Pa)
      ph(1,:)=flvt(:,it)
      call r2dfld('taf',flv1t,ilev1,nstp,1,ilev1,it,it)   ! Air temperature at grid cell interfaces (K)
      do iz=1,ilev
        tfrow(1,iz)=flv1t(iz+1,it)
      end do
      call r2dfld('ta',flvt,ilev,nstp,1,ilev,it,it)       ! Air temperature (K)
      throwr(1,:)=flvt(:,it)
      call r2dfld('ua',flvt,ilev,nstp,1,ilev,it,it)       ! Horizontal wind speed (m/s)
      urowr(1,:)=flvt(:,it)
      call r2dfld('wa',flvt,ilev,nstp,1,ilev,it,it)       ! Vertical wind speed (m/s)
      wg(1,:)=flvt(:,it)
      call r2dfld('rv',flvt,ilev,nstp,1,ilev,it,it)       ! Water vapour mass mixing ratio (kg/kg)
      qrowr(1,:)=flvt(:,it)
      call r2dfld('rl',flvt,ilev,nstp,1,ilev,it,it)       ! Liquid water mass mixing ratio (kg/kg)
      qlwcr(1,:)=flvt(:,it)
      call r2dfld('perta',flvt,ilev,nstp,1,ilev,it,it)    ! Temperature perturbation (K/s)
      perta(1,:)=flvt(:,it)
      call r2dfld('perrv',flvt,ilev,nstp,1,ilev,it,it)    ! Water mixing ratio perturbation (kg/kg/s)
      perrv(1,:)=flvt(:,it)
      call r1dfld('rliqgnd',ft,nstp,it,it)                ! Soil water content (kg/kg)
      thliqg(1)=ft(it)
      call r1dfld('zo',ft,nstp,it,it)                     ! Roughness length (m)
      zo(1)=ft(it)
      call r1dfld('tsurf',ft,nstp,it,it)                  ! Surface temperature of ground (K)
      gtrow(1)=ft(it)
      !
      !> >>> * AEROSOL * >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      !
      call r3dfld('rtrac',flvtn,ilev,nstp,ntrac,1,ilev,it,it,1,ntrac)
      do n=1,ntrac
        xrowr(1,:,n)=flvtn(:,it,n)
      end do
      call r3dfld('strac',flvtn,ilev,nstp,ntrac,1,ilev,it,it,1,ntrac)
      do n=1,ntrac
        sfrcr(1,:,n)=flvtn(:,it,n)
      end do
      call r2dfld('rvo3',flvt,ilev,nstp,1,ilev,it,it)
      o3row(1,:)=flvt(:,it)
      call r2dfld('rvoh',flvt,ilev,nstp,1,ilev,it,it)
      ohrow(1,:)=flvt(:,it)
      call r2dfld('rvno3',flvt,ilev,nstp,1,ilev,it,it)
      no3row(1,:)=flvt(:,it)
      call r2dfld('rvhno3',flvt,ilev,nstp,1,ilev,it,it)
      hno3row(1,:)=flvt(:,it)
      call r2dfld('rvnh3',flvt,ilev,nstp,1,ilev,it,it)
      nh3row(1,:)=flvt(:,it)
      call r2dfld('rvnh4',flvt,ilev,nstp,1,ilev,it,it)
      nh4row(1,:)=flvt(:,it)
      call r2dfld('rvh2o2',flvt,ilev,nstp,1,ilev,it,it)
      bghpo(1,:)=flvt(:,it)
      call r2dfld('emiso2',flvt,ilev,nstp,1,ilev,it,it)
      so2em(1,:)=flvt(:,it)
      call r2dfld('emisoap',flvt,ilev,nstp,1,ilev,it,it)
      orgem(1,:)=flvt(:,it)
      call r2dfld('emibc1',flvt,ilev,nstp,1,ilev,it,it)
      dbcem1(1,:)=flvt(:,it)
      call r2dfld('emibc2',flvt,ilev,nstp,1,ilev,it,it)
      dbcem2(1,:)=flvt(:,it)
      call r2dfld('emibc3',flvt,ilev,nstp,1,ilev,it,it)
      dbcem3(1,:)=flvt(:,it)
      call r2dfld('emibc4',flvt,ilev,nstp,1,ilev,it,it)
      dbcem4(1,:)=flvt(:,it)
      call r2dfld('emioa1',flvt,ilev,nstp,1,ilev,it,it)
      docem1(1,:)=flvt(:,it)
      call r2dfld('emioa2',flvt,ilev,nstp,1,ilev,it,it)
      docem2(1,:)=flvt(:,it)
      call r2dfld('emioa3',flvt,ilev,nstp,1,ilev,it,it)
      docem3(1,:)=flvt(:,it)
      call r2dfld('emioa4',flvt,ilev,nstp,1,ilev,it,it)
      docem4(1,:)=flvt(:,it)
      call r2dfld('emias1',flvt,ilev,nstp,1,ilev,it,it)
      dsuem1(1,:)=flvt(:,it)
      call r2dfld('emias2',flvt,ilev,nstp,1,ilev,it,it)
      dsuem2(1,:)=flvt(:,it)
      call r2dfld('emias3',flvt,ilev,nstp,1,ilev,it,it)
      dsuem3(1,:)=flvt(:,it)
      call r2dfld('emias4',flvt,ilev,nstp,1,ilev,it,it)
      dsuem4(1,:)=flvt(:,it)
      call r1dfld('dmso',ft,nstp,it,it)
      dmso(1)=ft(it)
      call r1dfld('flnd',ft,nstp,it,it)
      flnd(1)=ft(it)
      call r1dfld('focn',ft,nstp,it,it)
      focn(1)=ft(it)
      call r1dfld('sicn',ft,nstp,it,it)
      sicn(1)=ft(it)
      call r1dfld('fsm',ft,nstp,it,it)
      smfrac(1)=ft(it)
      call r1dfld('fcan1',ft,nstp,it,it)
      fcanrow(1,1)=ft(it)
      call r1dfld('fcan2',ft,nstp,it,it)
      fcanrow(1,2)=ft(it)
      call r1dfld('fcan3',ft,nstp,it,it)
      fcanrow(1,3)=ft(it)
      call r1dfld('fcan4',ft,nstp,it,it)
      fcanrow(1,4)=ft(it)
      call r1dfld('fsnw',ft,nstp,it,it)
      fnrol(1)=ft(it)
      call r1dfld('suzo',ft,nstp,it,it)
      suz0row(1)=ft(it)
      call r1dfld('spot',ft,nstp,it,it)
      spotrow(1)=ft(it)
      call r1dfld('st2',ft,nstp,it,it)
      st02row(1)=ft(it)
      call r1dfld('st3',ft,nstp,it,it)
      st03row(1)=ft(it)
      call r1dfld('st4',ft,nstp,it,it)
      st04row(1)=ft(it)
      call r1dfld('st6',ft,nstp,it,it)
      st06row(1)=ft(it)
      call r1dfld('st13',ft,nstp,it,it)
      st13row(1)=ft(it)
      call r1dfld('st14',ft,nstp,it,it)
      st14row(1)=ft(it)
      call r1dfld('st15',ft,nstp,it,it)
      st15row(1)=ft(it)
      call r1dfld('st16',ft,nstp,it,it)
      st16row(1)=ft(it)
      call r1dfld('st17',ft,nstp,it,it)
      st17row(1)=ft(it)
      call r1dfld('fbgnd',ft,nstp,it,it)
      bsfrac(1)=ft(it)
      call r1dfld('fpds',ft,nstp,it,it)
      pdsfrow(1)=ft(it)
      !
      !< <<< * AEROSOL * <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
      !
      vrow=0.
      !
      !----------------------------------------------------------------------
      !       pressure-level information.
      !----------------------------------------------------------------------
      !
      shtj(:,1)=pft(:)/pressg(:)
      dp(:,1)=pf(:,1)-pft(:)
      do iz=2,ilev
        shtj(:,iz)=pf(:,iz-1)/pressg(:)
        dp(:,iz)=pf(:,iz)-pf(:,iz-1)
      end do
      do iz=1,ilev
        shj(:,iz)=ph(:,iz)/pressg(:)
        dshj(:,iz)=dp(:,iz)/pressg(:)
      end do
      !
      !----------------------------------------------------------------------
      !       insert temperature and moisture perturbations (if applicable).
      !----------------------------------------------------------------------
      !
      throw=throw+dt*perta
      qrow=qrow+dt*perrv
      taux=tauxr
      taut=tautr
      tauq=tauqr
      taum=taumr
      if (it < modl%trax) taux=0.
      if (it < modl%trat) taut=0.
      if (it < modl%traq) tauq=0.
      !
      !----------------------------------------------------------------------
      !       vertical advection (subsidence). the code contains the option
      !       to advect tracers (xrow), which is currently unused.
      !----------------------------------------------------------------------
      !

      call vadv(dxdt,xrow,dtdt,throw,dqdt,qrow,dqldt,qlwc, &
                  dudt,urow,wg,zf,zh,ph,dp,almc,almx, &
                  dt,il1,il2,ilg,ilev,ilevp1,msgp2,ntrac)
      xrow=max(xrow+dxdt*dt,0.)
      throw=throw+dtdt*dt
      qrow = qrow+dqdt*dt
      qlwc = qlwc+dqldt*dt
      urow = urow+dudt*dt
      !
      dtadv=dtdt*real(yspday)
      dqadv=dqdt*real(yspday)
      !
      !       * specified updraft wind speed and/or cloud droplet number
      !
      if (it >= modl%trup .and. abs(modl%cdnn + spv) > 0.001) &
          modl%cdn=modl%cdnn
      if (abs(modl%cdn + spv) > 0.001) zcdnrow=modl%cdn*1.e+06
      cdnrow=1.e-06*zcdnrow
      !
      !----------------------------------------------------------------------
      !       vertical diffusion. the code contains the option
      !       to mix tracers (xrow), which is currently unused.
      !----------------------------------------------------------------------
      isub=30
      dts=dt/real(isub)
      dtvdf=0.
      dqvdf=0.
      do is=1,isub
        call vrtdf(dxdt,xrow,dtdt,throw,dqdt,qrow,dqldt,qlwc, &
                     dudt,urow,vrow,wsub,rkh,rkm,ri,cdm,ustar,zspd, &
                     zspda,zf,pf,tfrow,zh,ph,dp,almc,almx,pbltrow,tvp, &
                     qsens,qlat,zclf,gtrow,thliqg,zo,shtj,shj,dshj, &
                     pressg,dts,modl%nugalt,il1,il2,ilg,ilev,ilevp1, &
                     msgp2,ntrac)
        xrow=max(xrow+dxdt*dts,0.)
        throw=throw+dtdt*dts
        qrow = qrow+dqdt*dts
        qlwc = qlwc+dqldt*dts
        urow = urow+dudt*dts
        dtvdf=dtvdf+dtdt*real(yspday)/real(isub)
        dqvdf=dqvdf+(dqdt+dqldt)*real(yspday)/real(isub)
      end do
      !
      !----------------------------------------------------------------------
      !       simple microphysics for warm clouds.
      !----------------------------------------------------------------------
      !
      dtdt=throw
      dqdt=qrow+qlwc
      call cldphys(zclf,pcp,clrfr,clrfs,zfrain,zfsnow,zfevap, &
                     zfsubl,zmlwc,zmratep,rh,rhc,hmn,qtn,qrow,qlwc, &
                     zcdnrow,qcwa,throw,ph,zh,dp,almc,almx,dt,ilg,ilev, &
                     modl%case)
      dtdt=(throw-dtdt)/dt
      dqdt=(qrow+qlwc-dqdt)/dt
      !
      dtphs=dtdt*real(yspday)
      dqphs=dqdt*real(yspday)
      !
      !> >>> * AEROSOL * >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      !
      !----------------------------------------------------------------------
      !       land-use type fractions (from class).
      !----------------------------------------------------------------------
      !
      fcant=sum(fcanrow,dim=2)
      fcs=fnrol*fcant          ! fraction for canopy over snow.
      fgs=fnrol*(1.-fcant)     ! fraction for snow-covered ground.
      fc=(1.-fnrol)*fcant      ! fraction for canopy over bare ground.
      fg=(1.-fnrol)*(1.-fcant) ! fraction for bare ground.
      fcanrow(:,5)=0.
      !
      !       * specify vertical velocity for aerosol activation.
      !
      !        wsub=.03
      !
      !       * drag coefficients above ground (normalized, dimensionless) above
      !       * ocean and land, and friction velocity for bare ground (m/sec).
      !
      cdmnl=cdm
      cdml=cdm
      ustarbs=ustar
      !
      !       * wind speeds at anemometer level above ocean and bare ground (m/sec).
      !
      zspdso=zspda
      zspdsbs=zspda
      !
      !       * total cloud fraction and solar zenith angle (for diagnostics).
      !
      cldt=0.
      do l=1,ilev
        cldt=max(cldt,zclf(:,l))
      end do
      rmu=1.
      !
      !----------------------------------------------------------------------
      !       emissions of gas-phase species.
      !----------------------------------------------------------------------
      !
      !       * insert direct emissions in each layer.
      !
      !       note on units of tracers: all tracer mass mixing ratios
      !       are for total (dry) masses of tracers, except so2, dms, and h2o2.
      !       the latter tracers are for mass of sulphur so that chemical
      !       conversions between these tracers are straightforward.
      !
      dxdt=0.
      wso2=64.059e-03
      dxdt(:,msgp2:ilevp1,iso2)=so2em(:,:)*ws/wso2  ! so2 emissions (kg-s/m2/sec)
      dxdt(:,msgp2:ilevp1,igsp)=orgem(:,:)
      xrow=max(xrow+dxdt*dt,0.)
      !
      !       * dms emissions (kg-s/m2/sec).
      !
      call emidms(ddmsdt,focn,dmso,gtrow,zspdso,dp,ilg,ilev)
      xrow(:,ilevp1,idms)=xrow(:,ilevp1,idms) &
                           +ddmsdt*dt*grav/dp(:,ilev)
      !
      !----------------------------------------------------------------------
      !       gas-phase chemistry and dry deposition of so2.
      !----------------------------------------------------------------------
      !
      !     * calculate cosine of current local zenith angle (cszrow).
      !
      doy  = modl%sdoy + nday                               ! current day of year
      dec  = 23.45*sin((2.0*(283.0+doy)/365.0)*pi)/180.0*pi ! declination of earth for current doy (radians)
      radj = modl%lat*pi/180.                               ! latitude (radians)
      frtime=(float(nhr)*3600.+(float(nmin)*60.)+(float(nsec))) ! fractional time of day
      hr_angle=(2.*pi*(frtime/86400.+0./360.))  ! hour angle (radians)
      !
      do il=il1,il2
        cszrow(il) = sin(radj(il))*sin(dec) - cos(radj(il))*cos(dec)*cos(hr_angle)
        cszrow(il) = max(cszrow(il),zero)
      end do
      !
      !     * calculate fraction of sunlit hours per day (zdayl).
      !
      call sunlit_hours_per_day(radj,sin(dec),cos(dec),il1,il2,ilg,zdayl) ! nb zdayl used in xtchempam is the inverse of zdayl from sunlit_hours_per_day
      !
      !     * gas-phase chemistry
      !
      call xtchempam(xrow,throw,qrow, &
                       sgpp,dshj,shj,cszrow,1/zdayl,pressg,iaind,ntracp, &
                       sfrcrol,ohrow,bghpo,o3row,no3row, &
                       hno3row,nh3row,nh4row, &
                       dox4row,doxdrow,noxdrow,zclf,zmratep,zfsnow, &
                       zfrain,zmlwc,zfevap,clrfr,clrfs,zfsubl,atau, &
                       itrwet,iso2,idms,ihpo,dt,1, &
                       ntrac,ilg,il1,il2,ilev)
      ogpp=0.
      snorow=0.
      hmfnrow=0.
      zfor=0.
      call drydepg(xrow,flnd,gtrow,gtrow,pressg,dshj,throw, &
                     shj,sicn,snorow,thliqg,qrow,hmfnrow, &
                     zfor,dd4row,dt,iso2,ilg,il1,il2,ilev,ntrac)
      !
      !----------------------------------------------------------------------
      !       interface with pla aerosol model (pam).
      !----------------------------------------------------------------------
      !
      leva=ilev-1
      call pamdriv(oednrow,oercrow,oidnrow,oircrow,svvbrow, &
                     psvvrow,svmbrow,svcbrow,qnvbrow, &
                     qnmbrow,qncbrow,qsvbrow,qsmbrow,qscbrow, &
                     qgvbrow,qgmbrow,qgcbrow,qdvbrow,qdmbrow, &
                     qdcbrow, &
                     devbrow,pdevrow,dembrow,decbrow,divbrow, &
                     pdivrow,dimbrow,dicbrow,revbrow,prevrow, &
                     rembrow,recbrow,rivbrow,privrow,rimbrow, &
                     ricbrow, &
                     xrow,sfrcrol,iaindt,ilg,ilevp1,ntrac,msgp2, &
                     il1,il2,iso2,ihpo,igs6,igsp,ntracp, &
                     ilev,ican,msgp1,zh,zf,ph,pf,dp,throw,qrow, &
                     qtn,hmn,rhc,rh,wg,wsub,zmratep,zmlwc,zfsnow, &
                     zfrain,zclf,clrfr,clrfs,zfevap,zfsubl, &
                     pbltrow,smfrac,fcs,fgs,fc,fg, &
                     zspd,zspdso,zspdsbs,cdml,cdmnl,ustarbs, &
                     flnd,focn,sicn,gtrow,fnrol, &
                     fcanrow,spotrow,st02row,st03row,st04row, &
                     st06row,st13row,st14row,st15row,st16row, &
                     st17row,suz0row,bsfrac,pdsfrow,atau,zfsh,pressg, &
                     sgpp,ogpp,docem1,docem2,docem3,docem4,dbcem1, &
                     dbcem2,dbcem3,dbcem4,dsuem1,dsuem2,dsuem3, &
                     dsuem4,o3row,hno3row,nh3row,nh4row,dt, &
                     co2_ppm,itx,tmin,zcdnpam,bcicrow,depbrol, &
                     cornrow,cormrow,rsn1row,rsm1row, &
                     vrn1row,vrm1row,rsn2row,rsm2row,vrn2row, &
                     vrm2row,rsn3row,rsm3row,vrn3row,vrm3row, &
                     vncnrow,vasnrow,vscdrow,vgsprow,sncnrow, &
                     ssunrow,scndrow,sgsprow,defxrow,defnrow, &
                     acasrow,acoarow,acbcrow,acssrow,acmdrow, &
                     ntrow,n20row,n50row,n100row,n200row, &
                     wtrow,w20row,w50row,w100row,w200row,ccnrow, &
                     ccnerow,cc02row,cc04row,cc08row,cc16row, &
                     rcrirow,supsrow,voaerow,vbcerow,vaserow, &
                     vmderow,vsserow,voawrow,vbcwrow,vaswrow, &
                     vmdwrow,vsswrow,voadrow,vbcdrow,vasdrow, &
                     vmddrow,vssdrow,voagrow,vbcgrow,vasgrow, &
                     vmdgrow,vssgrow,vasirow,vas1row,vas2row, &
                     vas3row,henrrow,o3frrow,h2o2frrow,wparrow, &
                     vccnrow,vcnerow,dmacrow,dmcorow,dnacrow, &
                     dncorow,duwdrow,dustrow,duthrow,fallrow, &
                     fa10row,fa2row ,fa1row ,usmkrow,defarow, &
                     defcrow,sicn_crt,saverad,isvdust,leva)
      !
      !       * diagnostic calculations for aerosol optical and other
      !       * properties.
      !
      call pamdiag(xrow,ilg,ilevp1,ntrac,iaindt,il1,il2, &
                     ilev,dp,throw,rhc,ph,cldt,rmu, &
                     ssldrow,ressrow,vessrow,dsldrow,redsrow, &
                     vedsrow,bcldrow,rebcrow,vebcrow,ocldrow, &
                     reocrow,veocrow,amldrow,reamrow,veamrow, &
                     fr1row,fr2row,sulifrc,rsuifrc,vsuifrc, &
                     f1sufrc,f2sufrc,bclifrc,rbcifrc,vbcifrc, &
                     f1bcfrc,f2bcfrc,oclifrc,rocifrc,vocifrc, &
                     f1ocfrc,f2ocfrc,sdvlrow,sdnurow,sdmarow, &
                     sdacrow,sdcorow,sssnrow,smdnrow,sianrow, &
                     sssmrow,smdmrow,sewmrow,ssumrow,socmrow, &
                     sbcmrow,siwmrow,tnssrow,tnmdrow,tniarow, &
                     tmssrow,tmmdrow,tmocrow,tmbcrow,tmsprow, &
                     snssrow,snmdrow,sniarow,smssrow,smmdrow, &
                     smocrow,smbcrow,smsprow,siwhrow,sewhrow, &
                     pm25row,pm10row,dm25row,dm10row,saverad,irfrc)
      !
      !       * use simulated cloud droplet number from pam for cloud microphysics.
      !
      if (kcdnc) zcdnrow=zcdnpam
      !
      !----------------------------------------------------------------------
      !       * aerosol radiative properties.
      !----------------------------------------------------------------------
      !
      exta   = 1.0e-20
      exoma  = 1.0e-20
      exomga = 1.0e-20
      fa     = 0.0
      absa   = 1.0e-20
      dprad  = dp*0.1/grav ! units for dprad are g/cm2 = 0.1 kg/m2
      aods   = 0.
      aod86  = 0.
      mass   = 0.
      abss   = 0.
      exts   = 0.
      angstr = 0.
      !
      !     * mass mixing ratios of internally mixed aerosol species.
      !
      do k = 1, ilev
        do i = il1, il2
          fr3          = min(max((1. - fr1row(i,k) - fr2row(i,k)), &
                         0.),1.)
          so4load(i,k) = fr1row(i,k) * amldrow(i,k)
          bcyload(i,k) = fr2row(i,k) * amldrow(i,k)
          ocyload(i,k) = fr3 * amldrow(i,k)
          !
          !     * mass mixing ratios of externally mixed aerosol species.
          !
          bcoload(i,k) = bcldrow(i,k)
          ocoload(i,k) = ocldrow(i,k)
          !
          !     * effective radius and variance of hydrophilic species that
          !     * are treated as externally mixed (unused).
          !
          reso4(i,k) = reamrow(i,k)
          rebcy(i,k) = reamrow(i,k)
          reocy(i,k) = reamrow(i,k)
          veso4(i,k) = veamrow(i,k)
          vebcy(i,k) = veamrow(i,k)
          veocy(i,k) = veamrow(i,k)
        end do
      end do
      !
      !----------------------------------------------------------------------
      !       * calculate the aerosol optical properties for internal mixing of
      !       * sulfate, bc and oc. aerosol is hygroscopic growth dependent,
      !       * continuous size distribution.
      !
      call aeromx3op (exta, exoma, exomga, fa, absa, taua055, absa055, &
                        taua086, rhc, so4load, bcoload, bcyload,ocoload, &
                        ocyload, reamrow, veamrow, reso4, veso4, rebcy, &
                        vebcy, reocy, veocy, rebcrow, vebcrow, reocrow, &
                        veocrow, il1, il2, ilg, ilev)
      do k = 1, ilev
        do i = il1, il2
          fr3          = min(max((1. - fr1row(i,k) - fr2row(i,k)), &
                         0.),1.)
          am3loads      =  10000. * amldrow(i,k) * dprad(i,k)
          !
          aods(i,1)     =  aods(i,1) +  taua055(i,k,1) * dprad(i,k)
          aods(i,4)     =  aods(i,4) +  taua055(i,k,2) * dprad(i,k)
          aods(i,5)     =  aods(i,5) +  taua055(i,k,3) * dprad(i,k)
          aods(i,6)     =  aods(i,6) +  taua055(i,k,4) * dprad(i,k)
          aod86(i,1)    =  aod86(i,1) + taua086(i,k,1) * dprad(i,k)
          aod86(i,4)    =  aod86(i,4) + taua086(i,k,2) * dprad(i,k)
          aod86(i,5)    =  aod86(i,5) + taua086(i,k,3) * dprad(i,k)
          aod86(i,6)    =  aod86(i,6) + taua086(i,k,4) * dprad(i,k)
          !
          mass(i,1)     =  mass(i,1) + am3loads * fr1row(i,k)
          mass(i,4)     =  mass(i,4) + am3loads * fr2row(i,k)
          mass(i,5)     =  mass(i,5) + am3loads * fr3
          mass(i,6)     =  mass(i,6) + am3loads
          !
          abss(i,1)     =  abss(i,1) + absa055(i,k,1) * dprad(i,k)
          abss(i,4)     =  abss(i,4) + absa055(i,k,2) * dprad(i,k)
          abss(i,5)     =  abss(i,5) + absa055(i,k,3) * dprad(i,k)
          abss(i,6)     =  abss(i,6) + absa055(i,k,4) * dprad(i,k)
        end do
      end do
      !
      !----------------------------------------------------------------------
      !       * calculate the sea salt (ss) aerosol optical properties, the
      !       * aerosol is hygroscopic growth dependent, continuous size
      !       * distribution.
      !
      call ssaltaerop(exta, exoma, exomga, fa, absa, sext055, sext086, &
                        sssa055, rhc, ssldrow, ressrow, vessrow, &
                        il1, il2, ilg, ilev)
      !
      do k = 1, ilev
        do i = il1, il2
          ssloads       =  10000.0 * ssldrow(i,k) * dprad(i,k)
          x55           =  sext055(i,k) * ssloads
          x86           =  sext086(i,k) * ssloads
          !
          aod86(i,2)    =  aod86(i,2) + x86
          aods(i,2)     =  aods(i,2) + x55
          mass(i,2)     =  mass(i,2) + ssloads
          abss(i,2)     =  abss(i,2) + x55 * (1.0 - sssa055(i,k))
        end do
      end do
      !
      !----------------------------------------------------------------------
      !       * calculate the dust (ds) aerosol optical properties, no
      !       * hygroscopic effect, continuous size distribution.
      !
      call dustaerop(exta, exoma, exomga, fa, absa, dext055, dext086, &
                       dssa055, dsldrow, redsrow, vedsrow, il1, il2,ilg, &
                       ilev)
      !
      do k = 1, ilev
        do i = il1, il2
          dsloads       =  10000.0 * dsldrow(i,k) * dprad(i,k)
          x55           =  dext055(i,k) * dsloads
          x86           =  dext086(i,k) * dsloads
          !
          aod86(i,3)    =  aod86(i,3) + x86
          aods(i,3)     =  aods(i,3) + x55
          mass(i,3)     =  mass(i,3) + dsloads
          abss(i,3)     =  abss(i,3) + x55 * (1.0 - dssa055(i,k))
        end do
      end do
      !
      !----------------------------------------------------------------------
      !       * diagnose all aerosol radiative properties.
      !
      do i = il1, il2
        aods(i,6)     =  aods(i,6) + aods(i,2) + aods(i,3)
        abss(i,6)     =  abss(i,6) + abss(i,2) + abss(i,3)
        mass(i,6)     =  mass(i,6) + mass(i,2) + mass(i,3)
        aod86(i,6)    =  aod86(i,6) + aod86(i,2) + aod86(i,3)
        do j = 1, 6
          if (mass(i,j) > 0.0) then
            exts(i,j) =  aods(i,j) / mass(i,j)
          else
            exts(i,j) =  0.0
          end if
        end do
        do j = 1, 6
          if (aod86(i,j) < 1.e-05) then
            angstr(i,j)   =  0.0
          else
            angstr(i,j)   =  2.20843 * log (aods(i,j) / aod86(i,j))
          end if
        end do
      end do
      !
      !----------------------------------------------------------------------
      !       diagnose tracer burdens and concentrations.
      !----------------------------------------------------------------------
      !
      rhoa=ph/(rgas*throw)
      xburd=0.
      do n=1,ntrac
        do iz=1,ilev
          xburd(:,n)=xburd(:,n)+xrow(:,iz,n)*dp(:,iz)/grav
        end do
      end do
      call trburd (voabrd,vbcbrd,vasbrd,vmdbrd,vssbrd,xrow, &
                     dshj,pressg,grav,ntrac,iaindt,il1,il2,ilg,ilev, &
                     ilevp1,msgt)
      call trconc (coacnc,cbccnc,cascnc,cmdcnc,csscnc,xrow,rhoa, &
                     ntrac,iaindt,il1,il2,ilg,ilev,ilevp1,msgt)
      call trnum (cssn,cmdn,cinn,vssn,vmdn,vinn,xrow,rhoa, &
                    dshj,pressg,grav,ntrac,iaindt, &
                    il1,il2,ilg,ilev,ilevp1,msgt)
      !
      !< <<< * AEROSOL * <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
      !
      !----------------------------------------------------------------------
      !       nudge tracers, temperature, and moisture profiles towards
      !       specified large-scale values.
      !----------------------------------------------------------------------
      !
      !! !
      !        where ( tauq < taul - 1.e-20)
      !          qrowr=qrowr*0.9
      !       end where
      where (taux < taul - 1.e-20)
        dxdt=-(xrow-xrowr)/max(taux,dt)
      else where
        dxdt=0.
      end where
      where (taut < taul - 1.e-20)
        dtdt=-(throw-throwr)/max(taut,dt)
      else where
        dtdt=0.
      end where
      where (tauq < taul - 1.e-20)
        dqdt=-(qrow-qrowr)/max(tauq,dt)
      else where
        dqdt=0.
      end where
      where (tauq < taul - 1.e-20)
        dqldt=-(qlwc-qlwcr)/max(tauq,dt)
      else where
        dqldt=0.
      end where
      where (taum < taul - 1.e-20)
        dudt=-(urow-urowr)/max(taum,dt)
      else where
        dudt=0.
      end where
      !
      xrow=max(xrow+dxdt*dt,0.)
      throw=throw+dt*dtdt
      qrow=qrow+dt*dqdt
      qlwc=qlwc+dt*dqldt
      urow=urow+dt*dudt
      !
      where (taux < 1.e-03)
        dxdt=dxdt+(xrowr-xrow)/dt
        xrow=xrowr
      end where
      where (taut < 1.e-03)
        dtdt=dtdt+(throwr-throw)/dt
        throw=throwr
      end where
      where (tauq < 1.e-03)
        dqdt=dqdt+(qrowr-qrow)/dt
        qrow=qrowr
      end where
      where (tauq < 1.e-03)
        dqldt=dqldt+(qlwcr-qlwc)/dt
        qlwc=qlwcr
      end where
      where (urow < 1.e-03) urow=urowr
      !
      dtndg=dtdt*real(yspday)
      dqndg=(dqdt+dqldt)*real(yspday)
      !
      !       * temperature at grid cell interfaces
      !
      where (taut(:,ilev) > 1.e-03) tfrow(:,ilev)=gtrow(:)
      do iz=ilev-1,1,-1
        where (taut(:,iz) > 1.e-03)
          wgt=(zf(:,iz)-zh(:,iz+1))/(zh(:,iz)-zh(:,iz+1))
          tfrow(:,iz)=wgt*throw(:,iz)+(1.-wgt)*throw(:,iz+1)
        end where
      end do
      !
      !       * ensure consistency for cloud fraction.
      !
      where (qlwc < 1.e-06) zclf=0.
      !
      !----------------------------------------------------------------------
      !       diagnose cloud liquid water effective radius.
      !----------------------------------------------------------------------
      !
      do iz=1,ilev
        where (cdnrow(:,iz) > 1. .and. qlwc(:,iz) > ysmall &
            .and. zclf(:,iz) > ysmall)
          !            beta(:,iz)=1.22+0.00084*cdnrow(:,iz)
          beta(:,iz)=1.18+0.00045*cdnrow(:,iz)
          !            beta(:,iz)=1.
          radeqv(:,iz)=beta(:,iz)*62.035*(qlwc(:,iz)*rhoa(:,iz)*1000. &
                        /(zclf(:,iz)*cdnrow(:,iz)))**(1./3.)
          radeqva(:,iz)=beta(:,iz)*62.035*(qcwa(:,iz)*rhoa(:,iz)*1000. &
                        /(cdnrow(:,iz)))**(1./3.)
        else where
          radeqv(:,iz)=0.
          radeqva(:,iz)=0.
        end where
      end do
      !----------------------------------------------------------------------
      !       copy pam results to output at specified output time interval.
      !----------------------------------------------------------------------
      !
      if (mod(it+modl%pltfq-1,modl%pltfq) == 0) then
        ity=ity+1
        if (ktime1) then
          atime(ity)=dim(ndtim)%val(it)
          call w1ddim('time1',atime,nstpo,ity,ity)
        end if
        if (kmetout) then
          flvt1(:,ity)=zclf(1,:)
          call w2dfld('clf',flvt1,ilev,nstpo,1,ilev,ity,ity)
          flvt1(:,ity)=urow(1,:)
          call w2dfld('ua',flvt1,ilev,nstpo,1,ilev,ity,ity)
          flvt1(:,ity)=throw(1,:)
          call w2dfld('ta',flvt1,ilev,nstpo,1,ilev,ity,ity)
          flvt1(:,ity)=qrow(1,:)
          call w2dfld('rv',flvt1,ilev,nstpo,1,ilev,ity,ity)
          flvt1(:,ity)=qlwc(1,:)
          call w2dfld('rl',flvt1,ilev,nstpo,1,ilev,ity,ity)
          flvt1(:,ity)=rh(1,:)
          call w2dfld('relhum',flvt1,ilev,nstpo,1,ilev,ity,ity)
          flvt1(:,ity)=rhc(1,:)
          call w2dfld('relhumclr',flvt1,ilev,nstpo,1,ilev,ity,ity)
          flvt1(:,ity)=ri(1,:)
          call w2dfld('ri',flvt1,ilev,nstpo,1,ilev,ity,ity)
          flvt1(:,ity)=rkm(1,:)
          call w2dfld('rkm',flvt1,ilev,nstpo,1,ilev,ity,ity)
          flvt2(ilev+1,ity)=qlat(1)/rl*real(yspday)
          do iz=ilev,1,-1
            fac=grav/dp(1,iz)
            flvt2(iz,ity)=flvt2(iz+1,ity)-dqvdf(1,iz)/fac
          end do
          call w2dfld('rvflx',flvt2,ilev+1,nstpo,1,ilev+1,ity,ity)
          ft1(ity)=pcp(1)*real(yspday)
          call w1dfld('pr',ft1,nstpo,ity,ity)
          flvt1(:,ity)=clrfr(1,:)
          call w2dfld('frsclr',flvt1,ilev,nstpo,1,ilev,ity,ity)
          flvt1(:,ity)=zmratep(1,:)
          call w2dfld('prrscld',flvt1,ilev,nstpo,1,ilev,ity,ity)
          flvt1(:,ity)=zmlwc(1,:)
          call w2dfld('rlrscld',flvt1,ilev,nstpo,1,ilev,ity,ity)
          flvt1(:,ity)=zfrain(1,:)
          call w2dfld('prclr',flvt1,ilev,nstpo,1,ilev,ity,ity)
          flvt1(:,ity)=zfevap(1,:)
          call w2dfld('fprevapclr',flvt1,ilev,nstpo,1,ilev,ity,ity)
          ft1(ity)=qsens(1)
          call w1dfld('hfss',ft1,nstpo,ity,ity)
          ft1(ity)=qlat(1)
          call w1dfld('hfls',ft1,nstpo,ity,ity)
          ft1(ity)=zf(1,min(ilev,max(1,nint(pbltrow(1))-1)))
          call w1dfld('pblh',ft1,nstpo,ity,ity)
          flvt1(:,ity)=tvp(1,:)
          call w2dfld('tavp',flvt1,ilev,nstpo,1,ilev,ity,ity)
          ft1(ity)=0.
          do iz=ilev,1,-1
            fac=grav/dp(1,iz)
            !              ft1(ity)=ft1(ity)+(dqndg(1,iz)+dqvdf(1,iz))/fac
            ft1(ity)=ft1(ity)+(dqndg(1,iz))/fac
          end do
          call w1dfld('qfndg',ft1,nstpo,ity,ity)
          flvt1(:,ity)=dqndg(1,:)
          call w2dfld('drdtndg',flvt1,ilev,nstpo,1,ilev,ity,ity)
          flvt1(:,ity)=dtndg(1,:)
          call w2dfld('dtadtndg',flvt1,ilev,nstpo,1,ilev,ity,ity)
          flvt1(:,ity)=dqadv(1,:)
          call w2dfld('drdtadv',flvt1,ilev,nstpo,1,ilev,ity,ity)
          flvt1(:,ity)=dtadv(1,:)
          call w2dfld('dtadtadv',flvt1,ilev,nstpo,1,ilev,ity,ity)
          flvt1(:,ity)=dqvdf(1,:)
          call w2dfld('drdtvdf',flvt1,ilev,nstpo,1,ilev,ity,ity)
          flvt1(:,ity)=dtvdf(1,:)
          call w2dfld('dtadtvdf',flvt1,ilev,nstpo,1,ilev,ity,ity)
          flvt1(:,ity)=dqphs(1,:)
          call w2dfld('drdtphs',flvt1,ilev,nstpo,1,ilev,ity,ity)
          flvt1(:,ity)=dtphs(1,:)
          call w2dfld('dtadtphs',flvt1,ilev,nstpo,1,ilev,ity,ity)
          flvt1(:,ity)=radeqv(1,:)
          call w2dfld('cldreff',flvt1,ilev,nstpo,1,ilev,ity,ity)
          flvt1(:,ity)=radeqva(1,:)
          call w2dfld('cldreffadiab',flvt1,ilev,nstpo,1,ilev,ity,ity)
          flvt1(:,ity)=qcwa(1,:)
          call w2dfld('rladiab',flvt1,ilev,nstpo,1,ilev,ity,ity)
          flvt1(:,ity)=rhoa(1,:)
          call w2dfld('rhoa',flvt1,ilev,nstpo,1,ilev,ity,ity)
          flvt1(:,ity)=zcdnrow(1,:)*1.e-06
          call w2dfld('cldnc',flvt1,ilev,nstpo,1,ilev,ity,ity)
          flvt1(:,ity)=rhoa(1,:)*qlwc(1,:)*1.e+03
          call w2dfld('clw',flvt1,ilev,nstpo,1,ilev,ity,ity)
          ft1(ity)=0.
          do l=1,ilev
            ft1(ity)=ft1(ity)+dp(1,l)*qlwc(1,l)*1.e+03/grav
          end do
          call w1dfld('clwp',ft1,nstpo,ity,ity)
          flvt1(:,ity)=tauq(1,:)
          call w2dfld('tauq',flvt1,ilev,nstpo,1,ilev,ity,ity)
        end if
        !
        !> >>> * AEROSOL * >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
        !         * write tracer mixing ratios to output.
        !
        do n=1,ntrac
          flvt1n(:,ity,n)=xrow(1,:,n)
        end do
        call w3dfld('rtrac',flvt1n,ilev,nstpo,ntrac,1,ilev,ity,ity, &
                      1,ntrac)
        !
        !         * tracer mixing ratios for clear and cloudy sky.
        !
        do n=1,ntrac
          tmpscl=1.
          where (zclf < zcthr .and. (1.-zclf) < zcthr)
            where (sfrcrol(:,:,n) < -ysmall)
              tmpscl=-xrow(:,:,n)*(1.-zclf)/sfrcrol(:,:,n)
            else where (sfrcrol(:,:,n) > ysmall)
              tmpscl=xrow(:,:,n)*zclf/sfrcrol(:,:,n)
            else where
              tmpscl=0.
            end where
          end where
          tmpscl=max(min(tmpscl,1.),0.)
          sfrcrol(:,:,n)=tmpscl*sfrcrol(:,:,n)
          xarow(:,:,n)=xrow(:,:,n)
          xcrow(:,:,n)=xrow(:,:,n)
          where (zclf < zcthr .and. (1.-zclf) < zcthr)
            xarow(:,:,n)=xarow(:,:,n)+sfrcrol(:,:,n)/(1.-zclf)
            xcrow(:,:,n)=xcrow(:,:,n)-sfrcrol(:,:,n)/zclf
          end where
        end do
        !
        !         * write tracer mixing ratios for clear- and cloudy-sky to output.
        !
        do n=1,ntrac
          flvt1n(:,ity,n)=xarow(1,:,n)
        end do
        call w3dfld('rtracclr',flvt1n,ilev,nstpo,ntrac,1,ilev,ity,ity, &
                      1,ntrac)
        do n=1,ntrac
          flvt1n(:,ity,n)=xcrow(1,:,n)
        end do
        call w3dfld('rtraccld',flvt1n,ilev,nstpo,ntrac,1,ilev,ity,ity, &
                      1,ntrac)
        !
        !         * write instantaneous results at current time step to output.
        !
        ft1(ity)=vbcbrd(1)
        call w1dfld('loadbc',ft1,nstpo,ity,ity)
        ft1(ity)=voabrd(1)
        call w1dfld('loadoa',ft1,nstpo,ity,ity)
        ft1(ity)=vasbrd(1)
        call w1dfld('loadas',ft1,nstpo,ity,ity)
        ft1(ity)=vssbrd(1)
        call w1dfld('loadss',ft1,nstpo,ity,ity)
        ft1(ity)=vmdbrd(1)
        call w1dfld('loadmd',ft1,nstpo,ity,ity)
        ft1(ity)=vinn(1)
        call w1dfld('nloadint',ft1,nstpo,ity,ity)
        ft1(ity)=vssn(1)
        call w1dfld('nloadss',ft1,nstpo,ity,ity)
        ft1(ity)=vmdn(1)
        call w1dfld('nloadmd',ft1,nstpo,ity,ity)
        !
        flvt1(:,ity)=cbccnc(1,:)
        call w2dfld('concbc',flvt1,ilev,nstpo,1,ilev,ity,ity)
        flvt1(:,ity)=coacnc(1,:)
        call w2dfld('concoa',flvt1,ilev,nstpo,1,ilev,ity,ity)
        flvt1(:,ity)=cascnc(1,:)
        call w2dfld('concas',flvt1,ilev,nstpo,1,ilev,ity,ity)
        flvt1(:,ity)=csscnc(1,:)
        call w2dfld('concss',flvt1,ilev,nstpo,1,ilev,ity,ity)
        flvt1(:,ity)=cmdcnc(1,:)
        call w2dfld('concmd',flvt1,ilev,nstpo,1,ilev,ity,ity)
        flvt1(:,ity)=cinn(1,:)
        call w2dfld('nconcint',flvt1,ilev,nstpo,1,ilev,ity,ity)
        flvt1(:,ity)=cssn(1,:)
        call w2dfld('nconcss',flvt1,ilev,nstpo,1,ilev,ity,ity)
        flvt1(:,ity)=cmdn(1,:)
        call w2dfld('nconcmd',flvt1,ilev,nstpo,1,ilev,ity,ity)
        !
        if (kxtra1) then
          ft1(ity)=vccnrow(1)
          call w1dfld('loadcldnci',ft1,nstpo,ity,ity)
          flvt1(:,ity)=ccnrow(1,:)
          call w2dfld('cldnci',flvt1,ilev,nstpo,1,ilev,ity,ity)
        end if
        if (kxtra2) then
          flvt1s(:,ity,:)=sssnrow(1,:,:)
          call w3dfld('nsdss',flvt1s,ilev,nstpo,isdnum,1,ilev, &
                        ity,ity,1,isdnum)
          flvt1s(:,ity,:)=smdnrow(1,:,:)
          call w3dfld('nsdmd',flvt1s,ilev,nstpo,isdnum,1,ilev, &
                        ity,ity,1,isdnum)
          flvt1s(:,ity,:)=sianrow(1,:,:)
          call w3dfld('nsdint',flvt1s,ilev,nstpo,isdnum,1,ilev, &
                        ity,ity,1,isdnum)
          flvt1s(:,ity,:)=sdnurow(1,:,:)
          call w3dfld('nsd',flvt1s,ilev,nstpo,isdnum,1,ilev, &
                        ity,ity,1,isdnum)
          flvt1s(:,ity,:)=sdmarow(1,:,:)
          call w3dfld('msd',flvt1s,ilev,nstpo,isdnum,1,ilev, &
                        ity,ity,1,isdnum)
          flvt1s(:,ity,:)=sdcorow(1,:,:)
          call w3dfld('msdextdry',flvt1s,ilev,nstpo,isdnum,1,ilev, &
                        ity,ity,1,isdnum)
          flvt1s(:,ity,:)=sdacrow(1,:,:)
          call w3dfld('msdintdry',flvt1s,ilev,nstpo,isdnum,1,ilev, &
                        ity,ity,1,isdnum)
          flvt1s(:,ity,:)=sssmrow(1,:,:)
          call w3dfld('msdss',flvt1s,ilev,nstpo,isdnum,1,ilev, &
                        ity,ity,1,isdnum)
          flvt1s(:,ity,:)=smdmrow(1,:,:)
          call w3dfld('msdmd',flvt1s,ilev,nstpo,isdnum,1,ilev, &
                        ity,ity,1,isdnum)
          flvt1s(:,ity,:)=sewmrow(1,:,:)
          call w3dfld('msdwatext',flvt1s,ilev,nstpo,isdnum,1,ilev, &
                        ity,ity,1,isdnum)
          flvt1s(:,ity,:)=siwmrow(1,:,:)
          call w3dfld('msdwatint',flvt1s,ilev,nstpo,isdnum,1,ilev, &
                        ity,ity,1,isdnum)
          flvt1s(:,ity,:)=ssumrow(1,:,:)
          call w3dfld('msdas',flvt1s,ilev,nstpo,isdnum,1,ilev, &
                        ity,ity,1,isdnum)
          flvt1s(:,ity,:)=socmrow(1,:,:)
          call w3dfld('msdoa',flvt1s,ilev,nstpo,isdnum,1,ilev, &
                        ity,ity,1,isdnum)
          flvt1s(:,ity,:)=sbcmrow(1,:,:)
          call w3dfld('msdbc',flvt1s,ilev,nstpo,isdnum,1,ilev, &
                        ity,ity,1,isdnum)
        end if
        !
        ft1(ity)=dd4row(1)
        call w1dfld('dryso2',ft1,nstpo,ity,ity)
        ft1(ity)=dox4row(1)
        call w1dfld('oxso2oh',ft1,nstpo,ity,ity)
        ft1(ity)=doxdrow(1)
        call w1dfld('oxdmsoh',ft1,nstpo,ity,ity)
        ft1(ity)=noxdrow(1)
        call w1dfld('oxdmsno3',ft1,nstpo,ity,ity)
        ft1(ity)=aods(1,6)
        call w1dfld('od550aer',ft1,nstpo,ity,ity)
        ft1(ity)=abss(1,6)
        call w1dfld('abs550aer',ft1,nstpo,ity,ity)
        ft1(ity)=angstr(1,6)
        call w1dfld('angaer',ft1,nstpo,ity,ity)
        !
        !         * output of accumulated fields.
        !
        if (kxtra1) then
          !
          !           * use instantaneous instead of accumulated results
          !           * at first time step.
          !
          if (itx == 1) then
            vbcerow=vbcerow/saverad
            voaerow=voaerow/saverad
            vaserow=vaserow/saverad
            vsserow=vsserow/saverad
            vmderow=vmderow/saverad
            vbcwrow=vbcwrow/saverad
            voawrow=voawrow/saverad
            vaswrow=vaswrow/saverad
            vsswrow=vsswrow/saverad
            vmdwrow=vmdwrow/saverad
            vbcgrow=vbcgrow/saverad
            voagrow=voagrow/saverad
            vasgrow=vasgrow/saverad
            vssgrow=vssgrow/saverad
            vmdgrow=vmdgrow/saverad
            vbcdrow=vbcdrow/saverad
            voadrow=voadrow/saverad
            vasdrow=vasdrow/saverad
            vssdrow=vssdrow/saverad
            vmddrow=vmddrow/saverad
            !
            acasrow=acasrow/saverad
            acoarow=acoarow/saverad
            acbcrow=acbcrow/saverad
            acssrow=acssrow/saverad
            acmdrow=acmdrow/saverad
            ntrow=ntrow/saverad
            n20row=n20row/saverad
            n50row=n50row/saverad
            n100row=n100row/saverad
            n200row=n200row/saverad
            wtrow=wtrow/saverad
            w20row=w20row/saverad
            w50row=w50row/saverad
            w100row=w100row/saverad
            w200row=w200row/saverad
            cc02row=cc02row/saverad
            cc04row=cc04row/saverad
            cc08row=cc08row/saverad
            cc16row=cc16row/saverad
            ssunrow=ssunrow/saverad
            vasirow=vasirow/saverad
            vas1row=vas1row/saverad
            vas2row=vas2row/saverad
            vas3row=vas3row/saverad
          end if
          !
          ft1(ity)=vbcerow(1)
          call w1dfld('emibc',ft1,nstpo,ity,ity)
          ft1(ity)=voaerow(1)
          call w1dfld('emioa',ft1,nstpo,ity,ity)
          ft1(ity)=vaserow(1)
          call w1dfld('emias',ft1,nstpo,ity,ity)
          ft1(ity)=vsserow(1)
          call w1dfld('emiss',ft1,nstpo,ity,ity)
          ft1(ity)=vmderow(1)
          call w1dfld('emimd',ft1,nstpo,ity,ity)
          ft1(ity)=vbcwrow(1)
          call w1dfld('wetbcl',ft1,nstpo,ity,ity)
          ft1(ity)=voawrow(1)
          call w1dfld('wetoal',ft1,nstpo,ity,ity)
          ft1(ity)=vaswrow(1)
          call w1dfld('wetasl',ft1,nstpo,ity,ity)
          ft1(ity)=vsswrow(1)
          call w1dfld('wetssl',ft1,nstpo,ity,ity)
          ft1(ity)=vmdwrow(1)
          call w1dfld('wetmdl',ft1,nstpo,ity,ity)
          ft1(ity)=vbcgrow(1)
          call w1dfld('vgtpbc',ft1,nstpo,ity,ity)
          ft1(ity)=voagrow(1)
          call w1dfld('vgtpoa',ft1,nstpo,ity,ity)
          ft1(ity)=vasgrow(1)
          call w1dfld('vgtpas',ft1,nstpo,ity,ity)
          ft1(ity)=vssgrow(1)
          call w1dfld('vgtpss',ft1,nstpo,ity,ity)
          ft1(ity)=vmdgrow(1)
          call w1dfld('vgtpmd',ft1,nstpo,ity,ity)
          ft1(ity)=vbcdrow(1)
          call w1dfld('drybc',ft1,nstpo,ity,ity)
          ft1(ity)=voadrow(1)
          call w1dfld('dryoa',ft1,nstpo,ity,ity)
          ft1(ity)=vasdrow(1)
          call w1dfld('dryas',ft1,nstpo,ity,ity)
          ft1(ity)=vssdrow(1)
          call w1dfld('dryss',ft1,nstpo,ity,ity)
          ft1(ity)=vmddrow(1)
          call w1dfld('drymd',ft1,nstpo,ity,ity)
          ft1(ity)=vasirow(1)
          call w1dfld('incldasl',ft1,nstpo,ity,ity)
          ft1(ity)=vas1row(1)
          call w1dfld('wetasl1',ft1,nstpo,ity,ity)
          ft1(ity)=vas2row(1)
          call w1dfld('wetasl2',ft1,nstpo,ity,ity)
          ft1(ity)=vas3row(1)
          call w1dfld('wetasl3',ft1,nstpo,ity,ity)
          flvt1(:,ity)=acasrow(1,:)
          call w2dfld('concasd',flvt1,ilev,nstpo,1,ilev,ity,ity)
          flvt1(:,ity)=acoarow(1,:)
          call w2dfld('concoad',flvt1,ilev,nstpo,1,ilev,ity,ity)
          flvt1(:,ity)=acbcrow(1,:)
          call w2dfld('concbcd',flvt1,ilev,nstpo,1,ilev,ity,ity)
          flvt1(:,ity)=acssrow(1,:)
          call w2dfld('concssd',flvt1,ilev,nstpo,1,ilev,ity,ity)
          flvt1(:,ity)=acmdrow(1,:)
          call w2dfld('concmdd',flvt1,ilev,nstpo,1,ilev,ity,ity)
          flvt1(:,ity)=ntrow(1,:)
          call w2dfld('cn0',flvt1,ilev,nstpo,1,ilev,ity,ity)
          flvt1(:,ity)=n20row(1,:)
          call w2dfld('cn20',flvt1,ilev,nstpo,1,ilev,ity,ity)
          flvt1(:,ity)=n50row(1,:)
          call w2dfld('cn50',flvt1,ilev,nstpo,1,ilev,ity,ity)
          flvt1(:,ity)=n100row(1,:)
          call w2dfld('cn100',flvt1,ilev,nstpo,1,ilev,ity,ity)
          flvt1(:,ity)=n200row(1,:)
          call w2dfld('cn200',flvt1,ilev,nstpo,1,ilev,ity,ity)
          flvt1(:,ity)=wtrow(1,:)
          call w2dfld('wcn0',flvt1,ilev,nstpo,1,ilev,ity,ity)
          flvt1(:,ity)=w20row(1,:)
          call w2dfld('wcn20',flvt1,ilev,nstpo,1,ilev,ity,ity)
          flvt1(:,ity)=w50row(1,:)
          call w2dfld('wcn50',flvt1,ilev,nstpo,1,ilev,ity,ity)
          flvt1(:,ity)=w100row(1,:)
          call w2dfld('wcn100',flvt1,ilev,nstpo,1,ilev,ity,ity)
          flvt1(:,ity)=w200row(1,:)
          call w2dfld('wcn200',flvt1,ilev,nstpo,1,ilev,ity,ity)
          flvt1(:,ity)=cc02row(1,:)
          call w2dfld('ccnc02',flvt1,ilev,nstpo,1,ilev,ity,ity)
          flvt1(:,ity)=cc04row(1,:)
          call w2dfld('ccnc04',flvt1,ilev,nstpo,1,ilev,ity,ity)
          flvt1(:,ity)=cc08row(1,:)
          call w2dfld('ccnc08',flvt1,ilev,nstpo,1,ilev,ity,ity)
          flvt1(:,ity)=cc16row(1,:)
          call w2dfld('ccnc16',flvt1,ilev,nstpo,1,ilev,ity,ity)
          flvt1(:,ity)=scndrow(1,:)
          call w2dfld('gtpcndas',flvt1,ilev,nstpo,1,ilev,ity,ity)
          flvt1(:,ity)=sncnrow(1,:)
          call w2dfld('gtpnclan',flvt1,ilev,nstpo,1,ilev,ity,ity)
          flvt1(:,ity)=ssunrow(1,:)
          call w2dfld('gtpnclas',flvt1,ilev,nstpo,1,ilev,ity,ity)
          flvt1(:,ity)=sgsprow(1,:)
          call w2dfld('gppsa',flvt1,ilev,nstpo,1,ilev,ity,ity)
          flvt1(:,ity)=rcrirow(1,:)
          call w2dfld('rcrit',flvt1,ilev,nstpo,1,ilev,ity,ity)
          flvt1(:,ity)=supsrow(1,:)
          call w2dfld('supmax',flvt1,ilev,nstpo,1,ilev,ity,ity)
          flvt1(:,ity)=henrrow(1,:)
          call w2dfld('henr',flvt1,ilev,nstpo,1,ilev,ity,ity)
          flvt1(:,ity)=o3frrow(1,:)
          call w2dfld('fo3',flvt1,ilev,nstpo,1,ilev,ity,ity)
          flvt1(:,ity)=h2o2frrow(1,:)
          call w2dfld('fhp',flvt1,ilev,nstpo,1,ilev,ity,ity)
        end if
        !
        !         * reset accumulated fields.
        !
        if (kxtra1) then
          vbcerow=0.
          voaerow=0.
          vaserow=0.
          vsserow=0.
          vmderow=0.
          vbcwrow=0.
          voawrow=0.
          vaswrow=0.
          vsswrow=0.
          vmdwrow=0.
          vbcdrow=0.
          voadrow=0.
          vasdrow=0.
          vssdrow=0.
          vmddrow=0.
          vbcgrow=0.
          voagrow=0.
          vasgrow=0.
          vssgrow=0.
          vmdgrow=0.
          ssunrow=0.
          vasnrow=0.
          vncnrow=0.
          vscdrow=0.
          vgsprow=0.
          sncnrow=0.
          scndrow=0.
          sgsprow=0.
          acasrow=0.
          acoarow=0.
          acbcrow=0.
          acssrow=0.
          acmdrow=0.
          ntrow=0.
          n20row=0.
          n50row=0.
          n100row=0.
          n200row=0.
          wtrow=0.
          w20row=0.
          w50row=0.
          w100row=0.
          w200row=0.
          cc02row=0.
          cc04row=0.
          cc08row=0.
          cc16row=0.
          vasirow=0.
          vas1row=0.
          vas2row=0.
          vas3row=0.
        end if
        if (kxtra2) then
          cornrow=0.
          cormrow=0.
          rsn1row=0.
          rsm1row=0.
          rsn2row=0.
          rsm2row=0.
          rsn3row=0.
          rsm3row=0.
          vrn1row=0.
          vrm1row=0.
          vrn2row=0.
          vrm2row=0.
          vrn3row=0.
          vrm3row=0.
          sdvlrow=0.
          defxrow=0.
          defnrow=0.
          dmacrow=0.
          dmcorow=0.
          dnacrow=0.
          dncorow=0.
          defarow=0.
          defcrow=0.
        end if
        if (isvdust > 0) then
          duwdrow=0.
          dustrow=0.
          duthrow=0.
          fallrow=0.
          fa10row=0.
          fa2row =0.
          fa1row =0.
          usmkrow=0.
        end if
        !
        !< <<< * AEROSOL * <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
      end if
    end do
    !----------------------------------------------------------------------
    !     * copy final results of pam simulation. these fields will
    !     * be needed for model restarts.
    !
    write(6,'(A26)') 'WRITING OF MODEL OUTPUT...'
    !
    !     * write initial prognostic fields to restart file.
    !
    call w0dfldrs('tstep',real(0))
    call w0dfldrs('ustar',ustar(1))
    call w1dfldrs('ua',urow(1,:),ilev)
    call w1dfldrs('ta',throw(1,:),ilev)
    call w1dfldrs('rv',qrow(1,:),ilev)
    call w1dfldrs('rl',qlwc(1,:),ilev)
    call w1dfldrs('clf',zclf(1,:),ilev)
    call w1dfldrs('cldnc',zcdnrow(1,:),ilev)
    call w1dfldrs('ri',ri(1,:),ilev)
    call w0dfldrs('indpbl',pbltrow(1))
    !
    !> >>> * AEROSOL * >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    !
    call w2dfldrs('rtrac',xrow(1,:,:),ilev,ntrac)
    call w2dfldrs('strac',sfrcrol(1,:,:),ilev,ntrac)
    !
    !     * pla work fields.
    !
    call w1dfldrs('oidn',oidnrow(1,:),ilev)
    call w1dfldrs('oirc',oircrow(1,:),ilev)
    call w1dfldrs('svvb',svvbrow(1,:),ilev)
    call w1dfldrs('psvv',psvvrow(1,:),ilev)
    call w1dfldrs('qgvb',qgvbrow(1,:),ilev)
    call w1dfldrs('qdvb',qdvbrow(1,:),ilev)
    call w1dfldrs('divb',divbrow(1,:),ilev)
    call w1dfldrs('pdiv',pdivrow(1,:),ilev)
    call w1dfldrs('rivb',rivbrow(1,:),ilev)
    call w1dfldrs('priv',privrow(1,:),ilev)
    call w2dfldrs('oedn',oednrow(1,:,:),ilev,kextt)
    call w2dfldrs('oerc',oercrow(1,:,:),ilev,kextt)
    call w2dfldrs('devb',devbrow(1,:,:),ilev,kextt)
    call w2dfldrs('pdev',pdevrow(1,:,:),ilev,kextt)
    call w2dfldrs('revb',revbrow(1,:,:),ilev,kextt)
    call w2dfldrs('prev',prevrow(1,:,:),ilev,kextt)
    call w2dfldrs('qnvb',qnvbrow(1,:,:),ilev,isaintt)
    call w2dfldrs('svmb',svmbrow(1,:,:),ilev,nrmfld)
    call w2dfldrs('svcb',svcbrow(1,:,:),ilev,nrmfld)
    call w2dfldrs('qgmb',qgmbrow(1,:,:),ilev,nrmfld)
    call w2dfldrs('qgcb',qgcbrow(1,:,:),ilev,nrmfld)
    call w2dfldrs('qdmb',qdmbrow(1,:,:),ilev,nrmfld)
    call w2dfldrs('qdcb',qdcbrow(1,:,:),ilev,nrmfld)
    call w2dfldrs('dimb',dimbrow(1,:,:),ilev,nrmfld)
    call w2dfldrs('dicb',dicbrow(1,:,:),ilev,nrmfld)
    call w2dfldrs('rimb',rimbrow(1,:,:),ilev,nrmfld)
    call w2dfldrs('ricb',ricbrow(1,:,:),ilev,nrmfld)
    call w3dfldrs('qsvb',qsvbrow(1,:,:,:),ilev,isaintt,kintt)
    call w3dfldrs('qnmb',qnmbrow(1,:,:,:),ilev,isaintt,nrmfld)
    call w3dfldrs('qncb',qncbrow(1,:,:,:),ilev,isaintt,nrmfld)
    call w3dfldrs('demb',dembrow(1,:,:,:),ilev,kextt,nrmfld)
    call w3dfldrs('decb',decbrow(1,:,:,:),ilev,kextt,nrmfld)
    call w3dfldrs('remb',rembrow(1,:,:,:),ilev,kextt,nrmfld)
    call w3dfldrs('recb',recbrow(1,:,:,:),ilev,kextt,nrmfld)
    call w4dfldrs('qsmb',qsmbrow(1,:,:,:,:),ilev,isaintt,kintt,nrmfld)
    call w4dfldrs('qscb',qscbrow(1,:,:,:,:),ilev,isaintt,kintt,nrmfld)
    !
    !     * pla extra diagnostic fields.
    !
    if (kxtra1) then
      call w0dfldrs('emibc',vbcerow(1))
      call w0dfldrs('emioa',voaerow(1))
      call w0dfldrs('emias',vaserow(1))
      call w0dfldrs('emiss',vsserow(1))
      call w0dfldrs('emimd',vmderow(1))
      call w0dfldrs('wetbcl',vbcwrow(1))
      call w0dfldrs('wetoal',voawrow(1))
      call w0dfldrs('wetasl',vaswrow(1))
      call w0dfldrs('wetssl',vsswrow(1))
      call w0dfldrs('wetmdl',vmdwrow(1))
      call w0dfldrs('drybc',vbcdrow(1))
      call w0dfldrs('dryoa',voadrow(1))
      call w0dfldrs('dryas',vasdrow(1))
      call w0dfldrs('dryss',vssdrow(1))
      call w0dfldrs('drymd',vmddrow(1))
      call w0dfldrs('vgtpbc',vbcgrow(1))
      call w0dfldrs('vgtpoa',voagrow(1))
      call w0dfldrs('vgtpas',vasgrow(1))
      call w0dfldrs('vgtpss',vssgrow(1))
      call w0dfldrs('vgtpmd',vmdgrow(1))
      call w0dfldrs('wetasl1',vas1row(1))
      call w0dfldrs('wetasl2',vas2row(1))
      call w0dfldrs('wetasl3',vas3row(1))
      call w0dfldrs('incldasl',vasirow(1))
      call w0dfldrs('vnuclas',vasnrow(1))
      call w0dfldrs('vnucln',vncnrow(1))
      call w0dfldrs('vcondas',vscdrow(1))
      call w0dfldrs('vgppsa',vgsprow(1))
      call w0dfldrs('emimdacc',defarow(1))
      call w0dfldrs('emimdcoa',defcrow(1))
      call w1dfldrs('nuclas',ssunrow(1,:),ilev)
      call w1dfldrs('nucln',sncnrow(1,:),ilev)
      call w1dfldrs('condas',scndrow(1,:),ilev)
      call w1dfldrs('gppsa',sgsprow(1,:),ilev)
      call w1dfldrs('concasd',acasrow(1,:),ilev)
      call w1dfldrs('concoad',acoarow(1,:),ilev)
      call w1dfldrs('concbcd',acbcrow(1,:),ilev)
      call w1dfldrs('concssd',acssrow(1,:),ilev)
      call w1dfldrs('concmdd',acmdrow(1,:),ilev)
      call w1dfldrs('cn0',ntrow(1,:),ilev)
      call w1dfldrs('cn20',n20row(1,:),ilev)
      call w1dfldrs('cn50',n50row(1,:),ilev)
      call w1dfldrs('cn100',n100row(1,:),ilev)
      call w1dfldrs('cn200',n200row(1,:),ilev)
      call w1dfldrs('wcn0',wtrow(1,:),ilev)
      call w1dfldrs('wcn20',w20row(1,:),ilev)
      call w1dfldrs('wcn50',w50row(1,:),ilev)
      call w1dfldrs('wcn100',w100row(1,:),ilev)
      call w1dfldrs('wcn200',w200row(1,:),ilev)
      call w1dfldrs('ccnc02',cc02row(1,:),ilev)
      call w1dfldrs('ccnc04',cc04row(1,:),ilev)
      call w1dfldrs('ccnc08',cc08row(1,:),ilev)
      call w1dfldrs('ccnc16',cc16row(1,:),ilev)
    end if
    if (kxtra2) then
      call w0dfldrs('vresn1',vrn1row(1))
      call w0dfldrs('vresn2',vrn2row(1))
      call w0dfldrs('vresn3',vrn3row(1))
      call w0dfldrs('vresm1',vrm1row(1))
      call w0dfldrs('vresm2',vrm2row(1))
      call w0dfldrs('vresm3',vrm3row(1))
      call w1dfldrs('resn',cornrow(1,:),ilev)
      call w1dfldrs('resm',cormrow(1,:),ilev)
      call w1dfldrs('resn1',rsn1row(1,:),ilev)
      call w1dfldrs('resn2',rsn2row(1,:),ilev)
      call w1dfldrs('resn3',rsn3row(1,:),ilev)
      call w1dfldrs('resm1',rsm1row(1,:),ilev)
      call w1dfldrs('resm2',rsm2row(1,:),ilev)
      call w1dfldrs('resm3',rsm3row(1,:),ilev)
      call w1dfldrs('vsdvl',sdvlrow(1,:),isdiag)
      call w1dfldrs('defxmd',defxrow(1,:),isdiag)
      call w1dfldrs('defnmd',defnrow(1,:),isdiag)
      call w1dfldrs('diagmd1',dmacrow(1,:),isdust)
      call w1dfldrs('diagmd2',dmcorow(1,:),isdust)
      call w1dfldrs('diagmdn1',dnacrow(1,:),isdust)
      call w1dfldrs('diagmdn2',dncorow(1,:),isdust)
    end if
    if (isvdust > 0) then
      call w0dfldrs('umd',duwdrow(1))
      call w0dfldrs('ustarmd',dustrow(1))
      call w0dfldrs('ustarmdthr',duthrow(1))
      call w0dfldrs('fallmd',fallrow(1))
      call w0dfldrs('fa10md',fa10row(1))
      call w0dfldrs('fa2md',fa2row(1))
      call w0dfldrs('fa1md',fa1row(1))
      call w0dfldrs('ustarmdthtfr',usmkrow(1))
    end if
    !
    !< <<< * AEROSOL * <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
    !
    !-----------------------------------------------------------------------
    !     * write restart information to output file.
    !
    cfilr=trim(cfile)//'_tfin'
    call wncdatrs(cfilr)
    !
    !-----------------------------------------------------------------------
    !     * write pam time-dependent diagnostic results to output file.
    !
    cfilb=trim(cfile)//'_out'
    call wncdat(cfilb)
    !
  end program driver_pam
