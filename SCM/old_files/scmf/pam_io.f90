      MODULE RUNINFO
!-----------------------------------------------------------------------
!     PURPOSE:
!     --------
!     BASIC INFORMATION ABOUT EXPERIMENT AND AIR PROPERIES.
!
!     HISTORY:
!     --------
!     * SEP 19/2008 - K.VONSALZEN   ADD MUTLI LEVEL INFO
!     * AUG 11/2006 - K.VONSALZEN   NEW.
!
!-----------------------------------------------------------------------
!
      IMPLICIT REAL (A-H,O-Z), INTEGER (I-N)
!
!-----------------------------------------------------------------------
!     * GENERAL MODEL INFORMATION (NAME, LEVELS, TIME STEP).
!
      INTEGER, PARAMETER :: IMAXL=50
      TYPE MPRP
        CHARACTER(LEN=IMAXL) :: RUNNAME
        REAL :: NUGALT, NUGVD, TAUX, TAUT, TAUQ, TAUM
        REAL :: CDN, CDNN, ZREF
        INTEGER :: PLTFQ, TRAX, TRAT, TRAQ, TRUP, CASE
      END TYPE MPRP
      TYPE(MPRP) :: MODL
      NAMELIST /MODNML/ MODL
!
      END MODULE RUNINFO

#ifndef GM_PAM
!       MODULE SDPHYS
! !-----------------------------------------------------------------------
! !     PURPOSE:
! !     --------
! !     BASIC PHYISCAL CONSTANTS.
! !
! !     HISTORY:
! !     --------
! !     * AUG 10/2006 - K.VONSALZEN   NEW.
! !
! !-----------------------------------------------------------------------
! !
!       IMPLICIT REAL (A-H,O-Z), INTEGER (I-N)
! !
! !     * BASIC PHYSICAL CONSTANTS.
! !
!       REAL, PARAMETER :: RHOH2O = 1.E+03         ! DENSITY OF WATER (KG/M3)
!       REAL, PARAMETER :: WH2O   = 18.015E-03     ! MOLECULAR WEIGHT OF H2O (KG/MOL)
!       REAL, PARAMETER :: WA     = 28.97E-03      ! MOLECULAR WEIGHT OF AIR (KG/MOL)
!       REAL, PARAMETER :: RGASM  = 8.31441        ! MOLAR GAS CONSTANT (J/MOL/K)
!       REAL, PARAMETER :: RGAS   = 287.04         ! GAS CONSTANT OF DRY AIR (J/KG/K)
!       REAL, PARAMETER :: RGOCP  = 2./7.
!       REAL, PARAMETER :: CPRES  = RGAS/RGOCP     ! HEAT CAPACITY AT CONSTANT PRESSURE
!                                                  ! FOR DRY AIR (J/KG/K)
!       REAL, PARAMETER :: EPS1   = 0.622          ! RATIO OF GAS CONSTANT FOR DRY
!                                                  ! AIR OVER GAS CONSTANT FOR VAPOUR
!       REAL, PARAMETER :: GRAV   = 9.80616        ! GRAVITATIONAL CONTSTANT (M/S2)
!       REAL, PARAMETER :: RL     = 2.501E+06      ! LATENT HEAT OF VAPOURIZATION (J/KG)
! !
! !     * FITTING PARAMETERS FOR CALCULATION OF WATER VAPOUR SATURATION
! !     * MIXING RATIO.
! !
!       REAL, PARAMETER :: RW1    = 53.67957
!       REAL, PARAMETER :: RW2    = -6743.769
!       REAL, PARAMETER :: RW3    = -4.8451
!       REAL, PARAMETER :: RI1    = 23.33086
!       REAL, PARAMETER :: RI2    = -6111.72784
!       REAL, PARAMETER :: RI3    = 0.15215
! !
!       REAL, PARAMETER :: PPA    = 21.656
!       REAL, PARAMETER :: PPB    = 5418.
! !
!       END MODULE SDPHYS


      MODULE IODAT
!
      USE FPDEF
!
      IMPLICIT REAL (A-H,O-Z),INTEGER (I-N)
!
      INTEGER, PARAMETER :: IMAXL=100  ! MAXIMUM LENGTH OF VARIABLE LONG NAMES
      INTEGER, PARAMETER :: ITOT=1000  ! MAXIMUM NUMBER OF FIELDS PER FILE
      INTEGER, PARAMETER :: ISMX=20    ! MAXIMUM NUMBER OF DIMENSIONS FOR IO FIELDS
!
      INTEGER, PARAMETER :: INAX=-9
      REAL(R4), PARAMETER :: YNAX=-9999., YSMALLX=1.E-20
      INTEGER, DIMENSION(ISMX) :: IDIMA
      CHARACTER(LEN=IMAXL) :: CNA, CTMP
      TYPE MODPRPV
        REAL, ALLOCATABLE, DIMENSION(:,:,:,:) :: FLD4D
        REAL, ALLOCATABLE, DIMENSION(:,:,:) :: FLD3D
        REAL, ALLOCATABLE, DIMENSION(:,:) :: FLD2D
        REAL, ALLOCATABLE, DIMENSION(:) :: FLD1D
        REAL :: FLD0D
      END TYPE MODPRPV
      TYPE(MODPRPV), DIMENSION(ITOT) :: IVARV,OVARV,RVARV
      TYPE MODPRP
        CHARACTER(LEN=IMAXL) :: NAME,LNAME,UNITS
        CHARACTER(LEN=IMAXL), DIMENSION(ISMX) :: DIM
      END TYPE MODPRP
      TYPE(MODPRP), DIMENSION(ITOT) :: IVAR,OVAR,RVAR
      NAMELIST /IVARNML/ IVAR
      NAMELIST /OVARNML/ OVAR
      NAMELIST /RVARNML/ RVAR
!
      INTEGER :: NDIM
      TYPE DIMPRP
        CHARACTER(LEN=IMAXL) :: NAME,LNAME,UNITS
        INTEGER :: SIZE,FLGI,FLGO,FLGRS,FLGX
        REAL, ALLOCATABLE, DIMENSION(:) :: VAL
      END TYPE DIMPRP
      TYPE(DIMPRP), ALLOCATABLE, DIMENSION(:) :: DIM,DIMT
!
      END MODULE IODAT


      MODULE IODATIF
!
      INTERFACE
        SUBROUTINE W1DDIM(CNAME,AVAR,IDIM1,J1,J2)
        CHARACTER(LEN=*), INTENT(IN) :: CNAME
        REAL, INTENT(OUT), DIMENSION(IDIM1) :: AVAR
        INTEGER, INTENT(IN) :: IDIM1
        INTEGER, OPTIONAL :: J1,J2
        END SUBROUTINE W1DDIM
!
        SUBROUTINE W1DFLD(CNAME,AVAR,IDIM1,J1,J2)
        CHARACTER(LEN=*), INTENT(IN) :: CNAME
        REAL, INTENT(IN), DIMENSION(IDIM1) :: AVAR
        INTEGER, INTENT(IN) :: IDIM1
        INTEGER, OPTIONAL :: J1,J2
        END SUBROUTINE W1DFLD
!
        SUBROUTINE W2DFLD(CNAME,AVAR,IDIM1,IDIM2,J1,J2,K1,K2)
        CHARACTER(LEN=*), INTENT(IN) :: CNAME
        REAL, INTENT(IN), DIMENSION(IDIM1,IDIM2) :: AVAR
        INTEGER, INTENT(IN) :: IDIM1,IDIM2
        INTEGER, OPTIONAL :: J1,J2,K1,K2
        END SUBROUTINE W2DFLD
!
        SUBROUTINE W3DFLD(CNAME,AVAR,IDIM1,IDIM2,IDIM3, &
                          J1,J2,K1,K2,L1,L2)
        CHARACTER(LEN=*), INTENT(IN) :: CNAME
        REAL, INTENT(IN), DIMENSION(IDIM1,IDIM2,IDIM3) :: AVAR
        INTEGER, INTENT(IN) :: IDIM1,IDIM2,IDIM3
        INTEGER, OPTIONAL :: J1,J2,K1,K2,L1,L2
        END SUBROUTINE W3DFLD
!
        SUBROUTINE W4DFLD(CNAME,AVAR,IDIM1,IDIM2,IDIM3,IDIM4, &
                          J1,J2,K1,K2,L1,L2,M1,M2)
        CHARACTER(LEN=*), INTENT(IN) :: CNAME
        REAL, INTENT(IN), DIMENSION(IDIM1,IDIM2,IDIM3,IDIM4) :: AVAR
        INTEGER, INTENT(IN) :: IDIM1,IDIM2,IDIM3,IDIM4
        INTEGER, OPTIONAL :: J1,J2,K1,K2,L1,L2,M1,M2
        END SUBROUTINE W4DFLD
!
        SUBROUTINE W1DFLDRS(CNAME,AVAR,IDIM1,J1,J2)
        CHARACTER(LEN=*), INTENT(IN) :: CNAME
        REAL, INTENT(IN), DIMENSION(IDIM1) :: AVAR
        INTEGER, INTENT(IN) :: IDIM1
        INTEGER, OPTIONAL :: J1,J2
        END SUBROUTINE W1DFLDRS
!
        SUBROUTINE W2DFLDRS(CNAME,AVAR,IDIM1,IDIM2,J1,J2,K1,K2)
        CHARACTER(LEN=*), INTENT(IN) :: CNAME
        REAL, INTENT(IN), DIMENSION(IDIM1,IDIM2) :: AVAR
        INTEGER, INTENT(IN) :: IDIM1,IDIM2
        INTEGER, OPTIONAL :: J1,J2,K1,K2
        END SUBROUTINE W2DFLDRS
!
        SUBROUTINE W3DFLDRS(CNAME,AVAR,IDIM1,IDIM2,IDIM3, &
                            J1,J2,K1,K2,L1,L2)
        CHARACTER(LEN=*), INTENT(IN) :: CNAME
        REAL, INTENT(IN), DIMENSION(IDIM1,IDIM2,IDIM3) :: AVAR
        INTEGER, INTENT(IN) :: IDIM1,IDIM2,IDIM3
        INTEGER, OPTIONAL :: J1,J2,K1,K2,L1,L2
        END SUBROUTINE W3DFLDRS
!
        SUBROUTINE W4DFLDRS(CNAME,AVAR,IDIM1,IDIM2,IDIM3,IDIM4, &
                            J1,J2,K1,K2,L1,L2,M1,M2)
        CHARACTER(LEN=*), INTENT(IN) :: CNAME
        REAL, INTENT(IN), DIMENSION(IDIM1,IDIM2,IDIM3,IDIM4) :: AVAR
        INTEGER, INTENT(IN) :: IDIM1,IDIM2,IDIM3,IDIM4
        INTEGER, OPTIONAL :: J1,J2,K1,K2,L1,L2,M1,M2
        END SUBROUTINE W4DFLDRS
!
        SUBROUTINE R1DFLD(CNAME,AVAR,IDIM1,J1,J2)
        CHARACTER(LEN=*), INTENT(IN) :: CNAME
        REAL, INTENT(IN), DIMENSION(IDIM1) :: AVAR
        INTEGER, INTENT(IN) :: IDIM1
        INTEGER, OPTIONAL :: J1,J2
        END SUBROUTINE R1DFLD
!
        SUBROUTINE R2DFLD(CNAME,AVAR,IDIM1,IDIM2,J1,J2,K1,K2)
        CHARACTER(LEN=*), INTENT(IN) :: CNAME
        REAL, INTENT(IN), DIMENSION(IDIM1,IDIM2) :: AVAR
        INTEGER, INTENT(IN) :: IDIM1,IDIM2
        INTEGER, OPTIONAL :: J1,J2,K1,K2
        END SUBROUTINE R2DFLD
!
        SUBROUTINE R3DFLD(CNAME,AVAR,IDIM1,IDIM2,IDIM3, &
                          J1,J2,K1,K2,L1,L2)
        CHARACTER(LEN=*), INTENT(IN) :: CNAME
        REAL, INTENT(IN), DIMENSION(IDIM1,IDIM2,IDIM3) :: AVAR
        INTEGER, INTENT(IN) :: IDIM1,IDIM2,IDIM3
        INTEGER, OPTIONAL :: J1,J2,K1,K2,L1,L2
        END SUBROUTINE R3DFLD
!
        SUBROUTINE R4DFLD(CNAME,AVAR,IDIM1,IDIM2,IDIM3,IDIM4, &
                          J1,J2,K1,K2,L1,L2,M1,M2)
        CHARACTER(LEN=*), INTENT(IN) :: CNAME
        REAL, INTENT(IN), DIMENSION(IDIM1,IDIM2,IDIM3,IDIM4) :: AVAR
        INTEGER, INTENT(IN) :: IDIM1,IDIM2,IDIM3,IDIM4
        INTEGER, OPTIONAL :: J1,J2,K1,K2,L1,L2,M1,M2
        END SUBROUTINE R4DFLD
!
        SUBROUTINE R1DFLDRS(CNAME,AVAR,IDIM1,J1,J2)
        CHARACTER(LEN=*), INTENT(IN) :: CNAME
        REAL, INTENT(IN), DIMENSION(IDIM1) :: AVAR
        INTEGER, INTENT(IN) :: IDIM1
        INTEGER, OPTIONAL :: J1,J2
        END SUBROUTINE R1DFLDRS
!
        SUBROUTINE R2DFLDRS(CNAME,AVAR,IDIM1,IDIM2,J1,J2,K1,K2)
        CHARACTER(LEN=*), INTENT(IN) :: CNAME
        REAL, INTENT(IN), DIMENSION(IDIM1,IDIM2) :: AVAR
        INTEGER, INTENT(IN) :: IDIM1,IDIM2
        INTEGER, OPTIONAL :: J1,J2,K1,K2
        END SUBROUTINE R2DFLDRS
!
        SUBROUTINE R3DFLDRS(CNAME,AVAR,IDIM1,IDIM2,IDIM3, &
                            J1,J2,K1,K2,L1,L2)
        CHARACTER(LEN=*), INTENT(IN) :: CNAME
        REAL, INTENT(IN), DIMENSION(IDIM1,IDIM2,IDIM3) :: AVAR
        INTEGER, INTENT(IN) :: IDIM1,IDIM2,IDIM3
        INTEGER, OPTIONAL :: J1,J2,K1,K2,L1,L2
        END SUBROUTINE R3DFLDRS
!
        SUBROUTINE R4DFLDRS(CNAME,AVAR,IDIM1,IDIM2,IDIM3,IDIM4, &
                            J1,J2,K1,K2,L1,L2,M1,M2)
        CHARACTER(LEN=*), INTENT(IN) :: CNAME
        REAL, INTENT(IN), DIMENSION(IDIM1,IDIM2,IDIM3,IDIM4) :: AVAR
        INTEGER, INTENT(IN) :: IDIM1,IDIM2,IDIM3,IDIM4
        INTEGER, OPTIONAL :: J1,J2,K1,K2,L1,L2,M1,M2
        END SUBROUTINE R4DFLDRS
      END INTERFACE
!
      END MODULE IODATIF


      SUBROUTINE RDNCDIMA(FILE1,FILE2,ILEV,ILEV1,ITIME,IRDDR,NTRAC, KEXTT,KINTT,ISAINTT,NRMFLD,ISDUST,KTIME1)
!
      USE IODAT
      USE NETCDF
!
      IMPLICIT NONE
!
      CHARACTER(LEN=*), INTENT(IN) :: FILE1,FILE2
      INTEGER, INTENT(OUT) :: ILEV,ILEV1,ITIME,IRDDR,NTRAC,KEXTT, &
                              KINTT,ISAINTT,NRMFLD,ISDUST
      LOGICAL, INTENT(IN) :: KTIME1
      INTEGER, PARAMETER :: NFIL=2
      INTEGER, DIMENSION(NFIL) :: ncid
      INTEGER :: status,ND,NF,I
      INTEGER, DIMENSION(:), ALLOCATABLE :: lvl_dimid

      print *, 'RDNCDIMA. Open files:', trim(file1), '   ', trim(file2)
!
!     * INITIALIZATION OF FIELD DIMENSIONS.
!
      ILEV=INAX
      ILEV1=INAX
      ITIME=INAX
      IRDDR=INAX
      NTRAC=INAX
      KEXTT=INAX
      KINTT=INAX
      ISAINTT=INAX
      NRMFLD=INAX
      ISDUST=INAX
!
!     * OPEN NETCDF FILES WITH INPUT AND RESTART DATA.
!
      status=nf90_open(TRIM(FILE1)//'.nc', nf90_nowrite, ncid(1))
      IF (status /= nf90_NoErr) CALL XIT('RDNCDIM',-1)
      status=nf90_open(TRIM(FILE2)//'.nc', nf90_nowrite, ncid(2))
      IF (status /= nf90_NoErr) CALL XIT('RDNCDIM',-2)
!
!     * DEFINE 10 NETCDF DIMENSION NAMES. AN ADDITIONAL TIME DIMENSION
!     * MAY BE GENERATED TO SUPPORT OUTPUT FIELDS, IF NECESSARY.
!
      NDIM=10
      IF ( KTIME1 ) NDIM=NDIM+1
!
!     * INITIALIZATION.
!
      ALLOCATE(DIM(NDIM))
      DO ND=1,NDIM
      DO I=1,IMAXL
        DIM(ND)%NAME(I:I)=' '
      ENDDO
      ENDDO
!
!     * DIMENSION NAMES.
!
      DIM(1 )%NAME(1:10)='level     '
      DIM(2 )%NAME(1:10)='level1    '
      DIM(3 )%NAME(1:10)='time      '
      DIM(4 )%NAME(1:10)='raddry    '
      DIM(5 )%NAME(1:10)='ntrac     '
      DIM(6 )%NAME(1:10)='kextt     '
      DIM(7 )%NAME(1:10)='kintt     '
      DIM(8 )%NAME(1:10)='isaintt   '
      DIM(9 )%NAME(1:10)='nrmfld    '
      DIM(10)%NAME(1:10)='isdust    '
!
!     * EXTRA TIME DIMENSION FOR OUTPUT.
!
      IF ( KTIME1 ) DIM(NDIM)%NAME(1:10)='time1     '
!
!     * INITIALIZATIONS.
!
      ALLOCATE(lvl_dimid(NDIM))
      DIM(:)%FLGI=0
      DIM(:)%FLGO=0
      DIM(:)%FLGRS=0
      DIM(:)%FLGX=0
!
!     * READ THE DIMENSIONS AND COORDINATES FROM THE FILES.
!
      DO NF=1,NFIL
      DO ND=1,NDIM
        IF ( DIM(ND)%FLGX==0 ) THEN
          status=nf90_inq_dimid(ncid(NF), TRIM(DIM(ND)%NAME), lvl_dimid(ND))
          IF (status == nf90_NoErr) THEN
            DIM(ND)%FLGX=1
            status=nf90_inquire_dimension(ncid(NF), lvl_dimid(ND), len=DIM(ND)%SIZE)
            status=nf90_get_att  (ncid(NF), lvl_dimid(ND), "units", DIM(ND)%UNITS )
            status=nf90_get_att  (ncid(NF), lvl_dimid(ND), "long_name", DIM(ND)%LNAME )
            status=nf90_inq_varid(ncid(NF), TRIM(DIM(ND)%NAME), lvl_dimid(ND))
            ALLOCATE(DIM(ND)%VAL(DIM(ND)%SIZE))
            status=nf90_get_var  (ncid(NF), lvl_dimid(ND), DIM(ND)%VAL)
!
!           * ASSOCIATE DIMENSIONS IN MODEL WITH NETCDF DIMENSIONS.
!
            SELECT CASE (ND)
              CASE (1)
                ILEV =DIM(ND)%SIZE
              CASE (2)
                ILEV1=DIM(ND)%SIZE
              CASE (3)
                ITIME=DIM(ND)%SIZE
              CASE (4)
                IRDDR=DIM(ND)%SIZE
              CASE (5)
                NTRAC=DIM(ND)%SIZE
              CASE (6)
                KEXTT=DIM(ND)%SIZE
              CASE (7)
                KINTT=DIM(ND)%SIZE
              CASE (8)
                ISAINTT=DIM(ND)%SIZE
              CASE (9)
                NRMFLD=DIM(ND)%SIZE
              CASE (10)
                ISDUST=DIM(ND)%SIZE
            END SELECT
            print '(a,i3,a,a8,a,i3)', 'RDNCDIMA. nd:', nd, '  dimname:',DIM(nd)%NAME,'  dimsize:', DIM(ND)%SIZE
          ENDIF
        ENDIF
      ENDDO
      ENDDO
      DO ND=1,NDIM
        IF ( DIM(ND)%FLGX==0 ) THEN
          PRINT *,'COULD NOT FIND DIMENSION ', TRIM(DIM(ND)%NAME), ' IN INPUT FILES'
        ENDIF
      ENDDO
!
      END SUBROUTINE RDNCDIMA


      SUBROUTINE SETNCDIM(NSTPO,IDIM)
!
      USE IODAT
!
      IMPLICIT REAL (A-H,O-Z),INTEGER (I-N)
!
!     * DEFINE NETCDF DIMENSIONS AND ASSOCIATE WITH DIMENSIONS
!     * IN THE MODEL.
!
      PRINT*,'SETTING DIMENSION ',TRIM(DIM(IDIM)%NAME)
      DIM(IDIM)%LNAME(1:12)='Output time '
      DIM(IDIM)%UNITS(1:12)='s           '
      DIM(IDIM)%SIZE=NSTPO
      ALLOCATE(DIM(IDIM)%VAL(DIM(IDIM)%SIZE))
!
      END SUBROUTINE SETNCDIM


      SUBROUTINE INITIO(KTIME1)
!
      USE IODAT
!
      IMPLICIT REAL (A-H,O-Z),INTEGER (I-N)
!
      LOGICAL, INTENT(IN) :: KTIME1
!
!     * INITIALIZATIONS.
!
      DO IT=1,IMAXL
        CNA(IT:IT) = '*'
      ENDDO
      DO IT=1,ITOT
        IVAR(IT)%NAME=CNA
        IVAR(IT)%LNAME=CNA
        IVAR(IT)%UNITS=CNA
        IVAR(IT)%DIM=CNA
        IVARV(IT)%FLD0D=-YNAX
      ENDDO
      DO IT=1,ITOT
        OVAR(IT)%NAME=CNA
        OVAR(IT)%LNAME=CNA
        OVAR(IT)%UNITS=CNA
        OVAR(IT)%DIM=CNA
        OVARV(IT)%FLD0D=-YNAX
      ENDDO
      DO IT=1,ITOT
        RVAR(IT)%NAME=CNA
        RVAR(IT)%LNAME=CNA
        RVAR(IT)%UNITS=CNA
        RVAR(IT)%DIM=CNA
        RVARV(IT)%FLD0D=-YNAX
      ENDDO
!
!     * OPEN AND READ NAMELISTS.
!
      OPEN(10,FILE='INPUT')
      OPEN(20,FILE='OUTPUT')
      OPEN(30,FILE='RESTART')
      READ(10,NML=IVARNML)
      READ(20,NML=OVARNML)
      READ(30,NML=RVARNML)
!
!     * SAVE DIMENSIONS AND ALLOCATE MEMORY FOR I/O ARRAYS.
!
      DO IT=1,ITOT
        IF ( IVAR(IT)%NAME /= CNA ) THEN
          IF ( .NOT.(IVAR(IT)%LNAME /= CNA &
               .AND. IVAR(IT)%UNITS /= CNA) ) THEN
            PRINT*,'MISSING LNAME OR UNITS FOR INPUT VARIABLE ', &
                    TRIM(IVAR(IT)%NAME)
            CALL XIT('INITIO',-1)
          ENDIF
          DO ITX=1,IT-1
            IF ( TRIM(IVAR(IT)%NAME) == TRIM(IVAR(ITX)%NAME) ) THEN
              PRINT*,'MULTIPLE OCCURRENCES OF INPUT VARIABLE ', &
                      TRIM(IVAR(IT)%NAME)
              CALL XIT('INITIO',-2)
            ENDIF
          ENDDO
          IDIM=0
          DO IS=1,ISMX
            CTMP=IVAR(IT)%DIM(IS)
            IF ( CTMP /= CNA ) THEN
              IDIM=IDIM+1
              ICHK=0
              DO ND=1,NDIM
                IF ( TRIM(CTMP) == TRIM(DIM(ND)%NAME) ) THEN
                  DIM(ND)%FLGI=1
                  IDIMA(IDIM)=DIM(ND)%SIZE
                  IF ( DIM(ND)%SIZE == INAX ) THEN
                    PRINT*,'DIMENSION ',TRIM(DIM(ND)%NAME),' UNDEFINED'
                    CALL XIT ('INITIO',-3)
                  ENDIF
                  ICHK=1
                ENDIF
              ENDDO
              IF ( ICHK == 0 ) THEN
                PRINT*,'INCORRECT DIMENSION FOR INPUT VARIABLE ', &
                       TRIM(IVAR(IT)%NAME)
                CALL XIT('INITIO',-4)
              ENDIF
            ENDIF
          ENDDO
          IF ( IDIM == 0 ) THEN
            IVARV(IT)%FLD0D=YNAX
          ELSE IF ( IDIM == 1 ) THEN
            ALLOCATE(IVARV(IT)%FLD1D(IDIMA(1)))
            IVARV(IT)%FLD1D(:)=YNAX
          ELSE IF ( IDIM == 2 ) THEN
            ALLOCATE(IVARV(IT)%FLD2D(IDIMA(1),IDIMA(2)))
            IVARV(IT)%FLD2D(:,:)=YNAX
          ELSE IF ( IDIM == 3 ) THEN
            ALLOCATE(IVARV(IT)%FLD3D(IDIMA(1),IDIMA(2),IDIMA(3)))
            IVARV(IT)%FLD3D(:,:,:)=YNAX
          ELSE IF ( IDIM == 4 ) THEN
            ALLOCATE(IVARV(IT)%FLD4D(IDIMA(1),IDIMA(2),IDIMA(3), &
                                                            IDIMA(4)))
            IVARV(IT)%FLD4D(:,:,:,:)=YNAX
          ELSE
            CALL XIT('INITIO',-5)
          ENDIF
        ENDIF
      ENDDO
      DO IT=1,ITOT
        IF ( OVAR(IT)%NAME /= CNA ) THEN
          IF ( .NOT.(OVAR(IT)%LNAME /= CNA &
               .AND. OVAR(IT)%UNITS /= CNA) ) THEN
            PRINT*,'MISSING LNAME OR UNITS FOR OUTPUT VARIABLE ', &
                    TRIM(OVAR(IT)%NAME)
            CALL XIT('INITIO',-6)
          ENDIF
          DO ITX=1,IT-1
            IF ( TRIM(OVAR(IT)%NAME) == TRIM(OVAR(ITX)%NAME) ) THEN
              PRINT*,'MULTIPLE OCCURRENCES OF OUTPUT VARIABLE ', &
                      TRIM(OVAR(IT)%NAME)
              CALL XIT('INITIO',-7)
            ENDIF
          ENDDO
          IDIM=0
          DO IS=1,ISMX
            CTMP=OVAR(IT)%DIM(IS)
            IF ( KTIME1 .AND. TRIM(CTMP) == 'time' ) THEN
              OVAR(IT)%DIM(IS)='time1'
              CTMP=OVAR(IT)%DIM(IS)
            ENDIF
            IF ( CTMP /= CNA ) THEN
              IDIM=IDIM+1
              ICHK=0
              DO ND=1,NDIM
                IF ( TRIM(CTMP) == TRIM(DIM(ND)%NAME) ) THEN
                  DIM(ND)%FLGO=1
                  IDIMA(IDIM)=DIM(ND)%SIZE
                  IF ( DIM(ND)%SIZE == INAX ) THEN
                    PRINT*,'DIMENSION ',TRIM(DIM(ND)%NAME),' UNDEFINED'
                    CALL XIT ('INITIO',-8)
                  ENDIF
                  ICHK=1
                ENDIF
              ENDDO
              IF ( ICHK == 0 ) THEN
                PRINT*,'INCORRECT DIMENSION FOR OUTPUT VARIABLE ', &
                       TRIM(OVAR(IT)%NAME)
                CALL XIT('INITIO',-9)
              ENDIF
            ENDIF
          ENDDO
          IF ( IDIM == 0 ) THEN
            OVARV(IT)%FLD0D=YNAX
          ELSE IF ( IDIM == 1 ) THEN
            ALLOCATE(OVARV(IT)%FLD1D(IDIMA(1)))
            OVARV(IT)%FLD1D(:)=YNAX
          ELSE IF ( IDIM == 2 ) THEN
            ALLOCATE(OVARV(IT)%FLD2D(IDIMA(1),IDIMA(2)))
            OVARV(IT)%FLD2D(:,:)=YNAX
          ELSE IF ( IDIM == 3 ) THEN
            ALLOCATE(OVARV(IT)%FLD3D(IDIMA(1),IDIMA(2),IDIMA(3)))
            OVARV(IT)%FLD3D(:,:,:)=YNAX
          ELSE IF ( IDIM == 4 ) THEN
            ALLOCATE(OVARV(IT)%FLD4D(IDIMA(1),IDIMA(2),IDIMA(3), &
                                                            IDIMA(4)))
            OVARV(IT)%FLD4D(:,:,:,:)=YNAX
          ELSE
            CALL XIT('INITIO',-10)
          ENDIF
        ENDIF
      ENDDO
      DO IT=1,ITOT
        IF ( RVAR(IT)%NAME /= CNA ) THEN
          IF ( .NOT.(RVAR(IT)%LNAME /= CNA &
               .AND. RVAR(IT)%UNITS /= CNA) ) THEN
            PRINT*,'MISSING LNAME OR UNITS FOR RESTART VARIABLE ', &
                    TRIM(RVAR(IT)%NAME)
            CALL XIT('INITIO',-11)
          ENDIF
          DO ITX=1,IT-1
            IF ( TRIM(RVAR(IT)%NAME) == TRIM(RVAR(ITX)%NAME) ) THEN
              PRINT*,'MULTIPLE OCCURRENCES OF RESTART VARIABLE ', &
                      TRIM(RVAR(IT)%NAME)
              CALL XIT('INITIO',-12)
            ENDIF
          ENDDO
          IDIM=0
          DO IS=1,ISMX
            CTMP=RVAR(IT)%DIM(IS)
            IF ( CTMP /= CNA ) THEN
              IDIM=IDIM+1
              ICHK=0
              DO ND=1,NDIM
                IF ( TRIM(CTMP) == TRIM(DIM(ND)%NAME) ) THEN
                  DIM(ND)%FLGRS=1
                  IDIMA(IDIM)=DIM(ND)%SIZE
                  IF ( DIM(ND)%SIZE == INAX ) THEN
                    PRINT*,'DIMENSION ',TRIM(DIM(ND)%NAME),' UNDEFINED'
                    CALL XIT ('INITIO',-13)
                  ENDIF
                  ICHK=1
                ENDIF
              ENDDO
              IF ( ICHK == 0 ) THEN
                PRINT*,'INCORRECT DIMENSION FOR RESTART VARIABLE ', &
                       TRIM(RVAR(IT)%NAME)
                CALL XIT('INITIO',-14)
              ENDIF
            ENDIF
          ENDDO
          IF ( IDIM == 0 ) THEN
            RVARV(IT)%FLD0D=YNAX
          ELSE IF ( IDIM == 1 ) THEN
            ALLOCATE(RVARV(IT)%FLD1D(IDIMA(1)))
            RVARV(IT)%FLD1D(:)=YNAX
          ELSE IF ( IDIM == 2 ) THEN
            ALLOCATE(RVARV(IT)%FLD2D(IDIMA(1),IDIMA(2)))
            RVARV(IT)%FLD2D(:,:)=YNAX
          ELSE IF ( IDIM == 3 ) THEN
            ALLOCATE(RVARV(IT)%FLD3D(IDIMA(1),IDIMA(2),IDIMA(3)))
            RVARV(IT)%FLD3D(:,:,:)=YNAX
          ELSE IF ( IDIM == 4 ) THEN
            ALLOCATE(RVARV(IT)%FLD4D(IDIMA(1),IDIMA(2),IDIMA(3), &
                                                             IDIMA(4)))
            RVARV(IT)%FLD4D(:,:,:,:)=YNAX
          ELSE
            CALL XIT('INITIO',-15)
          ENDIF
        ENDIF
      ENDDO
!
!     * CLOSE INPUT FILES.
!
      CLOSE(10)
      CLOSE(11)
      CLOSE(12)
!
      END SUBROUTINE INITIO


      SUBROUTINE W1DDIM(CNAME,AVAR,IDIM1,J1,J2)
!
      USE IODAT
!
      IMPLICIT REAL (A-H,O-Z),INTEGER (I-N)
!
      CHARACTER(LEN=*), INTENT(IN) :: CNAME
      REAL, INTENT(IN), DIMENSION(IDIM1) :: AVAR
      INTEGER, INTENT(IN) :: IDIM1
      INTEGER, OPTIONAL :: J1,J2
      INTEGER :: J1T,J2T
      INTEGER, DIMENSION(1) :: ISHAPE
!
      IF ( PRESENT(J1) .AND. PRESENT(J2) ) THEN
        IF ( J1 < 1 .OR. J2 > IDIM1 ) CALL XIT('W1DDIM',-1)
        J1T=J1
        J2T=J2
      ELSE
        J1T=1
        J2T=IDIM1
      ENDIF
!
      ICHCK=0
      DO IT=1,NDIM
        IF ( ALLOCATED(DIM(IT)%VAL) &
              .AND. TRIM(DIM(IT)%NAME) == TRIM(CNAME) ) THEN
          ICHCK=1
!
          ISHAPE=SHAPE(DIM(IT)%VAL)
          IF ( ISHAPE(1) == IDIM1 ) THEN
            DIM(IT)%VAL(J1T:J2T)=AVAR(J1T:J2T)
          ELSE
            PRINT*,'INCOMPATIBLE DIMENSIONS VARIABLE ',TRIM(CNAME)
            CALL XIT('W1DDIM',-2)
          ENDIF
        ENDIF
      ENDDO
      IF ( ICHCK==0 ) THEN
        PRINT*,'NO DIMENSION ',TRIM(CNAME),' IN LIST OF DIMENSIONS'
        CALL XIT('W1DDIM',-3)
      ENDIF
!
      END SUBROUTINE W1DDIM


      SUBROUTINE W0DFLD(CNAME,AVAR)
!
      USE IODAT
!
      IMPLICIT REAL (A-H,O-Z),INTEGER (I-N)
!
      CHARACTER(LEN=*), INTENT(IN) :: CNAME
      REAL :: AVAR
!
      ICHCK=0
      DO IT=1,ITOT
        IF ( ABS(OVARV(IT)%FLD0D+YNAX) > YSMALLX &
              .AND. TRIM(OVAR(IT)%NAME) == TRIM(CNAME) ) THEN
          ICHCK=1
          OVARV(IT)%FLD0D=AVAR
        ENDIF
      ENDDO
      IF ( ICHCK==0 ) THEN
        PRINT*,'NO VARIABLE ',TRIM(CNAME),' IN LIST OF IO VARIABLES'
        CALL XIT('W0DFLD',-1)
      ENDIF
!
      END SUBROUTINE W0DFLD


      SUBROUTINE W1DFLD(CNAME,AVAR,IDIM1,J1,J2)
!
      USE IODAT
!
      IMPLICIT REAL (A-H,O-Z),INTEGER (I-N)
!
      CHARACTER(LEN=*), INTENT(IN) :: CNAME
      REAL, INTENT(IN), DIMENSION(IDIM1) :: AVAR
      INTEGER, INTENT(IN) :: IDIM1
      INTEGER, OPTIONAL :: J1,J2
      INTEGER :: J1T,J2T
      INTEGER, DIMENSION(1) :: ISHAPE
!
      IF ( PRESENT(J1) .AND. PRESENT(J2) ) THEN
        IF ( J1 < 1 .OR. J2 > IDIM1 ) CALL XIT('W1DFLD',-1)
        J1T=J1
        J2T=J2
      ELSE
        J1T=1
        J2T=IDIM1
      ENDIF
!
      ICHCK=0
      DO IT=1,ITOT
        IF ( ALLOCATED(OVARV(IT)%FLD1D) &
              .AND. TRIM(OVAR(IT)%NAME) == TRIM(CNAME) ) THEN
          ICHCK=1
!
          ISHAPE=SHAPE(OVARV(IT)%FLD1D)
          IF ( ISHAPE(1) == IDIM1 ) THEN
            OVARV(IT)%FLD1D(J1T:J2T)=AVAR(J1T:J2T)
          ELSE
            PRINT*,'INCOMPATIBLE DIMENSIONS VARIABLE ',TRIM(CNAME)
            CALL XIT('W1DFLD',-2)
          ENDIF
        ENDIF
      ENDDO
      IF ( ICHCK==0 ) THEN
        PRINT*,'NO VARIABLE ',TRIM(CNAME),' IN LIST OF IO VARIABLES'
        CALL XIT('W1DFLD',-3)
      ENDIF
!
      END SUBROUTINE W1DFLD


      SUBROUTINE W2DFLD(CNAME,AVAR,IDIM1,IDIM2,J1,J2,K1,K2)
!
      USE IODAT
!
      IMPLICIT REAL (A-H,O-Z),INTEGER (I-N)
!
      CHARACTER(LEN=*), INTENT(IN) :: CNAME
      REAL, INTENT(IN), DIMENSION(IDIM1,IDIM2) :: AVAR
      INTEGER, INTENT(IN) :: IDIM1,IDIM2
      INTEGER, OPTIONAL :: J1,J2,K1,K2
      INTEGER :: J1T,J2T,K1T,K2T
      INTEGER, DIMENSION(2) :: ISHAPE
!
      IF (       PRESENT(J1) .AND. PRESENT(J2) &
           .AND. PRESENT(K1) .AND. PRESENT(K2) ) THEN
        IF ( J1 < 1 .OR. K1 < 1 .OR. J2 > IDIM1 .OR. K2 > IDIM2 ) &
           CALL XIT('W2DFLD',-1)
        J1T=J1
        J2T=J2
        K1T=K1
        K2T=K2
      ELSE
        J1T=1
        J2T=IDIM1
        K1T=1
        K2T=IDIM2
      ENDIF
!
      ICHCK=0
      DO IT=1,ITOT
        IF ( ALLOCATED(OVARV(IT)%FLD2D) &
              .AND. TRIM(OVAR(IT)%NAME) == TRIM(CNAME) ) THEN
          ICHCK=1
!
          ISHAPE=SHAPE(OVARV(IT)%FLD2D)
          IF ( ISHAPE(1) == IDIM1 .AND. ISHAPE(2) == IDIM2 ) THEN
            OVARV(IT)%FLD2D(J1T:J2T,K1T:K2T)=AVAR(J1T:J2T,K1T:K2T)
          ELSE IF ( ISHAPE(1) == IDIM2 .AND. ISHAPE(2) == IDIM1 ) THEN
            DO I1=K1T,K2T
            DO I2=J1T,J2T
              OVARV(IT)%FLD2D(I1,I2)=AVAR(I2,I1)
            ENDDO
            ENDDO
          ELSE
            PRINT*,'INCOMPATIBLE DIMENSIONS VARIABLE ',TRIM(CNAME)
            CALL XIT('W2DFLD',-2)
          ENDIF
        ENDIF
      ENDDO
      IF ( ICHCK==0 ) THEN
        PRINT*,'NO VARIABLE ',TRIM(CNAME),' IN LIST OF IO VARIABLES'
        CALL XIT('W2DFLD',-3)
      ENDIF
!
      END SUBROUTINE W2DFLD


      SUBROUTINE W3DFLD(CNAME,AVAR,IDIM1,IDIM2,IDIM3, &
                        J1,J2,K1,K2,L1,L2)
!
      USE IODAT
!
      IMPLICIT REAL (A-H,O-Z),INTEGER (I-N)
!
      CHARACTER(LEN=*), INTENT(IN) :: CNAME
      REAL, INTENT(IN), DIMENSION(IDIM1,IDIM2,IDIM3) :: AVAR
      INTEGER, OPTIONAL :: J1,J2,K1,K2,L1,L2
      INTEGER, INTENT(IN) :: IDIM1,IDIM2,IDIM3
      INTEGER :: J1T,J2T,K1T,K2T,L1T,L2T
      INTEGER, DIMENSION(3) :: ISHAPE
!
      IF (       PRESENT(J1) .AND. PRESENT(J2) &
           .AND. PRESENT(K1) .AND. PRESENT(K2) &
           .AND. PRESENT(L1) .AND. PRESENT(L2) ) THEN
        IF (      J1 < 1 .OR. K1 < 1 .OR. L1 < 1 &
             .OR. J2 > IDIM1 .OR. K2 > IDIM2 .OR. L2 > IDIM3 ) &
           CALL XIT('W3DFLD',-1)
        J1T=J1
        J2T=J2
        K1T=K1
        K2T=K2
        L1T=L1
        L2T=L2
      ELSE
        J1T=1
        J2T=IDIM1
        K1T=1
        K2T=IDIM2
        L1T=1
        L2T=IDIM3
      ENDIF
!
      ICHCK=0
      DO IT=1,ITOT
        IF ( ALLOCATED(OVARV(IT)%FLD3D) &
              .AND. TRIM(OVAR(IT)%NAME) == TRIM(CNAME) ) THEN
          ICHCK=1
!
          ISHAPE=SHAPE(OVARV(IT)%FLD3D)
          IF (       ISHAPE(1) == IDIM1 .AND. ISHAPE(2) == IDIM2 &
               .AND. ISHAPE(3) == IDIM3 ) THEN
            OVARV(IT)%FLD3D(J1T:J2T,K1T:K2T,L1T:L2T)= &
                       AVAR(J1T:J2T,K1T:K2T,L1T:L2T)
          ELSE IF (  ISHAPE(1) == IDIM1 .AND. ISHAPE(2) == IDIM3 &
               .AND. ISHAPE(3) == IDIM2 ) THEN
            DO I2=K1T,K2T
            DO I3=L1T,L2T
              OVARV(IT)%FLD3D(J1T:J2T,I3,I2)=AVAR(J1T:J2T,I2,I3)
            ENDDO
            ENDDO
          ELSE IF (  ISHAPE(1) == IDIM2 .AND. ISHAPE(2) == IDIM1 &
               .AND. ISHAPE(3) == IDIM3 ) THEN
            DO I1=J1T,J2T
            DO I2=K1T,K2T
              OVARV(IT)%FLD3D(I2,I1,L1T:L2T)=AVAR(I1,I2,L1T:L2T)
            ENDDO
            ENDDO
          ELSE IF (  ISHAPE(1) == IDIM2 .AND. ISHAPE(2) == IDIM3 &
               .AND. ISHAPE(3) == IDIM1 ) THEN
            DO I1=J1T,J2T
            DO I2=K1T,K2T
            DO I3=L1T,L2T
              OVARV(IT)%FLD3D(I2,I3,I1)=AVAR(I1,I2,I3)
            ENDDO
            ENDDO
            ENDDO
          ELSE IF (  ISHAPE(1) == IDIM3 .AND. ISHAPE(2) == IDIM1 &
               .AND. ISHAPE(3) == IDIM2 ) THEN
            DO I1=J1T,J2T
            DO I2=K1T,K2T
            DO I3=L1T,L2T
              OVARV(IT)%FLD3D(I3,I1,I2)=AVAR(I1,I2,I3)
            ENDDO
            ENDDO
            ENDDO
          ELSE IF (  ISHAPE(1) == IDIM3 .AND. ISHAPE(2) == IDIM2 &
               .AND. ISHAPE(3) == IDIM1 ) THEN
            DO I1=J1T,J2T
            DO I3=L1T,L2T
              OVARV(IT)%FLD3D(I3,K1T:K2T,I1)=AVAR(I1,K1T:K2T,I3)
            ENDDO
            ENDDO
          ELSE
            PRINT*,'INCOMPATIBLE DIMENSIONS VARIABLE ',TRIM(CNAME)
            CALL XIT('W3DFLD',-2)
          ENDIF
        ENDIF
      ENDDO
      IF ( ICHCK==0 ) THEN
        PRINT*,'NO VARIABLE ',TRIM(CNAME),' IN LIST OF IO VARIABLES'
        CALL XIT('W3DFLD',-3)
      ENDIF
!
      END SUBROUTINE W3DFLD


      SUBROUTINE W4DFLD(CNAME,AVAR,IDIM1,IDIM2,IDIM3,IDIM4, &
                        J1,J2,K1,K2,L1,L2,M1,M2)
!
      USE IODAT
!
      IMPLICIT REAL (A-H,O-Z),INTEGER (I-N)
!
      CHARACTER(LEN=*), INTENT(IN) :: CNAME
      REAL, INTENT(IN), DIMENSION(IDIM1,IDIM2,IDIM3,IDIM4) :: AVAR
      INTEGER, INTENT(IN) :: IDIM1,IDIM2,IDIM3,IDIM4
      INTEGER, OPTIONAL :: J1,J2,K1,K2,L1,L2,M1,M2
      INTEGER :: J1T,J2T,K1T,K2T,L1T,L2T,M1T,M2T
      INTEGER, DIMENSION(4) :: ISHAPE
!
      IF (       PRESENT(J1) .AND. PRESENT(J2) &
           .AND. PRESENT(K1) .AND. PRESENT(K2) &
           .AND. PRESENT(L1) .AND. PRESENT(L2) &
           .AND. PRESENT(M1) .AND. PRESENT(M2) ) THEN
        IF (      J1 < 1 .OR. K1 < 1 .OR. L1 < 1 .OR. M1 < 1 &
             .OR. J2 > IDIM1 .OR. K2 > IDIM2 .OR. L2 > IDIM3 &
             .OR. M2 > IDIM4 ) &
           CALL XIT('W4DFLD',-1)
        J1T=J1
        J2T=J2
        K1T=K1
        K2T=K2
        L1T=L1
        L2T=L2
        M1T=M1
        M2T=M2
      ELSE
        J1T=1
        J2T=IDIM1
        K1T=1
        K2T=IDIM2
        L1T=1
        L2T=IDIM3
        M1T=1
        M2T=IDIM4
      ENDIF
!
      ICHCK=0
      DO IT=1,ITOT
        IF ( ALLOCATED(OVARV(IT)%FLD4D) &
              .AND. TRIM(OVAR(IT)%NAME) == TRIM(CNAME) ) THEN
          ICHCK=1
!
          ISHAPE=SHAPE(OVARV(IT)%FLD4D)
          IF (       ISHAPE(1) == IDIM1 .AND. ISHAPE(2) == IDIM2 &
               .AND. ISHAPE(3) == IDIM3 .AND. ISHAPE(4) == IDIM4 ) THEN
            OVARV(IT)%FLD4D(J1T:J2T,K1T:K2T,L1T:L2T,M1T:M2T)= &
                       AVAR(J1T:J2T,K1T:K2T,L1T:L2T,M1T:M2T)
          ELSE
            PRINT*,'INCOMPATIBLE DIMENSIONS VARIABLE ',TRIM(CNAME)
            CALL XIT('W4DFLD',-2)
          ENDIF
        ENDIF
      ENDDO
      IF ( ICHCK==0 ) THEN
        PRINT*,'NO VARIABLE ',TRIM(CNAME),' IN LIST OF IO VARIABLES'
        CALL XIT('W4DFLD',-3)
      ENDIF
!
      END SUBROUTINE W4DFLD


      SUBROUTINE W0DFLDRS(CNAME,AVAR)
!
      USE IODAT
!
      IMPLICIT REAL (A-H,O-Z),INTEGER (I-N)
!
      CHARACTER(LEN=*), INTENT(IN) :: CNAME
      REAL, INTENT(IN) :: AVAR
!
      ICHCK=0
      DO IT=1,ITOT
        IF ( ABS(RVARV(IT)%FLD0D+YNAX) > YSMALLX &
              .AND. TRIM(RVAR(IT)%NAME) == TRIM(CNAME) ) THEN
          ICHCK=1
          RVARV(IT)%FLD0D=AVAR
        ENDIF
      ENDDO
      IF ( ICHCK==0 ) THEN
        PRINT*,'NO VARIABLE ',TRIM(CNAME), &
               ' IN LIST OF RESTART VARIABLES'
        CALL XIT('W0DFLDRS',-1)
      ENDIF
!
      END SUBROUTINE W0DFLDRS


      SUBROUTINE W1DFLDRS(CNAME,AVAR,IDIM1,J1,J2)
!
      USE IODAT
!
      IMPLICIT REAL (A-H,O-Z),INTEGER (I-N)
!
      CHARACTER(LEN=*), INTENT(IN) :: CNAME
      REAL, INTENT(IN), DIMENSION(IDIM1) :: AVAR
      INTEGER, INTENT(IN) :: IDIM1
      INTEGER, OPTIONAL :: J1,J2
      INTEGER :: J1T,J2T
      INTEGER, DIMENSION(1) :: ISHAPE
!
      IF ( PRESENT(J1) .AND. PRESENT(J2) ) THEN
        IF ( J1 < 1 .OR. J2 > IDIM1 ) CALL XIT('W1DFLDRS',-1)
        J1T=J1
        J2T=J2
      ELSE
        J1T=1
        J2T=IDIM1
      ENDIF
!
      ICHCK=0
      DO IT=1,ITOT
        IF ( ALLOCATED(RVARV(IT)%FLD1D) &
              .AND. TRIM(RVAR(IT)%NAME) == TRIM(CNAME) ) THEN
          ICHCK=1
!
          ISHAPE=SHAPE(RVARV(IT)%FLD1D)
          IF ( ISHAPE(1) == IDIM1 ) THEN
            RVARV(IT)%FLD1D(J1T:J2T)=AVAR(J1T:J2T)
          ELSE
            PRINT*,'INCOMPATIBLE DIMENSIONS VARIABLE ',TRIM(CNAME)
            CALL XIT('W1DFLDRS',-2)
          ENDIF
        ENDIF
      ENDDO
      IF ( ICHCK==0 ) THEN
        PRINT*,'NO VARIABLE ',TRIM(CNAME), &
               ' IN LIST OF RESTART VARIABLES'
        CALL XIT('W1DFLDRS',-3)
      ENDIF
!
      END SUBROUTINE W1DFLDRS


      SUBROUTINE W2DFLDRS(CNAME,AVAR,IDIM1,IDIM2,J1,J2,K1,K2)
!
      USE IODAT
!
      IMPLICIT REAL (A-H,O-Z),INTEGER (I-N)
!
      CHARACTER(LEN=*), INTENT(IN) :: CNAME
      REAL, INTENT(IN), DIMENSION(IDIM1,IDIM2) :: AVAR
      INTEGER, INTENT(IN) :: IDIM1,IDIM2
      INTEGER, OPTIONAL :: J1,J2,K1,K2
      INTEGER :: J1T,J2T,K1T,K2T
      INTEGER, DIMENSION(2) :: ISHAPE
!
      IF (       PRESENT(J1) .AND. PRESENT(J2) &
           .AND. PRESENT(K1) .AND. PRESENT(K2) ) THEN
        IF ( J1 < 1 .OR. K1 < 1 .OR. J2 > IDIM1 .OR. K2 > IDIM2 ) &
           CALL XIT('W2DFLDRS',-1)
        J1T=J1
        J2T=J2
        K1T=K1
        K2T=K2
      ELSE
        J1T=1
        J2T=IDIM1
        K1T=1
        K2T=IDIM2
      ENDIF
!
      ICHCK=0
      DO IT=1,ITOT
        IF ( ALLOCATED(RVARV(IT)%FLD2D) &
              .AND. TRIM(RVAR(IT)%NAME) == TRIM(CNAME) ) THEN
          ICHCK=1
!
          ISHAPE=SHAPE(RVARV(IT)%FLD2D)
          IF ( ISHAPE(1) == IDIM1 .AND. ISHAPE(2) == IDIM2 ) THEN
            RVARV(IT)%FLD2D(J1T:J2T,K1T:K2T)=AVAR(J1T:J2T,K1T:K2T)
          ELSE IF ( ISHAPE(1) == IDIM2 .AND. ISHAPE(2) == IDIM1 ) THEN
            DO I1=K1T,K2T
            DO I2=J1T,J2T
              RVARV(IT)%FLD2D(I1,I2)=AVAR(I2,I1)
            ENDDO
            ENDDO
          ELSE
            PRINT*,'INCOMPATIBLE DIMENSIONS VARIABLE ',TRIM(CNAME)
            CALL XIT('W2DFLDRS',-2)
          ENDIF
        ENDIF
      ENDDO
      IF ( ICHCK==0 ) THEN
        PRINT*,'NO VARIABLE ',TRIM(CNAME), &
               ' IN LIST OF RESTART VARIABLES'
        CALL XIT('W2DFLDRS',-3)
      ENDIF
!
      END SUBROUTINE W2DFLDRS


      SUBROUTINE W3DFLDRS(CNAME,AVAR,IDIM1,IDIM2,IDIM3, &
                          J1,J2,K1,K2,L1,L2)
!
      USE IODAT
!
      IMPLICIT REAL (A-H,O-Z),INTEGER (I-N)
!
      CHARACTER(LEN=*), INTENT(IN) :: CNAME
      REAL, INTENT(IN), DIMENSION(IDIM1,IDIM2,IDIM3) :: AVAR
      INTEGER, INTENT(IN) :: IDIM1,IDIM2,IDIM3
      INTEGER, OPTIONAL :: J1,J2,K1,K2,L1,L2
      INTEGER :: J1T,J2T,K1T,K2T,L1T,L2T
      INTEGER, DIMENSION(3) :: ISHAPE
!
      IF (       PRESENT(J1) .AND. PRESENT(J2) &
           .AND. PRESENT(K1) .AND. PRESENT(K2) &
           .AND. PRESENT(L1) .AND. PRESENT(L2) ) THEN
        IF (      J1 < 1 .OR. K1 < 1 .OR. L1 < 1 &
             .OR. J2 > IDIM1 .OR. K2 > IDIM2 .OR. L2 > IDIM3 ) &
           CALL XIT('W3DFLDRS',-1)
        J1T=J1
        J2T=J2
        K1T=K1
        K2T=K2
        L1T=L1
        L2T=L2
      ELSE
        J1T=1
        J2T=IDIM1
        K1T=1
        K2T=IDIM2
        L1T=1
        L2T=IDIM3
      ENDIF
!
      ICHCK=0
      DO IT=1,ITOT
        IF ( ALLOCATED(RVARV(IT)%FLD3D) &
              .AND. TRIM(RVAR(IT)%NAME) == TRIM(CNAME) ) THEN
          ICHCK=1
!
          ISHAPE=SHAPE(RVARV(IT)%FLD3D)
          IF (       ISHAPE(1) == IDIM1 .AND. ISHAPE(2) == IDIM2 &
               .AND. ISHAPE(3) == IDIM3 ) THEN
            RVARV(IT)%FLD3D(J1T:J2T,K1T:K2T,L1T:L2T)= &
                        AVAR(J1T:J2T,K1T:K2T,L1T:L2T)
          ELSE IF (  ISHAPE(1) == IDIM1 .AND. ISHAPE(2) == IDIM3 &
               .AND. ISHAPE(3) == IDIM2 ) THEN
            DO I2=K1T,K2T
            DO I3=L1T,L2T
              RVARV(IT)%FLD3D(J1T:J2T,I3,I2)=AVAR(J1T:J2T,I2,I3)
            ENDDO
            ENDDO
          ELSE IF (  ISHAPE(1) == IDIM2 .AND. ISHAPE(2) == IDIM1 &
               .AND. ISHAPE(3) == IDIM3 ) THEN
            DO I1=J1T,J2T
            DO I2=K1T,K2T
              RVARV(IT)%FLD3D(I2,I1,L1T:L2T)=AVAR(I1,I2,L1T:L2T)
            ENDDO
            ENDDO
          ELSE IF (  ISHAPE(1) == IDIM2 .AND. ISHAPE(2) == IDIM3 &
               .AND. ISHAPE(3) == IDIM1 ) THEN
            DO I1=J1T,J2T
            DO I2=K1T,K2T
            DO I3=L1T,L2T
              RVARV(IT)%FLD3D(I2,I3,I1)=AVAR(I1,I2,I3)
            ENDDO
            ENDDO
            ENDDO
          ELSE IF (  ISHAPE(1) == IDIM3 .AND. ISHAPE(2) == IDIM1 &
               .AND. ISHAPE(3) == IDIM2 ) THEN
            DO I1=J1T,J2T
            DO I2=K1T,K2T
            DO I3=L1T,L2T
              RVARV(IT)%FLD3D(I3,I1,I2)=AVAR(I1,I2,I3)
            ENDDO
            ENDDO
            ENDDO
          ELSE IF (  ISHAPE(1) == IDIM3 .AND. ISHAPE(2) == IDIM2 &
               .AND. ISHAPE(3) == IDIM1 ) THEN
            DO I1=J1T,J2T
            DO I3=L1T,L2T
              RVARV(IT)%FLD3D(I3,K1T:K2T,I1)=AVAR(I1,K1T:K2T,I3)
            ENDDO
            ENDDO
          ELSE
            PRINT*,'INCOMPATIBLE DIMENSIONS VARIABLE ',TRIM(CNAME)
            CALL XIT('W3DFLDRS',-2)
          ENDIF
        ENDIF
      ENDDO
      IF ( ICHCK==0 ) THEN
        PRINT*,'NO VARIABLE ',TRIM(CNAME), &
               ' IN LIST OF RESTART VARIABLES'
        CALL XIT('W3DFLDRS',-3)
      ENDIF
!
      END SUBROUTINE W3DFLDRS


      SUBROUTINE W4DFLDRS(CNAME,AVAR,IDIM1,IDIM2,IDIM3,IDIM4, &
                          J1,J2,K1,K2,L1,L2,M1,M2)
!
      USE IODAT
!
      IMPLICIT REAL (A-H,O-Z),INTEGER (I-N)
!
      CHARACTER(LEN=*), INTENT(IN) :: CNAME
      REAL, INTENT(IN), DIMENSION(IDIM1,IDIM2,IDIM3,IDIM4) :: AVAR
      INTEGER, INTENT(IN) :: IDIM1,IDIM2,IDIM3,IDIM4
      INTEGER, OPTIONAL :: J1,J2,K1,K2,L1,L2,M1,M2
      INTEGER :: J1T,J2T,K1T,K2T,L1T,L2T,M1T,M2T
      INTEGER, DIMENSION(4) :: ISHAPE
!
      IF (       PRESENT(J1) .AND. PRESENT(J2) &
           .AND. PRESENT(K1) .AND. PRESENT(K2) &
           .AND. PRESENT(L1) .AND. PRESENT(L2) &
           .AND. PRESENT(M1) .AND. PRESENT(M2) ) THEN
        IF (      J1 < 1 .OR. K1 < 1 .OR. L1 < 1 .OR. M1 < 1 &
             .OR. J2 > IDIM1 .OR. K2 > IDIM2 .OR. L2 > IDIM3 &
             .OR. M2 > IDIM4 ) &
           CALL XIT('W4DFLDRS',-1)
        J1T=J1
        J2T=J2
        K1T=K1
        K2T=K2
        L1T=L1
        L2T=L2
        M1T=M1
        M2T=M2
      ELSE
        J1T=1
        J2T=IDIM1
        K1T=1
        K2T=IDIM2
        L1T=1
        L2T=IDIM3
        M1T=1
        M2T=IDIM4
      ENDIF
!
      ICHCK=0
      DO IT=1,ITOT
        IF ( ALLOCATED(RVARV(IT)%FLD4D) .AND. TRIM(RVAR(IT)%NAME) == TRIM(CNAME) ) THEN
          ICHCK=1
!
          ISHAPE=SHAPE(RVARV(IT)%FLD4D)
          IF (       ISHAPE(1) == IDIM1 .AND. ISHAPE(2) == IDIM2 .AND. ISHAPE(3) == IDIM3 .AND. ISHAPE(4) == IDIM4 ) THEN
            RVARV(IT)%FLD4D(J1T:J2T,K1T:K2T,L1T:L2T,M1T:M2T) = AVAR(J1T:J2T,K1T:K2T,L1T:L2T,M1T:M2T)
          ELSE
            PRINT*,'INCOMPATIBLE DIMENSIONS VARIABLE ',TRIM(CNAME)
            CALL XIT('W4DFLDRS',-2)
          ENDIF
        ENDIF
      ENDDO
      IF ( ICHCK==0 ) THEN
        PRINT*,'NO VARIABLE ',TRIM(CNAME), &
               ' IN LIST OF RESTART VARIABLES'
        CALL XIT('W4DFLDRS',-3)
      ENDIF
!
      END SUBROUTINE W4DFLDRS


      SUBROUTINE R0DFLD(CNAME,AVAR)
!
      USE IODAT
!
      IMPLICIT REAL (A-H,O-Z),INTEGER (I-N)
!
      CHARACTER(LEN=*), INTENT(IN) :: CNAME
      REAL :: AVAR
!
      ICHCK=0
      DO IT=1,ITOT
        IF ( ABS(IVARV(IT)%FLD0D+YNAX) > YSMALLX .AND. TRIM(IVAR(IT)%NAME) == TRIM(CNAME) ) THEN
          ICHCK=1
          AVAR=IVARV(IT)%FLD0D
        ENDIF
      ENDDO
      IF ( ICHCK==0 ) THEN
        PRINT*,'NO VARIABLE ',TRIM(CNAME),' IN LIST OF IO VARIABLES'
        CALL XIT('R0DFLD',-1)
      ENDIF
!
      print *, 'R0DFLD: ', CNAME, avar

      END SUBROUTINE R0DFLD


      SUBROUTINE R1DFLD(CNAME,AVAR,IDIM1,J1,J2)
!
      USE IODAT
!
      IMPLICIT REAL (A-H,O-Z),INTEGER (I-N)
!
      CHARACTER(LEN=*), INTENT(IN) :: CNAME
      REAL, INTENT(OUT), DIMENSION(IDIM1) :: AVAR
      INTEGER, INTENT(IN) :: IDIM1
      INTEGER, OPTIONAL :: J1,J2
      INTEGER :: J1T,J2T
      INTEGER, DIMENSION(1) :: ISHAPE
!
      IF ( PRESENT(J1) .AND. PRESENT(J2) ) THEN
        IF ( J1 < 1 .OR. J2 > IDIM1 ) CALL XIT('R1DFLD',-1)
        J1T=J1
        J2T=J2
      ELSE
        J1T=1
        J2T=IDIM1
      ENDIF
!
      ICHCK=0
      DO IT=1,ITOT
        IF ( ALLOCATED(IVARV(IT)%FLD1D) .AND. TRIM(IVAR(IT)%NAME) == TRIM(CNAME) ) THEN
          ICHCK=1
!
          ISHAPE=SHAPE(IVARV(IT)%FLD1D)
          IF ( ISHAPE(1) == IDIM1 ) THEN
            AVAR(J1T:J2T)=IVARV(IT)%FLD1D(J1T:J2T)
          ELSE
            PRINT*,'INCOMPATIBLE DIMENSIONS VARIABLE ',TRIM(CNAME)
            CALL XIT('R1DFLD',-2)
          ENDIF
        ENDIF
      ENDDO
      IF ( ICHCK==0 ) THEN
        PRINT*,'NO VARIABLE ',TRIM(CNAME),' IN LIST OF IO VARIABLES'
        CALL XIT('R1DFLD',-3)
      ENDIF
!
      print '("R1DFLD: ", a10, "   [",i2,"]  [",i2,"-",i2,"]", es12.2)', CNAME,IDIM1,J1T,J2T, sum(avar)/size(avar)

      END SUBROUTINE R1DFLD


      SUBROUTINE R2DFLD(CNAME,AVAR,IDIM1,IDIM2,J1,J2,K1,K2)
!
      USE IODAT
!
      IMPLICIT REAL (A-H,O-Z),INTEGER (I-N)
!
      CHARACTER(LEN=*), INTENT(IN) :: CNAME
      REAL, INTENT(OUT), DIMENSION(IDIM1,IDIM2) :: AVAR
      INTEGER, INTENT(IN) :: IDIM1,IDIM2
      INTEGER, OPTIONAL :: J1,J2,K1,K2
      INTEGER :: J1T,J2T,K1T,K2T
      INTEGER, DIMENSION(2) :: ISHAPE
!
      IF (       PRESENT(J1) .AND. PRESENT(J2) &
           .AND. PRESENT(K1) .AND. PRESENT(K2) ) THEN
        IF ( J1 < 1 .OR. K1 < 1 .OR. J2 > IDIM1 .OR. K2 > IDIM2 ) &
           CALL XIT('R2DFLD',-1)
        J1T=J1
        J2T=J2
        K1T=K1
        K2T=K2
      ELSE
        J1T=1
        J2T=IDIM1
        K1T=1
        K2T=IDIM2
      ENDIF
!
      ICHCK=0
      DO IT=1,ITOT
        IF ( ALLOCATED(IVARV(IT)%FLD2D) .AND. TRIM(IVAR(IT)%NAME) == TRIM(CNAME) ) THEN
          ICHCK=1
!
          ISHAPE=SHAPE(IVARV(IT)%FLD2D)
          IF ( ISHAPE(1) == IDIM1 .AND. ISHAPE(2) == IDIM2 ) THEN
            AVAR(J1T:J2T,K1T:K2T)=IVARV(IT)%FLD2D(J1T:J2T,K1T:K2T)
          ELSE IF ( ISHAPE(1) == IDIM2 .AND. ISHAPE(2) == IDIM1 ) THEN
            DO I1=K1T,K2T
              DO I2=J1T,J2T
                AVAR(I2,I1)=IVARV(IT)%FLD2D(I1,I2)
              ENDDO
            ENDDO
          ELSE
            PRINT*,'INCOMPATIBLE DIMENSIONS VARIABLE ',TRIM(CNAME)
            CALL XIT('R2DFLD',-2)
          ENDIF
        ENDIF
      ENDDO
      IF ( ICHCK==0 ) THEN
        PRINT*,'NO VARIABLE ',TRIM(CNAME),' IN LIST OF IO VARIABLES'
        CALL XIT('R2DFLD',-3)
      ENDIF
!
      print '("R2DFLD: ", a10, "   [",i2,"x",i2,"]  [",i2,"-",i2,"]  [",i2,"-",i2,"]", es12.2)', CNAME, IDIM1,IDIM2, J1T,J2T, K1T,K2T, sum(avar)/size(avar)

      END SUBROUTINE R2DFLD


      SUBROUTINE R3DFLD(CNAME,AVAR,IDIM1,IDIM2,IDIM3,J1,J2,K1,K2,L1,L2)
!
      USE IODAT
!
      IMPLICIT REAL (A-H,O-Z),INTEGER (I-N)
!
      CHARACTER(LEN=*), INTENT(IN) :: CNAME
      REAL, INTENT(OUT), DIMENSION(IDIM1,IDIM2,IDIM3) :: AVAR
      INTEGER, OPTIONAL :: J1,J2,K1,K2,L1,L2
      INTEGER, INTENT(IN) :: IDIM1,IDIM2,IDIM3
      INTEGER :: J1T,J2T,K1T,K2T,L1T,L2T
      INTEGER, DIMENSION(3) :: ISHAPE
!
      IF (       PRESENT(J1) .AND. PRESENT(J2) &
           .AND. PRESENT(K1) .AND. PRESENT(K2) &
           .AND. PRESENT(L1) .AND. PRESENT(L2) ) THEN
        IF (      J1 < 1 .OR. K1 < 1 .OR. L1 < 1 &
             .OR. J2 > IDIM1 .OR. K2 > IDIM2 .OR. L2 > IDIM3 ) &
           CALL XIT('R3DFLD',-1)
        J1T=J1
        J2T=J2
        K1T=K1
        K2T=K2
        L1T=L1
        L2T=L2
      ELSE
        J1T=1
        J2T=IDIM1
        K1T=1
        K2T=IDIM2
        L1T=1
        L2T=IDIM3
      ENDIF
!
      ICHCK=0
      DO IT=1,ITOT
        IF ( ALLOCATED(IVARV(IT)%FLD3D) &
              .AND. TRIM(IVAR(IT)%NAME) == TRIM(CNAME) ) THEN
          ICHCK=1
!
          ISHAPE=SHAPE(IVARV(IT)%FLD3D)
          IF (       ISHAPE(1) == IDIM1 .AND. ISHAPE(2) == IDIM2 &
               .AND. ISHAPE(3) == IDIM3 ) THEN
            AVAR(J1T:J2T,K1T:K2T,L1T:L2T)= &
                IVARV(IT)%FLD3D(J1T:J2T,K1T:K2T,L1T:L2T)
          ELSE IF (  ISHAPE(1) == IDIM1 .AND. ISHAPE(2) == IDIM3 &
               .AND. ISHAPE(3) == IDIM2 ) THEN
            DO I2=K1T,K2T
            DO I3=L1T,L2T
              AVAR(J1T:J2T,I2,I3)=IVARV(IT)%FLD3D(J1T:J2T,I3,I2)
            ENDDO
            ENDDO
          ELSE IF (  ISHAPE(1) == IDIM2 .AND. ISHAPE(2) == IDIM1 &
               .AND. ISHAPE(3) == IDIM3 ) THEN
            DO I1=J1T,J2T
            DO I2=K1T,K2T
              AVAR(I1,I2,L1T:L2T)=IVARV(IT)%FLD3D(I2,I1,L1T:L2T)
            ENDDO
            ENDDO
          ELSE IF (  ISHAPE(1) == IDIM2 .AND. ISHAPE(2) == IDIM3 &
               .AND. ISHAPE(3) == IDIM1 ) THEN
            DO I1=J1T,J2T
            DO I2=K1T,K2T
            DO I3=L1T,L2T
              AVAR(I1,I2,I3)=IVARV(IT)%FLD3D(I2,I3,I1)
            ENDDO
            ENDDO
            ENDDO
          ELSE IF (  ISHAPE(1) == IDIM3 .AND. ISHAPE(2) == IDIM1 &
               .AND. ISHAPE(3) == IDIM2 ) THEN
            DO I1=J1T,J2T
            DO I2=K1T,K2T
            DO I3=L1T,L2T
              AVAR(I1,I2,I3)=IVARV(IT)%FLD3D(I3,I1,I2)
            ENDDO
            ENDDO
            ENDDO
          ELSE IF (  ISHAPE(1) == IDIM3 .AND. ISHAPE(2) == IDIM2 &
               .AND. ISHAPE(3) == IDIM1 ) THEN
            DO I1=J1T,J2T
            DO I3=L1T,L2T
              AVAR(I1,K1T:K2T,I3)=IVARV(IT)%FLD3D(I3,K1T:K2T,I1)
            ENDDO
            ENDDO
          ELSE
            PRINT*,'INCOMPATIBLE DIMENSIONS VARIABLE ',TRIM(CNAME)
            CALL XIT('R3DFLD',-2)
          ENDIF
        ENDIF
      ENDDO
      IF ( ICHCK==0 ) THEN
        PRINT*,'NO VARIABLE ',TRIM(CNAME),' IN LIST OF IO VARIABLES'
        CALL XIT('R3DFLD',-3)
      ENDIF
!
      print '("R3DFLD: ", a10, "   [",i2,"x",i2,"x",i2,"]  [",i2,"-",i2,"]  [",i2,"-",i2,"]  [",i2,"-",i2,"]", es12.2)', CNAME,IDIM1,IDIM2,IDIM3,J1T,J2T,K1T,K2T,L1T,L2T, sum(avar)/size(avar)

      END SUBROUTINE R3DFLD


      SUBROUTINE R4DFLD(CNAME,AVAR,IDIM1,IDIM2,IDIM3,IDIM4,J1,J2,K1,K2,L1,L2,M1,M2)
!
      USE IODAT
!
      IMPLICIT REAL (A-H,O-Z),INTEGER (I-N)
!
      CHARACTER(LEN=*), INTENT(IN) :: CNAME
      REAL, INTENT(OUT), DIMENSION(IDIM1,IDIM2,IDIM3,IDIM4) :: AVAR
      INTEGER, INTENT(IN) :: IDIM1,IDIM2,IDIM3,IDIM4
      INTEGER, OPTIONAL :: J1,J2,K1,K2,L1,L2,M1,M2
      INTEGER :: J1T,J2T,K1T,K2T,L1T,L2T,M1T,M2T
      INTEGER, DIMENSION(4) :: ISHAPE
!
      IF (       PRESENT(J1) .AND. PRESENT(J2) &
           .AND. PRESENT(K1) .AND. PRESENT(K2) &
           .AND. PRESENT(L1) .AND. PRESENT(L2) &
           .AND. PRESENT(M1) .AND. PRESENT(M2) ) THEN
        IF (      J1 < 1 .OR. K1 < 1 .OR. L1 < 1 .OR. M1 < 1 &
             .OR. J2 > IDIM1 .OR. K2 > IDIM2 .OR. L2 > IDIM3 &
             .OR. M2 > IDIM4 ) &
           CALL XIT('R4DFLD',-1)
        J1T=J1
        J2T=J2
        K1T=K1
        K2T=K2
        L1T=L1
        L2T=L2
        M1T=M1
        M2T=M2
      ELSE
        J1T=1
        J2T=IDIM1
        K1T=1
        K2T=IDIM2
        L1T=1
        L2T=IDIM3
        M1T=1
        M2T=IDIM4
      ENDIF
!
      print '(2a10,32i5)', 'R4DFLD: ', CNAME,IDIM1,IDIM2,IDIM3,IDIM4,J1T,J2T,K1T,K2T,L1T,L2T,M1T,M2T

      ICHCK=0
      DO IT=1,ITOT
        IF ( ALLOCATED(IVARV(IT)%FLD4D) &
              .AND. TRIM(IVAR(IT)%NAME) == TRIM(CNAME) ) THEN
          ICHCK=1
!
          ISHAPE=SHAPE(IVARV(IT)%FLD4D)
          IF (       ISHAPE(1) == IDIM1 .AND. ISHAPE(2) == IDIM2 &
               .AND. ISHAPE(3) == IDIM3 .AND. ISHAPE(4) == IDIM4 ) THEN
            AVAR(J1T:J2T,K1T:K2T,L1T:L2T,M1T:M2T)= &
                IVARV(IT)%FLD4D(J1T:J2T,K1T:K2T,L1T:L2T,M1T:M2T)
          ELSE
            PRINT*,'INCOMPATIBLE DIMENSIONS VARIABLE ',TRIM(CNAME)
            CALL XIT('R4DFLD',-2)
          ENDIF
        ENDIF
      ENDDO
      IF ( ICHCK==0 ) THEN
        PRINT*,'NO VARIABLE ',TRIM(CNAME),' IN LIST OF IO VARIABLES'
        CALL XIT('R4DFLD',-3)
      ENDIF
!
      END SUBROUTINE R4DFLD


      SUBROUTINE R0DFLDRS(CNAME,AVAR)
!
      USE IODAT
!
      IMPLICIT REAL (A-H,O-Z),INTEGER (I-N)
!
      CHARACTER(LEN=*), INTENT(IN) :: CNAME
      REAL, INTENT(OUT) :: AVAR
!
      ICHCK=0
      DO IT=1,ITOT
        IF ( ABS(RVARV(IT)%FLD0D+YNAX) > YSMALLX .AND. TRIM(RVAR(IT)%NAME) == TRIM(CNAME) ) THEN
          ICHCK=1
          AVAR=RVARV(IT)%FLD0D
        ENDIF
      ENDDO
      IF ( ICHCK==0 ) THEN
        PRINT*,'NO VARIABLE ',TRIM(CNAME), ' IN LIST OF RESTART VARIABLES'
        CALL XIT('R0DFLDRS',-1)
      ENDIF
!
      print '(2a10,es12.2)', 'R0DFLDRS: ', CNAME, AVAR

      END SUBROUTINE R0DFLDRS


      SUBROUTINE R1DFLDRS(CNAME,AVAR,IDIM1,J1,J2)
!
      USE IODAT
!
      IMPLICIT REAL (A-H,O-Z),INTEGER (I-N)
!
      CHARACTER(LEN=*), INTENT(IN) :: CNAME
      REAL, INTENT(OUT), DIMENSION(IDIM1) :: AVAR
      INTEGER, INTENT(IN) :: IDIM1
      INTEGER, OPTIONAL :: J1,J2
      INTEGER :: J1T,J2T
      INTEGER, DIMENSION(1) :: ISHAPE
!
      IF ( PRESENT(J1) .AND. PRESENT(J2) ) THEN
        IF ( J1 < 1 .OR. J2 > IDIM1 ) CALL XIT('R1DFLDRS',-1)
        J1T=J1
        J2T=J2
      ELSE
        J1T=1
        J2T=IDIM1
      ENDIF
!
      ICHCK=0
      DO IT=1,ITOT
        IF ( ALLOCATED(RVARV(IT)%FLD1D) &
              .AND. TRIM(RVAR(IT)%NAME) == TRIM(CNAME) ) THEN
          ICHCK=1
!
          ISHAPE=SHAPE(RVARV(IT)%FLD1D)
          IF ( ISHAPE(1) == IDIM1 ) THEN
            AVAR(J1T:J2T)=RVARV(IT)%FLD1D(J1T:J2T)
          ELSE
            PRINT*,'INCOMPATIBLE DIMENSIONS VARIABLE ',TRIM(CNAME)
            CALL XIT('R1DFLDRS',-2)
          ENDIF
        ENDIF
      ENDDO
      IF ( ICHCK==0 ) THEN
        PRINT*,'NO VARIABLE ',TRIM(CNAME), &
               ' IN LIST OF RESTART VARIABLES'
        CALL XIT('R1DFLDRS',-3)
      ENDIF
!
      print '("R1DFLDRS: ", a10, "   [",i2,"]  [",i2,"-",i2,"]", es12.2)', CNAME,IDIM1,J1T,J2T, sum(avar)/size(avar)

      END SUBROUTINE R1DFLDRS


      SUBROUTINE R2DFLDRS(CNAME,AVAR,IDIM1,IDIM2,J1,J2,K1,K2)
!
      USE IODAT
!
      IMPLICIT REAL (A-H,O-Z),INTEGER (I-N)
!
      CHARACTER(LEN=*), INTENT(IN) :: CNAME
      REAL, INTENT(OUT), DIMENSION(IDIM1,IDIM2) :: AVAR
      INTEGER, INTENT(IN) :: IDIM1,IDIM2
      INTEGER, OPTIONAL :: J1,J2,K1,K2
      INTEGER :: J1T,J2T,K1T,K2T
      INTEGER, DIMENSION(2) :: ISHAPE
!
      IF (       PRESENT(J1) .AND. PRESENT(J2) &
           .AND. PRESENT(K1) .AND. PRESENT(K2) ) THEN
        IF ( J1 < 1 .OR. K1 < 1 .OR. J2 > IDIM1 .OR. K2 > IDIM2 ) &
           CALL XIT('R2DFLDRS',-1)
        J1T=J1
        J2T=J2
        K1T=K1
        K2T=K2
      ELSE
        J1T=1
        J2T=IDIM1
        K1T=1
        K2T=IDIM2
      ENDIF
!
      ICHCK=0
      DO IT=1,ITOT
        IF ( ALLOCATED(RVARV(IT)%FLD2D) &
              .AND. TRIM(RVAR(IT)%NAME) == TRIM(CNAME) ) THEN
          ICHCK=1
!
          ISHAPE=SHAPE(RVARV(IT)%FLD2D)
          IF ( ISHAPE(1) == IDIM1 .AND. ISHAPE(2) == IDIM2 ) THEN
            AVAR(J1T:J2T,K1T:K2T)=RVARV(IT)%FLD2D(J1T:J2T,K1T:K2T)
          ELSE IF ( ISHAPE(1) == IDIM2 .AND. ISHAPE(2) == IDIM1 ) THEN
            DO I1=K1T,K2T
            DO I2=J1T,J2T
              AVAR(I2,I1)=RVARV(IT)%FLD2D(I1,I2)
            ENDDO
            ENDDO
          ELSE
            PRINT*,'INCOMPATIBLE DIMENSIONS VARIABLE ',TRIM(CNAME)
            CALL XIT('R2DFLDRS',-2)
          ENDIF
        ENDIF
      ENDDO
      IF ( ICHCK==0 ) THEN
        PRINT*,'NO VARIABLE ',TRIM(CNAME), &
               ' IN LIST OF RESTART VARIABLES'
        CALL XIT('R2DFLDRS',-3)
      ENDIF
!
      print '("R2DFLDRS: ", a10, "   [",i2,"x",i2,"]  [",i2,"-",i2,"]  [",i2,"-",i2,"]", es12.2)', CNAME, IDIM1,IDIM2, J1T,J2T, K1T,K2T, sum(avar)/size(avar)

      END SUBROUTINE R2DFLDRS


      SUBROUTINE R3DFLDRS(CNAME,AVAR,IDIM1,IDIM2,IDIM3,J1,J2,K1,K2,L1,L2)
!
      USE IODAT
!
      IMPLICIT REAL (A-H,O-Z),INTEGER (I-N)
!
      CHARACTER(LEN=*), INTENT(IN) :: CNAME
      REAL, INTENT(OUT), DIMENSION(IDIM1,IDIM2,IDIM3) :: AVAR
      INTEGER, INTENT(IN) :: IDIM1,IDIM2,IDIM3
      INTEGER, OPTIONAL :: J1,J2,K1,K2,L1,L2
      INTEGER :: J1T,J2T,K1T,K2T,L1T,L2T
      INTEGER, DIMENSION(3) :: ISHAPE
!
      IF (       PRESENT(J1) .AND. PRESENT(J2) &
           .AND. PRESENT(K1) .AND. PRESENT(K2) &
           .AND. PRESENT(L1) .AND. PRESENT(L2) ) THEN
        IF (      J1 < 1 .OR. K1 < 1 .OR. L1 < 1 &
             .OR. J2 > IDIM1 .OR. K2 > IDIM2 .OR. L2 > IDIM3 ) &
           CALL XIT('R3DFLDRS',-1)
        J1T=J1
        J2T=J2
        K1T=K1
        K2T=K2
        L1T=L1
        L2T=L2
      ELSE
        J1T=1
        J2T=IDIM1
        K1T=1
        K2T=IDIM2
        L1T=1
        L2T=IDIM3
      ENDIF
!
      ICHCK=0
      DO IT=1,ITOT
        IF ( ALLOCATED(RVARV(IT)%FLD3D) &
              .AND. TRIM(RVAR(IT)%NAME) == TRIM(CNAME) ) THEN
          ICHCK=1
!
          ISHAPE=SHAPE(RVARV(IT)%FLD3D)
          IF (       ISHAPE(1) == IDIM1 .AND. ISHAPE(2) == IDIM2 &
               .AND. ISHAPE(3) == IDIM3 ) THEN
            AVAR(J1T:J2T,K1T:K2T,L1T:L2T)= &
                RVARV(IT)%FLD3D(J1T:J2T,K1T:K2T,L1T:L2T)
          ELSE IF (  ISHAPE(1) == IDIM1 .AND. ISHAPE(2) == IDIM3 &
               .AND. ISHAPE(3) == IDIM2 ) THEN
            DO I2=K1T,K2T
            DO I3=L1T,L2T
              AVAR(J1T:J2T,I2,I3)=RVARV(IT)%FLD3D(J1T:J2T,I3,I2)
            ENDDO
            ENDDO
          ELSE IF (  ISHAPE(1) == IDIM2 .AND. ISHAPE(2) == IDIM1 &
               .AND. ISHAPE(3) == IDIM3 ) THEN
            DO I1=J1T,J2T
            DO I2=K1T,K2T
              AVAR(I1,I2,L1T:L2T)=RVARV(IT)%FLD3D(I2,I1,L1T:L2T)
            ENDDO
            ENDDO
          ELSE IF (  ISHAPE(1) == IDIM2 .AND. ISHAPE(2) == IDIM3 &
               .AND. ISHAPE(3) == IDIM1 ) THEN
            DO I1=J1T,J2T
            DO I2=K1T,K2T
            DO I3=L1T,L2T
              AVAR(I1,I2,I3)=RVARV(IT)%FLD3D(I2,I3,I1)
            ENDDO
            ENDDO
            ENDDO
          ELSE IF (  ISHAPE(1) == IDIM3 .AND. ISHAPE(2) == IDIM1 &
               .AND. ISHAPE(3) == IDIM2 ) THEN
            DO I1=J1T,J2T
            DO I2=K1T,K2T
            DO I3=L1T,L2T
              AVAR(I1,I2,I3)=RVARV(IT)%FLD3D(I3,I1,I2)
            ENDDO
            ENDDO
            ENDDO
          ELSE IF (  ISHAPE(1) == IDIM3 .AND. ISHAPE(2) == IDIM2 &
               .AND. ISHAPE(3) == IDIM1 ) THEN
            DO I1=J1T,J2T
            DO I3=L1T,L2T
              AVAR(I1,K1T:K2T,I3)=RVARV(IT)%FLD3D(I3,K1T:K2T,I1)
            ENDDO
            ENDDO
          ELSE
            PRINT*,'INCOMPATIBLE DIMENSIONS VARIABLE ',TRIM(CNAME)
            CALL XIT('R3DFLDRS',-2)
          ENDIF
        ENDIF
      ENDDO
      IF ( ICHCK==0 ) THEN
        PRINT*,'NO VARIABLE ',TRIM(CNAME), &
               ' IN LIST OF RESTART VARIABLES'
        CALL XIT('R3DFLDRS',-3)
      ENDIF
!
      print '("R3DFLDRS: ", a10, "   [",i2,"x",i2,"x",i2,"]  [",i2,"-",i2,"]  [",i2,"-",i2,"]  [",i2,"-",i2,"]", es12.2)', CNAME,IDIM1,IDIM2,IDIM3,J1T,J2T,K1T,K2T,L1T,L2T, sum(avar)/size(avar)

      END SUBROUTINE R3DFLDRS


      SUBROUTINE R4DFLDRS(CNAME,AVAR,IDIM1,IDIM2,IDIM3,IDIM4,J1,J2,K1,K2,L1,L2,M1,M2)
!
      USE IODAT
!
      IMPLICIT REAL (A-H,O-Z),INTEGER (I-N)
!
      CHARACTER(LEN=*), INTENT(IN) :: CNAME
      REAL, INTENT(OUT), DIMENSION(IDIM1,IDIM2,IDIM3,IDIM4) :: AVAR
      INTEGER, INTENT(IN) :: IDIM1,IDIM2,IDIM3,IDIM4
      INTEGER, OPTIONAL :: J1,J2,K1,K2,L1,L2,M1,M2
      INTEGER :: J1T,J2T,K1T,K2T,L1T,L2T,M1T,M2T
      INTEGER, DIMENSION(4) :: ISHAPE
!
      IF (       PRESENT(J1) .AND. PRESENT(J2) &
           .AND. PRESENT(K1) .AND. PRESENT(K2) &
           .AND. PRESENT(L1) .AND. PRESENT(L2) &
           .AND. PRESENT(M1) .AND. PRESENT(M2) ) THEN
        IF (      J1 < 1 .OR. K1 < 1 .OR. L1 < 1 .OR. M1 < 1 &
             .OR. J2 > IDIM1 .OR. K2 > IDIM2 .OR. L2 > IDIM3 &
             .OR. M2 > IDIM4 ) &
           CALL XIT('R4DFLDRS',-1)
        J1T=J1
        J2T=J2
        K1T=K1
        K2T=K2
        L1T=L1
        L2T=L2
        M1T=M1
        M2T=M2
      ELSE
        J1T=1
        J2T=IDIM1
        K1T=1
        K2T=IDIM2
        L1T=1
        L2T=IDIM3
        M1T=1
        M2T=IDIM4
      ENDIF
!
      print '(2a10,32i5)', 'R4DFLDRS',CNAME,IDIM1,IDIM2,IDIM3,IDIM4,J1T,J2T,K1T,K2T,L1T,L2T,M1T,M2T

      ICHCK=0
      DO IT=1,ITOT
        IF ( ALLOCATED(RVARV(IT)%FLD4D) &
              .AND. TRIM(RVAR(IT)%NAME) == TRIM(CNAME) ) THEN
          ICHCK=1
!
          ISHAPE=SHAPE(RVARV(IT)%FLD4D)
          IF (       ISHAPE(1) == IDIM1 .AND. ISHAPE(2) == IDIM2 &
               .AND. ISHAPE(3) == IDIM3 .AND. ISHAPE(4) == IDIM4 ) THEN
            AVAR(J1T:J2T,K1T:K2T,L1T:L2T,M1T:M2T)= &
                RVARV(IT)%FLD4D(J1T:J2T,K1T:K2T,L1T:L2T,M1T:M2T)
          ELSE
            PRINT*,'INCOMPATIBLE DIMENSIONS VARIABLE ',TRIM(CNAME)
            CALL XIT('R4DFLDRS',-2)
          ENDIF
        ENDIF
      ENDDO
      IF ( ICHCK==0 ) THEN
        PRINT*,'NO VARIABLE ',TRIM(CNAME), &
               ' IN LIST OF RESTART VARIABLES'
        CALL XIT('R4DFLDRS',-3)
      ENDIF
!
      END SUBROUTINE R4DFLDRS


      SUBROUTINE WNCDAT(FILE_NAME)
!
      USE IODAT
      USE NETCDF
!
      IMPLICIT NONE
!
      INTEGER :: IS,IT,ND,IV,ICHCK,IDIM

      CHARACTER(LEN=*), INTENT(IN) :: FILE_NAME
      INTEGER :: ncid, fil_varid, vartype
      INTEGER, DIMENSION(NDIM) :: lvl_dimid, lvl_varid
      INTEGER, DIMENSION(ITOT) :: var_varid
      INTEGER, DIMENSION(1) :: DIMD1D, CNT1D
      INTEGER, DIMENSION(2) :: DIMD2D, CNT2D
      INTEGER, DIMENSION(3) :: DIMD3D, CNT3D
      INTEGER, DIMENSION(4) :: DIMD4D, CNT4D

      vartype = NF90_REAL8
!
!     * CREATE OUTPUT FILE.
!
      CALL CHECK (nf90_create(TRIM(FILE_NAME)//'.nc', NF90_HDF5, ncid) )                ! ??? was nf90_clobber
!
!     * DEFINE THE DIMENSIONS.
!
      DO ND=1,NDIM
        IF ( DIM(ND)%FLGO == 1 ) THEN
          CALL CHECK( nf90_def_dim(ncid, TRIM(DIM(ND)%NAME), DIM(ND)%SIZE, lvl_dimid(ND)) )
        ENDIF
      ENDDO
!
!     * DEFINE THE COORDINDATE VARIABLES.
!
      DO ND=1,NDIM
        IF ( DIM(ND)%FLGO == 1 ) THEN
          CALL CHECK( nf90_def_var(ncid, TRIM(DIM(ND)%NAME), &
                                   vartype, lvl_dimid(ND), &
                                   lvl_varid(ND)) )
        ENDIF
      ENDDO
!
!     * ASSIGN ATTRIBUTES TO COORDINATE VARIABLES.
!
      DO ND=1,NDIM
        IF ( DIM(ND)%FLGO == 1 ) THEN
          CALL CHECK( nf90_put_att(ncid, lvl_varid(ND), "units",     TRIM(DIM(ND)%UNITS)) )
          CALL CHECK( nf90_put_att(ncid, lvl_varid(ND), "long_name", TRIM(DIM(ND)%LNAME)) )
        ENDIF
      ENDDO
!
!      DEFINE THE NETCDF VARIABLES FOR EACH FIELD.
!
      IV=0
      DO IT=1,ITOT
        ICHCK=0
        IF ( ABS(OVARV(IT)%FLD0D+YNAX) > YSMALLX ) THEN
          ICHCK=1
          IV=IV+1
          CALL CHECK( nf90_def_var(ncid,TRIM(OVAR(IT)%NAME), &
                                   vartype, var_varid(IV)) )
        ELSE IF ( ALLOCATED(OVARV(IT)%FLD1D) ) THEN
          ICHCK=1
          IV=IV+1
          IDIM=0
          DO IS=1,ISMX
            CTMP=OVAR(IT)%DIM(IS)
            IF ( CTMP /= CNA ) THEN
              IDIM=IDIM+1
              DO ND=1,NDIM
                IF ( TRIM(CTMP) == TRIM(DIM(ND)%NAME) ) THEN
                  DIMD1D(IDIM)=lvl_varid(ND)
                ENDIF
              ENDDO
            ENDIF
          ENDDO
          CALL CHECK( nf90_def_var(ncid,TRIM(OVAR(IT)%NAME), &
                                   vartype, DIMD1D, var_varid(IV)) )
        ELSE IF ( ALLOCATED(OVARV(IT)%FLD2D) ) THEN
          ICHCK=1
          IV=IV+1
          IDIM=0
          DO IS=1,ISMX
            CTMP=OVAR(IT)%DIM(IS)
            IF ( CTMP /= CNA ) THEN
              IDIM=IDIM+1
              DO ND=1,NDIM
                IF ( TRIM(CTMP) == TRIM(DIM(ND)%NAME) ) THEN
                  DIMD2D(IDIM)=lvl_varid(ND)
                ENDIF
              ENDDO
            ENDIF
          ENDDO
          CALL CHECK( nf90_def_var(ncid,TRIM(OVAR(IT)%NAME), &
                                   vartype, DIMD2D, var_varid(IV)) )
        ELSE IF ( ALLOCATED(OVARV(IT)%FLD3D) ) THEN
          ICHCK=1
          IV=IV+1
          IDIM=0
          DO IS=1,ISMX
            CTMP=OVAR(IT)%DIM(IS)
            IF ( CTMP /= CNA ) THEN
              IDIM=IDIM+1
              DO ND=1,NDIM
                IF ( TRIM(CTMP) == TRIM(DIM(ND)%NAME) ) THEN
                  DIMD3D(IDIM)=lvl_varid(ND)
                ENDIF
              ENDDO
            ENDIF
          ENDDO
          CALL CHECK( nf90_def_var(ncid,TRIM(OVAR(IT)%NAME), &
                                   vartype, DIMD3D, var_varid(IV)) )
        ELSE IF ( ALLOCATED(OVARV(IT)%FLD4D) ) THEN
          ICHCK=1
          IV=IV+1
          IDIM=0
          DO IS=1,ISMX
            CTMP=OVAR(IT)%DIM(IS)
            IF ( CTMP /= CNA ) THEN
              IDIM=IDIM+1
              DO ND=1,NDIM
                IF ( TRIM(CTMP) == TRIM(DIM(ND)%NAME) ) THEN
                  DIMD4D(IDIM)=lvl_varid(ND)
                ENDIF
              ENDDO
            ENDIF
          ENDDO
          CALL CHECK( nf90_def_var(ncid,TRIM(OVAR(IT)%NAME), &
                                   vartype, DIMD4D, var_varid(IV)) )
        ENDIF
!
!       * ASSIGN ATTRIBUTES TO THE VARIABLE.
!
        IF ( ICHCK == 1 ) THEN
          CALL CHECK( nf90_put_att(ncid, var_varid(IV), "units"     , TRIM(OVAR(IT)%UNITS) ) )
          CALL CHECK( nf90_put_att(ncid, var_varid(IV), "long_name" , TRIM(OVAR(IT)%LNAME) ) )
!           CALL CHECK( nf90_put_att(ncid, var_varid(IV), "_FillValue", YNAX) )
          call check( NF90_DEF_VAR_DEFLATE( ncid, var_varid(IV), 1, 1, 2 ) )                              ! ??? add compression level 2
        ENDIF
      ENDDO
!
!     * END DEFINE MODE.
!
      CALL CHECK( nf90_enddef(ncid) )
!
!     * WRITE THE LEVEL DATA.
!
      DO ND=1,NDIM
        IF ( DIM(ND)%FLGO == 1 ) THEN
          CALL CHECK( nf90_put_var(ncid, lvl_varid(ND), DIM(ND)%VAL) )
        ENDIF
      ENDDO
!
!     * WRITE DATA FOR EACH FIELD.
!
      IV=0
      DO IT=1,ITOT
        IF ( ABS(OVARV(IT)%FLD0D+YNAX) > YSMALLX ) THEN
          IV=IV+1
          CALL CHECK( nf90_put_var(ncid, var_varid(IV), &
                      OVARV(IT)%FLD0D) )
        ELSE IF ( ALLOCATED(OVARV(IT)%FLD1D) ) THEN
          IV=IV+1
          CALL CHECK( nf90_put_var(ncid, var_varid(IV), &
                      OVARV(IT)%FLD1D) )
        ELSE IF ( ALLOCATED(OVARV(IT)%FLD2D) ) THEN
          IV=IV+1
          CALL CHECK( nf90_put_var(ncid, var_varid(IV), &
                      OVARV(IT)%FLD2D) )
        ELSE IF ( ALLOCATED(OVARV(IT)%FLD3D) ) THEN
          IV=IV+1
          CALL CHECK( nf90_put_var(ncid, var_varid(IV), &
                      OVARV(IT)%FLD3D) )
        ELSE IF ( ALLOCATED(OVARV(IT)%FLD4D) ) THEN
          IV=IV+1
          CALL CHECK( nf90_put_var(ncid, var_varid(IV), &
                      OVARV(IT)%FLD4D) )
        ENDIF
      ENDDO
!
!     * CLOSE THE FILE.
!
      CALL CHECK( nf90_close(ncid) )
!
      CONTAINS
      SUBROUTINE CHECK(status)
      integer, intent (in) :: status

      if(status /= nf90_noerr) then
        print *, trim(nf90_strerror(status))
        stop 2
      end if
      END SUBROUTINE CHECK
      END SUBROUTINE WNCDAT


      SUBROUTINE RDNCDATRS(FILE_NAME)
!
      USE IODAT
      USE NETCDF
!
      IMPLICIT NONE
!
      INTEGER :: ND,IT,IV
      CHARACTER(LEN=*), INTENT(IN) :: FILE_NAME
      INTEGER :: ncid, fil_varid
      INTEGER, DIMENSION(NDIM) :: lvl_dimid, lvl_varid
      INTEGER, DIMENSION(ITOT) :: var_varid
      character(len=64) :: str_units, str_long

      print *,'RDNCDATRS. Open file:', FILE_NAME
!
      CALL CHECK( nf90_open(TRIM(FILE_NAME)//'.nc', nf90_nowrite, ncid) )
!
!     * ALLOCATE MEMORY.
!
      ALLOCATE(DIMT(NDIM))
!
!     * READ THE DIMENSIONS.
!
      DO ND=1,NDIM
        IF ( DIM(ND)%FLGRS == 1 ) THEN
          CALL CHECK( nf90_inq_dimid(ncid, TRIM(DIM(ND)%NAME), lvl_dimid(ND)) )
          CALL CHECK( nf90_inquire_dimension(ncid, lvl_dimid(ND), len=DIMT(ND)%SIZE) )
          IF ( DIMT(ND)%SIZE /= DIM(ND)%SIZE ) THEN
            PRINT*,'CONFLICTING FIELD DIMENSION ',TRIM(DIM(ND)%NAME)
            CALL XIT('RDNCDATRS',-1)
          ENDIF
        ENDIF
      ENDDO
!
!      FIND AND READ FIELDS.
!
      IV=0
      DO IT=1,ITOT
        IF ( ABS(RVARV(IT)%FLD0D+YNAX) > YSMALLX ) THEN
          IV=IV+1
          CALL CHECK( nf90_inq_varid(ncid, TRIM(RVAR(IT)%NAME), var_varid(IV)) )
          CALL CHECK( nf90_get_var  (ncid, var_varid(IV), RVARV(IT)%FLD0D) )
          CALL CHECK( nf90_get_att  (ncid, var_varid(IV), "units"    , str_units) )
          CALL CHECK( nf90_get_att  (ncid, var_varid(IV), "long_name", str_long) )
          print '(i4," ",a,a8,a60,a12,es16.2)', it, 'RDNCDATRS. Read 0D var: ', RVAR(IT)%NAME, str_long, str_units, RVARV(IT)%FLD0D
        ELSE IF ( ALLOCATED(RVARV(IT)%FLD1D) ) THEN
          IV=IV+1
          CALL CHECK( nf90_inq_varid(ncid,TRIM(RVAR(IT)%NAME), var_varid(IV)) )
          CALL CHECK( nf90_get_var  (ncid, var_varid(IV), RVARV(IT)%FLD1D) )
          CALL CHECK( nf90_get_att  (ncid, var_varid(IV), "units"    , str_units) )
          CALL CHECK( nf90_get_att  (ncid, var_varid(IV), "long_name", str_long) )
          print '(i4," ",a,a8,a60,a12,es16.2)', it, 'RDNCDATRS. Read 1D var: ', RVAR(IT)%NAME, str_long, str_units, sum(RVARV(IT)%FLD1D)/size(RVARV(IT)%FLD1D)
        ELSE IF ( ALLOCATED(RVARV(IT)%FLD2D) ) THEN
          IV=IV+1
          CALL CHECK( nf90_inq_varid(ncid,TRIM(RVAR(IT)%NAME), var_varid(IV)) )
          CALL CHECK( nf90_get_var(ncid, var_varid(IV), RVARV(IT)%FLD2D) )
          CALL CHECK( nf90_get_att  (ncid, var_varid(IV), "units"    , str_units) )
          CALL CHECK( nf90_get_att  (ncid, var_varid(IV), "long_name", str_long) )
          print '(i4," ",a,a8,a60,a12,es16.2)', it, 'RDNCDATRS. Read 2D var: ', RVAR(IT)%NAME, str_long, str_units, sum(RVARV(IT)%FLD2D)/size(RVARV(IT)%FLD2D)
        ELSE IF ( ALLOCATED(RVARV(IT)%FLD3D) ) THEN
          IV=IV+1
          CALL CHECK( nf90_inq_varid(ncid,TRIM(RVAR(IT)%NAME), var_varid(IV)) )
          CALL CHECK( nf90_get_var(ncid, var_varid(IV), RVARV(IT)%FLD3D) )
          CALL CHECK( nf90_get_att  (ncid, var_varid(IV), "units"    , str_units) )
          CALL CHECK( nf90_get_att  (ncid, var_varid(IV), "long_name", str_long) )
          print '(i4," ",a,a8,a60,a12,es16.2)', it, 'RDNCDATRS. Read 3D var: ', RVAR(IT)%NAME, str_long, str_units, sum(RVARV(IT)%FLD3D)/size(RVARV(IT)%FLD3D)
        ELSE IF ( ALLOCATED(RVARV(IT)%FLD4D) ) THEN
          IV=IV+1
          CALL CHECK( nf90_inq_varid(ncid,TRIM(RVAR(IT)%NAME), var_varid(IV)) )
          CALL CHECK( nf90_get_var(ncid, var_varid(IV), RVARV(IT)%FLD4D) )
          CALL CHECK( nf90_get_att  (ncid, var_varid(IV), "units"    , str_units) )
          CALL CHECK( nf90_get_att  (ncid, var_varid(IV), "long_name", str_long) )
          print '(i4," ",a,a8,a60,a12,es16.2)', it, 'RDNCDATRS. Read 4D var: ', RVAR(IT)%NAME, str_long, str_units, sum(RVARV(IT)%FLD4D)/size(RVARV(IT)%FLD4D)
        ENDIF
      ENDDO
!
!     * DEALLOCATION.
!
      DEALLOCATE(DIMT)
!
      CONTAINS
      SUBROUTINE CHECK(status)
      integer, intent (in) :: status

      if(status /= nf90_noerr) then
        print *, trim(nf90_strerror(status))
        stop 2
      end if
      END SUBROUTINE CHECK
      END SUBROUTINE RDNCDATRS


      SUBROUTINE RDNCDAT(FILE_NAME)
!
      USE IODAT
      USE NETCDF
!
      IMPLICIT NONE
!
      INTEGER :: ND,IT,IV
      CHARACTER(LEN=*), INTENT(IN) :: FILE_NAME
      INTEGER :: ncid, fil_varid
      INTEGER, DIMENSION(NDIM) :: lvl_dimid, lvl_varid
      INTEGER, DIMENSION(ITOT) :: var_varid
      character(len=64) :: str_units, str_long

      print *,'RDNCDAT. Open file:', FILE_NAME
!
      CALL CHECK( nf90_open(TRIM(FILE_NAME)//'.nc', nf90_nowrite, ncid) )
!
!     * ALLOCATE MEMORY.
!
      ALLOCATE(DIMT(NDIM))
!
!     * READ THE DIMENSIONS.
!
      DO ND=1,NDIM
        IF ( DIM(ND)%FLGI == 1 ) THEN
          CALL CHECK( nf90_inq_dimid(ncid, TRIM(DIM(ND)%NAME), lvl_dimid(ND)) )
          CALL CHECK( nf90_inquire_dimension(ncid, lvl_dimid(ND), len=DIMT(ND)%SIZE) )
          IF ( DIMT(ND)%SIZE /= DIM(ND)%SIZE ) THEN
            PRINT *,'CONFLICTING FIELD DIMENSION ',TRIM(DIM(ND)%NAME)
            CALL XIT('RDNCDATRS',-2)
          ENDIF
        ENDIF
      ENDDO
!
!      FIND AND READ FIELDS.
!
      IV=0
      DO IT=1,ITOT
        IF ( ABS(IVARV(IT)%FLD0D+YNAX) > YSMALLX ) THEN
          IV=IV+1
          CALL CHECK( nf90_inq_varid(ncid, TRIM(IVAR(IT)%NAME), var_varid(IV)) )
          CALL CHECK( nf90_get_var  (ncid, var_varid(IV), IVARV(IT)%FLD0D) )
          CALL CHECK( nf90_get_att  (ncid, var_varid(IV), "units"    , str_units) )
          CALL CHECK( nf90_get_att  (ncid, var_varid(IV), "long_name", str_long) )
          print '(i4," ",a,a8,a60,a12,es16.2)', it, 'RDNCDAT. Read 0D var: ', IVAR(IT)%NAME, str_long, str_units, IVARV(IT)%FLD0D
        ELSE IF ( ALLOCATED(IVARV(IT)%FLD1D) ) THEN
          IV=IV+1
          CALL CHECK( nf90_inq_varid(ncid, TRIM(IVAR(IT)%NAME), var_varid(IV)) )
          CALL CHECK( nf90_get_var(ncid, var_varid(IV), IVARV(IT)%FLD1D) )
          CALL CHECK( nf90_get_att  (ncid, var_varid(IV), "units"    , str_units) )
          CALL CHECK( nf90_get_att  (ncid, var_varid(IV), "long_name", str_long) )
          print '(i4," ",a,a8,a60,a12,es16.2)', it, 'RDNCDAT. Read 1D var: ', IVAR(IT)%NAME, str_long, str_units, sum(IVARV(IT)%FLD1D)/size(IVARV(IT)%FLD1D)
        ELSE IF ( ALLOCATED(IVARV(IT)%FLD2D) ) THEN
          IV=IV+1
          CALL CHECK( nf90_inq_varid(ncid, TRIM(IVAR(IT)%NAME), var_varid(IV)) )
          CALL CHECK( nf90_get_var(ncid, var_varid(IV), IVARV(IT)%FLD2D) )
          CALL CHECK( nf90_get_att  (ncid, var_varid(IV), "units"    , str_units) )
          CALL CHECK( nf90_get_att  (ncid, var_varid(IV), "long_name", str_long) )
          print '(i4," ",a,a8,a60,a12,es16.2)', it, 'RDNCDAT. Read 2D var: ', IVAR(IT)%NAME, str_long, str_units, sum(IVARV(IT)%FLD2D)/size(IVARV(IT)%FLD2D)
        ELSE IF ( ALLOCATED(IVARV(IT)%FLD3D) ) THEN
          IV=IV+1
          CALL CHECK( nf90_inq_varid(ncid, TRIM(IVAR(IT)%NAME), var_varid(IV)) )
          CALL CHECK( nf90_get_var(ncid, var_varid(IV), IVARV(IT)%FLD3D) )
          CALL CHECK( nf90_get_att  (ncid, var_varid(IV), "units"    , str_units) )
          CALL CHECK( nf90_get_att  (ncid, var_varid(IV), "long_name", str_long) )
          print '(i4," ",a,a8,a60,a12,es16.2)', it, 'RDNCDAT. Read 3D var: ', IVAR(IT)%NAME, str_long, str_units, sum(IVARV(IT)%FLD3D)/size(IVARV(IT)%FLD3D)
        ELSE IF ( ALLOCATED(IVARV(IT)%FLD4D) ) THEN
          IV=IV+1
          CALL CHECK( nf90_inq_varid(ncid, TRIM(IVAR(IT)%NAME), var_varid(IV)) )
          CALL CHECK( nf90_get_var(ncid, var_varid(IV), IVARV(IT)%FLD4D) )
          CALL CHECK( nf90_get_att  (ncid, var_varid(IV), "units"    , str_units) )
          CALL CHECK( nf90_get_att  (ncid, var_varid(IV), "long_name", str_long) )
          print '(i4," ",a,a8,a60,a12,es16.2)', it, 'RDNCDAT. Read 4D var: ', IVAR(IT)%NAME, str_long, str_units, sum(IVARV(IT)%FLD4D)/size(IVARV(IT)%FLD4D)
        ENDIF
      ENDDO
!
!     * DEALLOCATION.
!
      DEALLOCATE(DIMT)
!
      CONTAINS
      SUBROUTINE CHECK(status)
      integer, intent (in) :: status

      if(status /= nf90_noerr) then
        print *, trim(nf90_strerror(status))
        stop 2
      end if
      END SUBROUTINE CHECK
      END SUBROUTINE RDNCDAT


      SUBROUTINE WNCDATRS(FILE_NAME)
!
      USE IODAT
      USE NETCDF
!
      IMPLICIT NONE
!
      INTEGER :: IS,IT,ND,IV,ICHCK,IDIM

      CHARACTER(LEN=*), INTENT(IN) :: FILE_NAME
      INTEGER :: ncid, fil_varid, vartype
      INTEGER, DIMENSION(NDIM) :: lvl_dimid, lvl_varid
      INTEGER, DIMENSION(ITOT) :: var_varid
      INTEGER, DIMENSION(1) :: DIMD1D, CNT1D
      INTEGER, DIMENSION(2) :: DIMD2D, CNT2D
      INTEGER, DIMENSION(3) :: DIMD3D, CNT3D
      INTEGER, DIMENSION(4) :: DIMD4D, CNT4D

      vartype = NF90_REAL8
!
!     * CREATE OUTPUT FILE.
!
      CALL CHECK (nf90_create(TRIM(FILE_NAME)//'.nc', NF90_HDF5, ncid) )                        ! ??? was nf90_clobber

!
!     * DEFINE THE DIMENSIONS.
!
      DO ND=1,NDIM
        IF ( DIM(ND)%FLGRS == 1 ) THEN
          CALL CHECK( nf90_def_dim(ncid, TRIM(DIM(ND)%NAME), DIM(ND)%SIZE, lvl_dimid(ND)) )
        ENDIF
      ENDDO
!
!     * DEFINE THE COORDINDATE VARIABLES.
!
      DO ND=1,NDIM
        IF ( DIM(ND)%FLGRS == 1 ) THEN
          CALL CHECK( nf90_def_var(ncid, TRIM(DIM(ND)%NAME), &
                                   vartype, lvl_dimid(ND), &
                                   lvl_varid(ND)) )
        ENDIF
      ENDDO
!
!     * ASSIGN ATTRIBUTES TO COORDINATE VARIABLES.
!
      DO ND=1,NDIM
        IF ( DIM(ND)%FLGRS == 1 ) THEN
          CALL CHECK( nf90_put_att(ncid, lvl_varid(ND), "units", &
                                   TRIM(DIM(ND)%UNITS)) )
          CALL CHECK( nf90_put_att(ncid, lvl_varid(ND), "long_name", &
                                    TRIM(DIM(ND)%LNAME)) )
        ENDIF
      ENDDO
!
!      DEFINE THE NETCDF VARIABLES FOR EACH FIELD.
!
      IV=0
      DO IT=1,ITOT
        ICHCK=0
        IF ( ABS(RVARV(IT)%FLD0D+YNAX) > YSMALLX ) THEN
          ICHCK=1
          IV=IV+1
          CALL CHECK( nf90_def_var(ncid,TRIM(RVAR(IT)%NAME), vartype, var_varid(IV)) )
        ELSE IF ( ALLOCATED(RVARV(IT)%FLD1D) ) THEN
          ICHCK=1
          IV=IV+1
          IDIM=0
          DO IS=1,ISMX
            CTMP=RVAR(IT)%DIM(IS)
            IF ( CTMP /= CNA ) THEN
              IDIM=IDIM+1
              DO ND=1,NDIM
                IF ( TRIM(CTMP) == TRIM(DIM(ND)%NAME) ) THEN
                  DIMD1D(IDIM)=lvl_varid(ND)
                ENDIF
              ENDDO
            ENDIF
          ENDDO
          CALL CHECK( nf90_def_var(ncid,TRIM(RVAR(IT)%NAME), vartype, DIMD1D, var_varid(IV)) )
        ELSE IF ( ALLOCATED(RVARV(IT)%FLD2D) ) THEN
          ICHCK=1
          IV=IV+1
          IDIM=0
          DO IS=1,ISMX
            CTMP=RVAR(IT)%DIM(IS)
            IF ( CTMP /= CNA ) THEN
              IDIM=IDIM+1
              DO ND=1,NDIM
                IF ( TRIM(CTMP) == TRIM(DIM(ND)%NAME) ) THEN
                  DIMD2D(IDIM)=lvl_varid(ND)
                ENDIF
              ENDDO
            ENDIF
          ENDDO
          CALL CHECK( nf90_def_var(ncid,TRIM(RVAR(IT)%NAME), vartype, DIMD2D, var_varid(IV)) )
        ELSE IF ( ALLOCATED(RVARV(IT)%FLD3D) ) THEN
          ICHCK=1
          IV=IV+1
          IDIM=0
          DO IS=1,ISMX
            CTMP=RVAR(IT)%DIM(IS)
            IF ( CTMP /= CNA ) THEN
              IDIM=IDIM+1
              DO ND=1,NDIM
                IF ( TRIM(CTMP) == TRIM(DIM(ND)%NAME) ) THEN
                  DIMD3D(IDIM)=lvl_varid(ND)
                ENDIF
              ENDDO
            ENDIF
          ENDDO
          CALL CHECK( nf90_def_var(ncid,TRIM(RVAR(IT)%NAME), vartype, DIMD3D, var_varid(IV)) )
        ELSE IF ( ALLOCATED(RVARV(IT)%FLD4D) ) THEN
          ICHCK=1
          IV=IV+1
          IDIM=0
          DO IS=1,ISMX
            CTMP=RVAR(IT)%DIM(IS)
            IF ( CTMP /= CNA ) THEN
              IDIM=IDIM+1
              DO ND=1,NDIM
                IF ( TRIM(CTMP) == TRIM(DIM(ND)%NAME) ) THEN
                  DIMD4D(IDIM)=lvl_varid(ND)
                ENDIF
              ENDDO
            ENDIF
          ENDDO
          CALL CHECK( nf90_def_var(ncid,TRIM(RVAR(IT)%NAME), vartype, DIMD4D, var_varid(IV)) )
        ENDIF
!
!       * ASSIGN ATTRIBUTES TO THE VARIABLE.
!
        IF ( ICHCK == 1 ) THEN
          CALL CHECK( nf90_put_att(ncid, var_varid(IV), "units"     , TRIM(RVAR(IT)%UNITS) ) )
          CALL CHECK( nf90_put_att(ncid, var_varid(IV), "long_name" , TRIM(RVAR(IT)%LNAME) ) )
!           CALL CHECK( nf90_put_att(ncid, var_varid(IV), "_FillValue", YNAX) )
          call check( NF90_DEF_VAR_DEFLATE( ncid, var_varid(IV), 1, 1, 9 ) )                              ! ??? add compression
        ENDIF
      ENDDO
!
!     * END DEFINE MODE.
!
      CALL CHECK( nf90_enddef(ncid) )
!
!     * WRITE THE LEVEL DATA.
!
      DO ND=1,NDIM
        IF ( DIM(ND)%FLGRS == 1 ) THEN
          CALL CHECK( nf90_put_var(ncid, lvl_varid(ND), DIM(ND)%VAL) )
        ENDIF
      ENDDO
!
!     * WRITE DATA FOR EACH FIELD.
!
      IV=0
      DO IT=1,ITOT
        IF ( ABS(RVARV(IT)%FLD0D+YNAX) > YSMALLX ) THEN
          IV=IV+1
          CALL CHECK( nf90_put_var(ncid, var_varid(IV), &
                      RVARV(IT)%FLD0D) )
        ELSE IF ( ALLOCATED(RVARV(IT)%FLD1D) ) THEN
          IV=IV+1
          CALL CHECK( nf90_put_var(ncid, var_varid(IV), &
                      RVARV(IT)%FLD1D) )
        ELSE IF ( ALLOCATED(RVARV(IT)%FLD2D) ) THEN
          IV=IV+1
          CALL CHECK( nf90_put_var(ncid, var_varid(IV), &
                      RVARV(IT)%FLD2D) )
        ELSE IF ( ALLOCATED(RVARV(IT)%FLD3D) ) THEN
          IV=IV+1
          CALL CHECK( nf90_put_var(ncid, var_varid(IV), &
                      RVARV(IT)%FLD3D) )
        ELSE IF ( ALLOCATED(RVARV(IT)%FLD4D) ) THEN
          IV=IV+1
          CALL CHECK( nf90_put_var(ncid, var_varid(IV), &
                      RVARV(IT)%FLD4D) )
        ENDIF
      ENDDO
!
!     * CLOSE THE FILE.
!
      CALL CHECK( nf90_close(ncid) )
!
      CONTAINS
      SUBROUTINE CHECK(status)
      integer, intent (in) :: status

      if(status /= nf90_noerr) then
        print *, trim(nf90_strerror(status))
        stop 2
      end if
      END SUBROUTINE CHECK
      END SUBROUTINE WNCDATRS
#endif
