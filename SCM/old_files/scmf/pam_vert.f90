      SUBROUTINE VRTDF(DXDT,XROW,DTDT,THROW,DQDT,QROW,DQLDT,QLWC, &
                       DUDT,U,V,WSUB,RKH,RKM,RI,CDM,USTAR,ZSPD, &
                       ZSPDA,ZF,PF,TF,ZH,PH,DP,ALMC,ALMX,PBLT,TVP, &
                       QSENS,QLAT,ZCLF,TZERO,THLIQG,ZO,SHTJ,SHJ,DSHJ, &
                       PRESSG,DT,ZTOP,IL1,IL2,ILG,ILEV,ILEVP1, &
                       MSGP,NTRAC)
!
      USE SDPHYS
!
      IMPLICIT REAL (A-H,O-Z), INTEGER (I-N)
!
      INTEGER, INTENT(IN) :: ILG,ILEV,ILEVP1,IL1,IL2,MSGP,NTRAC
      REAL, INTENT(OUT), DIMENSION(ILG) :: PBLT, CDM, ZSPD, ZSPDA, &
                                           QSENS, QLAT
      REAL, INTENT(INOUT), DIMENSION(ILG) :: USTAR, ZO
      REAL, INTENT(OUT), DIMENSION(ILG,ILEV) :: ALMC,ALMX,RKH,RKM,RI, &
                                                WSUB,TVP
      REAL, INTENT(INOUT), DIMENSION(ILG,ILEV) :: ZCLF
      REAL, INTENT(IN) :: DT
      REAL, INTENT(IN), DIMENSION(ILG) :: PRESSG, TZERO, THLIQG
      REAL, INTENT(IN), DIMENSION(ILG,ILEV) :: SHJ,DSHJ,TF,ZH,PH, &
                                               ZF,PF,DP
      REAL, INTENT(IN), DIMENSION(ILG,ILEVP1) :: SHTJ
      REAL, DIMENSION(ILG,ILEVP1), INTENT(INOUT) :: THROW,QROW
      REAL, DIMENSION(ILG,ILEV), INTENT(IN) :: QLWC
      REAL, DIMENSION(ILG,ILEV), INTENT(INOUT) :: U,V
      REAL, DIMENSION(ILG,ILEV,NTRAC) :: RKX
      REAL, DIMENSION(ILG,ILEVP1,NTRAC), INTENT(IN) :: XROW
      REAL, DIMENSION(ILG,ILEVP1,NTRAC), INTENT(OUT) :: DXDT
      REAL, DIMENSION(ILG,ILEVP1), INTENT(OUT) :: DQDT,DTDT
      REAL, DIMENSION(ILG,ILEV), INTENT(OUT) :: DQLDT
      REAL, DIMENSION(ILG,ILEV), INTENT(OUT) :: DUDT
!
      REAL, DIMENSION(ILG,ILEV) :: A,B,C,WORK,XROWT,DXDTT,RKXT,QT,HMN
      REAL, DIMENSION(ILG) :: UN,ZER,CL,CDVLT,RAUS,VINT,THT
      REAL, DIMENSION(ILG,ILEV) :: CVAR, ZCRAUT, QCWVAR, CVSG, CVDU, &
                                   ZFRAC, SHXKJ, DVDS, DTTDS, ALMXT
      REAL, DIMENSION(ILG,ILEVP1) :: SHTXKJ
      REAL, DIMENSION(ILG) :: PMBS, RRL, CPM, ALMIX, DHLDZ, DRWDZ, &
                              FLAGEST, QCW, SSH, VMODL, CDVLM, DVDZ
      REAL, DIMENSION(ILG) :: CDH, ZOSCLM, ZOSCLH, TVIRTS, TVIRTA, &
                              CRIB, CFLUX, EVAP, QZERO, PBLTI, QTG, &
                              HMNG
      INTEGER, DIMENSION(ILG) :: IPBL
      REAL, PARAMETER :: VKC=0.41, TVFA=0.608, SCALF=0.8

!
!-----------------------------------------------------------------------
!     * SURFACE PROPERTIES FOR BARE GROUND OR WATER.
!
      TFREZ=273.16
      DO I=IL1,IL2
         ZSPD(I)=SQRT(U(I,ILEV)**2+V(I,ILEV)**2) ! SURFACE WIND SPEED
         ESW=1.E+02*EXP(RW1+RW2/TZERO(I))*(TZERO(I)**RW3)
         WZERO=EPS1*ESW/(PRESSG(I)-ESW)
         Q0SAT=WZERO
!         IF( TZERO(I) >= TFREZ ) THEN
!           AC=17.269
!           BC=35.86
!         ELSE
!           AC=21.874
!           BC=7.66
!         ENDIF
!         WZERO=EPS1*611.0*EXP(AC*(TZERO(I)-TFREZ)/
!     1              (TZERO(I)-BC))/PRESSG(I)
!
!         Q0SAT=WZERO/(1.0+WZERO)     ! SATURATED SPECIFIC HUMIDITY
         IF( THLIQG(I)> 0.99 ) THEN  ! WATER SURFACE
           EVBETA=1.0
           QZERO(I)=Q0SAT
           ZO(I)=(0.016/GRAV)*USTAR(I)**2
         ELSE                        ! BARE SOIL
           THLMIN=0.1                ! MIMIMUM SOIL WATER CONTENT (M3/M3)
           THFC=0.4                  ! FIELD CAPACITY (M3/M3)
           IF( THLIQG(I) < (THLMIN+0.001) ) THEN
             IEVAP=0
             CEVAP=0.0
           ELSEIF( THLIQG(I) > THFC ) THEN
             IEVAP=1
             CEVAP=1.0
           ELSE
             IEVAP=1
             CEVAP=0.25*(1.0-COS(3.14159*THLIQG(I)/THFC))**2
           ENDIF
           EVBETA=CEVAP              ! SURFACE EVAPORATION EFFICIENCY
           QZERO(I)=EVBETA*Q0SAT+(1.0-EVBETA)*QROW(I,ILEVP1)
           IF( QZERO(I) > QROW(I,ILEVP1) .AND. IEVAP == 0) THEN
             EVBETA=0.0
             QZERO(I)=QROW(I,ILEVP1)
           ENDIF
         ENDIF
!         TVIRTS(I)=TZERO(I)*(1.0+0.61*QZERO(I))
         TVIRTS(I)=TZERO(I)*(1.+TVFA*QZERO(I)/(1.-QZERO(I)))
      ENDDO
      DO I=IL1,IL2
        ZRSLD=ZH(I,ILEVP1)+ZO(I)
        ZOSCLM(I)=ZO(I)/ZRSLD
        ZOSCLH(I)=ZOSCLM(I)
!        TVIRTA(I)=THROW(I,ILEVP1)*(1.0+0.61*QROW(I,ILEVP1))
        TVIRTA(I)=THROW(I,ILEVP1)*(1.+TVFA*QROW(I,ILEVP1) &
                                    /(1.-QROW(I,ILEVP1)))
        CRIB(I)=-GRAV*ZRSLD/(TVIRTA(I)*ZSPD(I)**2)
      ENDDO
!
!     * SURFACE DRAG COEFFICIENTS.
!
      CALL DRCOEF(CDM,CDH,CFLUX,ZOSCLM,ZOSCLH,CRIB,TVIRTS, &
                  TVIRTA,ZSPD,GRAV,VKC,ILG,IL1,IL2)
      USTAR=ZSPD*SQRT(CDM)
!
!     * SURFACE LATENT AND SENSIBLE HEAT FLUXES, AND EVAPORATION.
!
      DO I=IL1,IL2
        ZRHO0=PH(I,ILEVP1)/(RGAS*THROW(I,ILEVP1))
!!!!
        QSENS(I)=ZRHO0*CPRES*CFLUX(I)*(TZERO(I)-THROW(I,ILEVP1))
        EVAP(I)=ZRHO0*CFLUX(I)*(QZERO(I)-QROW(I,ILEVP1))
!
!        QSENS(I)=0.
!        EVAP(I)=0./RL
!
        QLAT(I)=RL*EVAP(I)
      ENDDO
!
!     * ANEMOMETER WIND SPEED (M/SEC).
!
      DO I=IL1,IL2
        ZRUF=ZO(I)
        ZANNOM=MAX(ZRUF,10.0)
        RATFCA=LOG(ZANNOM/ZRUF)/VKC
        RATFCA1=RATFCA*SQRT(CDM(I))
        RATFCA1=MIN(RATFCA1,1.)
        ZSPDA(I)=RATFCA1*ZSPD(I)
      ENDDO
!
!     * INSERT FLUXES INTO FIRST ATMOSPHERIC LEVEL ABOVE GROUND.
!
      DTDT=0.
      DQDT=0.
      DO I=IL1,IL2
        ZRHO0=PH(I,ILEVP1)/(RGAS*THROW(I,ILEVP1))
        FAC=GRAV/DP(I,ILEV)
!        THROW(I,ILEVP1)=THROW(I,ILEVP1)+DT*FAC*QSENS(I)/CPRES
!        QROW(I,ILEVP1)=QROW(I,ILEVP1)+DT*FAC*EVAP(I)
        DTDT(I,ILEVP1)=FAC*QSENS(I)/CPRES
        DQDT(I,ILEVP1)=FAC*EVAP(I)
!!!!
!        DU=DT*FAC*ZRHO0*CDM(I)*MAX(VMIN,ZSPD(IL))*U(I,ILEV)
!        U(I,ILEV)=U(I,ILEV)-DU
!        IF ( U(I,ILEV) >= 0. ) THEN
!          U(I,ILEV)=MAX(U(I,ILEV),0.)
!        ELSE
!          U(I,ILEV)=MIN(U(I,ILEV),0.)
!        ENDIF
      ENDDO
!
!     * PARAMETERS FOR CALCULATION OF VERTICAL DIFFUSION.
!
      VMIN=0.001
      DO IL=IL1,IL2
        UN   (IL)=1.
        ZER  (IL)=0.
        RAUS (IL)=PRESSG(IL)/GRAV
        CDVLT(IL)=0.
        VMODL(IL)=MAX(VMIN,ZSPD(IL))
      ENDDO
      SHXKJ=SHJ**RGOCP
      SHTXKJ=SHTJ**RGOCP
      TAUADJ=DT
      DO I=IL1,IL2
         CDVLM(I)=CDM(I)*VMODL(I)
         HEAT=TAUADJ*GRAV*SHXKJ(I,ILEV)/(RGAS*THROW(I,ILEV))
         FACMOM=1./(1.+HEAT*CDVLM(I)/DSHJ(I,ILEV))
!
!         CDVLM(I)=CDVLT(I)
!         FACMOM=1.
!
         UTMP =U(I,ILEV)*FACMOM
         VTMP =V(I,ILEV)*FACMOM
         DVDS(I,ILEV) = SQRT((UTMP-U(I,ILEV-1))**2 &
                     +(VTMP-V(I,ILEV-1))**2)/(SHJ(I,ILEV)-SHJ(I,ILEV-1))
      ENDDO
!
!     * EVALUATE DTTDS=D(THETA)/D(SIGMA) AT TOP INTERFACE OF LAYERS.
!
      L=1
      DO I=IL1,IL2
         ALWC=QLWC(I,L)
         THETVP=THROW(I,L)*(1.+TVFA*QROW(I,L)/(1.-QROW(I,L)) &
               -(1.+TVFA)*ALWC)/SHXKJ(I,L)
         TVP(I,L)=THETVP
      ENDDO
      DO L=2,ILEV
      DO I=IL1,IL2
         ALWC=QLWC(I,L)
         THETVP=THROW(I,L)*(1.+TVFA*QROW(I,L)/(1.-QROW(I,L)) &
               -(1.+TVFA)*ALWC)/SHXKJ(I,L)
         ALWC=QLWC(I,L-1)
         THETVM=THROW(I,L-1)*(1.+TVFA*QROW(I,L-1) &
               /(1.-QROW(I,L))-(1.+TVFA)*ALWC)/SHXKJ(I,L-1)
!!!!
!         THETVP=THROW(I,L)/SHXKJ(I,L)
!         THETVM=THROW(I,L-1)/SHXKJ(I,L-1)
         DTTDS(I,L)=(THETVP-THETVM)/(SHJ(I,L)-SHJ(I,L-1))
         TVP(I,L)=THETVP
      ENDDO
      ENDDO
!
!     * DVDS = MOD(DV/DSIGMA) AT TOP INTERFACE OF LAYERS.
!
      DO L=1,ILEV-2
      DO I=IL1,IL2
         DVDS(I,L+1)=SQRT((U(I,L+1)-U(I,L))**2 &
                         +(V(I,L+1)-V(I,L))**2)/(SHJ(I,L+1)-SHJ(I,L))
      ENDDO
      ENDDO
      DO I=IL1,IL2
         PBLTI(I)=REAL(ILEV)
!         IPBL(I)=1
      ENDDO
!
!     * RMS WIND SHEAR, DVMINS=(R*T/G)*(DV/DZ)=7.9 FOR DV/DZ=1.MS-1KM-1.
!     * DETERMINE PLANETARY BOUNDARY LAYER TOP AS LEVEL INDEX ABOVE
!     * WHICH THE RICHARDSON NUMBER EXCEEDS THE CRITICAL VALUE OF 1.00.
!
      DVMINS=7.9
!      TAUR=3600.
      TAUR=6.*3600.
      DO L=ILEV,2,-1
      DO I=IL1,IL2
         DVDS(I,L)=MAX(DVDS(I,L),DVMINS)
         RIT=-RGAS*SHTXKJ(I,L)*DTTDS(I,L)/(SHTJ(I,L)*DVDS(I,L)**2)
!!!!
         RI(I,L)=(RI(I,L)+(DT/TAUR)*RIT)/(1.+DT/TAUR)
!         RI(I,L)=RIT
      ENDDO
      ENDDO
      DO L=ILEV,3,-1
      DO I=IL1,IL2
!!!!
!         IF ( RI(I,L) < 1. .AND. RI(I,L-1) > 1. ) THEN
         IF ( RI(I,L) < 10. .AND. RI(I,L-1) > 10. ) THEN
            PBLX=REAL(L)
            PBLTI(I) = MIN(PBLTI(I), PBLX)
         ENDIF
      ENDDO
      ENDDO
!!!!
      PBLT=PBLTI
!      PBLT=MAX((PBLT+(DT/TAUR)*PBLTI)/(1.+DT/TAUR),2.)
!
      FACT=VKC*RGAS*273.
      XLMIN=10.
      BEEM=10.
      GAMRH=6.
      GAMRM=6.
      ALFAH=1.
      RINEUT=1.
      RIINF=0.25
      PRANDTL_MIN=1.
      PRANDTL_MAX=3.
      WSUB=0.
      DO L=2,ILEV
!
!        * CALCULATE THE DIFFUSION COEFFICIENTS (RKH,RKM)
!        * AT THE TOP INTERFACE OF LAYERS.
!        * HEAT: FINITE STABILITY CUTOFF.
!        * LOW XINGL USED SINCE PREVIOUSLY MIXED "INSTANTANEOUSLY".
!        * MOMENTUM: FINITE STABILITY CUTOFF.
!        * HIGH XINGL USED SINCE NOT PREVIOUSLY MIXED "INSTANTANEOUSLY".
!        * UNSTABLE CASE (RI <= 0.) - STABLE CASE (RI > 0.) .
!
         DO I=IL1,IL2
            DVDZ(I)=DVDS(I,L)*GRAV*SHTJ(I,L)/(RGAS*TF(I,L-1))
         ENDDO
!
         DO I=IL1,IL2
            FACT0=FACT*LOG(SHTJ(I,L))
            SARI=SQRT(ABS(RI(I,L)))
            LPBL=NINT(PBLT(I))
!
!           * LOWER CUTOFFS.
!
            ALL=0.5*VKC*ZF(I,LPBL-1)
            XIMINT=MAX(75.*ALL/(75.+ALL),XLMIN)
            ALL=0.5*VKC*ZH(I,L)
            XIMIN =MAX(75.*ALL/(75.+ALL),XLMIN)
!
!           * EFFECTIVE MIXING LENGTHS AS FUNCTION OF MIXING
!           * LENGTHS FOR UP- AND DOWNWARD MIXING.
!
            ALU=VKC*ZH(I,L)
            ALD=VKC*(ZF(I,LPBL-1)-ZH(I,L))
            IF ( ALD > 0. ) THEN
              XINGLH=MAX(ALU*ALD/(ALU+ALD),XIMIN)
              XINGLM=MAX(ALU*ALD/(ALU+ALD),XIMIN)
              ALMC(I,L)=XINGLH
            ELSE
              FAC=(PH(I,L)/PF(I,LPBL-1))**2.
              XINGLH=10.+(XIMINT-10.)*FAC
              XINGLM=10.+(XIMINT-10.)*FAC
              ALMC(I,L)=MIN(100.*FAC,VKC*(ZH(I,L)-ZF(I,LPBL-1)))
            END IF
            ALMX(I,L)=XINGLH
            ALMXT(I,L)=MAX(ALMX(I,L),ALMC(I,L),10.)
            XINGLH=(1.-ZCLF(I,L))*XINGLH+ZCLF(I,L)*ALMXT(I,L)
            XINGLM=XINGLH
!
!           * DIFFUSION COEFFICIENTS.
!
            ELH   = XINGLH**2
            EPSSH = 0.
            FACTH = 0.5*RI(I,L)*EPSSH*BEEM
            IF ( FACTH > 1. ) THEN
              XH  = 0.
            ELSE
              XH  = (1.-FACTH)**2
            ENDIF
            ELM   = XINGLM**2
            EPSSM = 0.
            FACTM = 0.5*RI(I,L)*EPSSM*BEEM
            IF ( FACTM > 1. ) THEN
              XM  = 0.
            ELSE
              XM  = (1.-FACTM)**2
            ENDIF
            IF ( RI(I,L) < 0. ) THEN
              RKH(I,L) = ELH*DVDZ(I)*(1.-ALFAH*GAMRH*BEEM*RI(I,L) / (GAMRH+ALFAH*BEEM*SARI))
              RKM(I,L) = ELM*DVDZ(I)*(1.-      GAMRM*BEEM*RI(I,L) / (GAMRM+      BEEM*SARI))
            ELSE
              RKH(I,L) = ELH*DVDZ(I)*XH/(1.+(1.-EPSSH)*BEEM*RI(I,L))
              RKM(I,L) = ELM*DVDZ(I)*XM/(1.+(1.-EPSSM)*BEEM*RI(I,L))
            ENDIF
!
!           * PRANDTL NUMBER SCALING FOR EDDY MOMENTUM DIFFUSIVITY,
!           * BASED ON APPROACH SUGGESTED BY SCHUMANN AND GERZ (1995).
!
            ATMP=0.
              IF ( RI(I,L) > 0. ) THEN
                ATMP=RINEUT*EXP(-RI(I,L)/(RINEUT*RIINF))
              ENDIF
            PRANDTL=ATMP+RI(I,L)/RIINF
            PRANDTL=MIN(MAX(PRANDTL,PRANDTL_MIN),PRANDTL_MAX)
            RKM(I,L)=RKM(I,L)*PRANDTL
!
!           * NO MIXING ABOVE SPECIFIED HEIGHT.
!
            IF ( ZH(I,L) > ZTOP ) THEN
!               RKH(I,L)=0.
!               RKM(I,L)=0.
            ENDIF
         ENDDO
      ENDDO
      DO I=IL1,IL2
         RKM(I,1)=RKM(I,2)
         RKH(I,1)=RKH(I,2)
      ENDDO
      DO N=1,NTRAC
        RKX(:,:,N)=RKH
      ENDDO
!
!     * DIAGNOSE SUBGRID-SCALE COMPONENT OF THE VERTICAL VELOCITY
!     * (STANDARD DEVIATION, GHAN ET AL., 1997). ACCORDING TO PENG
!     * ET AL. (2005), THE REPRESENTATIVE VERTICAL VELOCITY FOR
!     * AEROSOL ACTIVATION IS OBTAINED BY APPLYING A SCALING FACTOR
!     * TO THE VELOCITY STANDARD DEVIATION (SCALF).
!
      PI=3.141592653589793
      FACTS=SCALF*SQRT(2.*PI)
      DO L=2,ILEV
        WSUB(:,L)=FACTS*RKH(:,L)/(ZF(:,L-1)-ZF(:,L))
      ENDDO
!!!!
!      WSUB=10. &
! Fli ht 08
!      WSUB=0.05 &
! Fli ht 07, low
!      WSUB=0.1 &
! Fli ht 07, high
!      WSUB=0.2
!
!      WSUB=0.02
!
!     * MIX CHEMICAL TRACERS.
!
      DO N=1,NTRAC
        RKXT=RKX(:,:,N)
        THT=THROW(:,ILEVP1)
        CALL ABCVDQ6 ( A,B,C,CL,UN,CDVLT,GRAV, &
                       IL1,IL2,ILG,ILEV,ILEVP1,ILEV, &
                       RGAS,RKXT,SHTJ,SHJ,DSHJ, &
                       THT,TF,DT)
        XROWT=XROW(:,MSGP:ILEVP1,N)
        CALL IMPLVD7(A,B,C,CL,XROWT,ZER,IL1,IL2,ILG,ILEV, &
                     DT,DXDTT,DSHJ,RAUS,WORK,VINT)
        DXDT(:,MSGP:ILEVP1,N)=DXDTT(:,:)
      ENDDO
!
!     * MIX MOMENTUM.
!
      THT=THROW(:,ILEVP1)
      CALL ABCVDQ6 ( A,B,C,CL,UN,CDVLM,GRAV, &
                     IL1,IL2,ILG,ILEV,ILEVP1,ILEV, &
                     RGAS,RKM,SHTJ,SHJ,DSHJ, &
                     THT,TF,DT)
      CALL IMPLVD7(A,B,C,CL,U,ZER,IL1,IL2,ILG,ILEV, &
                   DT,DUDT,DSHJ,RAUS,WORK,VINT)
!
!     * TOTAL WATER MIXING RATIO AND CLOUD WATER STATIC ENERGY.
!
      QT=QROW(:,MSGP:ILEVP1)+QLWC
      HMN=CPRES*THROW(:,MSGP:ILEVP1)+GRAV*ZH-RL*QLWC
!
!     * LIQUID WATER STATIC ENERGY AND TOTAL WATER AT SURFACE.
!
      L=ILEV
      DO IL=IL1,IL2
        QTG(IL)=QZERO(IL)
        HMNG(IL)=CPRES*TZERO(IL)
      ENDDO
!
!     * MIX CLOUD WATER STATIC ENERGY AND TOTAL WATER.
!
      THT=THROW(:,ILEVP1)
      CALL ABCVDQ6 ( A,B,C,CL,UN,CDVLT,GRAV, &
                     IL1,IL2,ILG,ILEV,ILEVP1,ILEV, &
                     RGAS,RKH,SHTJ,SHJ,DSHJ, &
                     THT,TF,DT)
      CALL IMPLVD7(A,B,C,CL,HMN,HMNG,IL1,IL2,ILG,ILEV, &
                   DT,DXDTT,DSHJ,RAUS,WORK,VINT)
      HMN=HMN+DT*DXDTT
      CALL IMPLVD7(A,B,C,CL,QT,QTG,IL1,IL2,ILG,ILEV, &
                   DT,DXDTT,DSHJ,RAUS,WORK,VINT)
      QT=QT+DT*DXDTT
!
!     * CHECK CONSERVATION.
!
!      DQT=0.
!      DQ=0.
!      DO L=1,ILEV
!      DO I=1,1
!        DQT=DQT+DP(I,L)*DXDTT(I,L)/GRAV
!        DQ=DQ+DP(I,L)*QT(I,L)/GRAV
!      ENDDO
!      ENDDO
!      PRINT*,'DQ,DQT=',DQ,DQT
!
!     * TEMPERATURE AND WATER VAPOUR MIXING RATIO FROM STATISTICAL
!     * CLOUD SCHEME.
!
      FACTE=1./EPS1-1.
      I2NDIE=1
      FLAGEST=1.
      CVSG=0.
      CVDU=0.
      ZFRAC=0.
      ICVSG=1
      ISUBG=0
      ICALL=1
      DO JK=1,ILEV
        DO IL=1,ILG
          PMBS(IL)=0.01*PH(IL,JK)
          RRL(IL)=RL
          CPM(IL)=CPRES
          ALMIX(IL)=MAX(ALMC(IL,JK),ALMX(IL,JK),10.)
          IF(JK.GT.1)                              THEN
            DZ       =ZH  (IL,JK-1)-ZH (IL,JK)
            DHLDZ(IL)=(HMN(IL,JK-1)-HMN(IL,JK))/DZ
            DRWDZ(IL)=(QT (IL,JK-1)-QT (IL,JK))/DZ
          ELSE
            DHLDZ(IL)=0.
            DRWDZ(IL)=0.
          ENDIF
        ENDDO
        CALL STATCLD (QCW,ZCLF(1,JK),CVAR(1,JK),ZCRAUT(1,JK), &
                      QCWVAR(1,JK),SSH,CVSG(1,JK),QT(1,JK), &
                      HMN(1,JK),ZFRAC(1,JK),CPM, &
                      PMBS,ZH(1,JK),RRL,CVDU(1,JK),FLAGEST, &
                      ALMIX,DHLDZ,DRWDZ,I2NDIE, &
                      DT,ILEV,ILG,1,ILG,ICVSG,ISUBG, &
                      JK,ICALL)
!
        DO IL=1,ILG
          DTDT(IL,JK)=DTDT(IL,JK)+ &
                     ((HMN(IL,JK)-GRAV*ZH(IL,JK)+RRL(IL)*QCW(IL)) &
                       /CPM(IL)-THROW(IL,JK))/DT
          DQDT(IL,JK)=DQDT(IL,JK)+ &
                     ((QT(IL,JK)-QCW(IL))-QROW(IL,JK))/DT
          DQLDT(IL,JK)=(QCW(IL)-QLWC(IL,JK))/DT
        ENDDO
      ENDDO

!      DQT=0.
!      DQ=0.
!      DO JK=1,ILEV
!      DO IL=1,1
!        DQT=DQT+(QT(IL,JK)-QROW(IL,JK)-QLWC(IL,JK))*DP(IL,JK)/GRAV
!      ENDDO
!      ENDDO
!      PRINT*,'DQT=',DQT
!!!!
!        QROW(:,ILEVP1)=QZERO(:)
!        THROW(:,ILEVP1)=TZERO(:)
!
      END SUBROUTINE VRTDF


      SUBROUTINE DRCOEF(CDM,CDH,CFLUX,ZOMIN,ZOHIN,CRIB,TVIRTG, &
                        TVIRTA,VA,GRAV,VKC,ILG,IL1,IL2)
!
      IMPLICIT NONE

!     * CALCULATES DRAG COEFFICIENTS AND RELATED VARIABLES FOR CLASS.

!     * OUTPUT FIELDS ARE:
!     *    CDM    : STABILITY-DEPENDENT DRAG COEFFICIENT FOR MOMENTUM.
!     *    CDH    : STABILITY-DEPENDENT DRAG COEFFICIENT FOR HEAT.
!     *    CFLUX  : CD * MOD(V), BOUNDED BY FREE-CONVECTIVE LIMIT.
!     * INPUT FIELDS ARE:
!     *    ZOMIN/: ROUGHNESS HEIGHTS FOR MOMENTUM/HEAT NORMALIZED BY
!     *    ZOHIN   REFERENCE HEIGHT.
!     *    CRIB   : -RGAS*SLTHKEF/(VA**2), WHERE
!     *             SLTHKEF=-LOG(MAX(SGJ(ILEV),SHJ(ILEV)))
!     *    TVIRTG : "SURFACE" VIRTUAL TEMPERATURE.
!     *    TVIRTA : LOWEST LEVEL VIRTUAL TEMPERATURE.
!     *    VA     : AMPLITUDE OF LOWEST LEVEL WIND.
!     * OTHER VARIABLES ARE:
!     *    ZOM/  : WORK ARRAYS USED FOR SCALING ZOMIN/ZOHIN
!     *    ZOH     ON STABLE SIDE, AS PART OF CALCULATION.
!     *    RIB    : BULK RICHARDSON NUMBER.
!     *    USTAR  : FRICTION VELOCITY SCALE

!     * INTEGER CONSTANTS.

      INTEGER ILG,IL1,IL2,JL,I

!     * REAL CONSTANTS

      REAL GRAV,VKC

!     * OUTPUT ARRAYS.

      REAL CDM    (ILG),   CDH    (ILG),   RIB    (ILG),   CFLUX  (ILG)
      REAL USTAR  (ILG)

!     * INPUT ARRAYS.

      REAL ZOMIN  (ILG),   ZOHIN  (ILG),   CRIB   (ILG),   TVIRTG (ILG)
      REAL TVIRTA (ILG),   VA     (ILG)

!     * WORK ARRAYS.

      REAL ZOM    (ILG),   ZOH    (ILG)

!     * TEMPORARY VARIABLES.

      REAL AA,AA1,BETA,PR,ZLEV,ZS,ZOLN,ZMLN,CPR,ZI,OLSF,OLFACT, &
           ZL,ZMOL,ZHOL,XM,XH,BH1,BH2,BH,WB,WSTAR,RIB0,WSPEED, &
           AU1,OLS,PSIM1,PSIM0,PSIH1,PSIH0,TSTAR,WTS,AS1, &
           AS2,AS3,CLIMIT

!-------------------------------------------------------------
      AA=9.5285714
      AA1=14.285714
      BETA=1.2
      PR = 1.
!
      DO I=IL1,IL2
          RIB(I)=CRIB(I)*(TVIRTG(I)-TVIRTA(I))
          IF( RIB(I) >= 0. ) THEN
            ZLEV=-CRIB(I)*TVIRTA(I)*(VA(I)**2)/GRAV
            ZS=MAX(10.,5.*MAX(ZOMIN(I)*ZLEV, ZOHIN(I)*ZLEV))
            ZS=ZLEV*(1.+RIB(I))/(1.+(ZLEV/ZS)*RIB(I))
            ZOM(I)=ZOMIN(I)*ZLEV/ZS
            ZOH(I)=ZOHIN(I)*ZLEV/ZS
            RIB(I)=RIB(I)*ZS/ZLEV
          ELSE
            ZOM(I)=ZOMIN(I)
            ZOH(I)=ZOHIN(I)
          ENDIF
          ZOLN=LOG(ZOH(I))
          ZMLN=LOG(ZOM(I))
          IF( RIB(I) < 0. ) THEN
            CPR=MAX(ZOLN/ZMLN,0.74)
            CPR=MIN(CPR,1.0)
            ZI=1000.0
            OLSF=BETA**3*ZI*VKC**2/ZMLN**3
            OLFACT=1.7*(LOG(1.+ZOM(I)/ZOH(I)))**0.5+0.9
            OLSF=OLSF*OLFACT
            ZL = -CRIB(I)*TVIRTA(I)*(VA(I)**2)/GRAV
            ZMOL=ZOM(I)*ZL/OLSF
            ZHOL=ZOH(I)*ZL/OLSF
            XM=(1.00-15.0*ZMOL)**(0.250)
            XH=(1.00-9.0*ZHOL)**0.25
            BH1=-LOG(-2.41*ZMOL)+LOG(((1.+XM)/2.)**2*(1.+XM**2)/2.)
            BH1=BH1-2.*ATAN(XM)+ATAN(1.)*2.
            BH1=BH1**1.5
            BH2=-LOG(-0.25*ZHOL)+2.*LOG(((1.00+XH**2)/2.00))
            BH=VKC**3.*BETA**1.5/(BH1*(BH2)**1.5)
            WB=SQRT(GRAV*(TVIRTG(I)-TVIRTA(I))*ZI/TVIRTG(I))
            WSTAR=BH**(0.333333)*WB
            RIB0=RIB(I)

            WSPEED=SQRT(VA(I)**2+(BETA*WSTAR)**2)
            RIB(I)=RIB0*VA(I)**2/WSPEED**2
            AU1=1.+5.0*(ZOLN-ZMLN)*RIB(I)*(ZOH(I)/ZOM(I))**0.25
            OLS=-RIB(I)*ZMLN**2/(CPR*ZOLN)*(1.0+AU1/ &
                 (1.0-RIB(I)/(ZOM(I)*ZOH(I))**0.25))
            PSIM1=LOG(((1.00+(1.00-15.0*OLS)**0.250)/2.00)**2* &
                  (1.0+(1.00-15.0*OLS)**0.5)/2.0)-2.0*ATAN( &
                  (1.00-15.0*OLS)**0.250)+ATAN(1.00)*2.00
            PSIM0=LOG(((1.00+(1.00-15.0*OLS*ZOM(I))**0.250)/2.00)**2 &
                  *(1.0+(1.00-15.0*OLS*ZOM(I))**0.5)/2.0)-2.0* &
                  ATAN((1.00-15.0*OLS*ZOM(I))**0.250)+ATAN(1.00)*2.0
            PSIH1=LOG(((1.00+(1.00-9.0*OLS)**0.50)/2.00)**2)
            PSIH0=LOG(((1.00+(1.00-9.0*OLS*ZOH(I))**0.50)/2.00)**2)

            USTAR(I)=VKC/(-ZMLN-PSIM1+PSIM0)
            TSTAR=VKC/(-ZOLN-PSIH1+PSIH0)
            CDH(I)=USTAR(I)*TSTAR/PR
            WTS=CDH(I)*WSPEED*(TVIRTG(I)-TVIRTA(I))
            WSTAR=(GRAV*ZI/TVIRTG(I)*WTS)**(0.333333)

            WSPEED=SQRT(VA(I)**2+(BETA*WSTAR)**2)
            RIB(I)=RIB0*VA(I)**2/WSPEED**2
            AU1=1.+5.0*(ZOLN-ZMLN)*RIB(I)*(ZOH(I)/ZOM(I))**0.25
            OLS=-RIB(I)*ZMLN**2/(CPR*ZOLN)*(1.0+AU1/ &
                 (1.0-RIB(I)/(ZOM(I)*ZOH(I))**0.25))
            PSIM1=LOG(((1.00+(1.00-15.0*OLS)**0.250)/2.00)**2* &
                  (1.0+(1.00-15.0*OLS)**0.5)/2.0)-2.0*ATAN( &
                  (1.00-15.0*OLS)**0.250)+ATAN(1.00)*2.00
            PSIM0=LOG(((1.00+(1.00-15.0*OLS*ZOM(I))**0.250)/2.00)**2 &
                  *(1.0+(1.00-15.0*OLS*ZOM(I))**0.5)/2.0)-2.0* &
                  ATAN((1.00-15.0*OLS*ZOM(I))**0.250)+ATAN(1.00)*2.0
            PSIH1=LOG(((1.00+(1.00-9.0*OLS)**0.50)/2.00)**2)
            PSIH0=LOG(((1.00+(1.00-9.0*OLS*ZOH(I))**0.50)/2.00)**2)

          ELSE

            WSPEED=VA(I)
            AS1=10.0*ZMLN*(ZOM(I)-1.0)
            AS2=5.00/(2.0-8.53*RIB(I)*EXP(-3.35*RIB(I))+0.05*RIB(I)**2)
!<<<
            AS2=AS2*PR*SQRT(-ZMLN)/2.
            AS3=27./(8.*PR*PR)
!>>>
            OLS=RIB(I)*(ZMLN**2+AS3*AS1*(RIB(I)**2+AS2*RIB(I))) &
                /(AS1*RIB(I)-PR*ZOLN)
            PSIM1=-0.667*(OLS-AA1)*EXP(-0.35*OLS)-AA-OLS
            PSIM0=-0.667*(OLS*ZOM(I)-AA1)*EXP(-0.35*OLS*ZOM(I)) &
                  -AA-OLS*ZOM(I)
            PSIH1=-(1.0+2.0*OLS/3.0)**1.5-0.667*(OLS-AA1) &
                  *EXP(-0.35*OLS)-AA+1.0
            PSIH0=-(1.0+2.0*OLS*ZOH(I)/3.0)**1.5-0.667*(OLS*ZOH(I)-AA1) &
                  *EXP(-0.35*OLS*ZOH(I))-AA+1.0

          ENDIF

          USTAR(I)=VKC/(-ZMLN-PSIM1+PSIM0)

          TSTAR=VKC/(-ZOLN-PSIH1+PSIH0)

          CDM(I)=USTAR(I)**2.0
          CDH(I)=USTAR(I)*TSTAR/PR
!
!         * CALCULATE CD*MOD(V) UNDER FREE-CONVECTIVE LIMIT.
!
          IF( TVIRTG(I) > TVIRTA(I) ) THEN
            CLIMIT=1.9E-3*(TVIRTG(I)-TVIRTA(I))**0.333333
          ELSE
            CLIMIT=0.
          ENDIF
          CFLUX(I)=MAX(CDH(I)*WSPEED,CLIMIT)
      ENDDO
!
      END SUBROUTINE DRCOEF


      SUBROUTINE ABCVDQ6 (A,B,C,CL, EF ,CDVLH,GRAV, &
                          IL1,IL2,ILG,ILEV,LEV,LEVS, &
                          RGAS,RKQ,SHTJ,SHJ,DSHJ, &
                          THL,TF,TODT)

!     * JUL 15/88 - M.LAZARE : REVERSE ORDER OF LOCAL SIGMA ARRAYS.
!     * MAR 14/88 - R.LAPRISE: PREVIOUS HYBRID VERSION FOR GCM3H.

!     * CALCULATES THE THREE VECTORS FORMING THE TRI-DIAGONAL MATRIX FOR
!     * THE IMPLICIT VERTICAL DIFFUSION OF MOISTURE OH HYBRID VERSION OF
!     * MODEL. A IS THE LOWER DIAGONAL, B IS THE MAIN DIAGONAL
!     * AND C IS THE UPPER DIAGONAL.
!     * ILEV = NUMBER OF MODEL LEVELS,
!     * LEVS = NUMBER OF MOISTURE LEVELS.

      IMPLICIT REAL   (A-H,O-Z), &
      INTEGER (I-N)
      REAL   A   (ILG,ILEV),B   (ILG,ILEV),C   (ILG,ILEV),RKQ (ILG,ILEV)
      REAL   SHTJ(ILG, LEV),SHJ (ILG,ILEV),DSHJ(ILG,ILEV)
      REAL   TF  (ILG,ILEV),THL (ILG) ,    CL (ILG),EF (ILG),CDVLH (ILG)
!-----------------------------------------------------------------------
      MSG=ILEV-LEVS
      L=MSG+1
      M=L+1

      DO 50 I=IL1,IL2
         OVDS   = (GRAV*SHTJ(I,M)/RGAS)**2 &
                 /( DSHJ(I,L) * (SHJ(I,M)-SHJ(I,L)) )
         C(I,L) = OVDS*RKQ(I,M)*(1./TF(I,M))**2
   50 CONTINUE

      DO 75 I=IL1,IL2
         B(I,L) = -C(I,L)
   75 CONTINUE

      DO 100 L=MSG+2,ILEV
      DO 100 I=IL1,IL2
         OVDS   = (GRAV*SHTJ(I,L)/RGAS)**2 &
                 /( (SHJ(I,L)-SHJ(I,L-1)) * DSHJ(I,L) )
         A(I,L) = OVDS*(1./TF(I,L))**2*RKQ(I,L)
  100 CONTINUE

      DO 200 L=MSG+2,ILEV-1
      DO 200 I=IL1,IL2
         D      = DSHJ(I,L+1) / DSHJ(I,L)
         C(I,L) = A(I,L+1)*D
         B(I,L) =-A(I,L+1)*D - A(I,L)
  200 CONTINUE

      L=ILEV
      DO 250 I=IL1,IL2
         CL(I)=GRAV*SHJ(I,L)*EF(I)*CDVLH(I)/(RGAS*THL(I)*DSHJ(I,L))
  250 CONTINUE

      DO 300 I=IL1,IL2
         B(I,L) = -A(I,L) -CL(I)
  300 CONTINUE

!     * DEFINE MATRIX TO INVERT = I-2*DT*MAT(A,B,C).

      DO 500 L=MSG+1,ILEV-1
      DO 500 I=IL1,IL2
         A(I,L+1) = -TODT*A(I,L+1)
         C(I,L  ) = -TODT*C(I,L  )
  500 CONTINUE

      DO 550 L=MSG+1,ILEV
      DO 550 I=IL1,IL2
         B(I,L) = 1.-TODT*B(I,L)
  550 CONTINUE

      RETURN
      END
      SUBROUTINE IMPLVD7 (A,B,C, CL,X,XG, IL1,IL2,ILG,ILEV,TODT, &
                          TEND,DELSIG,RAUS,WORK,VINT)

!     * DEC 07/89 - D.VERSEGHY: SAME AS PREVIOUS VERSION IMPLVD6
!     *                         EXCEPT FOR REMOVAL OF UPDATE OF
!     *                         SURFACE FLUXES.

!     * CALCULATE TENDENCIES DUE TO VERTICAL DIFFUSION IN HYBRID MODEL.
!     * THE SCHEME IS IMPLICIT BACKWARD.
!     * A,B,C ARE RESPECTIVELY THE LOWER, MAIN AND UPPER DIAG.

      IMPLICIT REAL   (A-H,O-Z), &
      INTEGER (I-N)

      REAL   A     (ILG,ILEV),B     (ILG,ILEV),C     (ILG,ILEV)
      REAL   X     (ILG,ILEV),WORK  (ILG,ILEV)
      REAL   CL    (ILG),     XG    (ILG),     VINT(ILG)
      REAL   TEND  (ILG,ILEV),DELSIG(ILG,ILEV),RAUS  (ILG)
!-----------------------------------------------------------------------
!     * GET X+ (IN ARRAY TEND) FROM X-. SAVE X(I,ILEV) IN VINT(I).

      DO 100 I=IL1,IL2
         VINT(I)   = X(I,ILEV)
         X(I,ILEV) = X(I,ILEV) +TODT*CL(I)*XG(I)
  100 CONTINUE

      CALL VROSSR(TEND, A,B,C,X,WORK,ILG,IL1,IL2,ILEV)

      DO 150 I=IL1,IL2
         X(I,ILEV) = VINT(I)
  150 CONTINUE

!     * GET TENDENCY FROM X- AND X+.

      DO 200 L=1,ILEV
      DO 200 I=IL1,IL2
           TEND(I,L) = (TEND(I,L)-X(I,L))*(1./TODT)
  200 CONTINUE

!     * CALCULATE VERTICAL INTEGRAL VINT.

      DO 300 I=IL1,IL2
         VINT(I) = 0.
  300 CONTINUE

      DO 400 L=1,ILEV
      DO 400 I=IL1,IL2
            VINT(I) = VINT(I) +TEND(I,L)*DELSIG(I,L)
  400 CONTINUE

      RETURN
      END
      SUBROUTINE VROSSR(P, A,B,C,D,DELTA,ILG,IL1,IL2,M)

!     * CCRN MARS 18/85 - B.DUGAS. (VECTORISER...)
!     * PROGRAMMER  ANDREW STANIFORTH, RPN.
!     * SOLVE THE TRI-DIAGONAL MATRIX PROBLEM BELOW.

!  ** **                                      ****  ****     ***    **** &
!  ** **                                      ****  ****     ***    ****
!  ** B(1),C(1), 0  , 0  , 0  , - - - -  ,  0   **  **  P(1)  **    **
!  ** A(2),B(2),C(2), 0  , 0  , - - - -  ,  0   **  **  P(2)  **    **
!  **  0  ,A(3),B(3),C(3), 0  , - - - -  ,  0   **  **  P(3)  **    **
!  **  0  , 0  ,A(4),B(4),C(4), - - - -  ,  0   **  **  P(4)  **    **
!  **  0  , 0  , 0  ,A(5),B(5), - - - -  ,  0   **  **  P(5)  ** -- **
!  **  -                                    -   **  **    -   ** -- **
!  **  -                                    -   **  **    -   **    **
!  **  0  , - - , 0 ,A(M-2),B(M-2),C(M-2),  0   **  ** P(M-2) **    ** D
!  **  0  , - - , 0 ,  0   ,A(M-1),B(M-1),C(M-1)**  ** P(M-1) **    ** D
!  **  0  , - - , 0 ,  0   ,  0   , A(M) , B(M) **  **  P(M)  **    ** &
!  ** *                                       ****  ****    ****    **** &
!  ** *                                       ****  ****    ****    ****


!     * THE ROUTINE SOLVES IL2-IL1+1 OF THESE PROBLEMS SIMULTANIOUSLY,
!     * THAT IS, IT DOES ONE LATITUDE CIRCLE AT A TIME.
!     * DELTA IS A WORKING ARRAY OF DIMENSION ILG X M.

      IMPLICIT REAL   (A-H,O-Z), &
      INTEGER (I-N)

!      REAL   P(ILG,1),A(ILG,1),B(ILG,1),C(ILG,1),D(ILG,1)
!      REAL   DELTA(ILG,1)
      REAL   P(ILG,M),A(ILG,M),B(ILG,M),C(ILG,M),D(ILG,M)
      REAL   DELTA(ILG,M)
!-----------------------------------------------------------------------
      DO 10 I=IL1,IL2
         C(I,M)=0.
   10 CONTINUE

      DO 30 I=IL1,IL2
         X         =1./B(I,1)
         P(I,1)    =-C(I,1)*X
         DELTA(I,1)=D(I,1)*X
   30 CONTINUE

      DO 50 L=2,M
         DO 50 I=IL1,IL2
            X         =1./(B(I,L)+A(I,L)*P(I,L-1))
            P(I,L)    =-C(I,L)*X
            DELTA(I,L)=(D(I,L)-A(I,L)*DELTA(I,L-1))*X
   50 CONTINUE

      DO 60 I=IL1,IL2
         P(I,M)=DELTA(I,M)
   60 CONTINUE

      DO 70 L=M-1,1,-1
         DO 70 I=IL1,IL2
              P(I,L)=P(I,L)*P(I,L+1)+DELTA(I,L)
   70 CONTINUE

      RETURN
      END


      SUBROUTINE VADV(DXDT,XROW,DTDT,THROW,DQDT,QROW,DQLDT,QLWC, &
                      DUDT,U,W,ZF,ZH,PH,DP,ALMC,ALMX, &
                      DT,IL1,IL2,ILG,ILEV,ILEVP1,MSGP,NTRAC)
!
      USE SDPHYS
!
      IMPLICIT REAL (A-H,O-Z), INTEGER (I-N)
!
      REAL, INTENT(IN), DIMENSION(ILG,ILEV) :: ZF,ZH,PH,DP,ALMC,ALMX
      REAL, DIMENSION(ILG,ILEVP1), INTENT(INOUT) :: THROW,QROW
      REAL, DIMENSION(ILG,ILEV), INTENT(IN) :: QLWC
      REAL, DIMENSION(ILG,ILEV), INTENT(INOUT) :: U,W
      REAL, DIMENSION(ILG,ILEV,NTRAC) :: RKX
      REAL, DIMENSION(ILG,ILEVP1,NTRAC), INTENT(IN) :: XROW
      REAL, DIMENSION(ILG,ILEVP1,NTRAC), INTENT(OUT) :: DXDT
      REAL, DIMENSION(ILG,ILEVP1), INTENT(OUT) :: DQDT,DTDT
      REAL, DIMENSION(ILG,ILEV), INTENT(OUT) :: DQLDT
      REAL, DIMENSION(ILG,ILEV), INTENT(OUT) :: DUDT
      REAL, DIMENSION(ILG,ILEV) :: QT,HMN,WF
      REAL, DIMENSION(ILG,ILEV) :: QTN, HMNN, CVAR, ZCRAUT, QCWVAR, &
                                   CVSG, CVDU, ZFRAC, QC, VDPQ, &
                                   VDPH, VDPU, ZCLF
      REAL, DIMENSION(ILG,ILEV,NTRAC) :: VDPX
      REAL, DIMENSION(ILG) :: PMBS, RRL, CPM, ALMIX, DHLDZ, DRWDZ, &
                              FLAGEST, QCW, SSH, WGT, DZ, TMP
!
!     * TOTAL WATER MIXING RATIO AND CLOUD WATER STATIC ENERGY.
!
      QT=QROW(:,MSGP:ILEVP1)+QLWC
      HMN=CPRES*THROW(:,MSGP:ILEVP1)+GRAV*ZH-RL*QLWC
!
!     * VERTICAL WIND AT GRID CELL INTERFACES.
!
      WF(:,ILEV)=0.
      DO L=2,ILEV
        WGT(:)=(ZF(:,L)-ZH(:,L))/(ZH(:,L-1)-ZH(:,L))
        WF(:,L-1)=(1.-WGT)*W(:,L)+WGT*W(:,L-1)
      ENDDO
!
!     * ADVECT WATER MIXING RATIO, CLOUD WATER STATIC ENERGY VERTICALLY,
!     * WINDS, AND TRACERS USING EULER FORWARD DIFFERENCES.
!
      VDPQ(:,ILEV)=0.
      VDPH(:,ILEV)=0.
      VDPU(:,ILEV)=0.
      VDPX(:,ILEV,:)=0.
      DO L=3,ILEV
        WHERE ( WF(:,L-1) <= 0. )
          DZ=ZF(:,L-2)-ZF(:,L-1)
          TMP=MAX(WF(:,L-1)*DT/DZ,-1.)*DP(:,L-1)
          VDPQ(:,L-1)=QT(:,L-1)*TMP
          VDPH(:,L-1)=HMN(:,L-1)*TMP
          VDPU(:,L-1)=U(:,L-1)*TMP
        ELSEWHERE
          DZ=ZF(:,L-1)-ZF(:,L)
          TMP=MIN(WF(:,L-1)*DT/DZ,1.)*DP(:,L)
          VDPQ(:,L-1)=QT(:,L)*TMP
          VDPH(:,L-1)=HMN(:,L)*TMP
          VDPU(:,L-1)=U(:,L)*TMP
        ENDWHERE
        DO N=1,NTRAC
          WHERE ( WF(:,L-1) <= 0. )
            DZ=ZF(:,L-2)-ZF(:,L-1)
            TMP=MAX(WF(:,L-1)*DT/DZ,-1.)*DP(:,L-1)
            VDPX(:,L-1,N)=XROW(:,L-1,N)*TMP
          ELSEWHERE
            DZ=ZF(:,L-1)-ZF(:,L)
            TMP=MIN(WF(:,L-1)*DT/DZ,1.)*DP(:,L)
            VDPX(:,L-1,N)=XROW(:,L,N)*TMP
          ENDWHERE
        ENDDO
      ENDDO
      L=2
      VDPQ(:,1)=0.
      VDPH(:,1)=0.
      VDPU(:,1)=0.
      VDPX(:,1,:)=0.
      WHERE ( WF(:,L-1) > 0. )
        DZ=ZF(:,L-1)-ZF(:,L)
        TMP=MIN(WF(:,L-1)*DT/DZ,1.)*DP(:,L)
        VDPQ(:,L-1)=QT(:,L)*TMP
        VDPH(:,L-1)=HMN(:,L)*TMP
        VDPU(:,L-1)=U(:,L)*TMP
      ENDWHERE
      DO N=1,NTRAC
        WHERE ( WF(:,L-1) > 0. )
          DZ=ZF(:,L-1)-ZF(:,L)
          TMP=MIN(WF(:,L-1)*DT/DZ,1.)*DP(:,L)
          VDPX(:,L-1,N)=XROW(:,L,N)*TMP
        ENDWHERE
      ENDDO
      L=1
      QT(:,L)=QT(:,L)+VDPQ(:,L)/DP(:,L)
      HMN(:,L)=HMN(:,L)+VDPH(:,L)/DP(:,L)
      DUDT(:,L)=VDPU(:,L)/DP(:,L)/DT
      DO N=1,NTRAC
        DXDT(:,L,N)=VDPX(:,L,N)/DP(:,L)/DT
      ENDDO
      DO L=2,ILEV
        QT(:,L)=QT(:,L)-(VDPQ(:,L-1)-VDPQ(:,L))/DP(:,L)
        HMN(:,L)=HMN(:,L)-(VDPH(:,L-1)-VDPH(:,L))/DP(:,L)
        DUDT(:,L)=-(VDPU(:,L-1)-VDPU(:,L))/DP(:,L)/DT
        DO N=1,NTRAC
          DXDT(:,L,N)=-(VDPX(:,L-1,N)-VDPX(:,L,N))/DP(:,L)/DT
        ENDDO
      ENDDO
!
!     * TEMPERATURE AND WATER VAPOUR MIXING RATIO FROM STATISTICAL
!     * CLOUD SCHEME.
!
      FACTE=1./EPS1-1.
      I2NDIE=1
      FLAGEST=1.
      CVSG=0.
      CVDU=0.
      ZFRAC=0.
      ICVSG=1
      ISUBG=1
      ICALL=1
      DO JK=1,ILEV
        DO IL=1,ILG
          PMBS(IL)=0.01*PH(IL,JK)
          RRL(IL)=RL
          CPM(IL)=CPRES
          ALMIX(IL)=MAX(ALMC(IL,JK),ALMX(IL,JK),10.)
          IF(JK.GT.1)                              THEN
            DZT      =ZH  (IL,JK-1)-ZH (IL,JK)
            DHLDZ(IL)=(HMN(IL,JK-1)-HMN(IL,JK))/DZT
            DRWDZ(IL)=(QT (IL,JK-1)-QT (IL,JK))/DZT
          ELSE
            DHLDZ(IL)=0.
            DRWDZ(IL)=0.
          ENDIF
        ENDDO
        CALL STATCLD (QCW,ZCLF(1,JK),CVAR(1,JK),ZCRAUT(1,JK), &
                      QCWVAR(1,JK),SSH,CVSG(1,JK),QT(1,JK), &
                      HMN(1,JK),ZFRAC(1,JK),CPM, &
                      PMBS,ZH(1,JK),RRL,CVDU(1,JK),FLAGEST, &
                      ALMIX,DHLDZ,DRWDZ,I2NDIE, &
                      DT,ILEV,ILG,1,ILG,ICVSG,ISUBG, &
                      JK,ICALL)
!
        DO IL=1,ILG
          DTDT(IL,JK)=((HMN(IL,JK)-GRAV*ZH(IL,JK)+RRL(IL)*QCW(IL)) &
                       /CPM(IL)-THROW(IL,JK))/DT
          DQDT(IL,JK)=((QT(IL,JK)-QCW(IL))-QROW(IL,JK))/DT
          DQLDT(IL,JK)=(QCW(IL)-QLWC(IL,JK))/DT
        ENDDO
      ENDDO
!
      END SUBROUTINE VADV
