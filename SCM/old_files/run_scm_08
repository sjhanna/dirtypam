#!/bin/sh
set -ex

# Name of the simulation

runname="prof1f08"

# Copy meteorological input files
cp -r ./PROF1F08/* .

# Model configuration parameters

cat > PARAM <<EOF
&MODNML

! *** GENERAL MODEL INFORMATION ***
!
! PARAMETER DICTIONARY:
!    RUNNAME - EXPERIMENT NAME
!    PLTFQ - DATA OUTPUT FREQUENCY
!    NUGALT  - TOP OF NUDGED LAYER (M)
!    NUGVD   - VERTICAL DEPTH OF TRANSITION ZONE FOR NUDGING (M)
!    TAUT    - NUDGING TIME SCALE FOR TEMPERATURE (DAYS)
!    TAUQ    - NUDGING TIME SCALE FOR MOISTURE (DAYS)
!    TAUM    - NUDGING TIME SCALE FOR MOMENTUM (DAYS)
!    CDN     - 2ST SPECIFIED CLOUD DROPLET NUMBER (1/CM3)
!    CDNN    - 2ND SPECIFIED CLOUD DROPLET NUMBER (1/CM3)
!    TRUP    - TRANSITION TIME STEP FROM 1ST TO 2ND SPECIFIED
!              CLOUD DROPLET NUMBER
!    ZREF    - TOP OF PROGNOSTIC LAYER (M)
!    CASE    - CHOICE OF AUTOCONVERSION PARAMETERIZATION
!
! NOTE: SI-UNITS ARE REQUIRED.

MODL%RUNNAME="${runname}"
MODL%NUGALT=230.
MODL%TAUQ=1.E+20
MODL%TAUM=10800.

MODL%ZREF=150.
MODL%CASE=3

/
EOF

cat > SCMPRE <<EOF
&MODNML

! *** GENERAL MODEL INFORMATION ***
!
! PARAMETER DICTIONARY:
!    RUNNAME - EXPERIMENT NAME
!    PLEV    - NUMBER OF VERTICAL LEVELS
!    DZSFC   - VERTICAL GRID SPACING
!    DT      - TIME STEP LENGTH
!    NTSTP   - NUMBER OF TIME STEPS
!
! NOTE: SI-UNITS ARE REQUIRED.

MODL%RUNNAME="${runname}"

MODL%PLEV=30
MODL%DZSFC=10.
MODL%DT=900.
MODL%NTSTP=384

/
&AIRNML

! *** PERTURBATIONS TO MEAN ATMOSPHERIC STATE ***
!
! PARAMETER DICTIONARY:
!    NPER   - NUMBER OF CLOUD, TEMPERATURE OR MOISTURE EVENTS
!    DTA    - TEMPERATURE PERTURBATION (K/DAY)
!    DRV    - WATER MIXING RATIO PERTURBATION (KG/KG/DAY)
!    CLDF   - CLOUD FRACTION
!    LVBOT  - LEVEL INDEX FOR BOTTOM OF PERTURBATION (USED IF CLDF>0 OR DTA/=0 OR DQV /=0)
!    LVTOP  - LEVEL INDEX FOR TOP OF PERTURBATION (USED IF CDLF>0 OR DTA/=0 OR DQV /=0)
!    SVICLD - SUPERSATURATION IN CLOUDY PART OF ATMOSPHERE (FRACTION, USED IF CLDF>0)
!    PERON  - TIME STEP AT WHICH PERTURBATION STARTS (USED IF CLDF>0 OR DTA/=0 OR DQV /=0)
!    PEROFF - TIME STEP AT WHICH PERTURBATION STOPS (USED IF CLDF>0 OR DTA/=0 OR DQV /=0)
!    DTG    - SURFACE TEMPERATURE PERTURBATION (K)
!    DTGON  - TIME STEP AT WHICH PERTURBATION STARTS (USED IF DTG/= 0)
!    DTGOFF - TIME STEP AT WHICH PERTURBATION STOPS (USED IF DTG/= 0)

AIR%TEMPI=273.0        ! TEMPERATURE AT SURFACE (K)
AIR%PRESI=101325.0     ! SURFACE PRESSURE (PA)

/
EOF

# Compile i/o list manager

#gfortran -o configlist configlist.f

# Run i/o list manager

configlist F

# Compile the SCM meteorology preprocessor

gfortran -o preproc -fdefault-real-8 -fbounds-check -ffree-form preproc.f90 `/HOME/opt/package/bin/nf-config --fflags --flibs` -lnetcdff -lnetcdf -lm

# Copy input files and run the preprocessor

cp -r OUTPUT_PRE OUTPUT
cp -r RESTART_PRE RESTART

#./preproc
env LD_LIBRARY_PATH=`/HOME/opt/package/bin/nf-config --prefix`/lib ./preproc

# Clean up preprocessor files

rm -rf PREPROC OUTPUT RESTART RESTART_PRE RESTART_PRE OUTPUT_PRE OUTPUT_PRE

# Compile the SCM

gfortran -o scm -fdefault-real-8 -fbounds-check -ffree-form scm.f90 `/HOME/opt/package/bin/nf-config --fflags --flibs` -lnetcdff -lnetcdf -lm

# Copy SCM input files

cp -r INPUT_MOD INPUT
cp -r OUTPUT_MOD OUTPUT
cp -r RESTART_MOD RESTART

# Run the SCM and restart, if necessary

restarts=1
rstrt=1
while [ $rstrt -le $restarts ] ; do
#  ./scm
  env LD_LIBRARY_PATH=`/HOME/opt/package/bin/nf-config --prefix`/lib ./scm
#  cp ${runname}_tfin.nc ${runname}_tini.nc
  rstrt=`expr $rstrt + 1`
done

# Final cleanup

rm -rf PARAM SCMPRE OUTPUT INPUT RESTART RESTART_MOD OUTPUT_MOD INPUT_MOD
rm -rf scm preproc
rm -rf PROFDAT
