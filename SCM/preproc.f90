program preproc
  !
  use fpdef           ! floating point precision information
  use runinfo         ! atmospheric properties and other information
  ! about general setup of simulation.
  use psizes          ! contains dimensional parameters for
  ! atmospheric model
  use iodat           ! contains variables that are written to the
  ! output file
  use iodatif         ! contains i/o subroutine interface defitions
  use sdphys          ! physical constants
  !
  implicit none
  !
  real :: acdnc
  real :: aclip
  real :: dt
  integer :: ilevi
  integer :: ilx
  integer :: inum
  integer :: ioff
  integer :: it
  integer :: its
  integer :: iz
  integer :: izt
  integer :: iztt
  integer :: nc
  integer :: nupar
  real :: rhipt
  real :: thip
  real :: uip
  real :: wgt
  real :: zhin
  real :: zhip
  real :: ztmp
  real :: ztmpp
  !
  character(len=50) :: cfile !<
  character(len=50) :: cfilx !<
  !
  real, allocatable, dimension(:,:) :: qr !<
  real, allocatable, dimension(:,:) :: qrv !<
  real, allocatable, dimension(:,:) :: qrc !<
  real, allocatable, dimension(:,:) :: ri !<
  real, allocatable, dimension(:,:) :: relhi !<
  real, allocatable, dimension(:,:) :: relhib !<
  real, allocatable, dimension(:,:) :: qrb !<
  real, allocatable, dimension(:,:) :: qrvb !<
  real, allocatable, dimension(:,:) :: wg !<
  real, allocatable, dimension(:,:) :: tmp1 !<
  real, allocatable, dimension(:,:) :: tmp2 !<
  real, allocatable, dimension(:,:) :: tmp3 !<
  real, allocatable, dimension(:,:) :: esw !<
  real, allocatable, dimension(:,:) :: rs !<
  real, allocatable, dimension(:,:) :: phi !<
  real, allocatable, dimension(:,:) :: pfi !<
  real, allocatable, dimension(:,:) :: thi !<
  real, allocatable, dimension(:,:) :: tfi !<
  real, allocatable, dimension(:,:) :: dp !<
  real, allocatable, dimension(:,:) :: vitrm !< Pressure in \f$kg/m^2\f$ 
  real, allocatable, dimension(:,:) :: svi !<
  real, allocatable, dimension(:,:) :: uspd !<
  real, allocatable, dimension(:,:) :: zclf !<
  real, allocatable, dimension(:,:) :: zcdnrow !<
  real, allocatable, dimension(:,:) :: uspdf !<
  real, allocatable, dimension(:,:) :: perta !<
  real, allocatable, dimension(:,:) :: perrv !<
  real, allocatable, dimension(:,:) :: tfix !<
  real, allocatable, dimension(:,:) :: pfix !<
  real, allocatable, dimension(:,:) :: rhoa !< Air density \f$[kg/m^3]\f$ 
  real, allocatable, dimension(:,:) :: tfixx !<
  real, allocatable, dimension(:,:) :: thixx !<
  real, allocatable, dimension(:) :: smfrac !<
  real, allocatable, dimension(:) :: thliqg !<
  real, allocatable, dimension(:) :: zo !<
  real, allocatable, dimension(:) :: modtim !<
  real, allocatable, dimension(:) :: gtrow !<
  real, allocatable, dimension(:) :: zhi !<
  real, allocatable, dimension(:) :: zfi !<
  real, allocatable, dimension(:) :: zfix !<
  real, allocatable, dimension(:) :: ustar !<
  real, allocatable, dimension(:) :: pbli !<
  real, allocatable, dimension(:) :: dz !<
  real, allocatable, dimension(:) :: rhip !<
  !
  real, parameter :: yspday=24.*60.*60. !<
  !
  !     * input/output file unit numbers.
  !
  data nupar /5 /
  !
  !-----------------------------------------------------------------------
  !     * open and read file that contains basic information about the
  !     * air properties.
  !
  open(nupar,file='SCMPRE')
  !
  !-----------------------------------------------------------------------
  !     * read in basic information about simulation (e.g. name, vertical
  !     * levels, time step etc.).
  !
  modl%ntstp=inax
  read(nupar,nml=modnml)
  !
  !-----------------------------------------------------------------------
  !     * grid dimensions.
  !
  ilev=modl%plev   ! number of vertical grid cells for atmosphere
  ilevp1=ilev+1
  its=modl%ntstp   ! number of time steps
  !
  !-----------------------------------------------------------------------
  !     * read file that contains information about air properties.
  !
  air%nper=0
  air%cldf=0.
  air%dtg=0.
  air%dta=0.
  air%drv=0.
  air%dtgon=1
  air%dtgoff=1
  air%peron=1
  air%peroff=1
  air%zhi=0.
  rewind(nupar)
  read(nupar,nml=airnml)
  rewind(nupar)
  !
  !-----------------------------------------------------------------------
  !     * check input information and trim.
  !
  call chkio
  cfile=trim(modl%runname)
  if (modl%ntstp == inax) call xit('MAIN',-1)
  dt=modl%dt
  !
  !-----------------------------------------------------------------------
  !     * allocate memory.
  !
  allocate(modtim (its))
  allocate(dz     (ilev))
  allocate(zhi    (ilev))
  allocate(zfi    (0:ilev))
  allocate(qr     (ilev,its))
  allocate(qrv    (ilev,its))
  allocate(qrc    (ilev,its))
  allocate(zcdnrow(ilev,its))
  allocate(relhi  (ilev,its))
  allocate(relhib (ilev,its))
  allocate(qrb    (ilev,its))
  allocate(qrvb   (ilev,its))
  allocate(wg     (ilev,its))
  allocate(tmp1   (ilev,its))
  allocate(tmp2   (ilev,its))
  allocate(tmp3   (ilev,its))
  allocate(esw    (ilev,its))
  allocate(rs     (ilev,its))
  allocate(rhoa   (ilev,its))
  allocate(phi    (ilev,its))
  allocate(pfi    (0:ilev,its))
  allocate(pfix   (1:ilevp1,its))
  allocate(dp     (ilev,its))
  allocate(vitrm  (ilev,its))
  allocate(thi    (ilev,its))
  allocate(rhip   (ilev))
  allocate(tfi    (0:ilev,its))
  allocate(tfixx  (0:ilev,its))
  allocate(tfix   (1:ilevp1,its))
  allocate(zfix   (1:ilevp1))
  allocate(svi    (ilev,its))
  allocate(zclf   (ilev,its))
  allocate(uspd   (ilev,its))
  allocate(uspdf  (ilev,its))
  allocate(ri     (ilev,its))
  allocate(perta  (ilev,its))
  allocate(perrv  (ilev,its))
  !
  allocate(ustar  (its))
  allocate(zo     (its))
  allocate(thliqg (its))
  allocate(gtrow  (its))
  allocate(pbli   (its))
  !
  allocate(thixx(ilev,its))
  !
  !-----------------------------------------------------------------------
  !     * model time.
  !
  do iz=1,its
    modtim(iz)=real(iz-1)*dt
  end do
  !
  !     * vertical level heights (in metres). "F" refers to grid cell
  !     * interfaces, "H" to mid-points.
  !
  dz=modl%dzsfc
  zfi(ilev)=air%zhi
  zfi(ilev-1)=zfi(ilev)+1.5*dz(ilev)
  do iz=ilev-2,0,-1
    zfi(iz)=zfi(iz+1)+dz(iz+1)
  end do
  zhi(ilev)=dz(ilev)
  do iz=ilev-1,1,-1
    zhi(iz)=zhi(iz+1)+dz(iz+1)
  end do
  !
  !-----------------------------------------------------------------------
  !     * read profiles of temperature, relative humidity, and horizontal
  !     * wind speed. it is assumed that continuous vertical profiles are
  !     * available above a miminum and below a maximum altitude, according
  !     * to aircraft altitude range.
  !
  open(33,file='PROFDAT')
  read(33,'(1X)')
  izt=0
  do iz=1,2*ilev
    read(33,'(F8.0)',end=777) ztmp
    if (ztmp > modl%plev*modl%dzsfc+1.e-20) goto 777
    izt=izt+1
    if (iz == 1) then
      zhin=ztmp
    else
      if (abs((ztmp-ztmpp)-modl%dzsfc) > 1.e-20) &
          call xit('MAIN',-2)
    end if
    ztmpp=ztmp
  end do
777 continue
  ilevi=izt
  rewind(33)
  read(33,'(1X)')
  !
  !     * check if point is either a grid point value
  !
  if ( .not. mod(zhin,modl%dzsfc) < 1.e-20) call xit('MAIN',-3)
  if (mod(zhin,modl%dzsfc) < 1.e-20) then
    izt=nint(zhin/modl%dzsfc)-1
  end if
  inum=ilevi
  if (inum < 1) call xit('MAIN',-4)
  if (inum+izt < modl%plev) then
    print*,'REQUESTED NUMBER OF LEVELS = ',modl%plev
    print*,'AVAILABLE NUMBER OF LEVELS = ',inum+izt
    call xit('MAIN',-5)
  end if
  !
  !     * read first interface value, if available
  !
  izt=ilev-izt
  do ilx=1,inum
    !
    !       * read grid point value
    !
    read(33,'(F8.3,3X,F7.3,4(8X,E11.4))') &
                zhip,thip,rhipt,uip,aclip,acdnc
    thi(izt,:)=thip
    uspd(izt,:)=uip
    rhip(izt)=min(rhipt/100.,1.)
    qrc(izt,:)=max(0.,aclip)*1.e-03
    zcdnrow(izt,:)=max(0.,acdnc)*1.e+06
    !
    !       * linear interpolation between lowest available level and ground
    !
    if (ilx==1) then
      do iz=izt+1,ilev
        thi(iz,:)=air%tempi+(thi(izt,:)-air%tempi) &
                             *(zhi(iz)-air%zhi)/(zhi(izt)-air%zhi)
        uspd(iz,:)=uspd(izt,:) &
                             *(zhi(iz)-air%zhi)/(zhi(izt)-air%zhi)
        rhip(iz)=rhip(izt)
        qrc(iz,:)=qrc(izt,:)
        zcdnrow(iz,:)=zcdnrow(izt,:)
      end do
    end if
    izt=izt-1
  end do
  !
  !     * smooth wind speed profile, if applicable.
  !
  ioff=0
  do iz=1,ilev
    iztt=0
    uspdf(iz,:)=0.
    do izt=iz-ioff,iz+ioff
      if (izt <= ilev .and. izt >= 1) then
        uspdf(iz,:)=uspdf(iz,:)+uspd(izt,:)
        iztt=iztt+1
      end if
    end do
    uspdf(iz,:)=uspdf(iz,:)/real(iztt)
  end do
  uspd=uspdf
  !
  !     * temperature (in kelvin).
  !
  tfi(ilev,:)=air%tempi
  do iz=ilev-1,1,-1
    wgt=(zfi(iz)-zhi(iz+1))/(zhi(iz)-zhi(iz+1))
    tfi(iz,:)=wgt*thi(iz,:)+(1.-wgt)*thi(iz+1,:)
  end do
  tfi(0,:)=thi(1,:)
  !
  !     * vertical pressure levels, based on hydrostatic assumption and
  !     * ideal gas law (in pascal).
  !
  pfi(ilev,:)=air%presi
  do iz=ilev-1,0,-1
    pfi(iz,:)=pfi(iz+1,:)*exp(-grav*dz(iz+1)/(rgas*thi(iz+1,:)))
  end do
  phi(ilev,:)=pfi(ilev,:)*exp(-grav*(zhi(ilev)-zfi(ilev)) &
                                 /(rgas*tfi(ilev,:)))
  do iz=ilev-1,1,-1
    phi(iz,:)=phi(iz+1,:)*exp(-grav*(zhi(iz)-zhi(iz+1)) &
                                 /(rgas*tfi(iz,:)))
  end do
  rhoa=phi/(rgas*thi)
  !
  !     * pressure level depth and scaling factor for calculation
  !     * of vertical integrals.
  !
  do iz=1,ilev
    dp(iz,:)=pfi(iz,:)-pfi(iz-1,:)
  end do
  vitrm=dp/grav
  !
  !     * saturation mixing ratio (in kg/kg).
  !
  tmp3=rw1+rw2/thi
  tmp1=exp(tmp3)
  tmp3=rw3
  tmp2=thi**tmp3
  esw=1.e+02*tmp1*tmp2
  rs=eps1*esw/( phi-esw)
  !
  !     * initial supersaturation in cloudy part of the grid cells.
  !     * used to generate total water mixing ratio in the following.
  !
  svi=0.
  !
  !     * corresponding relative humidity for cloudy conditions.
  !
  do iz=ilev,1,-1
    relhi(iz,:)=svi(iz,:)+rhip(iz)
  end do
  !
  !     * total water mixing, cloud water and water vapour
  !     * mixing ratio in the cloudy parts of the grid cells.
  !
  qrv=rs*relhi/(1.+rs*(1.-relhi)/eps1)
  qrc=qrc/rhoa
  qr=qrv+qrc
  !
  !     * fraction of grid cell in which liquid cloud water occurs.
  !
  zclf=0.
  !
  !     * add specified temperature and moisture perturbations.
  !
  perta=0.
  perrv=0.
  do nc=1,air%nper
    if (abs(air%dta(nc)) > 1.e-09) &
        perta(air%lvtop(nc):air%lvbot(nc), &
        air%peron(nc):air%peroff(nc))=air%dta(nc)/yspday
    if (abs(air%drv(nc)) > 1.e-09) &
        perrv(air%lvtop(nc):air%lvbot(nc), &
        air%peron(nc):air%peroff(nc))=air%drv(nc)/yspday
  end do
  !
  !     * longwave cooling of the atmosphere (k/sec)
  !
  perta=perta-0.1/yspday
  !
  !     * resolved (wg) vertical velocity (m/sec).
  !
  do iz=1,ilev
    wg(iz,:)=-0.e-09*zhi(iz)
  end do
  !
  !     * friction velocity (m/sec, used to initialize calculations).
  !
  ustar=0.2
  !
  !     * richardson number (dimensionless, used to initialize calculations).
  !
  ri=0.
  !
  !     * boundary layer top level index (dimensionless, used to initialiaze calculations).
  !
  pbli=ilev
  !
  !     * aerodynamic surface roughness (m, only used for land surfaces).
  !
  zo=0.0001
  !
  !     * select maximum cloud droplet number concentration.
  !
  do it=1,its
    acdnc=0.
    do iz=1,ilev
      if (zcdnrow(iz,it) > acdnc) acdnc=zcdnrow(iz,it)
    end do
    zcdnrow(:,it)=acdnc
  end do
  !
  !-----------------------------------------------------------------------
  !     * land-surface properties.
  !
  gtrow=air%tempi    ! surface temperature (k, from class).
  do nc=1,air%nper
    gtrow(air%dtgon(nc):air%dtgoff(nc))= &
                                             gtrow(air%dtgon(nc):air%dtgoff(nc))+air%dtg(nc)
  end do
  !
  !     * soil water content (kg/kg).
  !
  thliqg=1.
  !
  !=======================================================================
  !     * output.
  !=======================================================================
  !     * definition of field dimensions.
  !
  zfix(1:ilevp1)=zfi(0:ilev)
  call setncdimpre(zhi,zfix,modtim,ilev,ilevp1,its)
  !
  !     * initialize input and output arrays.
  !
  call initio(.false.,.false.)
  !
  !     * write atmospheric boundary conditions for cloud and pla
  !     * parameterizations.
  !
  call w0dfld('delt',dt)
  !
  pfix(1:ilevp1,:)=pfi(0:ilev,:)
  call w2dfld('paf',pfix,ilevp1,its)
  call w2dfld('pa',phi,ilev,its)
  tfix(1:ilevp1,:)=tfi(0:ilev,:)
  call w2dfld('taf',tfix,ilevp1,its)
  call w2dfld('ta',thi,ilev,its)
  call w2dfld('ua',uspd,ilev,its)
  call w2dfld('wa',wg,ilev,its)
  call w2dfld('rv',qrv,ilev,its)
  call w2dfld('rl',qrc,ilev,its)
  call w2dfld('perta',perta,ilev,its)
  call w2dfld('perrv',perrv,ilev,its)
  call w1dfld('rliqgnd',thliqg,its)
  call w1dfld('zo',zo,its)
  call w1dfld('tsurf',gtrow,its)
  !
  !     * write boundary conditions to output file.
  !
  cfilx=trim(cfile)//'_bnd'
  call wncdat(cfilx)
  !
  !-----------------------------------------------------------------------
  !     * write initial prognostic fields to restart file.
  !
  call w0dfldrs('tstep',real(0))
  call w0dfldrs('ustar',ustar(1))
  call w1dfldrs('ua',uspd(:,1),ilev)
  call w1dfldrs('ta',thi(:,1),ilev)
  call w1dfldrs('rv',qrv(:,1),ilev)
  call w1dfldrs('rl',qrc(:,1),ilev)
  call w1dfldrs('clf',zclf(:,1),ilev)
  call w1dfldrs('cldnc',zcdnrow(:,1),ilev)
  call w1dfldrs('ri',ri(:,1),ilev)
  call w0dfldrs('indpbl',pbli(1))
  !
  !     * write the data.
  !
  cfilx=trim(cfile)//'_tini'
  call wncdatrs(cfilx)
  !
end program preproc
