program pam_pre
  !
  use runinfo         ! atmospheric properties and other information
  ! about general setup of simulation.
  use psizes          ! contains dimensional parameters for
  ! atmospheric model
  use sdparm          ! numerical constants for pla model.
  use sdphys          ! physical constants for pla model.
  use iodat           ! contains variables that are written to the
  ! output file
  use iodatif         ! contains i/o subroutine interface defitions
  use trcind, only : iso2,ihpo,igs6,igsp,idms ! gas-phase tracer indices
  !
  !
  implicit none
  !
  real :: co2_ppm
  real :: dt
  real :: factc
  integer :: i
  integer :: ilevi
  integer :: ilx
  integer :: imode
  integer :: inum
  integer :: its
  integer :: iz
  integer :: izt
  integer :: mynode
  integer :: n
  integer :: nucoa
  integer :: nuemi
  integer :: nupar
  real :: thip
  real :: wgt
  real :: wst
  real :: zhin
  real :: zhip
  real :: zmolgair
  real :: ztmp
  real :: ztmpp
  !
  real(r8), parameter :: tstt2=1. !<
  real(r4), parameter :: tstt1=1. !<
  real, parameter :: tstt=1. !<
  !
  !     * pla parameters - these need to be consistent with the same
  !     * parameters in pam.
  !
  integer, parameter :: kextt=4 ! number of externally mixed species
  integer, parameter :: kintt=3 ! number of internally mixed species
  integer, parameter :: isaintt=3 !<
  integer, parameter :: nrmfld=2 ! maximum number of fields for
  ! averaging of results
  integer, parameter :: isdiag=22 !<
  integer, parameter :: isdust=22 !<
  integer, parameter :: isvdust=0 !<
  logical, parameter :: kxtra1=.true. ! switch for pla extra output, type 1
  logical, parameter :: kxtra2=.true. ! switch for pla extra output, type 2
  real, parameter :: wso2    =64.059e-03 ! molecular weight of so2 (kg/mol)
  logical :: kcoagd !<
  logical :: kemiss !<
  !
  character(len=50) :: cfile !<
  character(len=50) :: cfilx !<
  !
  real, allocatable, dimension(:,:,:) :: trac !<
  real, allocatable, dimension(:,:,:) :: sfrc !<
  real, allocatable, dimension(:,:,:) :: sfrcrol !<
  real, allocatable, dimension(:,:,:) :: xrow !<
  real, allocatable, dimension(:,:) :: so2em !<
  real, allocatable, dimension(:,:) :: orgem !<
  real, allocatable, dimension(:,:) :: docem1 !<
  real, allocatable, dimension(:,:) :: docem2 !<
  real, allocatable, dimension(:,:) :: docem3 !<
  real, allocatable, dimension(:,:) :: docem4 !<
  real, allocatable, dimension(:,:) :: dbcem1 !<
  real, allocatable, dimension(:,:) :: dbcem2 !<
  real, allocatable, dimension(:,:) :: dbcem3 !<
  real, allocatable, dimension(:,:) :: dbcem4 !<
  real, allocatable, dimension(:,:) :: dsuem1 !<
  real, allocatable, dimension(:,:) :: dsuem2 !<
  real, allocatable, dimension(:,:) :: dsuem3 !<
  real, allocatable, dimension(:,:) :: dsuem4 !<
  real, allocatable, dimension(:,:) :: o3row !<
  real, allocatable, dimension(:,:) :: hno3row !<
  real, allocatable, dimension(:,:) :: nh3row !<
  real, allocatable, dimension(:,:) :: nh4row !<
  real, allocatable, dimension(:,:) :: no3row !<
  real, allocatable, dimension(:,:) :: ohrow !<
  real, allocatable, dimension(:,:) :: fcanrow !<
  real, allocatable, dimension(:,:) :: so2ema !<
  real, allocatable, dimension(:,:) :: bghpo !<
  real, allocatable, dimension(:) :: smfrac !<
  real, allocatable, dimension(:) :: bsfrac !<
  real, allocatable, dimension(:) :: modtim !<
  real, allocatable, dimension(:) :: pdsfrow !<
  real, allocatable, dimension(:) :: flnd !<
  real, allocatable, dimension(:) :: focn !<
  real, allocatable, dimension(:) :: sicn !<
  real, allocatable, dimension(:) :: fnrol !<
  real, allocatable, dimension(:) :: spotrow !<
  real, allocatable, dimension(:) :: st02row !<
  real, allocatable, dimension(:) :: st03row !<
  real, allocatable, dimension(:) :: st04row !<
  real, allocatable, dimension(:) :: st06row !<
  real, allocatable, dimension(:) :: st13row !<
  real, allocatable, dimension(:) :: st14row !<
  real, allocatable, dimension(:) :: st15row !<
  real, allocatable, dimension(:) :: st16row !<
  real, allocatable, dimension(:) :: st17row !<
  real, allocatable, dimension(:) :: suz0row !<
  real, allocatable, dimension(:) :: zhi !<
  real, allocatable, dimension(:) :: zfi !<
  real, allocatable, dimension(:) :: zfix !<
  real, allocatable, dimension(:) :: dmso !<
  real, allocatable, dimension(:) :: dz !<
  real, allocatable, dimension(:,:) :: thi !<
  real, allocatable, dimension(:,:) :: tfi !<
  real, allocatable, dimension(:,:) :: phi !<
  real, allocatable, dimension(:,:) :: pfi !<
  real, allocatable, dimension(:,:) :: rhoa !< Air density \f$[kg/m^3]\f$ 
  !
  !     * fields used for i/o for pla.
  !
  real, allocatable, dimension(:,:) :: oednrow !<
  real, allocatable, dimension(:,:) :: oercrow !<
  real, allocatable, dimension(:) :: oidnrow !<
  real, allocatable, dimension(:) :: oircrow !<
  real, allocatable, dimension(:) :: svvbrow !<
  real, allocatable, dimension(:) :: psvvrow !<
  real, allocatable, dimension(:,:) :: svmbrow !<
  real, allocatable, dimension(:,:) :: svcbrow !<
  real, allocatable, dimension(:,:) :: qnvbrow !<
  real, allocatable, dimension(:,:,:) :: qnmbrow !<
  real, allocatable, dimension(:,:,:) :: qncbrow !<
  real, allocatable, dimension(:,:,:) :: qsvbrow !<
  real, allocatable, dimension(:,:,:,:) :: qsmbrow !<
  real, allocatable, dimension(:,:,:,:) :: qscbrow !<
  real, allocatable, dimension(:) :: qgvbrow !<
  real, allocatable, dimension(:,:) :: qgmbrow !<
  real, allocatable, dimension(:,:) :: qgcbrow !<
  real, allocatable, dimension(:) :: qdvbrow !<
  real, allocatable, dimension(:,:) :: qdmbrow !<
  real, allocatable, dimension(:,:) :: qdcbrow !<
  real, allocatable, dimension(:,:) :: devbrow !<
  real, allocatable, dimension(:,:) :: pdevrow !<
  real, allocatable, dimension(:,:,:) :: dembrow !<
  real, allocatable, dimension(:,:,:) :: decbrow !<
  real, allocatable, dimension(:) :: divbrow !<
  real, allocatable, dimension(:) :: pdivrow !<
  real, allocatable, dimension(:,:) :: dimbrow !<
  real, allocatable, dimension(:,:) :: dicbrow !<
  real, allocatable, dimension(:,:) :: revbrow !<
  real, allocatable, dimension(:,:) :: prevrow !<
  real, allocatable, dimension(:,:,:) :: rembrow !<
  real, allocatable, dimension(:,:,:) :: recbrow !<
  real, allocatable, dimension(:) :: rivbrow !<
  real, allocatable, dimension(:) :: privrow !<
  real, allocatable, dimension(:,:) :: rimbrow !<
  real, allocatable, dimension(:,:) :: ricbrow !<
  !
  real :: voaerow !<
  real :: vbcerow !<
  real :: vaserow !<
  real :: vmderow !<
  real :: vsserow !<
  real :: voawrow !<
  real :: vbcwrow !<
  real :: vaswrow !<
  real :: vmdwrow !<
  real :: vsswrow !<
  real :: voadrow !<
  real :: vbcdrow !<
  real :: vasdrow !<
  real :: vmddrow !<
  real :: vssdrow !<
  real :: voagrow !<
  real :: vbcgrow !<
  real :: vasgrow !<
  real :: vmdgrow !<
  real :: vssgrow !<
  real :: vasirow !<
  real :: vas1row !<
  real :: vas2row !<
  real :: vas3row !<
  real :: vasnrow !<
  real :: vncnrow !<
  real :: vscdrow !<
  real :: vgsprow !<
  real :: defarow !<
  real :: defcrow !<

  real, allocatable, dimension(:) :: sncnrow !<
  real, allocatable, dimension(:) :: ssunrow !<
  real, allocatable, dimension(:) :: scndrow !<
  real, allocatable, dimension(:) :: sgsprow !<
  real, allocatable, dimension(:) :: cc02row !<
  real, allocatable, dimension(:) :: cc04row !<
  real, allocatable, dimension(:) :: cc08row !<
  real, allocatable, dimension(:) :: cc16row !<
  real, allocatable, dimension(:) :: ntrow !<
  real, allocatable, dimension(:) :: n20row !<
  real, allocatable, dimension(:) :: n50row !<
  real, allocatable, dimension(:) :: n100row !<
  real, allocatable, dimension(:) :: n200row !<
  real, allocatable, dimension(:) :: wtrow !<
  real, allocatable, dimension(:) :: w20row !<
  real, allocatable, dimension(:) :: w50row !<
  real, allocatable, dimension(:) :: w100row !<
  real, allocatable, dimension(:) :: w200row !<
  real, allocatable, dimension(:) :: acasrow !<
  real, allocatable, dimension(:) :: acoarow !<
  real, allocatable, dimension(:) :: acbcrow !<
  real, allocatable, dimension(:) :: acssrow !<
  real, allocatable, dimension(:) :: acmdrow !<
  real :: vrn1row !<
  real :: vrm1row !<
  real :: vrn2row !<
  real :: vrm2row !<
  real :: vrn3row !<
  real :: vrm3row !<
  real, allocatable, dimension(:) :: cornrow !<
  real, allocatable, dimension(:) :: cormrow !<
  real, allocatable, dimension(:) :: rsn1row !<
  real, allocatable, dimension(:) :: rsm1row !<
  real, allocatable, dimension(:) :: rsn2row !<
  real, allocatable, dimension(:) :: rsm2row !<
  real, allocatable, dimension(:) :: rsn3row !<
  real, allocatable, dimension(:) :: rsm3row !<
  real, dimension(isdiag) :: sdvlrow !<
  real, dimension(isdiag) :: defxrow !<
  real, dimension(isdiag) :: defnrow !<
  real, dimension(isdust) :: dmacrow !<
  real, dimension(isdust) :: dmcorow !<
  real, dimension(isdust) :: dnacrow !<
  real, dimension(isdust) :: dncorow !<
  real :: duwdrow !<
  real :: dustrow !<
  real :: duthrow !<
  real :: fallrow !<
  real :: fa10row !<
  real :: fa2row !<
  real :: fa1row !<
  real :: usmkrow !<
  !
  real, dimension(isdiag) :: arddr !<
  real, dimension(kextt) :: aextt !<
  real, dimension(kintt) :: aintt !<
  real, dimension(isaintt) :: asain !<
  real, dimension(nrmfld) :: anrmf !<
  real, dimension(isdust) :: asdus !<
  !
  integer :: ntracp !<
  integer :: iaindt !< First index for PLA/PAM tracers 
  integer, allocatable, dimension(:) :: iainda !<
  integer, allocatable, dimension(:) :: itrwet !<
  integer, allocatable, dimension(:) :: iaind !<
  real,    allocatable, dimension(:) :: atrac !<
  !
  !     * input/output file unit numbers.
  !
  data nupar /5 /
  !
  ntrac = 29
  allocate(atrac(ntrac))
  allocate(iainda(ntrac))
  allocate(itrwet(ntrac))
  !
  !=======================================================================
  !     * open and read file that contains basic information about the
  !     * aerosol distribution and air properties.
  !
  open(nupar,file='SCMPRE')
  !
  !-----------------------------------------------------------------------
  !     * read in basic information about simulation (e.g. name, vertical
  !     * levels, time step etc.).
  !
  modl%ntstp=inax
  read(nupar,nml=modnml)
  read(nupar,nml=airnml)
  !
  !-----------------------------------------------------------------------
  !     * grid dimensions.
  !
  ilev=modl%plev   ! number of vertical grid cells for atmosphere
  ilevp1=ilev+1
  its=modl%ntstp   ! number of time steps
  air%zhi=0.       ! height above ground (m)
  !
  !-----------------------------------------------------------------------
  !     * tracer information for driving model.
  !
  call trinfo(ntracp,iainda,iaindt,itrwet,ntrac)
  !
  !     * allocate and initialize array for aerosol tracer indices.
  !
  if ( .not.allocated(iaind) ) allocate(iaind(ntracp))
  do n=1,ntracp
    iaind(n)=iainda(n)
  end do
  !
  !-----------------------------------------------------------------------
  !     * file name and model time.
  !
  cfile=trim(modl%runname)
  dt=modl%dt
  !
  !-----------------------------------------------------------------------
  !     * allocate memory.
  !
  allocate(modtim (its))
  allocate(dz     (ilev))
  allocate(zhi    (ilev))
  allocate(zfi    (0:ilev))
  allocate(zfix   (ilevp1))
  allocate(thi    (ilev,its))
  allocate(tfi    (0:ilev,its))
  allocate(phi    (ilev,its))
  allocate(pfi    (0:ilev,its))
  allocate(rhoa   (ilev,its))
  allocate(trac   (ilev,ntrac,its))
  allocate(sfrc   (ilev,ntrac,its))
  allocate(sfrcrol(1,ilev,ntrac))
  allocate(xrow   (1,ilev,ntrac))
  !
  allocate(so2em  (ilev,its))
  allocate(so2ema (ilev,its))
  allocate(orgem  (ilev,its))
  allocate(docem1 (ilev,its))
  allocate(docem2 (ilev,its))
  allocate(docem3 (ilev,its))
  allocate(docem4 (ilev,its))
  allocate(dbcem1 (ilev,its))
  allocate(dbcem2 (ilev,its))
  allocate(dbcem3 (ilev,its))
  allocate(dbcem4 (ilev,its))
  allocate(dsuem1 (ilev,its))
  allocate(dsuem2 (ilev,its))
  allocate(dsuem3 (ilev,its))
  allocate(dsuem4 (ilev,its))
  !
  allocate(ohrow  (ilev,its))
  allocate(o3row  (ilev,its))
  allocate(no3row (ilev,its))
  allocate(hno3row(ilev,its))
  allocate(nh3row (ilev,its))
  allocate(nh4row (ilev,its))
  allocate(bghpo  (ilev,its))
  !
  allocate(smfrac (its))
  allocate(bsfrac (its))
  allocate(pdsfrow(its))
  !
  allocate(flnd   (its))
  allocate(focn   (its))
  allocate(sicn   (its))
  allocate(fnrol  (its))
  allocate(dmso   (its))
  allocate(fcanrow(its,ican+1))
  !
  allocate(spotrow(its))
  allocate(st02row(its))
  allocate(st03row(its))
  allocate(st04row(its))
  allocate(st06row(its))
  allocate(st13row(its))
  allocate(st14row(its))
  allocate(st15row(its))
  allocate(st16row(its))
  allocate(st17row(its))
  allocate(suz0row(its))
  !
  allocate(oednrow(ilev,kextt))
  allocate(oercrow(ilev,kextt))
  allocate(oidnrow(ilev))
  allocate(oircrow(ilev))
  allocate(svvbrow(ilev))
  allocate(psvvrow(ilev))
  allocate(svmbrow(ilev,nrmfld))
  allocate(svcbrow(ilev,nrmfld))
  allocate(qnvbrow(ilev,isaintt))
  allocate(qnmbrow(ilev,isaintt,nrmfld))
  allocate(qncbrow(ilev,isaintt,nrmfld))
  allocate(qsvbrow(ilev,isaintt,kintt))
  allocate(qsmbrow(ilev,isaintt,kintt,nrmfld))
  allocate(qscbrow(ilev,isaintt,kintt,nrmfld))
  allocate(qgvbrow(ilev))
  allocate(qgmbrow(ilev,nrmfld))
  allocate(qgcbrow(ilev,nrmfld))
  allocate(qdvbrow(ilev))
  allocate(qdmbrow(ilev,nrmfld))
  allocate(qdcbrow(ilev,nrmfld))
  allocate(devbrow(ilev,kextt))
  allocate(pdevrow(ilev,kextt))
  allocate(dembrow(ilev,kextt,nrmfld))
  allocate(decbrow(ilev,kextt,nrmfld))
  allocate(divbrow(ilev))
  allocate(pdivrow(ilev))
  allocate(dimbrow(ilev,nrmfld))
  allocate(dicbrow(ilev,nrmfld))
  allocate(revbrow(ilev,kextt))
  allocate(prevrow(ilev,kextt))
  allocate(rembrow(ilev,kextt,nrmfld))
  allocate(recbrow(ilev,kextt,nrmfld))
  allocate(rivbrow(ilev))
  allocate(privrow(ilev))
  allocate(rimbrow(ilev,nrmfld))
  allocate(ricbrow(ilev,nrmfld))
  !
  allocate(sncnrow(ilev))
  allocate(ssunrow(ilev))
  allocate(scndrow(ilev))
  allocate(sgsprow(ilev))
  allocate(acasrow(ilev))
  allocate(acoarow(ilev))
  allocate(acbcrow(ilev))
  allocate(acssrow(ilev))
  allocate(acmdrow(ilev))
  allocate(ntrow(ilev))
  allocate(n20row(ilev))
  allocate(n50row(ilev))
  allocate(n100row(ilev))
  allocate(n200row(ilev))
  allocate(wtrow(ilev))
  allocate(w20row(ilev))
  allocate(w50row(ilev))
  allocate(w100row(ilev))
  allocate(w200row(ilev))
  allocate(cc02row(ilev))
  allocate(cc04row(ilev))
  allocate(cc08row(ilev))
  allocate(cc16row(ilev))
  allocate(cornrow(ilev))
  allocate(cormrow(ilev))
  allocate(rsn1row(ilev))
  allocate(rsm1row(ilev))
  allocate(rsn2row(ilev))
  allocate(rsm2row(ilev))
  allocate(rsn3row(ilev))
  allocate(rsm3row(ilev))
  !
  !-----------------------------------------------------------------------
  !     * model time.
  !
  do iz=1,its
    modtim(iz)=real(iz-1)*dt
  end do
  !
  !     * vertical level heights (in metres). "F" refers to grid cell
  !     * interfaces, "H" to mid-points.
  !
  dz=modl%dzsfc
  zfi(ilev)=air%zhi
  zfi(ilev-1)=zfi(ilev)+1.5*dz(ilev)
  do iz=ilev-2,0,-1
    zfi(iz)=zfi(iz+1)+dz(iz+1)
  end do
  zhi(ilev)=dz(ilev)
  do iz=ilev-1,1,-1
    zhi(iz)=zhi(iz+1)+dz(iz+1)
  end do
  !
  !-----------------------------------------------------------------------
  !     * read profiles of temperature, relative humidity, and horizontal
  !     * wind speed. it is assumed that continuous vertical profiles are
  !     * available above a miminum and below a maximum altitude, according
  !     * to aircraft altitude range.
  !
  open(33,file='PROFDAT')
  read(33,'(1X)')
  izt=0
  do iz=1,2*ilev
    read(33,'(F8.0)',end=777) ztmp
    if (ztmp > modl%plev*modl%dzsfc+1.e-20) goto 777
    izt=izt+1
    if (iz == 1) then
      zhin=ztmp
    else
      if (abs((ztmp-ztmpp)-modl%dzsfc) > 1.e-20) &
          call xit('MAIN',-1)
    end if
    ztmpp=ztmp
  end do
777 continue
  ilevi=izt
  rewind(33)
  read(33,'(1X)')
  !
  !     * check if point is either a grid point value
  !
  if ( .not. mod(zhin,modl%dzsfc) < 1.e-20) call xit('MAIN',-2)
  if (mod(zhin,modl%dzsfc) < 1.e-20) then
    izt=nint(zhin/modl%dzsfc)-1
  end if
  inum=ilevi
  if (inum < 1) call xit('MAIN',-3)
  if (inum+izt < modl%plev) then
    print*,'REQUESTED NUMBER OF LEVELS = ',modl%plev
    print*,'AVAILABLE NUMBER OF LEVELS = ',inum+izt
    call xit('MAIN',-4)
  end if
  !
  !     * read first interface value, if available
  !
  izt=ilev-izt
  do ilx=1,inum
    !
    !       * read grid point value
    !
    read(33,'(F8.0,3X,F7.3)') zhip,thip
    thi(izt,:)=thip
    !
    !       * linear interpolation between lowest available level and ground
    !
    if (ilx==1) then
      do iz=izt+1,ilev
        thi(iz,:)=air%tempi+(thi(izt,:)-air%tempi) &
                             *(zhi(iz)-air%zhi)/(zhi(izt)-air%zhi)
      end do
    end if
    izt=izt-1
  end do
  !
  !     * temperature (in kelvin).
  !
  tfi(ilev,:)=air%tempi
  do iz=ilev-1,1,-1
    wgt=(zfi(iz)-zhi(iz+1))/(zhi(iz)-zhi(iz+1))
    tfi(iz,:)=wgt*thi(iz,:)+(1.-wgt)*thi(iz+1,:)
  end do
  tfi(0,:)=thi(1,:)
  !
  !     * vertical pressure levels, based on hydrostatic assumption and
  !     * ideal gas law (in pascal).
  !
  pfi(ilev,:)=air%presi
  do iz=ilev-1,0,-1
    pfi(iz,:)=pfi(iz+1,:)*exp(-grav*dz(iz+1)/(rgas*thi(iz+1,:)))
  end do
  phi(ilev,:)=pfi(ilev,:)*exp(-grav*(zhi(ilev)-zfi(ilev)) &
                                 /(rgas*tfi(ilev,:)))
  do iz=ilev-1,1,-1
    phi(iz,:)=phi(iz+1,:)*exp(-grav*(zhi(iz)-zhi(iz+1)) &
                                 /(rgas*tfi(iz,:)))
  end do
  rhoa=phi/(rgas*thi)
  !
  !-----------------------------------------------------------------------
  !     * land-surface properties.
  !
  flnd=0.            ! fraction of land.
  sicn=0.            ! fraction of sea ice.
  focn=1.            ! fraction of ocean.
  smfrac=0.4         ! soil moisture fraction (from class).
  fcanrow(:,1)=0.    ! fraction for tall coniferous trees (from glc2000).
  fcanrow(:,2)=0.4   ! fraction for tall broadleaf trees (from glc2000).
  fcanrow(:,3)=0.    ! fraction for arable land and crops (from glc2000).
  fcanrow(:,4)=0.1   ! fraction for grass, swamp and tundra and other
  ! (from glc2000).
  fcanrow(:,5)=0.    ! not used.
  fnrol=0.           ! snow fraction (from class).
  !
  !     * additional parameters for mineral dust. references:
  !     * general soil types (tegen et al., jgr 2002, zobler, nasa technical
  !     *                     memorandum 1986),
  !     * asian soil types (cheng et al., acp 2008).
  !
  suz0row=0.552e-02  ! monthly mean aerodynamic surface roughness
  ! length (cm) (prigent et al., jgr 2005)
  spotrow=0.466e-01  ! compositional fraction of preferential soil type 1
  ! (paleo lake bed).
  st02row=0.901e-01  ! compositional fraction of soil type 2 (medium).
  st03row=0.141      ! compositional fraction of soil type 3 (fine).
  st04row=0.408      ! compositional fraction of soil type 4 (coarse medium).
  st06row=0.479e-02  ! compositional fraction of soil type 6 (medium fine).
  st13row=0.115e-03  ! compositional fraction of soil type 13 (asian taklimakan).
  st14row=0.847e-03  ! compositional fraction of soil type 14 (asian loess).
  st15row=0.453e-03  ! compositional fraction of soil type 15 (asian gobi).
  st16row=0.161e-03  ! compositional fraction of soil type 16 (asian desert
  ! and sandland).
  st17row=0.102e-02  ! compositional fraction of soil type 17 (asian other
  ! mixture soil).
  bsfrac=0.5         ! bare soil fraction passed from class (default)
  pdsfrow=0.5        ! pdsfrow is a prescribed potential dust source areal :: fraction
  ! obtained from an offline terrestrial model
  !
  !     * specified background concentrations for chemical compounds
  !     * (for calculations of in-cloud oxidation and cloud water ph).
  !
  co2_ppm=380.e-06   ! co2 volume mixing ratio.
  ohrow=1.e-14       ! total hydroxyl radical volume mixing ratio.
  o3row=5.e-08       ! total ozone volume mixing ratio.
  no3row=1.e-13      ! total nitrate radical volume mixing ratio.
  hno3row=1.e-09     ! total nitric acid volume mixing ratio.
  nh3row=1.e-09      ! total ammonia volume mixing ratio.
  nh4row=5.e-10      ! total ammonium volume mixing ratio.
  bghpo=1.e-11       ! total hydrogen volume mixing ratio.
  !
  !     * ocean dms concentration (nmole).
  !
  dmso=10.
  !
  !-----------------------------------------------------------------------
  !     * emissions of sulphur dioxide (kg/kg/sec).
  !
  so2em=0.
  so2em(ilev,:)=3.e-13
  !
  !     * emissions of sulphur dioxide (kg/kg/sec) that are directly
  !     * used to produce primary sulphate particles.
  !
  so2ema=so2em*0.025
  !
  !     * emissions of soa precursor gases (kg/kg/sec).
  !
  orgem=0.
  !      orgem(ilev,:)=1.e-13
  !
  !     * 3d primary aerosol emission rates for total aerosol mass
  !     * for particulate organic matter (kg/kg/sec).
  !     * 1 - open vegetation and biofuel burning
  !     * 2 - fossil fuel burning
  !     * 3 - aircraft
  !     * 4 - shipping
  !
  docem1=0.
  docem2=0.
  docem3=0.
  docem4=0.
  !
  !     * 3d primary aerosol emission rates for total aerosol mass
  !     * for black carbon (kg/kg/sec).
  !
  dbcem1=0.
  dbcem2=0.
  dbcem3=0.
  dbcem4=0.
  !      dbcem2(ilev,:)=5.e-14
  !
  !     * 3d primary aerosol emission rates for total aerosol mass
  !     * for ammoninum sulphate (kg/kg/sec).
  !
  dsuem1=0.25*so2ema*wamsul/wso2
  dsuem2=0.25*so2ema*wamsul/wso2
  dsuem3=0.25*so2ema*wamsul/wso2
  dsuem4=0.25*so2ema*wamsul/wso2
  !
  !-----------------------------------------------------------------------
  !     * if an emissions file is present, use emissions values
  !     * and land/ocean fractions from the file.
  !
  inquire(file='EMISSIONS',exist=kemiss)
  if (kemiss) then
    nuemi=15
    open(nuemi,file='EMISSIONS')
    read(nuemi,nml=emisnml)
    !
    so2em(ilev,:)=emis%so2em
    orgem(ilev,:)=emis%orgem
    !
    ohrow=emis%ohrow
    o3row=emis%o3row
    !
    docem1(ilev,:)=emis%docem1
    docem2(ilev,:)=emis%docem2
    docem3(ilev,:)=emis%docem3
    docem4(ilev,:)=emis%docem4
    !
    dbcem1(ilev,:)=emis%dbcem1
    dbcem2(ilev,:)=emis%dbcem2
    dbcem3(ilev,:)=emis%dbcem3
    dbcem4(ilev,:)=emis%dbcem4
    !
    flnd=emis%flnd
    focn=emis%focn
    !
    st02row=emis%st02row
    st03row=emis%st03row
    st04row=emis%st04row
    st06row=emis%st06row
    st13row=emis%st13row
    st14row=emis%st14row
    st15row=emis%st15row
    st16row=emis%st16row
    st17row=emis%st17row
    spotrow=emis%spotrow
    fcanrow(:,1)=emis%fcanrow1
    fcanrow(:,2)=emis%fcanrow2
    fcanrow(:,3)=emis%fcanrow3
    fcanrow(:,4)=emis%fcanrow4

    smfrac=emis%smfrac
    bsfrac=emis%bsfrac
  end if
  !
  !-----------------------------------------------------------------------
  !     * INITIAL TRACER MASS MIXING RATIO (KG/KG). ALL MMR'S ARE
  !     * initialized to zero, except hydrogen peroxide.
  !
  trac=0.
  !
  !     * initial cloudy-sky/clear-sky ratio for tracers (for
  !     * subgrid-scale processes affecting tracers).
  !
  sfrcrol=0.
  !
  !     * initialization of pla calculations.
  !
  nupar=5
  open(nupar,file='PARAM')
  inquire(file="COAGDAT",exist=kcoagd)
  if ( .not.kcoagd) then
    call xit('MAIN',-5)
  end if
  nucoa=77
  open(nucoa,file='COAGDAT')
  write(6,'(A41)') 'INITIALIZATION OF BASIC PLA PARAMETERS...'
  call sdconf(nupar,nucoa,mynode)
  !
  !     * initial aerosol size distributions.
  !
  call geninit
  imode=3
  call initphys(xrow,sfrcrol,iaind,ntracp,1,1,1,ilev,ilev,1,1, &
                    ntrac,imode,its)
  !
  !     * CONVERT CONCENTRATIONS TO MMR'S AND SAVE IN OUTPUT ARRAYS.
  !
  do iz=1,its
    do n=1,ntrac
      trac(:,n,iz)=xrow(1,:,n)/rhoa(:,iz)
      sfrc(:,n,iz)=sfrcrol(1,:,n)
    end do
  end do
  zmolgair=28.84e-03
  wst=32.060e-03
  factc=wst/zmolgair
  trac(:,ihpo,:)=bghpo*factc
  !
  !=======================================================================
  !     * output.
  !=======================================================================
  !     * definition of field dimensions.
  !
  zfix(1:ilevp1)=zfi(0:ilev)
  do i=1,isdiag
    arddr(i)=real(i)
  end do
  do i=1,ntrac
    atrac(i)=real(i)
  end do
  do i=1,kextt
    aextt(i)=real(i)
  end do
  do i=1,kintt
    aintt(i)=real(i)
  end do
  do i=1,isaintt
    asain(i)=real(i)
  end do
  do i=1,nrmfld
    anrmf(i)=real(i)
  end do
  do i=1,isdust
    asdus(i)=real(i)
  end do
  call setncdimprepam(zhi,zfix,modtim,arddr,atrac,aextt,aintt, &
                          asain,anrmf,asdus, &
                          ilev,ilevp1,its,isdiag,ntrac,kextt,kintt, &
                          isaintt,nrmfld,isdust)
  !
  !     * initialize input and output arrays.
  !
  call initio(.false.,.false.)
  !
  call w0dfld('rvco2',co2_ppm)
  call w1dfld('dmso',dmso,its)
  call w1dfld('flnd',flnd,its)
  call w1dfld('focn',focn,its)
  call w1dfld('sicn',sicn,its)
  call w1dfld('fsm',smfrac,its)
  call w1dfld('fcan1',fcanrow(:,1),its)
  call w1dfld('fcan2',fcanrow(:,2),its)
  call w1dfld('fcan3',fcanrow(:,3),its)
  call w1dfld('fcan4',fcanrow(:,4),its)
  call w1dfld('fsnw',fnrol,its)
  call w1dfld('suzo',suz0row,its)
  call w1dfld('spot',spotrow,its)
  call w1dfld('st2',st02row,its)
  call w1dfld('st3',st03row,its)
  call w1dfld('st4',st04row,its)
  call w1dfld('st6',st06row,its)
  call w1dfld('st13',st13row,its)
  call w1dfld('st14',st14row,its)
  call w1dfld('st15',st15row,its)
  call w1dfld('st16',st16row,its)
  call w1dfld('st17',st17row,its)
  call w1dfld('fbgnd',bsfrac,its)
  call w1dfld('fpds',pdsfrow,its)
  !
  call w2dfld('rvo3',o3row,ilev,its)
  call w2dfld('rvno3',no3row,ilev,its)
  call w2dfld('rvoh',ohrow,ilev,its)
  call w2dfld('rvhno3',hno3row,ilev,its)
  call w2dfld('rvnh3',nh3row,ilev,its)
  call w2dfld('rvnh4',nh4row,ilev,its)
  call w2dfld('rvh2o2',bghpo,ilev,its)
  call w2dfld('emiso2',so2em,ilev,its)
  call w2dfld('emisoap',orgem,ilev,its)
  call w2dfld('emibc1',dbcem1,ilev,its)
  call w2dfld('emibc2',dbcem2,ilev,its)
  call w2dfld('emibc3',dbcem3,ilev,its)
  call w2dfld('emibc4',dbcem4,ilev,its)
  call w2dfld('emioa1',docem1,ilev,its)
  call w2dfld('emioa2',docem2,ilev,its)
  call w2dfld('emioa3',docem3,ilev,its)
  call w2dfld('emioa4',docem4,ilev,its)
  call w2dfld('emias1',dsuem1,ilev,its)
  call w2dfld('emias2',dsuem2,ilev,its)
  call w2dfld('emias3',dsuem3,ilev,its)
  call w2dfld('emias4',dsuem4,ilev,its)
  !
  call w3dfld('rtrac',trac,ilev,ntrac,its)
  call w3dfld('strac',sfrc,ilev,ntrac,its)
  !
  !     * write boundary conditions to output file.
  !
  cfilx=trim(cfile)//'_pam_bnd'
  call wncdat(cfilx)
  !
  !-----------------------------------------------------------------------
  !     * initialize pam internal arrays.
  !
  oednrow=yna
  oercrow=yna
  oidnrow=yna
  oircrow=yna
  svvbrow=0.
  psvvrow=yna
  svmbrow=0.
  svcbrow=0.
  qnvbrow=0.
  qnmbrow=0.
  qncbrow=0.
  qsvbrow=0.
  qsmbrow=0.
  qscbrow=0.
  qgvbrow=0.
  qgmbrow=0.
  qgcbrow=0.
  qdvbrow=0.
  qdmbrow=0.
  qdcbrow=0.
  devbrow=0.
  pdevrow=yna
  dembrow=0.
  decbrow=0.
  divbrow=0.
  pdivrow=yna
  dimbrow=0.
  dicbrow=0.
  revbrow=yna
  prevrow=yna
  rembrow=0.
  recbrow=0.
  rivbrow=yna
  privrow=yna
  rimbrow=0.
  ricbrow=0.
  if (kxtra1) then
    vbcerow=0.
    voaerow=0.
    vaserow=0.
    vsserow=0.
    vmderow=0.
    vbcwrow=0.
    voawrow=0.
    vaswrow=0.
    vsswrow=0.
    vmdwrow=0.
    vbcdrow=0.
    voadrow=0.
    vasdrow=0.
    vssdrow=0.
    vmddrow=0.
    vbcgrow=0.
    voagrow=0.
    vasgrow=0.
    vssgrow=0.
    vmdgrow=0.
    ssunrow=0.
    vasnrow=0.
    vncnrow=0.
    vscdrow=0.
    vgsprow=0.
    sncnrow=0.
    scndrow=0.
    sgsprow=0.
    acasrow=0.
    acoarow=0.
    acbcrow=0.
    acssrow=0.
    acmdrow=0.
    ntrow=0.
    n20row=0.
    n50row=0.
    n100row=0.
    n200row=0.
    wtrow=0.
    w20row=0.
    w50row=0.
    w100row=0.
    w200row=0.
    cc02row=0.
    cc04row=0.
    cc08row=0.
    cc16row=0.
    vasirow=0.
    vas1row=0.
    vas2row=0.
    vas3row=0.
  end if
  if (kxtra2) then
    cornrow=0.
    cormrow=0.
    rsn1row=0.
    rsm1row=0.
    rsn2row=0.
    rsm2row=0.
    rsn3row=0.
    rsm3row=0.
    vrn1row=0.
    vrm1row=0.
    vrn2row=0.
    vrm2row=0.
    vrn3row=0.
    vrm3row=0.
    sdvlrow=0.
    defxrow=0.
    defnrow=0.
    dmacrow=0.
    dmcorow=0.
    dnacrow=0.
    dncorow=0.
    defarow=0.
    defcrow=0.
  end if
  if (isvdust > 0) then
    duwdrow=0.
    dustrow=0.
    duthrow=0.
    fallrow=0.
    fa10row=0.
    fa2row =0.
    fa1row =0.
    usmkrow=0.
  end if
  !
  !-----------------------------------------------------------------------
  !     * write initial prognostic fields to restart file.
  !
  !
  !     * pla work fields.
  !
  call w0dfldrs('tstep',real(0))
  call w2dfldrs('rtrac',trac(:,:,1),ilev,ntrac)
  call w2dfldrs('strac',sfrcrol(:,:,1),ilev,ntrac)
  call w1dfldrs('oidn',oidnrow,ilev)
  call w1dfldrs('oirc',oircrow,ilev)
  call w1dfldrs('svvb',svvbrow,ilev)
  call w1dfldrs('psvv',psvvrow,ilev)
  call w1dfldrs('qgvb',qgvbrow,ilev)
  call w1dfldrs('qdvb',qdvbrow,ilev)
  call w1dfldrs('divb',divbrow,ilev)
  call w1dfldrs('pdiv',pdivrow,ilev)
  call w1dfldrs('rivb',rivbrow,ilev)
  call w1dfldrs('priv',privrow,ilev)
  call w2dfldrs('oedn',oednrow,ilev,kextt)
  call w2dfldrs('oerc',oercrow,ilev,kextt)
  call w2dfldrs('devb',devbrow,ilev,kextt)
  call w2dfldrs('pdev',pdevrow,ilev,kextt)
  call w2dfldrs('revb',revbrow,ilev,kextt)
  call w2dfldrs('prev',prevrow,ilev,kextt)
  call w2dfldrs('qnvb',qnvbrow,ilev,isaintt)
  call w2dfldrs('svmb',svmbrow,ilev,nrmfld)
  call w2dfldrs('svcb',svcbrow,ilev,nrmfld)
  call w2dfldrs('qgmb',qgmbrow,ilev,nrmfld)
  call w2dfldrs('qgcb',qgcbrow,ilev,nrmfld)
  call w2dfldrs('qdmb',qdmbrow,ilev,nrmfld)
  call w2dfldrs('qdcb',qdcbrow,ilev,nrmfld)
  call w2dfldrs('dimb',dimbrow,ilev,nrmfld)
  call w2dfldrs('dicb',dicbrow,ilev,nrmfld)
  call w2dfldrs('rimb',rimbrow,ilev,nrmfld)
  call w2dfldrs('ricb',ricbrow,ilev,nrmfld)
  call w3dfldrs('qsvb',qsvbrow,ilev,isaintt,kintt)
  call w3dfldrs('qnmb',qnmbrow,ilev,isaintt,nrmfld)
  call w3dfldrs('qncb',qncbrow,ilev,isaintt,nrmfld)
  call w3dfldrs('demb',dembrow,ilev,kextt,nrmfld)
  call w3dfldrs('decb',decbrow,ilev,kextt,nrmfld)
  call w3dfldrs('remb',rembrow,ilev,kextt,nrmfld)
  call w3dfldrs('recb',recbrow,ilev,kextt,nrmfld)
  call w4dfldrs('qsmb',qsmbrow,ilev,isaintt,kintt,nrmfld)
  call w4dfldrs('qscb',qscbrow,ilev,isaintt,kintt,nrmfld)
  !
  !     * pla extra diagnostic fields.
  !
  if (kxtra1) then
    call w0dfldrs('emibc',vbcerow)
    call w0dfldrs('emioa',voaerow)
    call w0dfldrs('emias',vaserow)
    call w0dfldrs('emiss',vsserow)
    call w0dfldrs('emimd',vmderow)
    call w0dfldrs('wetbcl',vbcwrow)
    call w0dfldrs('wetoal',voawrow)
    call w0dfldrs('wetasl',vaswrow)
    call w0dfldrs('wetssl',vsswrow)
    call w0dfldrs('wetmdl',vmdwrow)
    call w0dfldrs('drybc',vbcdrow)
    call w0dfldrs('dryoa',voadrow)
    call w0dfldrs('dryas',vasdrow)
    call w0dfldrs('dryss',vssdrow)
    call w0dfldrs('drymd',vmddrow)
    call w0dfldrs('vgtpbc',vbcgrow)
    call w0dfldrs('vgtpoa',voagrow)
    call w0dfldrs('vgtpas',vasgrow)
    call w0dfldrs('vgtpss',vssgrow)
    call w0dfldrs('vgtpmd',vmdgrow)
    call w0dfldrs('wetasl1',vas1row)
    call w0dfldrs('wetasl2',vas2row)
    call w0dfldrs('wetasl3',vas3row)
    call w0dfldrs('incldasl',vasirow)
    call w0dfldrs('vnuclas',vasnrow)
    call w0dfldrs('vnucln',vncnrow)
    call w0dfldrs('vcondas',vscdrow)
    call w0dfldrs('vgppsa',vgsprow)
    call w0dfldrs('emimdacc',defarow)
    call w0dfldrs('emimdcoa',defcrow)
    call w1dfldrs('nuclas',ssunrow,ilev)
    call w1dfldrs('nucln',sncnrow,ilev)
    call w1dfldrs('condas',scndrow,ilev)
    call w1dfldrs('gppsa',sgsprow,ilev)
    call w1dfldrs('concasd',acasrow,ilev)
    call w1dfldrs('concoad',acoarow,ilev)
    call w1dfldrs('concbcd',acbcrow,ilev)
    call w1dfldrs('concssd',acssrow,ilev)
    call w1dfldrs('concmdd',acmdrow,ilev)
    call w1dfldrs('cn0',ntrow,ilev)
    call w1dfldrs('cn20',n20row,ilev)
    call w1dfldrs('cn50',n50row,ilev)
    call w1dfldrs('cn100',n100row,ilev)
    call w1dfldrs('cn200',n200row,ilev)
    call w1dfldrs('wcn0',wtrow,ilev)
    call w1dfldrs('wcn20',w20row,ilev)
    call w1dfldrs('wcn50',w50row,ilev)
    call w1dfldrs('wcn100',w100row,ilev)
    call w1dfldrs('wcn200',w200row,ilev)
    call w1dfldrs('ccnc02',cc02row,ilev)
    call w1dfldrs('ccnc04',cc04row,ilev)
    call w1dfldrs('ccnc08',cc08row,ilev)
    call w1dfldrs('ccnc16',cc16row,ilev)
  end if
  if (kxtra2) then
    call w0dfldrs('vresn1',vrn1row)
    call w0dfldrs('vresn2',vrn2row)
    call w0dfldrs('vresn3',vrn3row)
    call w0dfldrs('vresm1',vrm1row)
    call w0dfldrs('vresm2',vrm2row)
    call w0dfldrs('vresm3',vrm3row)
    call w1dfldrs('resn',cornrow,ilev)
    call w1dfldrs('resm',cormrow,ilev)
    call w1dfldrs('resn1',rsn1row,ilev)
    call w1dfldrs('resn2',rsn2row,ilev)
    call w1dfldrs('resn3',rsn3row,ilev)
    call w1dfldrs('resm1',rsm1row,ilev)
    call w1dfldrs('resm2',rsm2row,ilev)
    call w1dfldrs('resm3',rsm3row,ilev)
    call w1dfldrs('vsdvl',sdvlrow,isdiag)
    call w1dfldrs('defxmd',defxrow,isdiag)
    call w1dfldrs('defnmd',defnrow,isdiag)
    call w1dfldrs('diagmd1',dmacrow,isdust)
    call w1dfldrs('diagmd2',dmcorow,isdust)
    call w1dfldrs('diagmdn1',dnacrow,isdust)
    call w1dfldrs('diagmdn2',dncorow,isdust)
  end if
  if (isvdust > 0) then
    call w0dfldrs('umd',duwdrow)
    call w0dfldrs('ustarmd',dustrow)
    call w0dfldrs('ustarmdthr',duthrow)
    call w0dfldrs('fallmd',fallrow)
    call w0dfldrs('fa10md',fa10row)
    call w0dfldrs('fa2md',fa2row)
    call w0dfldrs('fa1md',fa1row)
    call w0dfldrs('ustarmdthtfr',usmkrow)
  end if
  !
  !     * write the data.
  !
  cfilx=trim(cfile)//'_pam_tini'
  call wncdatrs(cfilx)
  !
end program pam_pre
