subroutine setncdimpre(alev,alev1,atime,ilev,ilev1,itime)
  !
  use iodat
  !
  implicit none
  !
  integer :: i
  integer :: nd
  !
  integer, intent(in) :: ilev !<
  integer, intent(in) :: ilev1 !<
  integer, intent(in) :: itime !<
  real, intent(in), dimension(ilev)    :: alev !<
  real, intent(in), dimension(ilev1)   :: alev1 !<
  real, intent(in), dimension(itime)   :: atime !<
  !
  !     * define netcdf dimensions and associate with dimensions
  !     * in the model.
  !
  ndim=10
  allocate(dim(ndim))
  dim(:)%flgi=0
  dim(:)%flgo=0
  dim(:)%flgrs=0
  do nd=1,ndim
    do i=1,imaxl
      dim(nd)%name(i:i)=' '
      dim(nd)%units(i:i)=' '
      dim(nd)%lname(i:i)=' '
    end do
  end do
  !
  dim(1)%name (1:12)='level       '
  dim(1)%lname(1:12)='Altitude    '
  dim(1)%units(1:12)='m           '
  dim(1)%size=ilev
  allocate(dim(1)%val(dim(1)%size))
  dim(1)%val(:)=alev(:)
  !
  dim(2)%name (1:12)='level1      '
  dim(2)%lname(1:35)='Altitude of grid cell interfaces   '
  dim(2)%units(1:12)='m           '
  dim(2)%size=ilev+1
  allocate(dim(2)%val(dim(2)%size))
  dim(2)%val(:)=alev1(:)
  !
  dim(3)%name (1:12)='time        '
  dim(3)%lname(1:12)='Model time  '
  dim(3)%units(1:12)='s           '
  dim(3)%size=itime
  allocate(dim(3)%val(dim(3)%size))
  dim(3)%val(:)=atime(:)
  !
end subroutine setncdimpre
