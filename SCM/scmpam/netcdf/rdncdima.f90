subroutine rdncdima(file1,file2,ilev,ilev1,itime,irddr,ntrac, &
                          kextt,kintt,isaintt,nrmfld,isdust,ktime1)
  !
  use iodat
  use netcdf
  !
  implicit none
  !
  character(len=*), intent(in) :: file1 !<
  character(len=*), intent(in) :: file2 !<
  integer, intent(out) :: ilev !<
  integer, intent(out) :: ilev1 !<
  integer, intent(out) :: itime !<
  integer, intent(out) :: irddr !<
  integer, intent(out) :: ntrac !<
  integer, intent(out) :: kextt !<
  integer, intent(out) :: kintt !<
  integer, intent(out) :: isaintt !<
  integer, intent(out) :: nrmfld !<
  integer, intent(out) :: isdust !<
  logical, intent(in) :: ktime1 !<
  integer, parameter :: nfil=2 !<
  integer, dimension(nfil) :: ncid !<
  integer :: status !<
  integer :: nd !<
  integer :: nf !<
  integer :: i !<
  integer, dimension(:), allocatable :: lvl_dimid !<
  !
  !     * initialization of field dimensions.
  !
  ilev=inax
  ilev1=inax
  itime=inax
  irddr=inax
  ntrac=inax
  kextt=inax
  kintt=inax
  isaintt=inax
  nrmfld=inax
  isdust=inax
  !
  !     * open netcdf files with input and restart data.
  !
  status=nf90_open(trim(file1)//'.nc', nf90_nowrite, ncid(1))
  if (status /= nf90_noerr) call xit('RDNCDIM',-1)
  status=nf90_open(trim(file2)//'.nc', nf90_nowrite, ncid(2))
  if (status /= nf90_noerr) call xit('RDNCDIM',-2)
  !
  !     * define 10 netcdf dimension names. an additional time dimension
  !     * may be generated to support output fields, if necessary.
  !
  ndim=10
  if (ktime1) ndim=ndim+1
  !
  !     * initialization.
  !
  allocate(dim(ndim))
  do nd=1,ndim
    do i=1,imaxl
      dim(nd)%name(i:i)=' '
    end do
  end do
  !
  !     * dimension names.
  !
  dim(1)%name(1:10)='level     '
  dim(2)%name(1:10)='level1    '
  dim(3)%name(1:10)='time      '
  dim(4)%name(1:10)='raddry    '
  dim(5)%name(1:10)='ntrac     '
  dim(6)%name(1:10)='kextt     '
  dim(7)%name(1:10)='kintt     '
  dim(8)%name(1:10)='isaintt   '
  dim(9)%name(1:10)='nrmfld    '
  dim(10)%name(1:10)='isdust    '
  !
  !     * extra time dimension for output.
  !
  if (ktime1) dim(ndim)%name(1:10)='time1     '
  !
  !     * initializations.
  !
  allocate(lvl_dimid(ndim))
  dim(:)%flgi=0
  dim(:)%flgo=0
  dim(:)%flgrs=0
  dim(:)%flgx=0
  !
  !     * read the dimensions and coordinates from the files.
  !
  do nf=1,nfil
    do nd=1,ndim
      if (dim(nd)%flgx==0) then
        status=nf90_inq_dimid(ncid(nf), trim(dim(nd)%name), &
                                          lvl_dimid(nd))
        if (status == nf90_noerr) then
          dim(nd)%flgx=1
          status=nf90_inquire_dimension(ncid(nf), lvl_dimid(nd), &
                                          len=dim(nd)%size)
          status=nf90_get_att(ncid(nf), lvl_dimid(nd), "units", &
                                          dim(nd)%units)
          status=nf90_get_att(ncid(nf), lvl_dimid(nd), "long_name", &
                                          dim(nd)%lname)
          status=nf90_inq_varid(ncid(nf), trim(dim(nd)%name), &
                                            lvl_dimid(nd))
          allocate(dim(nd)%val(dim(nd)%size))
          status=nf90_get_var(ncid(nf), lvl_dimid(nd), dim(nd)%val)
          !
          !           * associate dimensions in model with netcdf dimensions.
          !
          select case (nd)
          case (1)
            ilev =dim(nd)%size
          case (2)
            ilev1=dim(nd)%size
          case (3)
            itime=dim(nd)%size
          case (4)
            irddr=dim(nd)%size
          case (5)
            ntrac=dim(nd)%size
          case (6)
            kextt=dim(nd)%size
          case (7)
            kintt=dim(nd)%size
          case (8)
            isaintt=dim(nd)%size
          case (9)
            nrmfld=dim(nd)%size
          case (10)
            isdust=dim(nd)%size
          end select
        end if
      end if
    end do
  end do
  do nd=1,ndim
    if (dim(nd)%flgx==0) then
      print*,'COULD NOT FIND DIMENSION ',trim(dim(nd)%name), &
                  ' IN INPUT FILES'
    end if
  end do
  !
end subroutine rdncdima
