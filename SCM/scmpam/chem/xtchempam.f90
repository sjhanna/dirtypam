subroutine xtchempam(xrow,th,qv, &
                           sgpp,dshj,shj,cszrow,zdayl,pressg,iaind,ntracp, &
                           sfrc,ohrow,h2o2row,o3row,no3row, &
                           hno3row,nh3row,nh4row, &
                           dox4row,doxdrow,noxdrow,zclf,zmratep,zfsnow, &
                           zfrain,zmlwc,zfevap,clrfr,clrfs,zfsubl,atau, &
                           itrwet,iso2,idms,ihpo,ztmst,nlatj, &
                           ntrac,ilg,il1,il2,ilev)
  !
  !     * june 2, 2013 - k.vonsalzen. pla version of xtchemie.
  !
  use sdphys
  !
  implicit none
  real :: asrphob
  real :: asrso4
  real :: fac
  real :: factcon
  integer, intent(in) :: idms
  integer, intent(in) :: ihpo
  integer :: il
  integer, intent(in) :: il1
  integer, intent(in) :: il2
  integer, intent(in) :: ilev
  integer, intent(in) :: ilg
  integer, intent(in) :: iso2
  integer :: iso4
  integer :: jk
  integer :: jt
  integer :: nage
  integer :: neqp
  integer, intent(in) :: nlatj
  integer, intent(in) :: ntrac
  integer, intent(in) :: ntracp
  real :: one
  real :: pcons2
  real :: plarge
  real :: pqtmst
  real :: scalf
  real :: t
  real :: x
  real :: y
  real :: ysmall
  real :: ysphr
  real :: ytau
  real :: zavo
  real :: zcthr
  real :: zdms
  real :: zero
  real :: zexp
  real :: zfarr1
  real :: zfarr2
  real :: zfarr3
  real :: zfarr4
  real :: zh
  real :: zhil
  real :: zk
  real :: zk2
  real :: zk2f
  real :: zk2i
  real :: zk3
  real :: zm
  real :: zmin
  real :: zmolgair
  real :: zmolgs
  real :: znamair
  real :: zso2
  real :: ztk1
  real :: ztk2
  real :: ztk23b
  real :: ztk3
  real :: ztka
  real :: ztkb
  real, intent(in) :: ztmst
  real :: ztpq
  real :: zxtp1dms
  real :: zxtp1so2
  !
  real, intent(inout)   :: xrow(ilg,ilev,ntrac) !<
  real, intent(inout)   :: sfrc(ilg,ilev,ntrac) !<
  !
  real, intent(in)   :: th  (ilg,ilev) !<
  real, intent(in)   :: qv  (ilg,ilev) !<
  real, intent(in)   :: dshj(ilg,ilev) !<
  real, intent(in)   :: shj (ilg,ilev) !<
  !
  real, intent(in)   :: cszrow(ilg) !<
  real, intent(in)   :: pressg(ilg) !<
  real, intent(inout)   :: atau(ilg) !<
  !
  !     * species passed in vmr (dimensionless).
  !
  real, intent(in)   :: ohrow  (ilg,ilev) !<
  real, intent(in)   :: h2o2row(ilg,ilev) !<
  real, intent(in)   :: o3row  (ilg,ilev) !<
  real, intent(in)   :: no3row (ilg,ilev) !<
  real, intent(in)   :: hno3row(ilg,ilev) !<
  real, intent(in)   :: nh3row (ilg,ilev) !<
  real, intent(in)   :: nh4row (ilg,ilev) !<
  !
  real, intent(inout)   :: dox4row(ilg) !<
  real, intent(inout)   :: doxdrow(ilg) !<
  real, intent(inout)   :: noxdrow(ilg) !<
  !
  !     * arrays shared with cond in physics.
  !
  real, intent(in)   :: zclf(ilg,ilev) !<
  real, intent(inout)   :: zmratep(ilg,ilev) !<
  real, intent(in)   :: zfrain(ilg,ilev) !<
  real, intent(in)   :: clrfr(ilg,ilev) !<
  real, intent(in)   :: zfsnow(ilg,ilev) !<
  real, intent(inout)   :: zmlwc(ilg,ilev) !<
  real, intent(in)   :: zfevap(ilg,ilev) !<
  real, intent(in)   :: clrfs(ilg,ilev) !<
  real, intent(in)   :: zfsubl(ilg,ilev) !<
  !
  !     * control index arrays for tracers.
  !
  integer, intent(in), dimension(ntrac) :: itrwet !<
  !
  !     * internal work arrays:
  !
  !     * general work arrays for xtchemie5
  !
  real  ,  dimension(ilg,ilev,ntrac)  :: zxte !<
  real  ,  dimension(ilg,ilev,ntrac)  :: xcld !<
  real  ,  dimension(ilg,ilev,ntrac)  :: xclr !<
  real  ,  dimension(ilg,ilev)  :: zxtp10 !<
  real  ,  dimension(ilg,ilev)  :: zxtp1c !<
  real  ,  dimension(ilg,ilev)  :: zhenry !<
  real  ,  dimension(ilg,ilev)  :: zso4 !<
  real  ,  dimension(ilg,ilev)  :: zzoh !<
  real  ,  dimension(ilg,ilev)  :: zzh2o2 !<
  real  ,  dimension(ilg,ilev)  :: zzo3 !<
  real  ,  dimension(ilg,ilev)  :: zzo2 !<
  real  ,  dimension(ilg,ilev)  :: zzno3 !<
  real  ,  dimension(ilg,ilev)  :: agthpo !<
  real  ,  dimension(ilg,ilev)  :: zrho0 !<
  real  ,  dimension(ilg,ilev)  :: zrhctox !<
  real  ,  dimension(ilg,ilev)  :: zrhxtoc !<
  real  ,  dimension(ilg,ilev)  :: zdep3d !<
  real  ,  dimension(ilg,ilev)  :: pdclr !<
  real  ,  dimension(ilg,ilev)  :: pdcld !<
  real  ,  dimension(ilg,ilev)  :: tmpscl !<
  real  ,  dimension(ilg,ilev)  :: xarow !<
  real  ,  dimension(ilg,ilev)  :: xcrow !<
  real  ,  dimension(ilg,ilev)  :: dxcdt !<
  real  ,  dimension(ilg,ilev)  :: dxadt !<
  real  ,  dimension(ilg,ilev)  :: dxdt !<
  real  ,  dimension(ilg)  :: za21 !<
  real  ,  dimension(ilg)  :: za22 !<
  real  ,  dimension(ilg)  :: zdt !<
  real  ,  dimension(ilg)  :: ze3 !<
  real  ,  dimension(ilg)  :: zfac1 !<
  real  ,  dimension(ilg)  :: zf_o3 !<
  real  ,  dimension(ilg)  :: zh2o2m !<
  real  ,  dimension(ilg)  :: zso2m !<
  real  ,  dimension(ilg)  :: zso4m !<
  real  ,  dimension(ilg)  :: zsumh2o2 !<
  real  ,  dimension(ilg)  :: zsumo3 !<
  real  ,  dimension(ilg)  :: zxtp1 !<
  real  ,  dimension(ilg)  :: zza !<
  real, intent(in)  ,  dimension(ilg)  :: zdayl !<
  real, intent(inout)  ,  dimension(ilg,ilev)  :: sgpp !<
  !
  !     * work arrays used in oxistr3 only.
  !
  parameter (nage=1)
  parameter (neqp=13)
  logical, dimension(ilg,ilev)  :: kcalc !<
  real  ,  dimension(ilg,ilev,neqp)  :: achpa !<
  real  ,  dimension(ilg,ilev)  :: roarow !<
  real  ,  dimension(ilg,ilev)  :: thg !<
  real  ,  dimension(ilg,ilev)  :: agtso2 !<
  real  ,  dimension(ilg,ilev)  :: agtso4 !<
  real  ,  dimension(ilg,ilev)  :: agto3 !<
  real  ,  dimension(ilg,ilev)  :: agtco2 !<
  real  ,  dimension(ilg,ilev)  :: agtho2 !<
  real  ,  dimension(ilg,ilev)  :: agthno3 !<
  real  ,  dimension(ilg,ilev)  :: agtnh3 !<
  real  ,  dimension(ilg,ilev)  :: antso2 !<
  real  ,  dimension(ilg,ilev)  :: antho2 !<
  real  ,  dimension(ilg,ilev)  :: antso4 !<
  real  ,  dimension(ilg,ilev)  :: aoh2o2 !<
  real  ,  dimension(ilg,ilev)  :: aresid !<
  real  ,  dimension(ilg,ilev)  :: wrk1 !<
  real  ,  dimension(ilg,ilev)  :: wrk2 !<
  real  ,  dimension(ilg,ilev)  :: wrk3 !<
  integer, dimension(ilg)       :: ilwcp !<
  integer, dimension(ilg)       :: ichem !<
  integer, intent(in), dimension(ntracp) :: iaind !<
  !
  data ytau / 1800. /
  data zcthr / .99 /
  data ysmall / 9.e-30 /
  !
  data zero,one,plarge /0., 1., 1.e+02/
  data zfarr1,zfarr2,zfarr3,zfarr4 /8.e+04, -3650., 9.7e+04, 6600./
  !=======================================================================
  !     * define function for changing the units
  !     * from mass-mixing ratio to molecules per cm**3 and vice versa
  !
  real :: xtoc
  real :: ctox
  real :: zfarr
  xtoc(x,y)=x*6.022e+20/y
  ctox(x,y)=y/(6.022e+20*x)
  !
  !     * x = density of air, y = mol weight in gramm
  !
  zfarr(zk,zh,ztpq)=zk*exp(zh*ztpq)
  !-----------------------------------------------------------------------
  !
  !     * constants.
  !
  pcons2=1./(ztmst*grav)
  pqtmst=1./ztmst
  zmin=1.e-20
  asrso4 = 0.9
  asrphob= 0.
  zk2i=2.0e-12
  zk2=4.0e-31
  zk2f=0.45
  zk3=1.9e-13
  zmolgs=32.064
  zmolgair=28.84
  zavo=6.022e+23
  znamair=1.e-03*zavo/zmolgair
  ysphr=3600.
  !
  !     * define constant 2-d slices.
  !
  do jk=1,ilev
    do il=il1,il2
      zrho0(il,jk)=shj(il,jk)*pressg(il)/(rgas*th(il,jk))
      zrhxtoc(il,jk)=xtoc(zrho0(il,jk),zmolgs)
      zrhctox(il,jk)=ctox(zrho0(il,jk),zmolgs)
    end do
  end do ! loop 100
  !
  !     * oxidant concentrations.
  !
  do jk=1,ilev
    do il=il1,il2
      !
      !       * convert to molecules/cm3 from vmr
      !
      factcon = zrho0(il,jk)*1.e-03*zavo/zmolgair
      zzoh  (il,jk)=ohrow  (il,jk)*factcon
      zzh2o2(il,jk)=h2o2row(il,jk)*factcon
      zzo3  (il,jk)=o3row  (il,jk)*factcon
      zzno3 (il,jk)=no3row (il,jk)*factcon
      !
      !       * oxygen concentration in molecules/cm3
      !
      zzo2 (il,jk)=0.21*1.e-06*factcon
    end do
  end do ! loop 115
  sgpp=0.
  !
  !     * background aging time scale for carbonaceous aerosol.
  !
  if (nage==0) then
    atau=36.*ysphr
  else
    atau=12.*ysphr
  end if
  !-----------------------------------------------------------------
  !     * hydrogen peroxide production and initialization.
  !
  do jk=1,ilev
    do il=il1,il2
      !
      !---    convert molecules/cm**3 to kg-s/kg
      !
      agthpo(il,jk) = zzh2o2(il,jk) * 1.e+06 * 32.06e-03 &
                        / (6.022045e+23 * zrho0(il,jk))
    end do
  end do ! loop 120
  !
  !     * initial hydrogen peroxide mixing ratios for clear and cloudy sky.
  !
  tmpscl(il1:il2,:)=1.
  where (zclf(il1:il2,:) < zcthr &
      .and. (1.-zclf(il1:il2,:)) < zcthr)
    where (sfrc(il1:il2,:,ihpo) < -ysmall)
      tmpscl(il1:il2,:)=-xrow(il1:il2,:,ihpo) &
                             *(1.-zclf(il1:il2,:))/sfrc(il1:il2,:,ihpo)
    else where (sfrc(il1:il2,:,ihpo) > ysmall)
      tmpscl(il1:il2,:)=xrow(il1:il2,:,ihpo) &
                                  *zclf(il1:il2,:)/sfrc(il1:il2,:,ihpo)
    else where
      tmpscl(il1:il2,:)=0.
    end where
  end where
  tmpscl(il1:il2,:)=max(min(tmpscl(il1:il2,:),1.),0.)
  sfrc(il1:il2,:,ihpo)=tmpscl(il1:il2,:)*sfrc(il1:il2,:,ihpo)
  xarow=xrow(il1:il2,:,ihpo)
  xcrow=xrow(il1:il2,:,ihpo)
  where (zclf(il1:il2,:) < zcthr &
      .and. (1.-zclf(il1:il2,:)) < zcthr)
    xarow(il1:il2,:)=xarow(il1:il2,:) &
                             +sfrc(il1:il2,:,ihpo)/(1.-zclf(il1:il2,:))
    xcrow(il1:il2,:)=xcrow(il1:il2,:) &
                                  -sfrc(il1:il2,:,ihpo)/zclf(il1:il2,:)
  end where
  !
  !     * change in concentration in cloudy and clear portions
  !     * of the grid cell by adjusting concentrations towards background
  !     * values.
  !
  dxcdt(il1:il2,:)=-(1./max(ytau,ztmst)) &
                                  *(xcrow(il1:il2,:)-agthpo(il1:il2,:))
  dxadt(il1:il2,:)=-(1./max(ytau,ztmst)) &
                                  *(xarow(il1:il2,:)-agthpo(il1:il2,:))
  xcrow(il1:il2,:)=xcrow(il1:il2,:)+dxcdt(il1:il2,:)*ztmst
  xarow(il1:il2,:)=xarow(il1:il2,:)+dxadt(il1:il2,:)*ztmst
  !
  !     * all-sky change.
  !
  dxdt(il1:il2,:)=zclf(il1:il2,:)*dxcdt(il1:il2,:) &
                                 +(1.-zclf(il1:il2,:))*dxadt(il1:il2,:)
  xrow(il1:il2,:,ihpo)=xrow(il1:il2,:,ihpo) &
                                                 +dxdt(il1:il2,:)*ztmst
  !
  !     * adjust clear/cloudy concentration difference to account
  !     * for production of h2o2 in cloudy portion of the grid cell.
  !
  sfrc(il1:il2,:,ihpo)=0.
  where (zclf(il1:il2,:) < zcthr &
      .and. (1.-zclf(il1:il2,:)) < zcthr)
    sfrc(il1:il2,:,ihpo)=zclf(il1:il2,:)*(1.-zclf(il1:il2,:)) &
                                   *(xarow(il1:il2,:)-xcrow(il1:il2,:))
  end where
  !
  do jt=1,ntrac
    do jk=1,ilev
      do il=il1,il2
        zxte(il,jk,jt)=0.
      end do
    end do
  end do ! loop 200
  !
  do jk=1,ilev
    do il=il1,il2
      zhenry(il,jk)=0.
      if (zclf(il,jk)<1.e-04.or.zmlwc(il,jk)<zmin) then
        zmratep(il,jk)=0.
        zmlwc(il,jk)=0.
      end if
    end do ! loop 210
  end do ! loop 212
  !
  !     * processes which are diferent inside and outside of clouds.
  !
  do jt=1,ntrac
    if (itrwet(jt)/=0 .and. jt/=iso2 .and. jt/=ihpo &
        .and. (jt<iaind(1) .or. jt>iaind(ntracp))) then
      do jk=1,ilev
        do il=il1,il2
          !
          !          * diagnose initial mixing ratios for clear and cloudy sky.
          !
          tmpscl(il,jk)=1.
          if (zclf(il,jk) < zcthr .and. (1.-zclf(il,jk)) < zcthr &
              ) then
            if (sfrc(il,jk,jt) < -ysmall) then
              tmpscl(il,jk)=-xrow(il,jk,jt) &
                                       *(1.-zclf(il,jk))/sfrc(il,jk,jt)
            else if (sfrc(il,jk,jt) > ysmall) then
              tmpscl(il,jk)=xrow(il,jk,jt) &
                                            *zclf(il,jk)/sfrc(il,jk,jt)
            else
              tmpscl(il,jk)=0.
            end if
          end if
          tmpscl(il,jk)=max(min(tmpscl(il,jk),1.),0.)
          sfrc(il,jk,jt)=tmpscl(il,jk)*sfrc(il,jk,jt)
          zxtp10(il,jk)=xrow(il,jk,jt)
          zxtp1c(il,jk)=xrow(il,jk,jt)
          if (zclf(il,jk) < zcthr .and. (1.-zclf(il,jk)) < zcthr &
              ) then
            zxtp10(il,jk)=zxtp10(il,jk) &
                             +sfrc(il,jk,jt)/(1.-zclf(il,jk))
            zxtp1c(il,jk)=zxtp1c(il,jk) &
                                  -sfrc(il,jk,jt)/zclf(il,jk)
          end if
          zdep3d(il,jk)=0.
        end do ! loop 332
      end do ! loop 333
      !
      if (itrwet(jt)==1) then
        do jk=1,ilev
          do il=il1,il2
            zhenry(il,jk)=asrso4
          end do
        end do ! loop 334
      else
        do jk=1,ilev
          do il=il1,il2
            zhenry(il,jk)=asrphob
          end do
        end do ! loop 335
      end if
      !
      iso4=0
      call wetdep4(ilg,il1,il2,ilev,ztmst, pcons2, zxtp10, &
                     zxtp1c,dshj,shj,pressg,th,qv, zmratep,zmlwc, &
                     zfsnow,zfrain,zdep3d,zclf,clrfr,zhenry, &
                     clrfs,zfevap,zfsubl,pdclr,pdcld,jt,iso4)
      do jk=1,ilev
        do il=il1,il2
          zxtp1(il)=(1.-zclf(il,jk))*zxtp10(il,jk)+ &
                 zclf(il,jk)*zxtp1c(il,jk)
          zxtp1(il)=zxtp1(il)-zdep3d(il,jk)
          zxte(il,jk,jt)=(zxtp1(il)-xrow(il,jk,jt))*pqtmst
          xcld(il,jk,jt)=zxtp1c(il,jk)-pdcld(il,jk)
          xclr(il,jk,jt)=zxtp10(il,jk)-pdclr(il,jk)
        end do
      end do ! loop 336
      !
    end if
  end do ! loop 354
  !
  jt=iso2
  !
  !     * day-time chemistry.
  !
  dox4row=0.
  doxdrow=0.
  noxdrow=0.
  do il=il1,il2
    if (cszrow(il)>0. .and. nage==1) atau(il)=1.*ysphr
  end do
  do jk=1,ilev
    do il=il1,il2
      if (cszrow(il)>0.) then
        zxtp1so2=xrow(il,jk,jt)+zxte(il,jk,jt)*ztmst
        if (zxtp1so2<=zmin) then
          zso2=0.
        else
          ztk2=zk2*(th(il,jk)/300.)**(-3.3)
          zm=zrho0(il,jk)*znamair
          zhil=ztk2*zm/zk2i
          zexp=log10(zhil)
          zexp=1./(1.+zexp*zexp)
          ztk23b=ztk2*zm/(1.+zhil)*zk2f**zexp
          zso2=zxtp1so2*zrhxtoc(il,jk)*zzoh(il,jk)*ztk23b &
               *zdayl(il)
          zso2=zso2*zrhctox(il,jk)
          zso2=min(zso2,zxtp1so2*pqtmst)
          zxte(il,jk,jt)=zxte(il,jk,jt)-zso2
        end if
        sgpp(il,jk)=zso2
        !
        zxtp1dms=xrow(il,jk,idms)+ zxte(il,jk,idms)*ztmst
        if (zxtp1dms<=zmin) then
          zdms=0.
        else
          t=th(il,jk)
          !          ztk1=(t*exp(-234./t)+8.46e-10*exp(7230./t)+
          !     1         2.68e-10*exp(7810./t))/(1.04e+11*t+88.1*exp(7460./t))
          !
          !         * dms+oh->so2+mo2+ch2o (jpl 2010)
          !
          ztk1=1.2e-11*exp(-280./t)
          !
          !         * dms+oh+o2->0.75so2+0.25msa+mo2 (jpl 2010)
          !
          !          ztka=8.2e-39*exp(5376./t)
          ztka=exp(5376./t-87.69668)
          ztkb=1.05e-05*exp(3644./t)
          ztk2=ztka*zzo2(il,jk)/(1.0+ztkb*0.2095)
          zdms=zxtp1dms*zrhxtoc(il,jk)*zzoh(il,jk)*(ztk1+ztk2) &
              *zdayl(il)
          zdms=zdms*zrhctox(il,jk)
          if (zdms > zxtp1dms*pqtmst) then
            scalf=zxtp1dms*pqtmst/zdms
            zdms=scalf*zdms
          end if
          zxte(il,jk,idms)=zxte(il,jk,idms)-zdms
          zxte(il,jk,jt)=zxte(il,jk,jt)+0.75*zdms
          sgpp(il,jk)=sgpp(il,jk)+0.25*zdms
        end if
        !
        fac=dshj(il,jk)*pressg(il)/grav
        dox4row(il)=dox4row(il)+fac*zso2
        doxdrow(il)=doxdrow(il)+fac*zdms
      end if
    end do ! loop 410
  end do ! loop 412
  !
  !     * night-time chemistry.
  !
  do il=il1,il2
    if (cszrow(il)<=0. .and. nage==1) atau(il)=24.*ysphr
  end do
  do jk=1,ilev
    do il=il1,il2
      if (cszrow(il)<=0.) then
        zxtp1dms=xrow(il,jk,idms)+ zxte(il,jk,idms)*ztmst
        if (zxtp1dms<=zmin) then
          zdms=0.
        else
          !
          !          *  dms+no3->so2+hno3+mo2+ch2o (jpl 2010)
          !
          !           ztk3=zk3*exp(520./th(il,jk))
          ztk3=1.9e-13*exp(530./th(il,jk))
          zdms=zxtp1dms*zrhxtoc(il,jk)*zzno3(il,jk)*ztk3
          zdms=zdms*zrhctox(il,jk)
          zdms=min(zdms,zxtp1dms*pqtmst)
          zxte(il,jk,idms)=zxte(il,jk,idms)-zdms
          zxte(il,jk,jt)=zxte(il,jk,jt)+zdms
          fac=dshj(il,jk)*pressg(il)/grav
          noxdrow(il)=noxdrow(il)+zdms*fac
        end if
      end if
    end do ! loop 414
  end do ! loop 416
  !
  !     * fraction of tracers in cloudy part of the grid cell.
  !
  do jt=1,ntrac
    do jk=1,ilev
      do il=il1,il2
        if (jt<iaind(1) .or. jt>iaind(ntracp) ) then
          xrow(il,jk,jt)=xrow(il,jk,jt)+zxte(il,jk,jt)*ztmst
          xrow(il,jk,jt)=max(xrow(il,jk,jt),zero)
          if (jt /= iso2 .and. jt /= ihpo) then
            if (itrwet(jt)/=0 .and. xrow(il,jk,jt) > zmin &
                .and. zclf(il,jk) > 1.e-02) then
              sfrc(il,jk,jt)=0.
              if (zclf(il,jk) < zcthr &
                  .and. (1.-zclf(il,jk)) < zcthr) then
                sfrc(il,jk,jt)=zclf(il,jk)*(1.-zclf(il,jk)) &
                                   *(xclr(il,jk,jt)-xcld(il,jk,jt))
              end if
            else
              sfrc(il,jk,jt)=0.
            end if
          end if
        end if
      end do ! loop 420
    end do ! loop 421
  end do ! loop 422
  !
  return
end
