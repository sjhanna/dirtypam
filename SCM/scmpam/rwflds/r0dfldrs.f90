subroutine r0dfldrs(cname,avar)
  !
  use iodat
  !
  implicit none
  !
  integer :: ichck
  integer :: it

  !
  character(len=*), intent(in) :: cname !<
  real, intent(out) :: avar !<
  !
  ichck=0
  do it=1,itot
    if (abs(rvarv(it)%fld0d+ynax) > ysmallx &
        .and. trim(rvar(it)%name) == trim(cname) ) then
      ichck=1
      avar=rvarv(it)%fld0d
    end if
  end do
  if (ichck==0) then
    print*,'NO VARIABLE ',trim(cname), &
               ' IN LIST OF RESTART VARIABLES'
    call xit('R0DFLDRS',-1)
  end if
  !
  print '(2a10,es12.2)', 'R0DFLDRS: ', cname, avar
end subroutine r0dfldrs
