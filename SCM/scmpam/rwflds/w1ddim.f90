subroutine w1ddim(cname,avar,idim1,j1,j2)
  !
  use iodat
  !
  implicit none
  !
  integer :: ichck
  integer :: it
  !
  character(len=*), intent(in) :: cname !<
  real, intent(in), dimension(idim1) :: avar !<
  integer, intent(in) :: idim1 !<
  integer, intent(in), optional :: j1 !<
  integer, intent(in), optional :: j2 !<
  integer :: j1t !<
  integer :: j2t !<
  integer, dimension(1) :: ishape !<
  !
  if (present(j1) .and. present(j2) ) then
    if (j1 < 1 .or. j2 > idim1) call xit('W1DDIM',-1)
    j1t=j1
    j2t=j2
  else
    j1t=1
    j2t=idim1
  end if
  !
  ichck=0
  do it=1,ndim
    if (allocated(dim(it)%val) &
        .and. trim(dim(it)%name) == trim(cname) ) then
      ichck=1
      !
      ishape=shape(dim(it)%val)
      if (ishape(1) == idim1) then
        dim(it)%val(j1t:j2t)=avar(j1t:j2t)
      else
        print*,'INCOMPATIBLE DIMENSIONS VARIABLE ',trim(cname)
        call xit('W1DDIM',-2)
      end if
    end if
  end do
  if (ichck==0) then
    print*,'NO DIMENSION ',trim(cname),' IN LIST OF DIMENSIONS'
    call xit('W1DDIM',-3)
  end if
  !
end subroutine w1ddim
