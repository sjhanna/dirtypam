subroutine r0dfld(cname,avar)
  !
  use iodat
  !
  implicit none
  !
  integer :: ichck
  integer :: it
  !
  character(len=*), intent(in) :: cname !<
  real, intent(out) :: avar !<
  !
  ichck=0
  do it=1,itot
    if (abs(ivarv(it)%fld0d+ynax) > ysmallx &
        .and. trim(ivar(it)%name) == trim(cname) ) then
      ichck=1
      avar=ivarv(it)%fld0d
    end if
  end do
  if (ichck==0) then
    print*,'NO VARIABLE ',trim(cname),' IN LIST OF IO VARIABLES'
    call xit('R0DFLD',-1)
  end if
  !
end subroutine r0dfld
