subroutine cldphys(zclf,zrfln,clrfr,clrfs,zfrain,zfsnow,zfevap, &
                         zfsubl,zmlwc,zmratep,rh,rhc,hmn,qt,qr,qlr, &
                         zcdnc,qcwa,th,ph,zh,dp,almc,almx,dt,ilg,ilev, &
                         icase)
  !
  use sdphys
  !
  implicit none
  real :: adfun
  real :: aexpf
  real :: afun
  real :: akr
  real :: alambr
  real :: alpha
  real :: api
  real :: apk
  real :: aqhat
  real :: aqhat1
  real :: aqhat2
  real :: ascl
  real :: aterm1
  real :: aterm2
  real :: aterm3
  real :: aterm4
  real :: beta6
  real :: cdnc
  real :: cdne
  real :: cdnp
  real :: cdnr
  real :: coacc
  real :: coaut
  real :: cotmp1
  real :: cotmp2
  real :: dqhdq
  real, intent(in) :: dt
  real :: dz
  real :: ep
  real :: eps2
  real :: epslim
  real :: esat
  real :: estref
  real :: etmp
  real :: faacc
  real :: faaut
  real :: facacc
  real :: facaut
  real :: facautn
  real :: facte
  integer :: i2ndie
  integer :: icall
  integer, intent(in) :: icase
  integer :: icvsg
  integer :: il
  integer, intent(in) :: ilev
  integer, intent(in) :: ilg
  integer :: isubg
  integer :: it
  integer :: itotevp
  integer :: jk
  integer :: l
  real :: qcwi
  real :: qcwl
  real :: qsw
  real :: r6
  real :: r6c
  real :: rgasv
  real :: rhcw
  real :: rhodz
  real :: rvol
  real :: susatw
  real :: ttt
  real :: zast
  real :: zbst
  real :: zcnd
  real :: zcons2
  real :: zdv
  real :: zepclc
  real :: zka
  real :: zlv
  real :: zlvdcp
  real :: zplim
  real :: zqest
  real :: zqest1
  real :: zqest2
  real :: zqrho0
  real :: zrflnl
  real :: zrflnl_max
  real :: zrho0
  real :: zsec
  real :: zsr
  real :: zxrp1
  real :: zxsec
  real :: zzdrr
  real :: zzepr
  real :: zzepr1
  !
  real, intent(inout), dimension(ilg,ilev) :: qr !<
  real, intent(inout), dimension(ilg,ilev) :: qlr !<
  real, intent(inout), dimension(ilg,ilev) :: th !<
  real, intent(inout), dimension(ilg,ilev) :: zclf !<
  real, intent(in), dimension(ilg,ilev) :: ph !<
  real, intent(in), dimension(ilg,ilev) :: zh !<
  real, intent(in), dimension(ilg,ilev) :: almc !<
  real, intent(in), dimension(ilg,ilev) :: almx !<
  real, intent(in), dimension(ilg,ilev) :: dp !<
  real, intent(in), dimension(ilg,ilev) :: zcdnc !<
  real, intent(out), dimension(ilg,ilev) :: hmn !<
  real, intent(out), dimension(ilg,ilev) :: qt !<
  real, intent(out), dimension(ilg,ilev) :: rh !<
  real, intent(out), dimension(ilg,ilev) :: rhc !<
  real, intent(out), dimension(ilg,ilev) :: zmlwc !<
  real, intent(out), dimension(ilg,ilev) :: clrfr !<
  real, intent(out), dimension(ilg,ilev) :: clrfs !<
  real, intent(out), dimension(ilg,ilev) :: zfrain !<
  real, intent(out), dimension(ilg,ilev) :: zfsnow !<
  real, intent(out), dimension(ilg,ilev) :: zfevap !<
  real, intent(out), dimension(ilg,ilev) :: zfsubl !<
  real, intent(out), dimension(ilg,ilev) :: zmratep !<
  real, intent(out), dimension(ilg,ilev) :: qcwa !<
  real, intent(out), dimension(ilg) :: zrfln !<
  real, dimension(ilg,ilev) :: qtn !<
  real, dimension(ilg,ilev) :: hmnn !<
  real, dimension(ilg,ilev) :: cvar !<
  real, dimension(ilg,ilev) :: zcraut !<
  real, dimension(ilg,ilev) :: qcwvar !<
  real, dimension(ilg,ilev) :: cvsg !<
  real, dimension(ilg,ilev) :: cvdu !<
  real, dimension(ilg,ilev) :: zfrac !<
  real, dimension(ilg,ilev) :: qc !<
  real, dimension(ilg) :: pmbs !<
  real, dimension(ilg) :: rrl !<
  real, dimension(ilg) :: cpm !<
  real, dimension(ilg) :: almix !<
  real, dimension(ilg) :: dhldz !<
  real, dimension(ilg) :: drwdz !<
  real, dimension(ilg) :: flagest !<
  real, dimension(ilg) :: qcw !<
  real, dimension(ilg) :: ssh !<
  real, dimension(ilg) :: zclfrm !<
  real, dimension(ilg) :: zclfr !<
  real, dimension(ilg) :: zclrr !<
  real, dimension(ilg) :: zrnft !<
  real, dimension(ilg) :: zrclr !<
  real, dimension(ilg) :: zclfsm !<
  real, dimension(ilg) :: zclfs !<
  real, dimension(ilg) :: zsfln !<
  real, dimension(ilg) :: zclrs !<
  real, dimension(ilg) :: zsnft !<
  real, dimension(ilg) :: zsclr !<
  real, dimension(ilg) :: vtr !<
  real, dimension(ilg) :: zrpr !<
  real, dimension(ilg) :: zevp !<
  real, dimension(ilg) :: zxlb !<
  integer, parameter :: iter=10 !<
  integer, parameter :: ispcdnc=0 ! switch to specified cdnc
  real, parameter :: spcdnc=300.e+06 !<
  !
  real :: esw
  esw(ttt) = exp(rw1+rw2/ttt)*ttt**rw3

  !
  !     * adiabatic core.
  !
  !     * total water mixing ratio and cloud water static energy.
  !
  do l=1,ilev
    qtn(:,l)=qr(:,ilev)+qlr(:,ilev)
    hmnn(:,l)=cpres*th(:,ilev)+grav*zh(:,ilev)-rl*qlr(:,ilev)
  end do
  facte=1./eps1-1.
  i2ndie=1
  flagest=1.
  cvsg=0.
  cvdu=0.
  zfrac=0.
  icvsg=1
  isubg=0
  icall=1
  do jk=1,ilev
    do il=1,ilg
      pmbs(il)=0.01*ph(il,jk)
      rrl(il)=rl
      cpm(il)=cpres
      almix(il)=max(almc(il,jk),almx(il,jk),10.)
      if (jk>1) then
        dz       =zh   (il,jk-1)-zh  (il,jk)
        dhldz(il)=(hmnn(il,jk-1)-hmnn(il,jk))/dz
        drwdz(il)=(qtn (il,jk-1)-qtn (il,jk))/dz
      else
        dhldz(il)=0.
        drwdz(il)=0.
      end if
    end do
    call statcld (qcw,zclf(1,jk),cvar(1,jk),zcraut(1,jk), &
                      qcwvar(1,jk),ssh,cvsg(1,jk),qtn(1,jk), &
                      hmnn(1,jk),zfrac(1,jk),cpm, &
                      pmbs,zh(1,jk),rrl,cvdu(1,jk),flagest, &
                      almix,dhldz,drwdz,i2ndie, &
                      dt,ilev,ilg,1,ilg,icvsg,isubg, &
                      jk,icall)
    !
    do il=1,ilg
      qcwa(il,jk)=qcw(il)
    end do
  end do
  !
  !     * total water mixing ratio and cloud water static energy.
  !
  qtn=qr+qlr
  hmnn=cpres*th+grav*zh-rl*qlr
  !
  !     * cloud fraction, evaporation, and condensation from statistical
  !     * cloud scheme.
  !
  facte=1./eps1-1.
  i2ndie=1
  flagest=1.
  cvsg=0.
  cvdu=0.
  zfrac=0.
  icvsg=1
  isubg=0
  icall=1
  do jk=1,ilev
    do il=1,ilg
      pmbs(il)=0.01*ph(il,jk)
      rrl(il)=rl
      cpm(il)=cpres
      almix(il)=max(almc(il,jk),almx(il,jk),10.)
      if (jk>1) then
        dz       =zh   (il,jk-1)-zh  (il,jk)
        dhldz(il)=(hmnn(il,jk-1)-hmnn(il,jk))/dz
        drwdz(il)=(qtn (il,jk-1)-qtn (il,jk))/dz
      else
        dhldz(il)=0.
        drwdz(il)=0.
      end if
    end do
    call statcld (qcw,zclf(1,jk),cvar(1,jk),zcraut(1,jk), &
                      qcwvar(1,jk),ssh,cvsg(1,jk),qtn(1,jk), &
                      hmnn(1,jk),zfrac(1,jk),cpm, &
                      pmbs,zh(1,jk),rrl,cvdu(1,jk),flagest, &
                      almix,dhldz,drwdz,i2ndie, &
                      dt,ilev,ilg,1,ilg,icvsg,isubg, &
                      jk,icall)
    !
    do il=1,ilg
      qcwi=zfrac(il,jk)*qcw(il)
      qcwl=(1.-zfrac(il,jk))*qcw(il)
      !
      !         * condensation and evaporation.
      !
      zcnd=qcw(il)-qlr(il,jk)
      zcnd=max(zcnd,-qlr(il,jk))
      ascl=1.
      if (zcnd>0. ) then
        ascl=min(max((qr(il,jk)-1.e-20)/zcnd,0.),1.)
      end if
      zcnd=ascl*zcnd
      !
      !         * update temperature, vapour, and condensate.
      !
      th(il,jk)=th(il,jk)+zcnd*rl/cpres
      qr(il,jk)=qr(il,jk)-zcnd
      qlr(il,jk)=qlr(il,jk)+zcnd
      !
      !         * relative humidity for all- and clear-sky. the relative
      !         * humidity is calculated based on the vapour pressure if the
      !         * saturation vapour pressure exceeds the total air pressure,
      !         * i.e. for conditions that do not permit formation of clouds.
      !         * otherwise, the relative humidity is calculated based on
      !         * SPECIFIC HUMIDITY (SEE CHAPTER 4 IN EMANUEL'S TEXTBOOK ON
      !         * convection) in order to permit calculations for clear-sky.
      !
      if (zclf(il,jk)<.99) then
        qc (il,jk)=(qr(il,jk)-zclf(il,jk)*ssh(il))/(1.-zclf(il,jk))
        rhc(il,jk)=min(max((qc(il,jk)*(1.+ssh(il)*facte)) &
                              /(ssh(il)*(1.+qc(il,jk)*facte)),0.),1.)
        rh (il,jk)=zclf(il,jk)+(1.-zclf(il,jk))*rhc(il,jk)
      else
        qc (il,jk)=qr(il,jk)
        rh (il,jk)=1.
        rhc(il,jk)=rh(il,jk)
      end if
      !
      !         * diagnose liquid water static energy and total water
      !         * in the clear-sky environment of the cloud.
      !
      qt(il,jk)=qc(il,jk)
      hmn(il,jk)=cpres*th(il,jk)+grav*zh(il,jk)
    end do
  end do
  !
  !     * constants and intializations.
  !
  zsec=1.e-9
  zepclc=1.e-2
  api=3.1415926535897
  coaut=3.
  coacc=1.15
  cotmp1=coaut-2.
  cotmp2=coacc-1.
  zlvdcp=rl/cpres
  akr=141.4
  zka=0.024
  zrflnl_max=0.028
  facacc=1.
  zcons2=1./(dt*grav)
  rgasv=461.50
  epslim=0.001
  zxsec=1.-1.e-12
  zmratep=0.
  zmlwc=0.
  clrfr=0.
  clrfs=0.
  zfrain=0.
  zfsnow=0.
  zfevap=0.
  zfsubl=0.
  zclfr=0.
  zrfln=0.
  zclfs=0.
  zsfln=0.
  do jk=1,ilev
    zsr=0.
    do il=1,ilg
      !
      !     * rain area fraction from maximum/random cloud overlap assumption.
      !     * only clouds that are subject to warm rain processes are
      !     * considered.
      !
      zclfrm(il)=zclfr(il)
      if (zrfln(il) > 1.e-20) then
        zclfr(il)=zclf(il,jk)
      else
        zclfr(il)=0.
      end if
      if (zclfrm(il)<1. ) then
        zclrr(il)=zclrr(il)*(1.-max(zclfr(il),zclfrm(il))) &
                            /(1.-zclfrm(il))
      end if
      zrnft(il)=1.-zclrr(il)
      !
      !      * fraction of grid cell for which clear-sky is affected by rain.
      !
      zrclr(il)=min(max(zrnft(il)-zclfr(il),0.),1.)
      !
      !     * snow area fraction from maximum/random cloud overlap assumption.
      !     * only clouds that are affected by snow are considered.
      !
      zclfsm(il)=zclfs(il)
      if (zsfln(il) > 1.e-20) then
        zclfs(il)=zclf(il,jk)
      else
        zclfs(il)=0.
      end if
      if (zclfsm(il)<1. ) then
        zclrs(il)=zclrs(il)*(1.-max(zclfs(il),zclfsm(il))) &
                            /(1.-zclfsm(il))
      end if
      zsnft(il)=1.-zclrs(il)
      !
      !      * fraction of grid cell for which clear-sky is affected by snow.
      !
      zsclr(il)=min(max(zsnft(il)-zclfs(il),0.),1.)
      !
      !      * rain water mixing ratio from rainfall rate (from rotstayn)
      !
      zrho0=ph(il,jk)/(rgas*th(il,jk))
      zqrho0=1.3/zrho0
      zxrp1=0.
      vtr(il)=0.
      if (zrnft(il)>zepclc) then
        zrflnl=zrfln(il)/zrnft(il)
        zrflnl=min(zrflnl,zrflnl_max)
        if (zrflnl>1.e-20) then
          alambr=714.*(1.2/zrho0)**(1./9.)*zrflnl**(-0.22)
          vtr(il)=1.94*akr*sqrt(1.2/(zrho0*alambr))
          zxrp1=zrflnl/(zrho0*vtr(il))
        end if
      end if
      !
      !      * autoconversion and accretion. 4 different autoconversion
      !      * parameterizations are available.
      !
      zxlb(il)=qlr(il,jk)
      if (zclf(il,jk)>zepclc .and. zxlb(il)>zsec) then
        zxlb(il)=qlr(il,jk)/zclf(il,jk)
      else
        zxlb(il)=qlr(il,jk)
      end if
      if (ispcdnc == 1) then
        cdnc=spcdnc
      else
        cdnc=zcdnc(il,jk)
      end if
      if (zxlb(il)*zclf(il,jk)>=zsec) then
        if (icase <= 3) then
          !
          !          * parameterization by wood (2005)
          !
          if (icase == 1) then
            !            * with constant cloud droplet number.
            !
            cdnr=120.e+06
            cdnp=0.
            facautn=1.3e+09
          else if (icase == 2) then
            !            * original wood parameterization.
            !
            cdnr=0.
            cdnp=1.
            facautn=1.3e+09
          else if (icase == 3) then
            !            * with parameters specified according to liu and daum (2004).
            !
            cdnr=0.
            cdnp=1.
            facautn=1.08e+10
          end if
          cdne=(cdnr**(1.-cdnp))*cdnc**cdnp
          rvol=1.e+06*(3.*zxlb(il)/(4.*api*rhoh2o*cdne))**(1./3.)
          beta6=((rvol+3.)/rvol)**(1./3.)
          r6=beta6*rvol
          r6c=7.5/(zxlb(il)**(1./6.)*sqrt(r6))
          ep=facautn*(beta6**6)
          if (r6 >= r6c) then
            faaut=zcraut(il,jk)*ep/cdne
          else
            faaut=0.
          end if
        else if (icase == 4) then
          !          * khairoutdinov and kogan (2000)
          !
          coaut=2.47
          cotmp1=coaut-2.
          facaut=2.5
          faaut=facaut*zcraut(il,jk)*1350.
          faaut=faaut*(cdnc/1.e+06)**(-1.79)
        else
          call xit('CLDPHYS',-1)
        end if
        !
        !        * initial guess and subsequent iterations.
        !
        zqest1=(zxlb(il)**(1.-coaut) &
               -dt*faaut*(1.-coaut))**(1./(1.-coaut))
        zqest=min(max(zqest1,zsec),zxlb(il))
        faacc=0.
        if (zxrp1>zsec) then
          faacc=facacc*67.*zxrp1**coacc
          zqest2=(zxlb(il)**(1.-coacc) &
                 -dt*faacc*(1.-coacc))**(1./(1.-coacc))
          zqest=max(min(zqest,zqest2),zsec)
          apk=-log(zqest/zxlb(il))/dt
          if (zqest<zxlb(il) .and. apk>0. ) then
            alpha=((1.-zqest/zxlb(il))*zxlb(il)/(apk*dt)-zqest) &
                  /(zxlb(il)-zqest)
            alpha=min(max(alpha,0.),1.)
            do it=1,iter
              aqhat =alpha*zxlb(il)+(1.-alpha)*zqest
              aqhat1=aqhat**cotmp1
              aqhat2=aqhat**cotmp2
              aterm1=faaut*aqhat1*zxlb(il)+faacc*aqhat2
              aterm2=faaut*aqhat1*zqest   +faacc*aqhat2
              aexpf =exp(-faacc*aqhat2*dt)
              afun=aterm1*zqest-aexpf*aterm2*zxlb(il)
              dqhdq=1.-alpha
              aterm3=faaut*cotmp1*aqhat1*dqhdq/aqhat
              aterm4=faacc*cotmp2*aqhat2*dqhdq/aqhat
              adfun=aterm1+zqest*(aterm3*zxlb(il)+aterm4) &
                    +aexpf*zxlb(il)*(dt*aterm4*aterm2 &
                                -aterm3*zqest+faaut*aqhat1+aterm4)
              zqest=max(zqest-afun/adfun,zsec)
            end do
          end if
        end if
        zrpr(il)=min(zclf(il,jk)*zxlb(il), &
                      max(-zclf(il,jk)*(zqest-zxlb(il)),0.))
      else
        zrpr(il)=zclf(il,jk)*zxlb(il)
      end if
      if (zclf(il,jk)>zepclc) then
        zmratep(il,jk)=zrpr(il)/zclf(il,jk)
        zmlwc(il,jk)=zxlb(il)
      end if
      zzdrr=max(0.,zcons2*dp(il,jk)*zrpr(il))
      zrfln(il)=zrfln(il)+zzdrr
      zsr=zsr+zrfln(il)
      !
      !      * evaporation of rain below clouds (from lohmann).
      !
      zevp(il)=0.
      eps2=1.-eps1
      if (zsr > 0. .and. zrfln(il)>0. .and. zrclr(il)>0. &
          ) then
        zrflnl=zrfln(il)/zrnft(il)
        zrflnl=min(zrflnl,zrflnl_max)
        rhodz=dp(il,jk)/grav
        etmp=esw(th(il,jk))
        estref=0.01*ph(il,jk)*(1.-epslim)/(1.-epslim*eps2)
        if (etmp<estref) then
          esat=etmp
        else
          esat=estref
        end if
        qsw=eps1*esat/(0.01*ph(il,jk)-eps2*esat)
        rhcw=min(max((qc(il,jk)*(1.+qsw*facte)) &
                     /(qsw*(1.+qc(il,jk)*facte)),0.),1.)
        susatw=rhcw-1.
        zdv=2.21/ph(il,jk)
        zlv=zlvdcp*cpres
        zast=zlv*(zlv/(rgasv*th(il,jk))-1.)/(zka*th(il,jk))
        zbst=rgasv*th(il,jk)/(zdv*esat)
        zzepr1=870.*susatw*zrflnl**0.61/(sqrt(zrho0)*(zast+zbst))
        zzepr=zzepr1*rhodz
        zplim=-zxsec*zrfln(il)/zrnft(il)
        itotevp=0
        if (zzepr < zplim) then
          zzepr=zplim
          itotevp=1
        end if
        zevp(il)=-zzepr*dt*zrclr(il)/rhodz
        zevp(il)=min(zevp(il), &
                 max(zxsec*((1.-rhcw)*qc(il,jk))*zrclr(il),0.))
        zevp(il)=max(zevp(il),0.)
        zzepr=-zevp(il)*rhodz/dt
        zfrain(il,jk)=zfrain(il,jk)+zrfln(il)
        clrfr(il,jk)=zrclr(il)
        if (itotevp == 1) then
          zfevap(il,jk)=min(max(0.,-zzepr/zrfln(il)),1.)
        end if
        zrfln(il)=zrfln(il)+zzepr
      end if
      !
      !      * tendencies.
      !
      th(il,jk)=th(il,jk)-zevp(il)*rl/cpres
      qr(il,jk)=qr(il,jk)+zevp(il)
      qlr(il,jk)=qlr(il,jk)-zrpr(il)
    end do
  end do
  !
end subroutine cldphys
