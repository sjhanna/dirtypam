function zcr(q1,imod)
  !
  !     * jun 26/2013 - k.vonsalzen.
  !
  !     * called by statcld5.
  !
  !     * ratio of moments of cloud liquid water content. the ratio
  !     * <qlwc**coeff>/<qlwc>**coeff is calculated by integrating
  !     * over a gaussian distribution using the statistical cloud
  !     * scheme by mellor (1977). for imod=0 -> coeff=2.47 and
  !     * for imod=1 -> coeff=3.
  !
  implicit none
  integer, intent(in) :: imod
  integer :: ind
  real, intent(in) :: q1
  real :: q1delt
  real :: q1v
  real :: wgt
  !
  real :: zcr !<
  real, parameter :: q1min=-2. !<
  real, parameter :: q1max=2. !<
  integer, parameter :: inum=61 !<
  real, dimension(inum) :: zcrld !<
  real, dimension(inum) :: zcrkk !<
  real, dimension(inum) :: zcrr !<
  data zcrld / 8889.942, 6454.475, 4721.926, 3480.747, 2584.975, &
                   1934.007, 1457.707, 1106.815,  846.462,  652.041, &
                    505.868,  395.250,  311.001,  246.407,  196.574, &
                    157.888,  127.668,  103.925,   85.150,   70.220, &
                     58.278,   48.672,   40.903,   34.586,   29.420, &
                     25.173,   21.665,   18.752,   16.322,   14.284, &
                     12.568,   11.117,    9.883,    8.830,    7.928, &
                      7.152,    6.482,    5.900,    5.395,    4.953, &
                      4.567,    4.227,    3.927,    3.662,    3.427, &
                      3.218,    3.032,    2.866,    2.716,    2.582, &
                      2.462,    2.353,    2.254,    2.165,    2.084, &
                      2.011,    1.944,    1.883,    1.827,    1.776, &
                      1.729 /
  data zcrkk /  708.636,  560.461,  445.761,  356.524,  286.721, &
                    231.848,  188.502,  154.094,  126.637,  104.629, &
                     86.902,   72.556,   60.895,   51.368,   43.552, &
                     37.112,   31.781,   27.350,   23.650,   20.549, &
                     17.939,   15.733,   13.862,   12.269,   10.908, &
                      9.739,    8.733,    7.864,    7.111,    6.456, &
                      5.885,    5.385,    4.946,    4.559,    4.218, &
                      3.916,    3.649,    3.410,    3.198,    3.008, &
                      2.838,    2.685,    2.548,    2.424,    2.312, &
                      2.211,    2.119,    2.036,    1.961,    1.892, &
                      1.829,    1.772,    1.720,    1.672,    1.628, &
                      1.588,    1.551,    1.517,    1.485,    1.457, &
                      1.430 /
  !
  q1delt=(q1max-q1min)/real(inum-1)
  if (imod == 0) then
    zcrr=zcrkk
  else
    zcrr=zcrld
  end if
  !
  if (q1 < q1min) then
    zcr=zcrr(1)
  else if (q1 > q1max) then
    zcr=zcrr(inum)
  else
    q1v=(q1-q1min)/q1delt+1.
    ind=int(q1v)
    wgt=q1v-real(ind)
    zcr=wgt*zcrr(ind+1)+(1.-wgt)*zcrr(ind)
  end if
  !
end function zcr
