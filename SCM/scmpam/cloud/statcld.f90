subroutine statcld (qcw,zclf,sigma,zcraut,qcwvar,ssh,cvsg, &
                          qt,hmn,zfrac,cpm,p,z,rrl,cons,crh, &
                          almix,dhldz,drwdz,i2ndie, &
                          ztmst,ilev,ilg,il1,il2,icvsg,isubg, &
                          lvl,icall)
  !
  !     * aug 16/2013 - k.vonsalzen. cosmetic change to abort condition
  !     *                            where now is qcw>qt instead of
  !     *                            qcw>=qt. this does not change
  !     *                            the answer but prevents infrequent
  !     *                            crashes with small downward
  !     *                            moisture fluxes causing very small
  !     *                            negative total water.
  !     * jul 31/2013 - m.lazare.    cosmetic change to pass in lvl
  !     *                            and icall to aid in future debugging.
  !     * jun 25/2013 - k.vonsalzen. new version for gcm17:
  !     *                            - second indirect effect parameter
  !     *                              i2ndie passed in and used.
  !     *                            - function zcr replaces look-up
  !     *                              table zcr to calculate autoconversion
  !     *                              efficiency.
  !     * apr 29/2012 - k.vonsalzen. previous version statcld4 for gcm16:
  !     *                            eliminate clouds and condensate if
  !     *                            cloud fraction or condensate mixing
  !     *                            ratio too low to ensure consistent
  !     *                            results for cloud fraction and cloud
  !     *                            condensate.
  !     * mar 25/2009 - l.solheim.   revised cosmetic change:
  !     *                            - use real*4 :: or real*8 :: argument to
  !     *                              "ERF", depending on the value of
  !     *                              "MACHINE", so will work seamlessly on
  !     *                              pgi compiler as well
  !     * dec 19/2007 - k.vonsalzen/ previous version statcld3 for gcm15g/h/i:
  !     *               m.lazare.    - eliminate unphysical results
  !     *                              by not permitting the inferred
  !     *                              water vapour mixing ratio (qcw)
  !     *                              to exceed a threshold (defined
  !     *                              by qcwmax).
  !     *                            - passes in adelt from physics
  !     *                              as ztmst instead of ztmst=2.*delt.
  !     * jan 13/2007 - k.vonsalzen. calculate normalized cloud water
  !     *                            variance to be used in cloud
  !     *                            inhomogeneity (qcwvar).
  !     * jun 19/2006 - m.lazare.  new version for gcm15f:
  !     *                          - cosmetic: use variable instead of
  !     *                            constant in intrinsics such as "MAX",
  !     *                            so that can compile in 32-bit mode
  !     *                            with real*8.
  !     * may 06/2006 - m.lazare/  previous version statcld for gcm15e.
  !     *               k.vonsalzen:

  !
  !     * this subroutine performs the bulk of the statistical cloud
  !     * scheme based on chaboureau and bechthold (c &b). it takes as
  !     * input liquid static energy and total water, and outputs
  !     * condensed water/ice, cloud fraction, the variance of mellors
  !     * variable ("S"), the efficiency factor for autoconversion
  !     * from cloud water to rain water, and the saturation specific
  !     * humidity.
  !
  !**** ***************************************************************
  !     * dictionary of variables:
  !
  !     * output:
  !     * ------
  !
  !     * qcw:     total condensed water/ice (both phases) in kg/kg.
  !     * zclf:    cloud fraction (dimensionless)
  !     * SIGMA:   VARIANCE OF MELLOR'S VARIABLE S IN KG/KG
  !     * zcraut:  efficiency of autoconversion from cloud water to
  !     *          rain water.
  !     * QCWVAR:  NORMALIZED CLOUD WATER VARIANCE (<W'**2>/<W>**2)
  !     *          (dimensionless).
  !     * ssh:     saturation specific humidity in kg/kg
  !     * cvsg:    contribution to total variance of s from convective
  !     *          processes in kg/kg (only if switch icvsg=1,
  !     *          otherwise it is input !).
  !
  !     * input:
  !     * -----
  !
  !     * qt:     total water (both phases) in kg/kg.
  !     * hmn:    total liquid static energy in joules/kg.
  !     * zfrac:  fraction of ice phase.
  !     * cpm:    specific heat in joules/(kg-degk).
  !     * p:      mid-layer pressure in mbs.
  !     * z:      mid-layer height above ground in metres.
  !     * pblt:   layer index of top of pbl.
  !     * rrl:    water/ice weighted latent heat in joules/kg.
  !     * cons:   field contributing to variance from convection.
  !     * crh:    flag (0/1) to control calculation.
  !     * almix:  mixing length for c &b parameterization in metres.
  !     * dhldz:  total water vertical gradient in (m-1).
  !     * drwdz:  liquid static energy gradient in joules/(kg-m). &
  !**** ***************************************************************
  !
  use sdphys
  use fpdef
  !
  implicit none
  real :: aexp
  real :: apa
  real :: apb
  real :: ar
  real :: csigma
  real :: dhmnpr
  real :: drssdt
  real :: drssdtl
  real :: eps2
  real :: est
  real :: expm1
  real :: hmnpr
  integer, intent(in) :: i2ndie
  integer, intent(in) :: icall
  integer, intent(in) :: icvsg
  integer :: il
  integer, intent(in) :: il1
  integer, intent(in) :: il2
  integer :: ilbad
  integer, intent(in) :: ilev
  integer, intent(in) :: ilg
  integer :: indx
  integer, intent(in) :: isubg
  integer, intent(in) :: lvl
  real :: one
  real :: pi
  real :: q1
  real :: q1m
  real :: q1max
  real :: q1maxm
  real :: q1min
  real :: q1min1
  real :: q1min2
  real :: q1minm
  real :: qcwmax
  real :: qcwt
  real :: qcwtbad
  real :: qdelt
  real :: ql
  real :: qlvar
  real :: rc
  real :: rss
  real :: rsstl
  real :: sigcbbad
  real :: sigfac
  real :: sigmacb
  real :: sigmamax
  real :: sigmasum
  real :: sigmaxbad
  real :: sqrt2
  real :: sqrt2pi
  real :: tausig
  real :: tl
  real :: ttt
  real :: tx
  real :: yfr
  real :: zcr
  real :: zepclc
  real :: zero
  real :: zsec
  real, intent(in) :: ztmst
  !
  real  , dimension(ilg), intent(out) :: qcw !<
  real  , dimension(ilg), intent(out) :: zclf !<
  real  , dimension(ilg), intent(out) :: sigma !<
  real  , dimension(ilg), intent(out) :: zcraut !<
  real  , dimension(ilg), intent(out) :: qcwvar !<
  real  , dimension(ilg), intent(out) :: ssh !<
  real  , dimension(ilg), intent(out) :: cvsg !<
  real  , dimension(ilg), intent(in)  :: hmn !<
  real  , dimension(ilg), intent(in)  :: qt !<
  real  , dimension(ilg), intent(in)  :: p !<
  real  , dimension(ilg), intent(in)  :: z !<
  real  , dimension(ilg), intent(in)  :: cpm !<
  real  , dimension(ilg), intent(in)  :: zfrac !<
  real  , dimension(ilg), intent(in)  :: cons !<
  real  , dimension(ilg), intent(in)  :: crh !<
  real  , dimension(ilg), intent(in)  :: rrl !<
  real  , dimension(ilg), intent(in)  :: almix !<
  real  , dimension(ilg), intent(in)  :: dhldz !<
  real  , dimension(ilg), intent(in)  :: drwdz !<
  !
  real*4 :: r4val !<
  real*8 :: r8val !<
  integer :: machine !<
  !
  real :: esw
  real :: esi
  esw(ttt)    = exp(rw1+rw2/ttt)*ttt**rw3
  esi(ttt)    = exp(ri1+ri2/ttt)*ttt**ri3
  !
  data zero,one /0., 1./
  !==============================================================================================
  !     * parameters.
  !
  zepclc=1.e-2
  zsec=1.e-9
  csigma=0.2
  yfr=0.50
  sqrt2=sqrt(2.)
  pi=3.141592653589793
  sqrt2pi=sqrt(2.*pi)
  q1min1=-3.
  q1min2=tan((zepclc-0.5)/0.36)/1.55
  q1min=max(q1min1,q1min2)
  q1minm=q1min/2.
  q1max=2.
  q1maxm=q1max/2.
  expm1=exp(-1.)
  qdelt=2./30.
  tausig=21600.
  sigfac=exp(-ztmst/tausig)
  eps2=1.-eps1
  machine=1
  !
  !     * initialization.
  !
  ilbad=0
  do il=il1,il2
    qcw   (il)=0.
    zclf  (il)=0.
    zcraut(il)=1.
    sigma (il)=0.
    ssh   (il)=0.
  end do
  !
  do il=il1,il2
    if (crh(il)/=0. ) then
      !
      !         * temperature under clear-sky conditions as first guess.
      !
      tx=(hmn(il)-grav*z(il))/cpm(il)
      tl=tx
      !
      !         * water vapour saturation mixing ratio under clear-sky conditions.
      !
      est=(1.-zfrac(il))*esw(tx)+zfrac(il)*esi(tx)
      rss=eps1*est/( p(il)-eps2*est)
      drssdt=eps1*p(il) &
                   *((1.-zfrac(il))*esw(tx)*(rw3-rw2/tx) &
                        +zfrac(il) *esi(tx)*(ri3-ri2/tx)) &
                    /(tx*(p(il)-eps2*est)**2)
      rsstl=rss
      drssdtl=drssdt
      !
      !         * account for cloud water/ice if saturation. use
      !         * series reversion and iterate.
      !
      if (rss<qt(il) ) then
        !
        !           * first iteration.
        !
        hmnpr=cpm(il)*tx+grav*z(il)-hmn(il)
        dhmnpr=cpm(il)
        rc=qt(il)-rss
        hmnpr=hmnpr-rrl(il)*rc
        dhmnpr=dhmnpr+rrl(il)*drssdt
        tx=tx-hmnpr/dhmnpr
        est=(1.-zfrac(il))*esw(tx)+zfrac(il)*esi(tx)
        rss=eps1*est/( p(il)-eps2*est)
        drssdt=eps1*p(il) &
                   *((1.-zfrac(il))*esw(tx)*(rw3-rw2/tx) &
                        +zfrac(il) *esi(tx)*(ri3-ri2/tx)) &
                    /(tx*(p(il)-eps2*est)**2)
        !
        !           * second iteration.
        !
        hmnpr=cpm(il)*tx+grav*z(il)-hmn(il)
        dhmnpr=cpm(il)
        rc=qt(il)-rss
        hmnpr=hmnpr-rrl(il)*rc
        dhmnpr=dhmnpr+rrl(il)*drssdt
        tx=tx-hmnpr/dhmnpr
        est=(1.-zfrac(il))*esw(tx)+zfrac(il)*esi(tx)
        rss=eps1*est/( p(il)-eps2*est)
        drssdt=eps1*p(il) &
                   *((1.-zfrac(il))*esw(tx)*(rw3-rw2/tx) &
                        +zfrac(il) *esi(tx)*(ri3-ri2/tx)) &
                    /(tx*(p(il)-eps2*est)**2)
        !
        !           * third iteration.
        !
        hmnpr=cpm(il)*tx+grav*z(il)-hmn(il)
        dhmnpr=cpm(il)
        rc=qt(il)-rss
        hmnpr=hmnpr-rrl(il)*rc
        dhmnpr=dhmnpr+rrl(il)*drssdt
        tx=tx-hmnpr/dhmnpr
        est=(1.-zfrac(il))*esw(tx)+zfrac(il)*esi(tx)
        rss=eps1*est/( p(il)-eps2*est)
        drssdt=eps1*p(il) &
                   *((1.-zfrac(il))*esw(tx)*(rw3-rw2/tx) &
                        +zfrac(il) *esi(tx)*(ri3-ri2/tx)) &
                    /(tx*(p(il)-eps2*est)**2)
        !
        !           * temperature-derivative of rss for t=tl. the derivative
        !           * is used in a taylor series approximation to determine the
        !           * water vapour saturation mixing ratio at temperature tx
        !           * (i.e. ambient temperature) about tl (i.e. liquid water
        !           * temperature).
        !
        if (tx/=tl .and. rss > rsstl) then
          drssdt=(rss-rsstl)/(tx-tl)
        else
          drssdt=drssdtl
          rss=rsstl
        end if
      end if
      !
      !         * save final itteration of saturation specific humidity for
      !         * use else where in calculation relative humidities.
      !
      ssh(il)=rss
      if (isubg==1) then
        !
        !           * a and b parameters for statistical cloud scheme.
        !
        apa=1./(1.+rrl(il)*drssdt/cpm(il))
        apb=apa*drssdt
        !
        !           * determine variance based on local approach (chaboureau and
        !           * bechthold).
        !
        sigmacb=csigma*almix(il)* &
                    abs(apa*drwdz(il)-apb*dhldz(il)/cpm(il))
        !
        if (icvsg==1) then
          !
          !             * calculate convective contribution to variance.
          !             * this must be output because of "AGEING".
          !             * note ** this is only done once near the beginning of the
          !             *         physics, due to the "AGEING" calculation (ie
          !             *         ONE DOESN'T WANT MULTIPLE AGEING PER TIMESTEP FOR
          !             *         each physics routine which calls this subroutine.
          !             *         this is controlled by the switch "ICVSG").
          !
          if (cons(il)>0.) then
            cvsg(il)=3.e-03*cons(il)/apa
            ! old            cvsg(il)=1.e-05*apa
          else
            cvsg(il)=cvsg(il)*sigfac
          end if
        end if
        !
        !           * add convective contribution to variance.
        !
        sigmasum=sigmacb+cvsg(il)
        !
        !           * limit variance to avoid unphysical cases with large variances
        !           * an low mean values for the total water probability distribution.
        !
        sigmamax=yfr*apa*qt(il)/2.
        sigma(il)=min(sigmasum,sigmamax)
      end if
      !
      !         * cloud fraction and cloud water content based on chaboureau
      !         * AND BECHTHOLD'S STATISTICAL CLOUD SCHEME AND AUTOCONVERSION
      !         * SCALING FACTOR BASED ON MELLORS'S STATISTICAL CLOUD SCHEME
      !         * (for the 2.47th moment of the total water probability
      !         * distribution).
      !
      qcwt=(qt(il)-rsstl)
      if (isubg == 1 .and. sigma(il) > zsec) then
        qcwt=qcwt*apa
        q1=qcwt/sigma(il)
        q1m=q1/2.
        indx=nint((q1/2.+2.)/qdelt)+1
        if (q1<q1min) then
          zclf(il)=0.
          qcw (il)=0.
          zcraut(il)=zcr(q1minm,i2ndie)
          qcwvar(il)=0.
        else if (q1>=q1min .and. q1<0. ) then
          zclf(il)=0.5+0.36*atan(1.55*q1)
          qcw (il)=sigma(il)*exp(1.2*q1-1.)
          zcraut(il)=zcr(q1m,i2ndie)
          !
          if (machine==2) then
            r4val=q1m/sqrt2
            ar=.5*(1.+erf(r4val))
          else
            r8val=q1m/sqrt2
            ar=.5*(1.+erf(r8val))
          end if
          !
          aexp=exp(-.5*q1m**2)
          ql=ar*q1m+aexp/sqrt2pi
          qlvar=ar*(1.+(q1m-ql)**2)+aexp*(q1m/sqrt2pi-ql)
          qcwvar(il)=qlvar/ql**2
        else if (q1>=0. .and. q1<=q1max) then
          zclf(il)=0.5+0.36*atan(1.55*q1)
          qcw (il)=sigma(il)*(expm1+0.66*q1+0.086*q1**2)
          zcraut(il)=zcr(q1m,i2ndie)
          !
          if (machine==2) then
            r4val=q1m/sqrt2
            ar=.5*(1.+erf(r4val))
          else
            r8val=q1m/sqrt2
            ar=.5*(1.+erf(r8val))
          end if
          !
          aexp=exp(-.5*q1m**2)
          ql=ar*q1m+aexp/sqrt2pi
          qlvar=ar*(1.+(q1m-ql)**2)+aexp*(q1m/sqrt2pi-ql)
          qcwvar(il)=qlvar/ql**2
        else
          zclf(il)=1.
          qcw (il)=qt(il)-rss
          zcraut(il)=1.
          qcwvar(il)=0.
        end if
        !
        !           * impose upper bound on cloud water. this is necessary since
        !           * the empirical fits on which the statistical cloud scheme is
        !           * is based may produce unphysical results under certain
        !           * circumstances (i.e. excessively large cloud water contents).
        !           * the approach here is not to allow the inferred water vapour
        !           * mixing ratio in the clear-sky to become smaller than a
        !           * given threshold (zsec).
        !
        qcwmax=max(qt(il)-((1.-zclf(il))*zsec+zclf(il)*rss),0.)
        if (q1>=q1min .and. q1<=q1max &
            .and. qcw(il)>=qcwmax) then
          qcw(il)=qcwmax
        end if
      else
        if (qcwt < 0. ) then
          zclf(il)=0.
          qcw (il)=0.
          zcraut(il)=zcr(q1minm,i2ndie)
          qcwvar(il)=0.
        else
          zclf(il)=1.
          qcw (il)=qt(il)-rss
          zcraut(il)=1.
          qcwvar(il)=0.
        end if
      end if
      qcw(il)=max(min(qcw(il),qt(il)),zero)
      zclf(il)=max(min(zclf(il),one),zero)
      !
      !         * eliminate cloudsand condensate if cloud fraction or
      !         * condensate mixing ratio too low to ensure consistent
      !         * results for cloud fraction and cloud condensate.
      !
      if (zclf(il)<0.01 .or. qcw(il)<1.e-12) then
        zclf(il)=0.
        qcw (il)=0.
        zcraut(il)=zcr(q1minm,i2ndie)
        qcwvar(il)=0.
      end if
      !
      !         * track unphysical values in preparation for aborting
      !         * (done outside loop in order to keep optimization).
      !
      if (isubg==1 .and. qcw(il)>qt(il) .and. ilbad==0) &
          then
        ilbad=il
        sigcbbad=sigmacb
        sigmaxbad=sigmamax
        qcwtbad=qcwt
      end if
    end if
  end do
  !
  !     * print out problem points and abort, if they exist.
  !
  if (ilbad/=0) then
    il=ilbad
    print*,'ICALL,LVL,IL,QCW,QT,RSS=',icall,lvl,ilbad,qcw(il), &
                qt(il),ssh(il)
    print*,'SIGMA,SIGMACB,CVSG,SIGMAMAX,ZCLF,ZCRAUT,QCWT=', &
                sigma(il),sigcbbad,cvsg(il),sigmaxbad,zclf(il), &
                zcraut(il),qcwtbad
    call xit('STATCLD5',-1)
  end if
  !
end subroutine statcld
