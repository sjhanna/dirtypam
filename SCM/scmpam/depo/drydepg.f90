subroutine drydepg(xrow,flndrow,gtrowl,gtrowi,pressg,dshj,throw, &
                         shj,sicnrow,snorow,zsoi,qrow,hmfnrow, &
                         zfor,dd4,ztmst,iso2,ilg,il1,il2,ilev,ntrac)
  !
  !     this routine calculates the  dry deposition flux for so2.
  !
  use sdphys
  !
  implicit none
  real :: fac1
  real :: fac1l
  real :: fac1nl
  integer :: il
  integer, intent(in) :: il1
  integer, intent(in) :: il2
  integer, intent(in) :: ilev
  integer, intent(in) :: ilg
  integer, intent(in) :: iso2
  integer, intent(in) :: ntrac
  real :: zero
  real :: zmaxvdry
  real :: zrho0
  real, intent(in) :: ztmst
  real :: zvd2ice
  real :: zvd2nof
  real :: zvd4ice
  real :: zvd4nof
  real :: zvdrd1
  real :: zvdrd1l
  real :: zvdrd1nl
  real :: zzdp
  real :: zzfcrow

  real, intent(in)   :: gtrowl(ilg) !<
  real, intent(in)   :: gtrowi(ilg) !<
  real, intent(in)   :: dshj(ilg,ilev) !<
  real, intent(in)   :: pressg(ilg) !<
  real, intent(in)   :: shj(ilg,ilev) !<
  real, intent(in)   :: throw(ilg,ilev) !<
  real, intent(in)   :: sicnrow(ilg) !<
  real, intent(in)   :: snorow(ilg) !<
  real, intent(in)   :: qrow(ilg,ilev) !<
  real, intent(inout)   :: xrow(ilg,ilev,ntrac) !<
  real, intent(in)   :: flndrow(ilg) !<
  real, intent(in)   :: hmfnrow(ilg) !<
  real   :: zvwc2 !<
  real   :: zvw02 !<
  real   :: zvwc4 !<
  real   :: zvw04 !<
  real   :: zsncri !<
  real   :: alfa !<
  real, intent(in), dimension(ilg) :: zsoi !<
  real, intent(in)   :: zfor(ilg) !<
  real, intent(inout)   :: dd4(ilg) !<
  real   :: t1s !<
  !
  data zero /0./
  !=====================================================================
  !     m water equivalent  critical snow height (from *surf*)
  zsncri=0.025
  !
  !     coefficients for zvdrd = function of soil moisture
  !
  zvwc2=(0.8e-2 - 0.2e-2)/(1. - 0.9)
  zvw02=zvwc2-0.8e-2
  zvwc4=(0.2e-2 - 0.025e-2)/(1. - 0.9)
  zvw04=zvwc4-0.2e-2
  !
  !*      2.    dry deposition.
  !             --- ----------
  t1s=273.16
  do il=il1,il2
    zrho0=shj(il,ilev)*pressg(il)/(rgas*throw(il,ilev))
    zzdp=zrho0*ztmst*grav/(dshj(il,ilev)*pressg(il))
    zmaxvdry=1./zzdp
    zvdrd1nl=0.
    zvdrd1l =0.
    !
    !      * dry deposition of so2, so4.
    !
    !      - sea(water or seaice) -
    if (flndrow(il)<1.) then
      !        - melting/not melting seaice-
      if (gtrowi(il)>=(t1s-0.1)) then
        zvd2ice=0.8e-2
        zvd4ice=0.2e-2
      else
        zvd2ice=0.1e-2
        zvd4ice=0.025e-2
      end if
      alfa=sicnrow(il)
      zvdrd1nl=(1.-alfa)*1.0e-2+alfa*zvd2ice
      zvdrd1nl=min(max(zvdrd1nl,0.),zmaxvdry)
    end if
    !
    !      - land -
    if (flndrow(il)>0.) then
      !        - non-forest areas -
      !        - snow/no snow -
      if (snorow(il)>zsncri) then
        !          - melting/not melting snow -
        if (hmfnrow(il)>0.) then
          zvd2nof=0.8e-2
          zvd4nof=0.2e-2
        else
          zvd2nof=0.1e-2
          zvd4nof=0.025e-2
        end if
      else
        !          - frozen/not frozen soil -
        if (gtrowl(il)<=t1s) then
          zvd2nof=0.2e-2
          zvd4nof=0.025e-2
        else
          !          - wet/dry -
          if (zsoi(il)>=0.99) then
            zvd2nof=0.8e-2
            zvd4nof=0.2e-2
          else if (zsoi(il)<0.9) then
            zvd2nof=0.2e-2
            zvd4nof=0.025e-2
          else
            zvd2nof=zvwc2*zsoi(il)-zvw02
            zvd4nof=zvwc4*zsoi(il)-zvw04
          end if
        end if
      end if
      zzfcrow=max(zfor(il),0.)
      zvdrd1l=zzfcrow*0.8e-2 &
                        +(1.-zzfcrow)*zvd2nof
      zvdrd1l=min(max(zvdrd1l,0.),zmaxvdry)
    end if
    zvdrd1 =flndrow(il)*zvdrd1l + (1.-flndrow(il))*zvdrd1nl
    !
    fac1nl = 1.-zvdrd1nl*zzdp
    fac1l  = 1.-zvdrd1l*zzdp
    fac1   = flndrow(il)*fac1l + (1.-flndrow(il))*fac1nl
    !
    !      * sulphate is done regardless of "IPAM".
    !
    xrow(il,ilev,iso2)=xrow(il,ilev,iso2)*fac1
    dd4(il)=xrow(il,ilev,iso2)*zvdrd1*zrho0
  end do ! loop 205
  !
  return
end

