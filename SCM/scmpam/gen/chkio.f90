subroutine chkio
  !-----------------------------------------------------------------------
  !     purpose:
  !     --------
  !     checks and trims model input.
  !
  !     history:
  !     --------
  !     * feb 12/2010 - k.vonsalzen   add new parameters.
  !     * sep 14/2007 - k.vonsalzen   new.
  !
  !-----------------------------------------------------------------------
  !
  use psizes
  use runinfo
  !
  implicit none
  !   
  integer :: ileng
  integer :: nc
  !
  !---------------------------------------------------------------------
  !     * check parameter ranges.
  !
  ileng=len_trim(modl%runname)
  if (ileng == 0 .or. ileng > imaxl) call xit('CHKIO',-1)
  if (modl%plev < 1) call xit('CHKIO',-2)
  if (modl%dzsfc <= 0. ) call xit('CHKIO',-3)
  if (modl%dt <= 0. ) call xit('CHKIO',-5)
  if (air%zhi < 0. )  call xit('CHKIO',-6)
  if (air%tempi <= 273.15-5. .or. air%tempi >= 323. ) &
      call xit('CHKIO',-7)
  if (air%presi <= 30000. .or. air%presi >= 110000. ) &
      call xit('CHKIO',-8)
  if (air%sviclr < -1. .or. air%sviclr > 0.3) &
      call xit('CHKIO',-9)
  if (air%nper < 0 .or. air%nper > npermx) &
      call xit('CHKIO',-10)
  do nc=1,air%nper
    if (abs(air%drv(nc)) > 1.e-09 &
        .or. abs(air%dta(nc)) > 1.e-09) then
      if (air%lvbot(nc) > ilev .or. air%lvtop(nc) < 1) &
          call xit('CHKIO',-12)
      if (air%lvbot(nc) < air%lvtop(nc) ) &
          call xit('CHKIO',-13)
      if (air%peron(nc) > air%peroff(nc) .or. air%peron(nc) < 1 &
          .or. air%peroff(nc) > modl%ntstp) &
          call xit('CHKIO',-14)
    end if
    if (air%cldf(nc) > 1. ) &
        call xit('CHKIO',-15)
    if (air%cldf(nc) > 0. ) then
      if (air%svicld(nc) < -1. .or. air%svicld(nc) > 0.3) &
          call xit('CHKIO',-16)
      if (air%lvbot(nc) > ilev .or. air%lvtop(nc) < 1) &
          call xit('CHKIO',-17)
      if (air%lvbot(nc) < air%lvtop(nc) ) &
          call xit('CHKIO',-18)
      if (air%peron(nc) > air%peroff(nc) .or. air%peron(nc) < 1 &
          .or. air%peroff(nc) > modl%ntstp) &
          call xit('CHKIO',-19)
    end if
    if (abs(air%dtg(nc)) > 1.e-09) then
      if (air%dtgon(nc) > air%dtgoff(nc) .or. air%dtgon(nc) < 1 &
          .or. air%dtgoff(nc) > modl%ntstp) &
          call xit('CHKIO',-20)
    end if
  end do
  !
end subroutine chkio
