subroutine trinfo(ntracp,iainda,iaindt,itrwet,ntrac)
  !-----------------------------------------------------------------------
  !     purpose:
  !     --------
  !     dummy routine replacing tracer information for atmospheric model.
  !
  !     history:
  !     --------
  !     * feb 11/2010 - k.vonsalzen   new
  !
  !-----------------------------------------------------------------------
  !
  use trcind, only : iso2,ihpo,igs6,igsp,idms ! gas-phase tracer indices
  !
  implicit none
  !
  integer :: i
  integer, intent(in) :: ntrac !< Number of tracers
  integer, intent(inout) :: ntracp !< Number of aerosol tracers
  !
  !     * maximum number of aerosol tracers. maxaero should be
  !     * equal to the number of aerosol tracers that are specified
  !     * according to input file "PARAM".
  !
  integer, parameter :: maxaero=24 !<
  !
  !     * number of other tracers in the model.
  !
  integer, parameter :: maxothr=5 !<
  !
  character(len=4) :: chtmp !<
  character(len=4), dimension(ntrac) :: chmnam !<
  integer, intent(inout), dimension(ntrac) :: iainda !< Tracer index array
  integer, intent(inout) :: iaindt !< First index for PLA/PAM tracers
  integer, intent(inout), dimension(ntrac) :: itrwet !< Flags to enable wet deposition
  !
  !-----------------------------------------------------------------------
  !     * check if sufficient memory is available to accommodate tracers.
  !     * increase ntrac, if necessary.
  !
  if (ntrac < maxaero+maxothr) call xit('TRINFO',-1)
  !
  !     * give warning in case the memory required for tracers is less
  !     * than specified. change ntrac accordingly if this warning appears.
  !
  if (ntrac > maxaero+maxothr) call wrn('TRINFO',-1)
  !
  !     * ALSO MAKE SURE THAT TRACER NAMES FOR AEROSOL DON'T EXCEED 3
  !     * letters.
  !
  if (maxaero > 99) call xit('TRINFO',-2)
  !
  !     * initialization of tracer names.
  !
  do i=1,ntrac
    write(chmnam(i),'(A4)') '****'
  end do
  !
  !-----------------------------------------------------------------------
  !     * specify tracer names for aerosol.
  !
  do i=1,maxaero
    if (i >= 10) then
      write(chmnam(i),'(1X,A1,I2)') 'A',i
    else
      write(chmnam(i),'(1X,A2,I1)') 'A0',i
    end if
  end do
  !
  !     * specify tracer names for gases (needs to be consistent with
  !     * parameter maxothr).
  !
  if (maxothr /= 5) call xit('TRINFO',-3)
  chmnam(maxaero+1)=' SO2'
  chmnam(maxaero+2)=' DMS'
  chmnam(maxaero+3)=' HPO'
  chmnam(maxaero+4)=' GS6'
  chmnam(maxaero+5)=' GSP'
  !
  !     * summary of gas tracers.
  !
  write(6,'(1X)')
  write(6,'(A9)')    '*** Gases'
  write(6,'(1X)')
  write(6,'(2X,A62)') &
            'Gas Index          Gas Type                             Tra &
      cer'
  write(6,'(3X,A4,14X,A14,24X,I2)') 'ISO2','SO2           ', &
                                        maxaero+1
  write(6,'(3X,A4,14X,A14,24X,I2)') 'IDMS','DMS           ', &
                                        maxaero+2
  write(6,'(3X,A4,14X,A14,24X,I2)') 'IHPO','H2O2          ', &
                                        maxaero+3
  write(6,'(3X,A4,14X,A14,24X,I2)') 'IGS6','H2SO4         ', &
                                        maxaero+4
  write(6,'(3X,A4,14X,A14,24X,I2)') 'IGSP','SOA PRECURSORS', &
                                        maxaero+5
  write(6,'(1X)')
  !
  !-----------------------------------------------------------------------
  !     * specify corresponding tracer indices. make sure that
  !     * indices are consistent with specification of common block
  !     * and tracer names above !
  !
  iso2=maxaero+1
  idms=maxaero+2
  ihpo=maxaero+3
  igs6=maxaero+4
  igsp=maxaero+5
  !
  !     * flag for wet depostion.
  !
  itrwet(iso2)=0
  itrwet(idms)=0
  itrwet(ihpo)=0
  itrwet(igs6)=1
  itrwet(igsp)=1
  !
  !-----------------------------------------------------------------------
  !     * extract aerosol tracers for pla calculations. all tracers
  !     * with names that start with an "A" and are followed by a number
  !     * ARE CONSIDERED AEROSOL TRACERS (I.E. FORMAT ' A##').
  !
  write(6,'(1X)')
  write(6,'(A12)')    '*** Aerosols'
  write(6,'(1X)')
  write(6,'(2X,A62)') &
            'Aer Index          Aer Type                             Tra &
      cer'
  iainda=-1
  ntracp=0
  do i=1,ntrac
    itrwet(i)=0
    chtmp=chmnam(i)
    if (i <= maxaero) then
      write(6,'(3X,A2,A2,14X,A4,34X,I2)') 'IA',chtmp(3:4),chtmp,i
    end if
    if (chtmp(2:2) == 'A' &
        .and. (chtmp(3:3) == '0' .or. chtmp(3:3) == '1' &
        .or. chtmp(3:3) == '2' .or. chtmp(3:3) == '3' &
        .or. chtmp(3:3) == '4' .or. chtmp(3:3) == '5' &
        .or. chtmp(3:3) == '6' .or. chtmp(3:3) == '7' &
        .or. chtmp(3:3) == '8' .or. chtmp(3:3) == '9') &
        .and. (chtmp(4:4) == '0' .or. chtmp(4:4) == '1' &
        .or. chtmp(4:4) == '2' .or. chtmp(4:4) == '3' &
        .or. chtmp(4:4) == '4' .or. chtmp(4:4) == '5' &
        .or. chtmp(4:4) == '6' .or. chtmp(4:4) == '7' &
        .or. chtmp(4:4) == '8' .or. chtmp(4:4) == '9') &
        ) then
      ntracp=ntracp+1
      if (ntracp > ntrac) call xit('TRINFO',-4)
      iainda(ntracp)=i
    end if
  end do
  iaindt = iainda(1)
  !
end subroutine trinfo
