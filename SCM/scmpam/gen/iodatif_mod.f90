module iodatif
  !
  interface
    subroutine w0dfld(cname,avar)
      character(len=*), intent(in) :: cname !<
      real, intent(in) :: avar !<
    end subroutine w0dfld

    subroutine w1ddim(cname,avar,idim1,j1,j2)
      character(len=*), intent(in) :: cname !<
      real, intent(out), dimension(idim1) :: avar !<
      integer, intent(in) :: idim1 !<
      integer, intent(in), optional :: j1 !<
      integer, intent(in), optional :: j2 !<
    end subroutine w1ddim
    !
    subroutine w1dfld(cname,avar,idim1,j1,j2)
      character(len=*), intent(in) :: cname !<
      real, intent(in), dimension(idim1) :: avar !<
      integer, intent(in) :: idim1 !<
      integer, intent(in), optional :: j1 !<
      integer, intent(in), optional :: j2 !<
    end subroutine w1dfld
    !
    subroutine w2dfld(cname,avar,idim1,idim2,j1,j2,k1,k2)
      character(len=*), intent(in) :: cname !<
      real, intent(in), dimension(idim1,idim2) :: avar !<
      integer, intent(in) :: idim1 !<
      integer, intent(in) :: idim2 !<
      integer, intent(in), optional :: j1 !<
      integer, intent(in), optional :: j2 !<
      integer, intent(in), optional :: k1 !<
      integer, intent(in), optional :: k2 !<
    end subroutine w2dfld
    !
    subroutine w3dfld(cname,avar,idim1,idim2,idim3, &
                          j1,j2,k1,k2,l1,l2)
      character(len=*), intent(in) :: cname !<
      real, intent(in), dimension(idim1,idim2,idim3) :: avar !<
      integer, intent(in) :: idim1 !<
      integer, intent(in) :: idim2 !<
      integer, intent(in) :: idim3 !<
      integer, intent(in), optional :: j1 !<
      integer, intent(in), optional :: j2 !<
      integer, intent(in), optional :: k1 !<
      integer, intent(in), optional :: k2 !<
      integer, intent(in), optional :: l1 !<
      integer, intent(in), optional :: l2 !<
    end subroutine w3dfld
    !
    subroutine w4dfld(cname,avar,idim1,idim2,idim3,idim4, &
                          j1,j2,k1,k2,l1,l2,m1,m2)
      character(len=*), intent(in) :: cname !<
      real, intent(in), dimension(idim1,idim2,idim3,idim4) :: avar !<
      integer, intent(in) :: idim1 !<
      integer, intent(in) :: idim2 !<
      integer, intent(in) :: idim3 !<
      integer, intent(in) :: idim4 !<
      integer, intent(in), optional :: j1 !<
      integer, intent(in), optional :: j2 !<
      integer, intent(in), optional :: k1 !<
      integer, intent(in), optional :: k2 !<
      integer, intent(in), optional :: l1 !<
      integer, intent(in), optional :: l2 !<
      integer, intent(in), optional :: m1 !<
      integer, intent(in), optional :: m2 !<
    end subroutine w4dfld
    !
    subroutine w1dfldrs(cname,avar,idim1,j1,j2)
      character(len=*), intent(in) :: cname !<
      real, intent(in), dimension(idim1) :: avar !<
      integer, intent(in) :: idim1 !<
      integer, intent(in), optional :: j1 !<
      integer, intent(in), optional :: j2 !<
    end subroutine w1dfldrs
    !
    subroutine w2dfldrs(cname,avar,idim1,idim2,j1,j2,k1,k2)
      character(len=*), intent(in) :: cname !<
      real, intent(in), dimension(idim1,idim2) :: avar !<
      integer, intent(in) :: idim1 !<
      integer, intent(in) :: idim2 !<
      integer, intent(in), optional :: j1 !<
      integer, intent(in), optional :: j2 !<
      integer, intent(in), optional :: k1 !<
      integer, intent(in), optional :: k2 !<
    end subroutine w2dfldrs
    !
    subroutine w3dfldrs(cname,avar,idim1,idim2,idim3, &
                            j1,j2,k1,k2,l1,l2)
      character(len=*), intent(in) :: cname !<
      real, intent(in), dimension(idim1,idim2,idim3) :: avar !<
      integer, intent(in) :: idim1 !<
      integer, intent(in) :: idim2 !<
      integer, intent(in) :: idim3 !<
      integer, intent(in), optional :: j1 !<
      integer, intent(in), optional :: j2 !<
      integer, intent(in), optional :: k1 !<
      integer, intent(in), optional :: k2 !<
      integer, intent(in), optional :: l1 !<
      integer, intent(in), optional :: l2 !<
    end subroutine w3dfldrs
    !
    subroutine w4dfldrs(cname,avar,idim1,idim2,idim3,idim4, &
                            j1,j2,k1,k2,l1,l2,m1,m2)
      character(len=*), intent(in) :: cname !<
      real, intent(in), dimension(idim1,idim2,idim3,idim4) :: avar !<
      integer, intent(in) :: idim1 !<
      integer, intent(in) :: idim2 !<
      integer, intent(in) :: idim3 !<
      integer, intent(in) :: idim4 !<
      integer, intent(in), optional :: j1 !<
      integer, intent(in), optional :: j2 !<
      integer, intent(in), optional :: k1 !<
      integer, intent(in), optional :: k2 !<
      integer, intent(in), optional :: l1 !<
      integer, intent(in), optional :: l2 !<
      integer, intent(in), optional :: m1 !<
      integer, intent(in), optional :: m2 !<
    end subroutine w4dfldrs
    !
    subroutine r1dfld(cname,avar,idim1,j1,j2)
      character(len=*), intent(in) :: cname !<
      real, intent(in), dimension(idim1) :: avar !<
      integer, intent(in) :: idim1 !<
      integer, intent(in), optional :: j1 !<
      integer, intent(in), optional :: j2 !<
    end subroutine r1dfld
    !
    subroutine r2dfld(cname,avar,idim1,idim2,j1,j2,k1,k2)
      character(len=*), intent(in) :: cname !<
      real, intent(in), dimension(idim1,idim2) :: avar !<
      integer, intent(in) :: idim1 !<
      integer, intent(in) :: idim2 !<
      integer, intent(in), optional :: j1 !<
      integer, intent(in), optional :: j2 !<
      integer, intent(in), optional :: k1 !<
      integer, intent(in), optional :: k2 !<
    end subroutine r2dfld
    !
    subroutine r3dfld(cname,avar,idim1,idim2,idim3, &
                          j1,j2,k1,k2,l1,l2)
      character(len=*), intent(in) :: cname !<
      real, intent(in), dimension(idim1,idim2,idim3) :: avar !<
      integer, intent(in) :: idim1 !<
      integer, intent(in) :: idim2 !<
      integer, intent(in) :: idim3 !<
      integer, intent(in), optional :: j1 !<
      integer, intent(in), optional :: j2 !<
      integer, intent(in), optional :: k1 !<
      integer, intent(in), optional :: k2 !<
      integer, intent(in), optional :: l1 !<
      integer, intent(in), optional :: l2 !<
    end subroutine r3dfld
    !
    subroutine r4dfld(cname,avar,idim1,idim2,idim3,idim4, &
                          j1,j2,k1,k2,l1,l2,m1,m2)
      character(len=*), intent(in) :: cname !<
      real, intent(in), dimension(idim1,idim2,idim3,idim4) :: avar !<
      integer, intent(in) :: idim1 !<
      integer, intent(in) :: idim2 !<
      integer, intent(in) :: idim3 !<
      integer, intent(in) :: idim4 !<
      integer, intent(in), optional :: j1 !<
      integer, intent(in), optional :: j2 !<
      integer, intent(in), optional :: k1 !<
      integer, intent(in), optional :: k2 !<
      integer, intent(in), optional :: l1 !<
      integer, intent(in), optional :: l2 !<
      integer, intent(in), optional :: m1 !<
      integer, intent(in), optional :: m2 !<
    end subroutine r4dfld
    !
    subroutine r1dfldrs(cname,avar,idim1,j1,j2)
      character(len=*), intent(in) :: cname !<
      real, intent(in), dimension(idim1) :: avar !<
      integer, intent(in) :: idim1 !<
      integer, intent(in), optional :: j1 !<
      integer, intent(in), optional :: j2 !<
    end subroutine r1dfldrs
    !
    subroutine r2dfldrs(cname,avar,idim1,idim2,j1,j2,k1,k2)
      character(len=*), intent(in) :: cname !<
      real, intent(in), dimension(idim1,idim2) :: avar !<
      integer, intent(in) :: idim1 !<
      integer, intent(in) :: idim2 !<
      integer, intent(in), optional :: j1 !<
      integer, intent(in), optional :: j2 !<
      integer, intent(in), optional :: k1 !<
      integer, intent(in), optional :: k2 !<
    end subroutine r2dfldrs
    !
    subroutine r3dfldrs(cname,avar,idim1,idim2,idim3, &
                            j1,j2,k1,k2,l1,l2)
      character(len=*), intent(in) :: cname !<
      real, intent(in), dimension(idim1,idim2,idim3) :: avar !<
      integer, intent(in) :: idim1 !<
      integer, intent(in) :: idim2 !<
      integer, intent(in) :: idim3 !<
      integer, intent(in), optional :: j1 !<
      integer, intent(in), optional :: j2 !<
      integer, intent(in), optional :: k1 !<
      integer, intent(in), optional :: k2 !<
      integer, intent(in), optional :: l1 !<
      integer, intent(in), optional :: l2 !<
    end subroutine r3dfldrs
    !
    subroutine r4dfldrs(cname,avar,idim1,idim2,idim3,idim4, &
                            j1,j2,k1,k2,l1,l2,m1,m2)
      character(len=*), intent(in) :: cname !<
      real, intent(in), dimension(idim1,idim2,idim3,idim4) :: avar !<
      integer, intent(in) :: idim1 !<
      integer, intent(in) :: idim2 !<
      integer, intent(in) :: idim3 !<
      integer, intent(in) :: idim4 !<
      integer, intent(in), optional :: j1 !<
      integer, intent(in), optional :: j2 !<
      integer, intent(in), optional :: k1 !<
      integer, intent(in), optional :: k2 !<
      integer, intent(in), optional :: l1 !<
      integer, intent(in), optional :: l2 !<
      integer, intent(in), optional :: m1 !<
      integer, intent(in), optional :: m2 !<
    end subroutine r4dfldrs
  end interface
  !
end module iodatif
