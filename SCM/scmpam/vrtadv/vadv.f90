subroutine vadv(dxdt,xrow,dtdt,throw,dqdt,qrow,dqldt,qlwc, &
                      dudt,u,w,zf,zh,ph,dp,almc,almx, &
                      dt,il1,il2,ilg,ilev,ilevp1,msgp,ntrac)
  !
  use sdphys
  !
  implicit none
  !
  real, intent(in) :: dt
  real :: dzt
  real :: facte
  integer :: i2ndie
  integer :: icall
  integer :: icvsg
  integer :: il
  integer, intent(in) :: il1
  integer, intent(in) :: il2
  integer, intent(in) :: ilev
  integer, intent(in) :: ilevp1
  integer, intent(in) :: ilg
  integer :: isubg
  integer :: jk
  integer :: l
  integer, intent(in) :: msgp
  integer :: n
  integer, intent(in) :: ntrac
  !
  real, intent(in), dimension(ilg,ilev) :: zf !<
  real, intent(in), dimension(ilg,ilev) :: zh !<
  real, intent(in), dimension(ilg,ilev) :: ph !<
  real, intent(in), dimension(ilg,ilev) :: dp !<
  real, intent(in), dimension(ilg,ilev) :: almc !<
  real, intent(in), dimension(ilg,ilev) :: almx !<
  real, dimension(ilg,ilevp1), intent(inout) :: throw !<
  real, dimension(ilg,ilevp1), intent(inout) :: qrow !<
  real, dimension(ilg,ilev), intent(in) :: qlwc !<
  real, dimension(ilg,ilev), intent(inout) :: u !<
  real, dimension(ilg,ilev), intent(inout) :: w !<
  real, dimension(ilg,ilev,ntrac) :: rkx !<
  real, dimension(ilg,ilevp1,ntrac), intent(in) :: xrow !<
  real, dimension(ilg,ilevp1,ntrac), intent(out) :: dxdt !<
  real, dimension(ilg,ilevp1), intent(out) :: dqdt !<
  real, dimension(ilg,ilevp1), intent(out) :: dtdt !<
  real, dimension(ilg,ilev), intent(out) :: dqldt !<
  real, dimension(ilg,ilev), intent(out) :: dudt !<
  real, dimension(ilg,ilev) :: qt !<
  real, dimension(ilg,ilev) :: hmn !<
  real, dimension(ilg,ilev) :: wf !<
  real, dimension(ilg,ilev) :: qtn !<
  real, dimension(ilg,ilev) :: hmnn !<
  real, dimension(ilg,ilev) :: cvar !<
  real, dimension(ilg,ilev) :: zcraut !<
  real, dimension(ilg,ilev) :: qcwvar !<
  real, dimension(ilg,ilev) :: cvsg !<
  real, dimension(ilg,ilev) :: cvdu !<
  real, dimension(ilg,ilev) :: zfrac !<
  real, dimension(ilg,ilev) :: qc !<
  real, dimension(ilg,ilev) :: vdpq !<
  real, dimension(ilg,ilev) :: vdph !<
  real, dimension(ilg,ilev) :: vdpu !<
  real, dimension(ilg,ilev) :: zclf !<
  real, dimension(ilg,ilev,ntrac) :: vdpx !<
  real, dimension(ilg) :: pmbs !<
  real, dimension(ilg) :: rrl !<
  real, dimension(ilg) :: cpm !<
  real, dimension(ilg) :: almix !<
  real, dimension(ilg) :: dhldz !<
  real, dimension(ilg) :: drwdz !<
  real, dimension(ilg) :: flagest !<
  real, dimension(ilg) :: qcw !<
  real, dimension(ilg) :: ssh !<
  real, dimension(ilg) :: wgt !<
  real, dimension(ilg) :: dz !<
  real, dimension(ilg) :: tmp !<
  !
  !     * total water mixing ratio and cloud water static energy.
  !
  qt=qrow(:,msgp:ilevp1)+qlwc
  hmn=cpres*throw(:,msgp:ilevp1)+grav*zh-rl*qlwc
  !
  !     * vertical wind at grid cell interfaces.
  !
  wf(:,ilev)=0.
  do l=2,ilev
    wgt(:)=(zf(:,l)-zh(:,l))/(zh(:,l-1)-zh(:,l))
    wf(:,l-1)=(1.-wgt)*w(:,l)+wgt*w(:,l-1)
  end do
  !
  !     * advect water mixing ratio, cloud water static energy vertically,
  !     * winds, and tracers using euler forward differences.
  !
  vdpq(:,ilev)=0.
  vdph(:,ilev)=0.
  vdpu(:,ilev)=0.
  vdpx(:,ilev,:)=0.
  do l=3,ilev
    where (wf(:,l-1) <= 0. )
      dz=zf(:,l-2)-zf(:,l-1)
      tmp=max(wf(:,l-1)*dt/dz,-1.)*dp(:,l-1)
      vdpq(:,l-1)=qt(:,l-1)*tmp
      vdph(:,l-1)=hmn(:,l-1)*tmp
      vdpu(:,l-1)=u(:,l-1)*tmp
    else where
      dz=zf(:,l-1)-zf(:,l)
      tmp=min(wf(:,l-1)*dt/dz,1.)*dp(:,l)
      vdpq(:,l-1)=qt(:,l)*tmp
      vdph(:,l-1)=hmn(:,l)*tmp
      vdpu(:,l-1)=u(:,l)*tmp
    end where
    do n=1,ntrac
      where (wf(:,l-1) <= 0. )
        dz=zf(:,l-2)-zf(:,l-1)
        tmp=max(wf(:,l-1)*dt/dz,-1.)*dp(:,l-1)
        vdpx(:,l-1,n)=xrow(:,l-1,n)*tmp
      else where
        dz=zf(:,l-1)-zf(:,l)
        tmp=min(wf(:,l-1)*dt/dz,1.)*dp(:,l)
        vdpx(:,l-1,n)=xrow(:,l,n)*tmp
      end where
    end do
  end do
  l=2
  vdpq(:,1)=0.
  vdph(:,1)=0.
  vdpu(:,1)=0.
  vdpx(:,1,:)=0.
  where (wf(:,l-1) > 0. )
    dz=zf(:,l-1)-zf(:,l)
    tmp=min(wf(:,l-1)*dt/dz,1.)*dp(:,l)
    vdpq(:,l-1)=qt(:,l)*tmp
    vdph(:,l-1)=hmn(:,l)*tmp
    vdpu(:,l-1)=u(:,l)*tmp
  end where
  do n=1,ntrac
    where (wf(:,l-1) > 0. )
      dz=zf(:,l-1)-zf(:,l)
      tmp=min(wf(:,l-1)*dt/dz,1.)*dp(:,l)
      vdpx(:,l-1,n)=xrow(:,l,n)*tmp
    end where
  end do
  l=1
  qt(:,l)=qt(:,l)+vdpq(:,l)/dp(:,l)
  hmn(:,l)=hmn(:,l)+vdph(:,l)/dp(:,l)
  dudt(:,l)=vdpu(:,l)/dp(:,l)/dt
  do n=1,ntrac
    dxdt(:,l,n)=vdpx(:,l,n)/dp(:,l)/dt
  end do
  do l=2,ilev
    qt(:,l)=qt(:,l)-(vdpq(:,l-1)-vdpq(:,l))/dp(:,l)
    hmn(:,l)=hmn(:,l)-(vdph(:,l-1)-vdph(:,l))/dp(:,l)
    dudt(:,l)=-(vdpu(:,l-1)-vdpu(:,l))/dp(:,l)/dt
    do n=1,ntrac
      dxdt(:,l,n)=-(vdpx(:,l-1,n)-vdpx(:,l,n))/dp(:,l)/dt
    end do
  end do
  !
  !     * temperature and water vapour mixing ratio from statistical
  !     * cloud scheme.
  !
  facte=1./eps1-1.
  i2ndie=1
  flagest=1.
  cvsg=0.
  cvdu=0.
  zfrac=0.
  icvsg=1
  isubg=1
  icall=1
  do jk=1,ilev
    do il=1,ilg
      pmbs(il)=0.01*ph(il,jk)
      rrl(il)=rl
      cpm(il)=cpres
      almix(il)=max(almc(il,jk),almx(il,jk),10.)
      if (jk>1) then
        dzt      =zh  (il,jk-1)-zh (il,jk)
        dhldz(il)=(hmn(il,jk-1)-hmn(il,jk))/dzt
        drwdz(il)=(qt (il,jk-1)-qt (il,jk))/dzt
      else
        dhldz(il)=0.
        drwdz(il)=0.
      end if
    end do
    call statcld (qcw,zclf(1,jk),cvar(1,jk),zcraut(1,jk), &
                      qcwvar(1,jk),ssh,cvsg(1,jk),qt(1,jk), &
                      hmn(1,jk),zfrac(1,jk),cpm, &
                      pmbs,zh(1,jk),rrl,cvdu(1,jk),flagest, &
                      almix,dhldz,drwdz,i2ndie, &
                      dt,ilev,ilg,1,ilg,icvsg,isubg, &
                      jk,icall)
    !
    do il=1,ilg
      dtdt(il,jk)=((hmn(il,jk)-grav*zh(il,jk)+rrl(il)*qcw(il)) &
                       /cpm(il)-throw(il,jk))/dt
      dqdt(il,jk)=((qt(il,jk)-qcw(il))-qrow(il,jk))/dt
      dqldt(il,jk)=(qcw(il)-qlwc(il,jk))/dt
    end do
  end do
  !
end subroutine vadv
