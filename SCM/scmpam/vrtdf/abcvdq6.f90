subroutine abcvdq6 (a,b,c,cl, ef ,cdvlh,grav, &
                          il1,il2,ilg,ilev,lev,levs, &
                          rgas,rkq,shtj,shj,dshj, &
                          thl,tf,todt)

  !     * jul 15/88 - m.lazare : reverse order of local sigma arrays.
  !     * mar 14/88 - r.laprise: previous hybrid version for gcm3h.

  !     * calculates the three vectors forming the tri-diagonal matrix for
  !     * the implicit vertical diffusion of moisture oh hybrid version of
  !     * model. a is the lower diagonal, b is the main diagonal
  !     * and c is the upper diagonal.
  !     * ilev = number of model levels,
  !     * levs = number of moisture levels.

  implicit none
  real :: d
  real, intent(in) :: grav
  integer :: i
  integer, intent(in) :: il1
  integer, intent(in) :: il2
  integer, intent(in) :: ilev
  integer, intent(in) :: ilg
  integer :: l
  integer, intent(in) :: lev
  integer, intent(in) :: levs
  integer :: m
  integer :: msg
  real :: ovds
  real, intent(in) :: rgas
  real, intent(in) :: todt
  real, intent(inout)   :: a   (ilg,ilev) !<
  real, intent(inout)   :: b   (ilg,ilev) !<
  real, intent(inout)   :: c   (ilg,ilev) !<
  real, intent(in)   :: rkq (ilg,ilev) !<
  real, intent(in)   :: shtj(ilg, lev) !<
  real, intent(in)   :: shj (ilg,ilev) !<
  real, intent(in)   :: dshj(ilg,ilev) !<
  real, intent(in)   :: tf  (ilg,ilev) !<
  real, intent(in)   :: thl (ilg) !<
  real, intent(inout)   :: cl (ilg) !<
  real, intent(in)   :: ef (ilg) !<
  real, intent(inout)   :: cdvlh (ilg) !<
  !-----------------------------------------------------------------------
  msg=ilev-levs
  l=msg+1
  m=l+1

  do i=il1,il2
    ovds   = (grav*shtj(i,m)/rgas)**2 &
                  /(dshj(i,l) * (shj(i,m)-shj(i,l)) )
    c(i,l) = ovds*rkq(i,m)*(1./tf(i,m))**2
  end do ! loop 50

  do i=il1,il2
    b(i,l) = -c(i,l)
  end do ! loop 75

  do l=msg+2,ilev
    do i=il1,il2
      ovds   = (grav*shtj(i,l)/rgas)**2 &
                  /( (shj(i,l)-shj(i,l-1)) * dshj(i,l) )
      a(i,l) = ovds*(1./tf(i,l))**2*rkq(i,l)
    end do
  end do ! loop 100

  do l=msg+2,ilev-1
    do i=il1,il2
      d      = dshj(i,l+1) / dshj(i,l)
      c(i,l) = a(i,l+1)*d
      b(i,l) =-a(i,l+1)*d - a(i,l)
    end do
  end do ! loop 200

  l=ilev
  do i=il1,il2
    cl(i)=grav*shj(i,l)*ef(i)*cdvlh(i)/(rgas*thl(i)*dshj(i,l))
  end do ! loop 250

  do i=il1,il2
    b(i,l) = -a(i,l) -cl(i)
  end do ! loop 300

  !     * define matrix to invert = i-2*dt*mat(a,b,c).

  do l=msg+1,ilev-1
    do i=il1,il2
      a(i,l+1) = -todt*a(i,l+1)
      c(i,l) = -todt*c(i,l)
    end do
  end do ! loop 500

  do l=msg+1,ilev
    do i=il1,il2
      b(i,l) = 1.-todt*b(i,l)
    end do
  end do ! loop 550

  return
end
