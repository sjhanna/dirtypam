subroutine drcoef(cdm,cdh,cflux,zomin,zohin,crib,tvirtg, &
                        tvirta,va,grav,vkc,ilg,il1,il2)
  !
  implicit none

  !     * calculates drag coefficients and related variables for class.

  !     * output fields are:
  !     *    cdm    : stability-dependent drag coefficient for momentum.
  !     *    cdh    : stability-dependent drag coefficient for heat.
  !     *    cflux  : cd * mod(v), bounded by free-convective limit.
  !     * input fields are:
  !     *    zomin/: roughness heights for momentum/heat normalized by
  !     *    zohin   reference height.
  !     *    crib   : -rgas*slthkef/(va**2), where
  !     *             slthkef=-log(max(sgj(ilev),shj(ilev)))
  !     *    tvirtg : "SURFACE" virtual temperature.
  !     *    tvirta : lowest level virtual temperature.
  !     *    va     : amplitude of lowest level wind.
  !     * other variables are:
  !     *    zom/  : work arrays used for scaling zomin/zohin
  !     *    zoh     on stable side, as part of calculation.
  !     *    rib    : bulk richardson number.
  !     *    ustar  : friction velocity scale

  !     * integer :: constants.

  integer, intent(in) :: ilg !<
  integer, intent(in) :: il1 !<
  integer, intent(in) :: il2 !<
  integer :: jl !<
  integer :: i !<

  !     * real :: constants

  real, intent(in) :: grav !<
  real, intent(in) :: vkc !<

  !     * output arrays.

  real, intent(inout) :: cdm    (ilg) !<
  real, intent(inout) :: cdh    (ilg) !<
  real :: rib    (ilg) !<
  real, intent(inout) :: cflux  (ilg) !<
  real, dimension(ilg) :: ustar !<

  !     * input arrays.

  real, intent(in) :: zomin  (ilg) !<
  real, intent(in) :: zohin  (ilg) !<
  real, intent(in) :: crib   (ilg) !<
  real, intent(in) :: tvirtg (ilg) !<
  real, intent(in) :: tvirta (ilg) !<
  real, intent(in) :: va     (ilg) !<

  !     * work arrays.

  real :: zom    (ilg) !<
  real :: zoh    (ilg) !<

  !     * temporary variables.

  real :: aa !<
  real :: aa1 !<
  real :: beta !<
  real :: pr !<
  real :: zlev !<
  real :: zs !<
  real :: zoln !<
  real :: zmln !<
  real :: cpr !<
  real :: zi !<
  real :: olsf !<
  real :: olfact !<
  real :: zl !<
  real :: zmol !<
  real :: zhol !<
  real :: xm !<
  real :: xh !<
  real :: bh1 !<
  real :: bh2 !<
  real :: bh !<
  real :: wb !<
  real :: wstar !<
  real :: rib0 !<
  real :: wspeed !<
  real :: au1 !<
  real :: ols !<
  real :: psim1 !<
  real :: psim0 !<
  real :: psih1 !<
  real :: psih0 !<
  real :: tstar !<
  real :: wts !<
  real :: as1 !<
  real :: as2 !<
  real :: as3 !<
  real :: climit !<

  !-------------------------------------------------------------
  aa=9.5285714
  aa1=14.285714
  beta=1.2
  pr = 1.
  !
  do i=il1,il2
    rib(i)=crib(i)*(tvirtg(i)-tvirta(i))
    if (rib(i) >= 0. ) then
      zlev=-crib(i)*tvirta(i)*(va(i)**2)/grav
      zs=max(10.,5.*max(zomin(i)*zlev, zohin(i)*zlev))
      zs=zlev*(1.+rib(i))/(1.+(zlev/zs)*rib(i))
      zom(i)=zomin(i)*zlev/zs
      zoh(i)=zohin(i)*zlev/zs
      rib(i)=rib(i)*zs/zlev
    else
      zom(i)=zomin(i)
      zoh(i)=zohin(i)
    end if
    zoln=log(zoh(i))
    zmln=log(zom(i))
    if (rib(i) < 0. ) then
      cpr=max(zoln/zmln,0.74)
      cpr=min(cpr,1.0)
      zi=1000.0
      olsf=beta**3*zi*vkc**2/zmln**3
      olfact=1.7*(log(1.+zom(i)/zoh(i)))**0.5+0.9
      olsf=olsf*olfact
      zl = -crib(i)*tvirta(i)*(va(i)**2)/grav
      zmol=zom(i)*zl/olsf
      zhol=zoh(i)*zl/olsf
      xm=(1.00-15.0*zmol)**(0.250)
      xh=(1.00-9.0*zhol)**0.25
      bh1=-log(-2.41*zmol)+log(((1.+xm)/2.)**2*(1.+xm**2)/2.)
      bh1=bh1-2.*atan(xm)+atan(1.)*2.
      bh1=bh1**1.5
      bh2=-log(-0.25*zhol)+2.*log(((1.00+xh**2)/2.00))
      bh=vkc**3.*beta**1.5/(bh1*(bh2)**1.5)
      wb=sqrt(grav*(tvirtg(i)-tvirta(i))*zi/tvirtg(i))
      wstar=bh**(0.333333)*wb
      rib0=rib(i)

      wspeed=sqrt(va(i)**2+(beta*wstar)**2)
      rib(i)=rib0*va(i)**2/wspeed**2
      au1=1.+5.0*(zoln-zmln)*rib(i)*(zoh(i)/zom(i))**0.25
      ols=-rib(i)*zmln**2/(cpr*zoln)*(1.0+au1/ &
                 (1.0-rib(i)/(zom(i)*zoh(i))**0.25))
      psim1=log(((1.00+(1.00-15.0*ols)**0.250)/2.00)**2* &
                  (1.0+(1.00-15.0*ols)**0.5)/2.0)-2.0*atan( &
                  (1.00-15.0*ols)**0.250)+atan(1.00)*2.00
      psim0=log(((1.00+(1.00-15.0*ols*zom(i))**0.250)/2.00)**2 &
                  *(1.0+(1.00-15.0*ols*zom(i))**0.5)/2.0)-2.0* &
                  atan((1.00-15.0*ols*zom(i))**0.250)+atan(1.00)*2.0
      psih1=log(((1.00+(1.00-9.0*ols)**0.50)/2.00)**2)
      psih0=log(((1.00+(1.00-9.0*ols*zoh(i))**0.50)/2.00)**2)

      ustar(i)=vkc/(-zmln-psim1+psim0)
      tstar=vkc/(-zoln-psih1+psih0)
      cdh(i)=ustar(i)*tstar/pr
      wts=cdh(i)*wspeed*(tvirtg(i)-tvirta(i))
      wstar=(grav*zi/tvirtg(i)*wts)**(0.333333)

      wspeed=sqrt(va(i)**2+(beta*wstar)**2)
      rib(i)=rib0*va(i)**2/wspeed**2
      au1=1.+5.0*(zoln-zmln)*rib(i)*(zoh(i)/zom(i))**0.25
      ols=-rib(i)*zmln**2/(cpr*zoln)*(1.0+au1/ &
                 (1.0-rib(i)/(zom(i)*zoh(i))**0.25))
      psim1=log(((1.00+(1.00-15.0*ols)**0.250)/2.00)**2* &
                  (1.0+(1.00-15.0*ols)**0.5)/2.0)-2.0*atan( &
                  (1.00-15.0*ols)**0.250)+atan(1.00)*2.00
      psim0=log(((1.00+(1.00-15.0*ols*zom(i))**0.250)/2.00)**2 &
                  *(1.0+(1.00-15.0*ols*zom(i))**0.5)/2.0)-2.0* &
                  atan((1.00-15.0*ols*zom(i))**0.250)+atan(1.00)*2.0
      psih1=log(((1.00+(1.00-9.0*ols)**0.50)/2.00)**2)
      psih0=log(((1.00+(1.00-9.0*ols*zoh(i))**0.50)/2.00)**2)

    else

      wspeed=va(i)
      as1=10.0*zmln*(zom(i)-1.0)
      as2=5.00/(2.0-8.53*rib(i)*exp(-3.35*rib(i))+0.05*rib(i)**2)
      !< <<
      as2=as2*pr*sqrt(-zmln)/2.
      as3=27./(8.*pr*pr)
      !> >>
      ols=rib(i)*(zmln**2+as3*as1*(rib(i)**2+as2*rib(i))) &
                /(as1*rib(i)-pr*zoln)
      psim1=-0.667*(ols-aa1)*exp(-0.35*ols)-aa-ols
      psim0=-0.667*(ols*zom(i)-aa1)*exp(-0.35*ols*zom(i)) &
                  -aa-ols*zom(i)
      psih1=-(1.0+2.0*ols/3.0)**1.5-0.667*(ols-aa1) &
                  *exp(-0.35*ols)-aa+1.0
      psih0=-(1.0+2.0*ols*zoh(i)/3.0)**1.5-0.667*(ols*zoh(i)-aa1) &
                  *exp(-0.35*ols*zoh(i))-aa+1.0

    end if

    ustar(i)=vkc/(-zmln-psim1+psim0)

    tstar=vkc/(-zoln-psih1+psih0)

    cdm(i)=ustar(i)**2.0
    cdh(i)=ustar(i)*tstar/pr
    !
    !         * calculate cd*mod(v) under free-convective limit.
    !
    if (tvirtg(i) > tvirta(i) ) then
      climit=1.9e-3*(tvirtg(i)-tvirta(i))**0.333333
    else
      climit=0.
    end if
    cflux(i)=max(cdh(i)*wspeed,climit)
  end do
  !
end subroutine drcoef
