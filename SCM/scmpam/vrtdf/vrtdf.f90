subroutine vrtdf(dxdt,xrow,dtdt,throw,dqdt,qrow,dqldt,qlwc, &
                       dudt,u,v,wsub,rkh,rkm,ri,cdm,ustar,zspd, &
                       zspda,zf,pf,tf,zh,ph,dp,almc,almx,pblt,tvp, &
                       qsens,qlat,zclf,tzero,thliqg,zo,shtj,shj,dshj, &
                       pressg,dt,ztop,il1,il2,ilg,ilev,ilevp1, &
                       msgp,ntrac)
  !
  use sdphys
  !
  implicit none
  !
  real :: ald
  real :: alfah
  real :: all
  real :: alu
  real :: alwc
  real :: atmp
  real :: beem
  real :: cevap
  real :: dvmins
  real :: dz
  real :: elh
  real :: elm
  real :: epssh
  real :: epssm
  real :: esw
  real :: evbeta
  real :: fac
  real :: facmom
  real :: fact
  real :: fact0
  real :: facte
  real :: facth
  real :: factm
  real :: facts
  real :: gamrh
  real :: gamrm
  real :: heat
  integer :: i
  integer :: i2ndie
  integer :: icall
  integer :: icvsg
  integer :: ievap
  integer :: il
  integer :: isubg
  integer :: jk
  integer :: l
  integer :: lpbl
  integer :: n
  real :: pblx
  real :: pi
  real :: prandtl
  real :: prandtl_max
  real :: prandtl_min
  real :: q0sat
  real :: ratfca
  real :: ratfca1
  real :: riinf
  real :: rineut
  real :: rit
  real :: sari
  real :: tauadj
  real :: taur
  real :: tfrez
  real :: thetvm
  real :: thetvp
  real :: thfc
  real :: thlmin
  real :: utmp
  real :: vmin
  real :: vtmp
  real :: wzero
  real :: xh
  real :: ximin
  real :: ximint
  real :: xinglh
  real :: xinglm
  real :: xlmin
  real :: xm
  real :: zannom
  real :: zrho0
  real :: zrsld
  real :: zruf
  real, intent(in) :: ztop
  !
  integer, intent(in) :: ilg !<
  integer, intent(in) :: ilev !<
  integer, intent(in) :: ilevp1 !<
  integer, intent(in) :: il1 !<
  integer, intent(in) :: il2 !<
  integer, intent(in) :: msgp !<
  integer, intent(in) :: ntrac !<
  real, intent(out), dimension(ilg) :: pblt !<
  real, intent(out), dimension(ilg) :: cdm !<
  real, intent(out), dimension(ilg) :: zspd !<
  real, intent(out), dimension(ilg) :: zspda !<
  real, intent(out), dimension(ilg) :: qsens !<
  real, intent(out), dimension(ilg) :: qlat !<
  real, intent(inout), dimension(ilg) :: ustar !<
  real, intent(inout), dimension(ilg) :: zo !<
  real, intent(out), dimension(ilg,ilev) :: almc !<
  real, intent(out), dimension(ilg,ilev) :: almx !<
  real, intent(out), dimension(ilg,ilev) :: rkh !<
  real, intent(out), dimension(ilg,ilev) :: rkm !<
  real, intent(out), dimension(ilg,ilev) :: ri !<
  real, intent(out), dimension(ilg,ilev) :: wsub !<
  real, intent(out), dimension(ilg,ilev) :: tvp !<
  real, intent(inout), dimension(ilg,ilev) :: zclf !<
  real, intent(in) :: dt !<
  real, intent(in), dimension(ilg) :: pressg !<
  real, intent(in), dimension(ilg) :: tzero !<
  real, intent(in), dimension(ilg) :: thliqg !<
  real, intent(in), dimension(ilg,ilev) :: shj !<
  real, intent(in), dimension(ilg,ilev) :: dshj !<
  real, intent(in), dimension(ilg,ilev) :: tf !<
  real, intent(in), dimension(ilg,ilev) :: zh !<
  real, intent(in), dimension(ilg,ilev) :: ph !<
  real, intent(in), dimension(ilg,ilev) :: zf !<
  real, intent(in), dimension(ilg,ilev) :: pf !<
  real, intent(in), dimension(ilg,ilev) :: dp !<
  real, intent(in), dimension(ilg,ilevp1) :: shtj !<
  real, dimension(ilg,ilevp1), intent(inout) :: throw !<
  real, dimension(ilg,ilevp1), intent(inout) :: qrow !<
  real, dimension(ilg,ilev), intent(in) :: qlwc !<
  real, dimension(ilg,ilev), intent(inout) :: u !<
  real, dimension(ilg,ilev), intent(inout) :: v !<
  real, dimension(ilg,ilev,ntrac) :: rkx !<
  real, dimension(ilg,ilevp1,ntrac), intent(in) :: xrow !<
  real, dimension(ilg,ilevp1,ntrac), intent(out) :: dxdt !<
  real, dimension(ilg,ilevp1), intent(out) :: dqdt !<
  real, dimension(ilg,ilevp1), intent(out) :: dtdt !<
  real, dimension(ilg,ilev), intent(out) :: dqldt !<
  real, dimension(ilg,ilev), intent(out) :: dudt !<
  !
  real, dimension(ilg,ilev) :: a !<
  real, dimension(ilg,ilev) :: b !<
  real, dimension(ilg,ilev) :: c !<
  real, dimension(ilg,ilev) :: work !<
  real, dimension(ilg,ilev) :: xrowt !<
  real, dimension(ilg,ilev) :: dxdtt !<
  real, dimension(ilg,ilev) :: rkxt !<
  real, dimension(ilg,ilev) :: qt !<
  real, dimension(ilg,ilev) :: hmn !<
  real, dimension(ilg) :: un !<
  real, dimension(ilg) :: zer !<
  real, dimension(ilg) :: cl !<
  real, dimension(ilg) :: cdvlt !<
  real, dimension(ilg) :: raus !<
  real, dimension(ilg) :: vint !<
  real, dimension(ilg) :: tht !<
  real, dimension(ilg,ilev) :: cvar !<
  real, dimension(ilg,ilev) :: zcraut !<
  real, dimension(ilg,ilev) :: qcwvar !<
  real, dimension(ilg,ilev) :: cvsg !<
  real, dimension(ilg,ilev) :: cvdu !<
  real, dimension(ilg,ilev) :: zfrac !<
  real, dimension(ilg,ilev) :: shxkj !<
  real, dimension(ilg,ilev) :: dvds !<
  real, dimension(ilg,ilev) :: dttds !<
  real, dimension(ilg,ilev) :: almxt !<
  real, dimension(ilg,ilevp1) :: shtxkj !<
  real, dimension(ilg) :: pmbs !<
  real, dimension(ilg) :: rrl !<
  real, dimension(ilg) :: cpm !<
  real, dimension(ilg) :: almix !<
  real, dimension(ilg) :: dhldz !<
  real, dimension(ilg) :: drwdz !<
  real, dimension(ilg) :: flagest !<
  real, dimension(ilg) :: qcw !<
  real, dimension(ilg) :: ssh !<
  real, dimension(ilg) :: vmodl !<
  real, dimension(ilg) :: cdvlm !<
  real, dimension(ilg) :: dvdz !<
  real, dimension(ilg) :: cdh !<
  real, dimension(ilg) :: zosclm !<
  real, dimension(ilg) :: zosclh !<
  real, dimension(ilg) :: tvirts !<
  real, dimension(ilg) :: tvirta !<
  real, dimension(ilg) :: crib !<
  real, dimension(ilg) :: cflux !<
  real, dimension(ilg) :: evap !<
  real, dimension(ilg) :: qzero !<
  real, dimension(ilg) :: pblti !<
  real, dimension(ilg) :: qtg !<
  real, dimension(ilg) :: hmng !<
  integer, dimension(ilg) :: ipbl !<
  real, parameter :: vkc=0.41 !<
  real, parameter :: tvfa=0.608 !<
  real, parameter :: scalf=0.8 !<

  !
  !-----------------------------------------------------------------------
  !     * surface properties for bare ground or water.
  !
  tfrez=273.16
  do i=il1,il2
    zspd(i)=sqrt(u(i,ilev)**2+v(i,ilev)**2) ! surface wind speed
    esw=1.e+02*exp(rw1+rw2/tzero(i))*(tzero(i)**rw3)
    wzero=eps1*esw/(pressg(i)-esw)
    q0sat=wzero
    !         if ( tzero(i) >= tfrez) then
    !           ac=17.269
    !           bc=35.86
    !         else
    !           ac=21.874
    !           bc=7.66
    !         end if
    !         wzero=eps1*611.0*exp(ac*(tzero(i)-tfrez)/
    !     1              (tzero(i)-bc))/pressg(i)
    !
    !         q0sat=wzero/(1.0+wzero)     ! saturated specific humidity
    if (thliqg(i)> 0.99) then  ! water surface
      evbeta=1.0
      qzero(i)=q0sat
      zo(i)=(0.016/grav)*ustar(i)**2
    else                        ! bare soil
      thlmin=0.1                ! mimimum soil water content (m3/m3)
      thfc=0.4                  ! field capacity (m3/m3)
      if (thliqg(i) < (thlmin+0.001) ) then
        ievap=0
        cevap=0.0
      else if (thliqg(i) > thfc) then
        ievap=1
        cevap=1.0
      else
        ievap=1
        cevap=0.25*(1.0-cos(3.14159*thliqg(i)/thfc))**2
      end if
      evbeta=cevap              ! surface evaporation efficiency
      qzero(i)=evbeta*q0sat+(1.0-evbeta)*qrow(i,ilevp1)
      if (qzero(i) > qrow(i,ilevp1) .and. ievap == 0) then
        evbeta=0.0
        qzero(i)=qrow(i,ilevp1)
      end if
    end if
    !         tvirts(i)=tzero(i)*(1.0+0.61*qzero(i))
    tvirts(i)=tzero(i)*(1.+tvfa*qzero(i)/(1.-qzero(i)))
  end do
  do i=il1,il2
    zrsld=zh(i,ilevp1)+zo(i)
    zosclm(i)=zo(i)/zrsld
    zosclh(i)=zosclm(i)
    !        tvirta(i)=throw(i,ilevp1)*(1.0+0.61*qrow(i,ilevp1))
    tvirta(i)=throw(i,ilevp1)*(1.+tvfa*qrow(i,ilevp1) &
                                    /(1.-qrow(i,ilevp1)))
    crib(i)=-grav*zrsld/(tvirta(i)*zspd(i)**2)
  end do
  !
  !     * surface drag coefficients.
  !
  call drcoef(cdm,cdh,cflux,zosclm,zosclh,crib,tvirts, &
                  tvirta,zspd,grav,vkc,ilg,il1,il2)
  ustar=zspd*sqrt(cdm)
  !
  !     * surface latent and sensible heat fluxes, and evaporation.
  !
  do i=il1,il2
    zrho0=ph(i,ilevp1)/(rgas*throw(i,ilevp1))
    !! !!
    qsens(i)=zrho0*cpres*cflux(i)*(tzero(i)-throw(i,ilevp1))
    evap(i)=zrho0*cflux(i)*(qzero(i)-qrow(i,ilevp1))
    !
    !        qsens(i)=0.
    !        evap(i)=0./rl
    !
    qlat(i)=rl*evap(i)
  end do
  !
  !     * anemometer wind speed (m/sec).
  !
  do i=il1,il2
    zruf=zo(i)
    zannom=max(zruf,10.0)
    ratfca=log(zannom/zruf)/vkc
    ratfca1=ratfca*sqrt(cdm(i))
    ratfca1=min(ratfca1,1.)
    zspda(i)=ratfca1*zspd(i)
  end do
  !
  !     * insert fluxes into first atmospheric level above ground.
  !
  dtdt=0.
  dqdt=0.
  do i=il1,il2
    zrho0=ph(i,ilevp1)/(rgas*throw(i,ilevp1))
    fac=grav/dp(i,ilev)
    !        throw(i,ilevp1)=throw(i,ilevp1)+dt*fac*qsens(i)/cpres
    !        qrow(i,ilevp1)=qrow(i,ilevp1)+dt*fac*evap(i)
    dtdt(i,ilevp1)=fac*qsens(i)/cpres
    dqdt(i,ilevp1)=fac*evap(i)
    !! !!
    !        du=dt*fac*zrho0*cdm(i)*max(vmin,zspd(il))*u(i,ilev)
    !        u(i,ilev)=u(i,ilev)-du
    !        if ( u(i,ilev) >= 0. ) then
    !          u(i,ilev)=max(u(i,ilev),0.)
    !        else
    !          u(i,ilev)=min(u(i,ilev),0.)
    !        end if
  end do
  !
  !     * parameters for calculation of vertical diffusion.
  !
  vmin=0.001
  do il=il1,il2
    un   (il)=1.
    zer  (il)=0.
    raus (il)=pressg(il)/grav
    cdvlt(il)=0.
    vmodl(il)=max(vmin,zspd(il))
  end do
  shxkj=shj**rgocp
  shtxkj=shtj**rgocp
  tauadj=dt
  do i=il1,il2
    cdvlm(i)=cdm(i)*vmodl(i)
    heat=tauadj*grav*shxkj(i,ilev)/(rgas*throw(i,ilev))
    facmom=1./(1.+heat*cdvlm(i)/dshj(i,ilev))
    !
    !         cdvlm(i)=cdvlt(i)
    !         facmom=1.
    !
    utmp =u(i,ilev)*facmom
    vtmp =v(i,ilev)*facmom
    dvds(i,ilev) = sqrt((utmp-u(i,ilev-1))**2 &
                        +(vtmp-v(i,ilev-1))**2)/(shj(i,ilev)-shj(i,ilev-1))
  end do
  !
  !     * evaluate dttds=d(theta)/d(sigma) at top interface of layers.
  !
  l=1
  do i=il1,il2
    alwc=qlwc(i,l)
    thetvp=throw(i,l)*(1.+tvfa*qrow(i,l)/(1.-qrow(i,l)) &
               -(1.+tvfa)*alwc)/shxkj(i,l)
    tvp(i,l)=thetvp
  end do
  do l=2,ilev
    do i=il1,il2
      alwc=qlwc(i,l)
      thetvp=throw(i,l)*(1.+tvfa*qrow(i,l)/(1.-qrow(i,l)) &
               -(1.+tvfa)*alwc)/shxkj(i,l)
      alwc=qlwc(i,l-1)
      thetvm=throw(i,l-1)*(1.+tvfa*qrow(i,l-1) &
               /(1.-qrow(i,l))-(1.+tvfa)*alwc)/shxkj(i,l-1)
      !! !!
      !         thetvp=throw(i,l)/shxkj(i,l)
      !         thetvm=throw(i,l-1)/shxkj(i,l-1)
      dttds(i,l)=(thetvp-thetvm)/(shj(i,l)-shj(i,l-1))
      tvp(i,l)=thetvp
    end do
  end do
  !
  !     * dvds = mod(dv/dsigma) at top interface of layers.
  !
  do l=1,ilev-2
    do i=il1,il2
      dvds(i,l+1)=sqrt((u(i,l+1)-u(i,l))**2 &
                         +(v(i,l+1)-v(i,l))**2)/(shj(i,l+1)-shj(i,l))
    end do
  end do
  do i=il1,il2
    pblti(i)=real(ilev)
    !         ipbl(i)=1
  end do
  !
  !     * rms wind shear, dvmins=(r*t/g)*(dv/dz)=7.9 for dv/dz=1.ms-1km-1.
  !     * determine planetary boundary layer top as level index above
  !     * which the richardson number exceeds the critical value of 1.00.
  !
  dvmins=7.9
  !      taur=3600.
  taur=6.*3600.
  do l=ilev,2,-1
    do i=il1,il2
      dvds(i,l)=max(dvds(i,l),dvmins)
      rit=-rgas*shtxkj(i,l)*dttds(i,l)/(shtj(i,l)*dvds(i,l)**2)
      !! !!
      ri(i,l)=(ri(i,l)+(dt/taur)*rit)/(1.+dt/taur)
      !         ri(i,l)=rit
    end do
  end do
  do l=ilev,3,-1
    do i=il1,il2
      !! !!
      !         if ( ri(i,l) < 1. .and. ri(i,l-1) > 1. ) then
      if (ri(i,l) < 10. .and. ri(i,l-1) > 10. ) then
        pblx=real(l)
        pblti(i) = min(pblti(i), pblx)
      end if
    end do
  end do
  !! !!
  pblt=pblti
  !      pblt=max((pblt+(dt/taur)*pblti)/(1.+dt/taur),2.)
  !
  fact=vkc*rgas*273.
  xlmin=10.
  beem=10.
  gamrh=6.
  gamrm=6.
  alfah=1.
  rineut=1.
  riinf=0.25
  prandtl_min=1.
  prandtl_max=3.
  wsub=0.
  do l=2,ilev
    !
    !        * calculate the diffusion coefficients (rkh,rkm)
    !        * at the top interface of layers.
    !        * heat: finite stability cutoff.
    !        * low xingl used since previously mixed "INSTANTANEOUSLY".
    !        * momentum: finite stability cutoff.
    !        * high xingl used since not previously mixed "INSTANTANEOUSLY".
    !        * unstable case (ri <= 0.) - stable case (ri > 0.) .
    !
    do i=il1,il2
      dvdz(i)=dvds(i,l)*grav*shtj(i,l)/(rgas*tf(i,l-1))
    end do
    !
    do i=il1,il2
      fact0=fact*log(shtj(i,l))
      sari=sqrt(abs(ri(i,l)))
      lpbl=nint(pblt(i))
      !
      !           * lower cutoffs.
      !
      all=0.5*vkc*zf(i,lpbl-1)
      ximint=max(75.*all/(75.+all),xlmin)
      all=0.5*vkc*zh(i,l)
      ximin =max(75.*all/(75.+all),xlmin)
      !
      !           * effective mixing lengths as function of mixing
      !           * lengths for up- and downward mixing.
      !
      alu=vkc*zh(i,l)
      ald=vkc*(zf(i,lpbl-1)-zh(i,l))
      if (ald > 0. ) then
        xinglh=max(alu*ald/(alu+ald),ximin)
        xinglm=max(alu*ald/(alu+ald),ximin)
        almc(i,l)=xinglh
      else
        fac=(ph(i,l)/pf(i,lpbl-1))**2.
        xinglh=10.+(ximint-10.)*fac
        xinglm=10.+(ximint-10.)*fac
        almc(i,l)=min(100.*fac,vkc*(zh(i,l)-zf(i,lpbl-1)))
      end if
      almx(i,l)=xinglh
      almxt(i,l)=max(almx(i,l),almc(i,l),10.)
      xinglh=(1.-zclf(i,l))*xinglh+zclf(i,l)*almxt(i,l)
      xinglm=xinglh
      !
      !           * diffusion coefficients.
      !
      elh   = xinglh**2
      epssh = 0.
      facth = 0.5*ri(i,l)*epssh*beem
      if (facth > 1. ) then
        xh  = 0.
      else
        xh  = (1.-facth)**2
      end if
      elm   = xinglm**2
      epssm = 0.
      factm = 0.5*ri(i,l)*epssm*beem
      if (factm > 1. ) then
        xm  = 0.
      else
        xm  = (1.-factm)**2
      end if
      if (ri(i,l) < 0. ) then
        rkh(i,l) = elh*dvdz(i)*(1.-alfah*gamrh*beem*ri(i,l) / &
                         (gamrh+alfah*beem*sari))
        rkm(i,l) = elm*dvdz(i)*(1.-      gamrm*beem*ri(i,l) / &
                         (gamrm+      beem*sari))
      else
        rkh(i,l) = elh*dvdz(i)*xh/(1.+(1.-epssh)*beem*ri(i,l))
        rkm(i,l) = elm*dvdz(i)*xm/(1.+(1.-epssm)*beem*ri(i,l))
      end if
      !
      !           * prandtl number scaling for eddy momentum diffusivity,
      !           * based on approach suggested by schumann and gerz (1995).
      !
      atmp=0.
      if (ri(i,l) > 0. ) then
        atmp=rineut*exp(-ri(i,l)/(rineut*riinf))
      end if
      prandtl=atmp+ri(i,l)/riinf
      prandtl=min(max(prandtl,prandtl_min),prandtl_max)
      rkm(i,l)=rkm(i,l)*prandtl
      !
      !           * no mixing above specified height.
      !
      if (zh(i,l) > ztop) then
        !               rkh(i,l)=0.
        !               rkm(i,l)=0.
      end if
    end do
  end do
  do i=il1,il2
    rkm(i,1)=rkm(i,2)
    rkh(i,1)=rkh(i,2)
  end do
  do n=1,ntrac
    rkx(:,:,n)=rkh
  end do
  !
  !     * diagnose subgrid-scale component of the vertical velocity
  !     * (standard deviation, ghan et al., 1997). according to peng
  !     * et al. (2005), the representative vertical velocity for
  !     * aerosol activation is obtained by applying a scaling factor
  !     * to the velocity standard deviation (scalf).
  !
  pi=3.141592653589793
  facts=scalf*sqrt(2.*pi)
  do l=2,ilev
    wsub(:,l)=facts*rkh(:,l)/(zf(:,l-1)-zf(:,l))
  end do
  !! !!
  !      wsub=10. &
  ! fli ht 08
  !      wsub=0.05 &
  ! fli ht 07, low
  !      wsub=0.1 &
  ! fli ht 07, high
  !      wsub=0.2
  !
  !      wsub=0.02
  !
  !     * mix chemical tracers.
  !
  do n=1,ntrac
    rkxt=rkx(:,:,n)
    tht=throw(:,ilevp1)
    call abcvdq6 (a,b,c,cl,un,cdvlt,grav, &
                      il1,il2,ilg,ilev,ilevp1,ilev, &
                      rgas,rkxt,shtj,shj,dshj, &
                      tht,tf,dt)
    xrowt=xrow(:,msgp:ilevp1,n)
    call implvd7(a,b,c,cl,xrowt,zer,il1,il2,ilg,ilev, &
                     dt,dxdtt,dshj,raus,work,vint)
    dxdt(:,msgp:ilevp1,n)=dxdtt(:,:)
  end do
  !
  !     * mix momentum.
  !
  tht=throw(:,ilevp1)
  call abcvdq6 (a,b,c,cl,un,cdvlm,grav, &
                    il1,il2,ilg,ilev,ilevp1,ilev, &
                    rgas,rkm,shtj,shj,dshj, &
                    tht,tf,dt)
  call implvd7(a,b,c,cl,u,zer,il1,il2,ilg,ilev, &
                   dt,dudt,dshj,raus,work,vint)
  !
  !     * total water mixing ratio and cloud water static energy.
  !
  qt=qrow(:,msgp:ilevp1)+qlwc
  hmn=cpres*throw(:,msgp:ilevp1)+grav*zh-rl*qlwc
  !
  !     * liquid water static energy and total water at surface.
  !
  l=ilev
  do il=il1,il2
    qtg(il)=qzero(il)
    hmng(il)=cpres*tzero(il)
  end do
  !
  !     * mix cloud water static energy and total water.
  !
  tht=throw(:,ilevp1)
  call abcvdq6 (a,b,c,cl,un,cdvlt,grav, &
                    il1,il2,ilg,ilev,ilevp1,ilev, &
                    rgas,rkh,shtj,shj,dshj, &
                    tht,tf,dt)
  call implvd7(a,b,c,cl,hmn,hmng,il1,il2,ilg,ilev, &
                   dt,dxdtt,dshj,raus,work,vint)
  hmn=hmn+dt*dxdtt
  call implvd7(a,b,c,cl,qt,qtg,il1,il2,ilg,ilev, &
                   dt,dxdtt,dshj,raus,work,vint)
  qt=qt+dt*dxdtt
  !
  !     * check conservation.
  !
  !      dqt=0.
  !      dq=0.
  !      do l=1,ilev
  !      do i=1,1
  !        dqt=dqt+dp(i,l)*dxdtt(i,l)/grav
  !        dq=dq+dp(i,l)*qt(i,l)/grav
  !      end do
  !      end do
  !      PRINT*,'DQ,DQT=',DQ,DQT
  !
  !     * temperature and water vapour mixing ratio from statistical
  !     * cloud scheme.
  !
  facte=1./eps1-1.
  i2ndie=1
  flagest=1.
  cvsg=0.
  cvdu=0.
  zfrac=0.
  icvsg=1
  isubg=0
  icall=1
  do jk=1,ilev
    do il=1,ilg
      pmbs(il)=0.01*ph(il,jk)
      rrl(il)=rl
      cpm(il)=cpres
      almix(il)=max(almc(il,jk),almx(il,jk),10.)
      if (jk>1) then
        dz       =zh  (il,jk-1)-zh (il,jk)
        dhldz(il)=(hmn(il,jk-1)-hmn(il,jk))/dz
        drwdz(il)=(qt (il,jk-1)-qt (il,jk))/dz
      else
        dhldz(il)=0.
        drwdz(il)=0.
      end if
    end do
    call statcld (qcw,zclf(1,jk),cvar(1,jk),zcraut(1,jk), &
                      qcwvar(1,jk),ssh,cvsg(1,jk),qt(1,jk), &
                      hmn(1,jk),zfrac(1,jk),cpm, &
                      pmbs,zh(1,jk),rrl,cvdu(1,jk),flagest, &
                      almix,dhldz,drwdz,i2ndie, &
                      dt,ilev,ilg,1,ilg,icvsg,isubg, &
                      jk,icall)
    !
    do il=1,ilg
      dtdt(il,jk)=dtdt(il,jk)+ &
                     ((hmn(il,jk)-grav*zh(il,jk)+rrl(il)*qcw(il)) &
                       /cpm(il)-throw(il,jk))/dt
      dqdt(il,jk)=dqdt(il,jk)+ &
                     ((qt(il,jk)-qcw(il))-qrow(il,jk))/dt
      dqldt(il,jk)=(qcw(il)-qlwc(il,jk))/dt
    end do
  end do

  !      dqt=0.
  !      dq=0.
  !      do jk=1,ilev
  !      do il=1,1
  !        dqt=dqt+(qt(il,jk)-qrow(il,jk)-qlwc(il,jk))*dp(il,jk)/grav
  !      end do
  !      end do
  !      PRINT*,'DQT=',DQT
  !! !!
  !        qrow(:,ilevp1)=qzero(:)
  !        throw(:,ilevp1)=tzero(:)
  !
end subroutine vrtdf
