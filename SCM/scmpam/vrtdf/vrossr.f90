subroutine vrossr(p, a,b,c,d,delta,ilg,il1,il2,m)

  !     * ccrn mars 18/85 - b.dugas. (vectoriser...)
  !     * programmer  andrew staniforth, rpn.
  !     * solve the tri-diagonal matrix problem below.

  !  ** **                                      ****  ****     ***    **** &
  !  ** **                                      ****  ****     ***    ****
  !  ** b(1),c(1), 0  , 0  , 0  , - - - -  ,  0   **  **  p(1)  **    **
  !  ** a(2),b(2),c(2), 0  , 0  , - - - -  ,  0   **  **  p(2)  **    **
  !  **  0  ,a(3),b(3),c(3), 0  , - - - -  ,  0   **  **  p(3)  **    **
  !  **  0  , 0  ,a(4),b(4),c(4), - - - -  ,  0   **  **  p(4)  **    **
  !  **  0  , 0  , 0  ,a(5),b(5), - - - -  ,  0   **  **  p(5)  ** -- **
  !  **  -                                    -   **  **    -   ** -- **
  !  **  -                                    -   **  **    -   **    **
  !  **  0  , - - , 0 ,a(m-2),b(m-2),c(m-2),  0   **  ** p(m-2) **    ** d
  !  **  0  , - - , 0 ,  0   ,a(m-1),b(m-1),c(m-1)**  ** p(m-1) **    ** d
  !  **  0  , - - , 0 ,  0   ,  0   , a(m) , b(m) **  **  p(m)  **    ** &
  !  ** *                                       ****  ****    ****    **** &
  !  ** *                                       ****  ****    ****    ****


  !     * the routine solves il2-il1+1 of these problems simultaniously,
  !     * that is, it does one latitude circle at a time.
  !     * delta is a working array of dimension ilg x m.

  implicit none
  integer :: i
  integer, intent(in) :: il1
  integer, intent(in) :: il2
  integer, intent(in) :: ilg
  integer :: l
  integer, intent(in) :: m
  real :: x

  !      real   :: p(ilg,1),a(ilg,1),b(ilg,1),c(ilg,1),d(ilg,1)
  !      real   :: delta(ilg,1)
  real, intent(inout)   :: p(ilg,m) !<
  real, intent(in)   :: a(ilg,m) !<
  real, intent(in)   :: b(ilg,m) !<
  real, intent(inout)   :: c(ilg,m) !<
  real, intent(in)   :: d(ilg,m) !<
  real, intent(inout), dimension(ilg,m) :: delta !<
  !-----------------------------------------------------------------------
  do i=il1,il2
    c(i,m)=0.
  end do ! loop 10

  do i=il1,il2
    x         =1./b(i,1)
    p(i,1)    =-c(i,1)*x
    delta(i,1)=d(i,1)*x
  end do ! loop 30

  do l=2,m
    do i=il1,il2
      x         =1./(b(i,l)+a(i,l)*p(i,l-1))
      p(i,l)    =-c(i,l)*x
      delta(i,l)=(d(i,l)-a(i,l)*delta(i,l-1))*x
    end do
  end do ! loop 50

  do i=il1,il2
    p(i,m)=delta(i,m)
  end do ! loop 60

  do l=m-1,1,-1
    do i=il1,il2
      p(i,l)=p(i,l)*p(i,l+1)+delta(i,l)
    end do
  end do ! loop 70

  return
end
