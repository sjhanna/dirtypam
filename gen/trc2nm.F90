!> \file
!> \brief Extract PLA aerosol number and mass concentrations from basic
!>       tracer arrays in the GCM. Calculate total mass and mass fractions
!>       for internally mixed types of aerosol.
!!
!! @author S. Hanna
!
!-----------------------------------------------------------------------
subroutine trc2nm (trac,penum,pemas,pinum,pimas,pifrc,ilga,leva)
  !
  use sdparm, only : imfex,imsex,infex,infin,insex,insin, &
                         isaext,isaint,kint,ntract,sintf,ytiny
  !
  implicit none
  !
  integer, intent(in) :: ilga !< Number of grid points in the horizontal direction
  integer, intent(in) :: leva !< Number of grid points in the vertical direction
  real, intent(inout), dimension(ilga,leva,isaint) :: pimas !< Aerosol (dry) mass concentration for internally mixed
  !< aerosol \f$[kg/kg]\f$
  real, intent(inout), dimension(ilga,leva,isaint) :: pinum !< Aerosol number concentration for internally mixed
  !< aerosol \f$[1/kg]\f$
  real, intent(inout), dimension(ilga,leva,isaext) :: pemas !< Aerosol (dry) mass concentration for externally mixed
  !< aerosol \f$[kg/kg]\f$
  real, intent(inout), dimension(ilga,leva,isaext) :: penum !< Aerosol number concentration for externally mixed
  !< aerosol \f$[1/kg]\f$
  real, intent(inout), dimension(ilga,leva,isaint,kint) :: pifrc !< Aerosol (dry) mass fraction for each aerosol type
  !< for internally mixed aerosol
  real, intent(in), dimension(ilga,leva,ntract) :: trac !< Tracer array
  !
  !     internal work variables
  !
  integer :: kx !<
  integer :: imlin !<
  integer :: il !<
  integer :: l !<
  integer :: is !<
  integer :: ist !<
  real :: xrtmp !<
  !
  !-----------------------------------------------------------------------
  !
  !     * extract pla aerosol number and mass concentrations from basic
  !     * tracer arrays in the gcm.
  !
  if (isaext > 0) then
    penum=trac(:,:,insex:infex)
    pemas=trac(:,:,imsex:imfex)
  end if
  if (isaint > 0) then
    pinum=trac(:,:,insin:infin)
  end if
  !
  !-----------------------------------------------------------------------
  !     * total mass and mass fractions for internally mixed types
  !     * of aerosol.
  !
  !
  if (isaint > 0) pimas=0.
  do is=1,isaint
    do ist=1,sintf%isaer(is)%itypt
      imlin=sintf%isaer(is)%ism(ist)
      pimas(:,:,is)=pimas(:,:,is)+trac(:,:,imlin)
    end do
  end do
  do is=1,isaint
    do ist=1,sintf%isaer(is)%itypt
      kx=sintf%isaer(is)%ityp(ist)
      imlin=sintf%isaer(is)%ism(ist)
      do l=1,leva
        do il=1,ilga
          xrtmp=trac(il,l,imlin)
          if (2+abs(exponent(xrtmp) - exponent(pimas(il,l,is))) &
              < maxexponent(xrtmp) .and. pimas(il,l,is)/=0. ) then
            if (xrtmp > ytiny) then
              pifrc(il,l,is,kx)=min(max(xrtmp/pimas(il,l,is),0.),1.)
            else
              pifrc(il,l,is,kx)=0.
            end if
          else
            pifrc(il,l,is,kx)=1./real(sintf%isaer(is)%itypt)
          end if
        end do
      end do
    end do
  end do
  !
end subroutine trc2nm
