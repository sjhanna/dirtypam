!> \file
!> \brief Basic array dimensions.
!!
!! @author K. von Salzen
!
!-----------------------------------------------------------------------
module psizes
  !
  implicit none
  !
  !     * number of grid points in horizontal direction
  integer :: ilg !<
  !     * vertical levels in driving atmospheric model.
  integer :: ilev !<
  integer :: ilevp1 !<
  integer :: levs !<
  !     * array dimensions for pla aerosol calculations
  integer :: msgt !<
  integer :: msgp1 !<
  integer :: msgp2 !<
  !     * tracers
  integer, parameter :: ntrac  = 29 ! number of all tracers
  integer, parameter :: ntracp = 24 ! number of aerosol tracers
  !     * number of distinct canopy types in class
  integer, parameter :: ican = 4 !<
  !
end module psizes
