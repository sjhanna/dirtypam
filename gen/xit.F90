!> \file
!> \brief Terminates a program by printing the program name and
!>       a line across the page followed by a number N.
!!
!! @authors J.D. Henderson and E. Chan
!
!-----------------------------------------------------------------------
subroutine xit(name,n)
  !
  implicit none
  !
  integer :: i !<
  integer :: n !<
  character(len=*) :: name !<
  character(len=8) :: name8 !<
  character(len=8) :: dash !<
  character(len=8) :: star !<
  character(len=30) :: frmt !<
  !
  frmt = "('0',A8,'  END  ',A8,9A8,I8)"
  !
  data dash /'--------'/, star /'********'/
  !---------------------------------------------------------------------
  !
  name8 = name
  if (n>=0) write(6,frmt) dash,name8,(dash,i=1,9),n
  !
  if (n<0) write(6,frmt) star,name8,(star,i=1,9),n
  !
  if (n>=0 .or. n<-100) then
    stop
  else
    call abort
  end if
  !
end
!> \file
!! \subsection ssec_details Details
!!     N > 0 is for a normal end. The line is dashed.
!!     normal ends terminate with stop.
!!     N < 0 is for an abnormal end. The line is dotted.
!!     If N is less than -100 the program simply terminates.
!!     Otherwise if N is less than zero the program aborts.

