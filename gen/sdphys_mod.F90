!> \file
!> \brief Basic phyiscal constants.
!!
!! @author K. von Salzen
!
!-----------------------------------------------------------------------
module sdphys
  !
  implicit none
  !
  !     * basic physical constants.
  !
  real, parameter :: rhoh2o = 1.e+03 ! density of water (kg/m3)
  real, parameter :: wh2o   = 18.015e-03 ! molecular weight of h2o (kg/mol)
  real, parameter :: wa     = 28.97e-03 ! molecular weight of air (kg/mol)
  real, parameter :: rgasm  = 8.31441 ! molar gas constant (j/mol/k)
  real, parameter :: rgas   = 287.04 ! gas constant of dry air (j/kg/k)
  real, parameter :: rgocp  = 2./7. !<
  real, parameter :: cpres  = rgas/rgocp ! heat capacity at constant pressure
  ! for dry air (j/kg/k)
  real, parameter :: eps1   = 0.622 ! ratio of gas constant for dry
  ! air over gas constant for vapour
  real, parameter :: grav   = 9.80616 ! gravitational contstant (m/s2)
  real, parameter :: rl     = 2.501e+06 ! latent heat of vapourization (j/kg)
  !
  !     * fitting parameters for calculation of water vapour saturation
  !     * mixing ratio.
  !
  real, parameter :: rw1    = 53.67957 !<
  real, parameter :: rw2    = -6743.769 !<
  real, parameter :: rw3    = -4.8451 !<
  real, parameter :: ri1    = 23.33086 !<
  real, parameter :: ri2    = -6111.72784 !<
  real, parameter :: ri3    = 0.15215 !<
  !
  real, parameter :: ppa    = 21.656 !<
  real, parameter :: ppb    = 5418. !<
  !
  !     * parameters for particle growth calculations.
  !
  real, parameter :: alpht  = 1. ! thermal accommodation coefficient
  real, parameter :: alphc  = 1. ! water accommodation coefficient
  !
  !     * switch to determine method for saturation vapour pressure
  !     * formula.
  !
  logical, parameter :: ksatf=.true. !<
  !
  !     * switch to determine defintion of static energy.
  !
  logical, parameter :: knorm=.false. !<
  !
end module sdphys
