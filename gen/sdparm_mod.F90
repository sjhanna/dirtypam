!> \file
!> \brief Basic constants and arrays for PLA method.
!!
!! @author K. von Salzen
!
!-----------------------------------------------------------------------
module sdparm
  !
  use mbnds, only : bnds
  use fpdef, only : r8
  !
  implicit none
  save
  !
  !     * numerical and mathematical constants.
  !
  integer, parameter :: ifyes=1 !<
  integer, parameter :: ifno =0 !<
  real, parameter :: ysmall  =9.e-30 !<
  real, parameter :: ysec    =0.99 !<
  real, parameter :: ysecs   =0.001 !<
  character(len=1), parameter :: cna=' ' !<
  !
  !     * physical and chemical constants.
  !
  real, parameter :: ace     =1. ! accommodation coefficient
  real, parameter :: akb     =1.380662e-23 ! boltzmann constant (j/k)
  real, parameter :: ara     =1. ! relative acidity
  real, parameter :: atune   =1. ! tuning factor for nucleation
  real, parameter :: avo     =6.022045e+23 ! avogadro constant (1/mol)
  !
  !     * molecular weights.
  !
  real, parameter :: wh2so4  =98.073e-03 ! molecular weight of sulphuric
  ! acid (kg/mol)
  real, parameter :: wso4    =96.058e-03 ! molecular weight of sulphate
  real, parameter :: wnh3    =17.030e-03 ! molecular weight of nh3 (kg/mol)
  real, parameter :: ws      =32.060e-03 ! molecular weight of s (kg/mol)
  real, parameter :: wamsul  =wh2so4+2.*wnh3 !<
  !                                              ! molecular weight of (nh4)2so4
  !
  !     * densities.
  !
  real, parameter :: dnh2o   =1.e+03 !<
  !
  !     * key array size parameters and other numerical parameters.
  !
  real :: ylarge !<
  real :: ylarges !<
  real :: ytiny !<
  real :: yna !<
  real :: r0 !<
  real :: ypi !<
  real :: sqrtpi !<
  real :: ycnst !<
  real :: drydn0 !<
  real(r8) :: ylarge8 !<
  real(r8) :: ytiny8 !<
  logical, parameter :: kpext=.true. !<
  logical, parameter :: kpint=.false. !<
  integer :: ilarge !<
  integer :: ina !<
  integer :: isatr !<
  integer :: idrbm !<
  integer :: kext !<
  integer :: kint !<
  integer :: isaert !<
  integer :: isaext !<
  integer :: isaextb !<
  integer :: isaint !<
  integer :: isaintb !<
  integer :: iswett !<
  integer :: iswext !<
  integer :: iswint !<
  integer :: kwext !<
  integer :: kwint !<
  integer :: ntracs !<
  integer :: kextso4 !<
  integer :: kintso4 !<
  integer :: isextso4 !<
  integer :: isintso4 !<
  integer :: isextss !<
  integer :: isintss !<
  integer :: isextno3 !<
  integer :: isintno3 !<
  integer :: isextcl !<
  integer :: isintcl !<
  integer :: isextoc !<
  integer :: isintoc !<
  integer :: isextbc !<
  integer :: isintbc !<
  integer :: isextmd !<
  integer :: isintmd !<
  integer :: kextsac !<
  integer :: kintsac !<
  integer :: isextsac !<
  integer :: isintsac !<
  integer :: iso4s !<
  integer :: isss !<
  integer :: ino3s !<
  integer :: icls !<
  integer :: iocs !<
  integer :: ibcs !<
  integer :: imds !<
  integer :: isacs !<
  integer :: kextss !<
  integer :: kextno3 !<
  integer :: kextcl !<
  integer :: kextoc !<
  integer :: kextbc !<
  integer :: kextmd !<
  integer :: kintss !<
  integer :: kintno3 !<
  integer :: kintcl !<
  integer :: kintoc !<
  integer :: kintbc !<
  integer :: kintmd !<
  integer :: iexkap !<
  integer :: iinkap !<
  integer :: insex !<
  integer :: infex !<
  integer :: imsex !<
  integer :: imfex !<
  integer :: insin !<
  integer :: infin !<
  integer :: jgs6 !<
  integer :: jgsp !<
  integer :: jso2 !<
  integer :: jhpo !<
  integer :: ngas !<
  integer :: ntract !<
  data kext     / 0 /
  data kint     / 0 /
  data kwext    / 0 /
  data kwint    / 0 /
  data ntracs   / 0 /
  data kextso4  / 0 /
  data kintso4  / 0 /
  data isextso4 / 0 /
  data isintso4 / 0 /
  data isextss  / 0 /
  data isintss  / 0 /
  data isextno3 / 0 /
  data isintno3 / 0 /
  data isextcl  / 0 /
  data isintcl  / 0 /
  data isextoc  / 0 /
  data isintoc  / 0 /
  data isextbc  / 0 /
  data isintbc  / 0 /
  data isextmd  / 0 /
  data isintmd  / 0 /
  data kextsac  / 0 /
  data kintsac  / 0 /
  data isextsac / 0 /
  data isintsac / 0 /
  data iso4s    / 0 /
  data isss     / 0 /
  data ino3s    / 0 /
  data icls     / 0 /
  data iocs     / 0 /
  data ibcs     / 0 /
  data imds     / 0 /
  data isacs    / 0 /
  data kextss   / 0 /
  data kextno3  / 0 /
  data kextcl   / 0 /
  data kextoc   / 0 /
  data kextbc   / 0 /
  data kextmd   / 0 /
  data kintss   / 0 /
  data kintno3  / 0 /
  data kintcl   / 0 /
  data kintoc   / 0 /
  data kintbc   / 0 /
  data kintmd   / 0 /
  data isaert   / 0 /
  data isaext   / 0 /
  data isaextb  / 0 /
  data isaint   / 0 /
  data isaintb  / 0 /
  data iswett   / 0 /
  data iswext   / 0 /
  data iswint   / 0 /
  data drydn0   / 0 /
  data iexkap   / 0 /
  data iinkap   / 0 /
  data insex    / 0 /
  data infex    / 0 /
  data imsex    / 0 /
  data imfex    / 0 /
  data insin    / 0 /
  data infin    / 0 /
  data jgs6     / 0 /
  data jgsp     / 0 /
  data jso2     / 0 /
  data jhpo     / 0 /
  data ngas     / 0 /
  data ntract   / 0 /
  !
  !     * derived-type pla parameter arrays.
  !
  real, allocatable, dimension(:) :: pedphis !<
  real, allocatable, dimension(:) :: pephiss !<
  real, allocatable, dimension(:) :: pedryrb !<
  real, allocatable, dimension(:) :: pedryrc !<
  real, allocatable, dimension(:) :: pidphis !<
  real, allocatable, dimension(:) :: piphiss !<
  real, allocatable, dimension(:) :: pidryrb !<
  real, allocatable, dimension(:) :: pidryrc !<
  type(bnds), allocatable, dimension(:) :: pedryr,pidryr
  integer, allocatable, dimension(:) :: peismin !<
  integer, allocatable, dimension(:) :: peismax !<
  integer, allocatable, dimension(:) :: peismnk !<
  integer, allocatable, dimension(:) :: peismxk !<
  integer, allocatable, dimension(:) :: piismin !<
  integer, allocatable, dimension(:) :: piismax !<
  type ptrant
    integer :: idrb !<
    logical, allocatable, dimension(:) :: flg !<
    type(bnds), allocatable, dimension(:) :: psib,phib,dphib
  end type ptrant
  type(ptrant), allocatable, dimension(:) :: pext,pint
  !
  !     * arrays that link tracer arrays to basic aerosol properties arrays.
  !
  type stextt
    integer :: ityp !<
    integer :: isi !<
    integer :: ism !<
    integer :: isn !<
  end type stextt
  type stext
    type(stextt), allocatable, dimension(:) :: isaer,iswet
  end type stext
  type(stext) :: sextf
  type stintt
    integer :: isi !<
    integer :: itypt !<
    integer :: isn !<
    integer, allocatable, dimension(:) :: ityp !<
    integer, allocatable, dimension(:) :: ism !<
  end type stintt
  type stint
    type(stintt), allocatable, dimension(:) :: isaer,iswet
  end type stint
  type(stint) :: sintf
  !
  !     * basic aerosol properties arrays for externally mixed aerosol types.
  !
  type textt
    integer :: isec !<
    integer :: idrb !<
    integer :: ntbl !<
    integer, allocatable, dimension(:) :: iso !<
    real :: radb !<
    real :: rade !<
    real :: dens !<
    real :: molw !<
    real :: nuio !<
    real :: dpst !<
    real :: dpstar !<
    real :: kappa !<
    real :: stick !<
    character(len=70) :: name !<
    real, allocatable, dimension(:) :: psim !<
    real, allocatable, dimension(:) :: dryrc !<
    logical, allocatable, dimension(:,:) :: flg !<
    type(bnds), allocatable, dimension(:) :: phi,dryr
    type(bnds), allocatable, dimension(:,:) :: psib,phib,dphib
  end type textt
  type text
    type(textt), allocatable, dimension(:) :: tp
  end type text
  type(text) :: aextf
  !
  !     * basic aerosol properties arrays for internally mixed aerosol types.
  !
  type tintt
    integer :: iscb !<
    integer :: isce !<
    real :: dens !<
    real :: molw !<
    real :: nuio !<
    real :: kappa !<
    character(len=70) :: name !<
  end type tintt
  type tintm
    integer :: isec !<
    integer :: idrb !<
    integer :: ntbl !<
    integer, allocatable, dimension(:) :: iso !<
    real :: radb !<
    real :: rade !<
    real :: dpst !<
    real :: dpstar !<
    real :: stick !<
    real, allocatable, dimension(:) :: psim !<
    real, allocatable, dimension(:) :: dryrc !<
    logical, allocatable, dimension(:,:) :: flg !<
    type(bnds), allocatable, dimension(:) :: phi,dryr
    type(bnds), allocatable, dimension(:,:) :: psib,phib,dphib
    type(tintt), allocatable, dimension(:) :: tp
  end type tintm
  type(tintm) :: aintf
  !
  integer, allocatable, dimension(:) :: iinso4 !<
  integer, allocatable, dimension(:) :: iexso4 !<
  integer, allocatable, dimension(:) :: itso4 !<
  integer, allocatable, dimension(:) :: iinss !<
  integer, allocatable, dimension(:) :: iexss !<
  integer, allocatable, dimension(:) :: itss !<
  integer, allocatable, dimension(:) :: iinno3 !<
  integer, allocatable, dimension(:) :: iexno3 !<
  integer, allocatable, dimension(:) :: itno3 !<
  integer, allocatable, dimension(:) :: iincl !<
  integer, allocatable, dimension(:) :: iexcl !<
  integer, allocatable, dimension(:) :: itcl !<
  integer, allocatable, dimension(:) :: iinoc !<
  integer, allocatable, dimension(:) :: iexoc !<
  integer, allocatable, dimension(:) :: itoc !<
  integer, allocatable, dimension(:) :: iinbc !<
  integer, allocatable, dimension(:) :: iexbc !<
  integer, allocatable, dimension(:) :: itbc !<
  integer, allocatable, dimension(:) :: iinmd !<
  integer, allocatable, dimension(:) :: iexmd !<
  integer, allocatable, dimension(:) :: itmd !<
  integer, allocatable, dimension(:) :: iinsac !<
  integer, allocatable, dimension(:) :: iexsac !<
  integer, allocatable, dimension(:) :: itsac !<
  !
  !     * parameters for coagulation calculations.
  !
  logical :: kcoag !<
  data kcoag / .true. /
  type tcoag
    integer :: isec !<
    real :: dpstar !<
  end type tcoag
  type(tcoag) :: coagp
  integer, allocatable, dimension(:)   :: iro0 !<
  integer, allocatable, dimension(:,:) :: iro !<
  integer, allocatable, dimension(:,:) :: itr !<
  integer, allocatable, dimension(:,:) :: itrm !<
  !
  integer :: isfint !<
  integer :: isfintb !<
  integer :: isfintm !<
  integer :: isftri !<
  integer :: isftrim !<
  data isfint  / 0 /
  data isfintb / 0 /
  data isfintm / 0 /
  data isftri  / 0 /
  data isftrim / 0 /
  !
  real, allocatable, dimension(:) :: fidryrc !<
  real, allocatable, dimension(:) :: fidryrb !<
  real(r8), allocatable, dimension(:) :: fidryvb !<
  real, allocatable, dimension(:,:) :: fiphib0 !<
  type(bnds), allocatable, dimension(:) :: fiphi,fidryr
  !
  !     * aerosol properties arrays for ex- and internally mixed
  !     * aerosol types for hygroscopic growth calculations.
  !
  type tatg
    integer :: ntstp !<
    real, allocatable, dimension(:) :: radt !<
  end type tatg
  type tamg
    type(tatg), allocatable, dimension(:) :: sec
  end type tamg
  type(tamg) :: aextfg,aintfg
  !
end module sdparm
