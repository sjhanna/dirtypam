!>    \file
!>    \brief Read in PAM configuration data
!!
!
!-----------------------------------------------------------------------
subroutine rdconf(nurad)
  !
  use rdmod, only : somgt2,idim2,sabst2,sextt2,sgt2
  !
  implicit none
  !
  integer, intent(in) :: nurad !< File unit number
  integer :: istatus !<
  integer :: irec !<
  !
  if (nurad>0) then
    do irec=1,idim2
      read(nurad,'(6(1X,E14.7))') sextt2(irec,:)
      read(nurad,'(5(1X,E14.7))') somgt2(irec,:)
      read(nurad,'(5(1X,E14.7))')   sgt2(irec,:)
      read(nurad,'(9(1X,E14.7))') sabst2(irec,:)
    end do
  end if

#ifdef GM_PAM
    ! broadcast read variables to all processes
    call rpn_comm_bcast(sextt2, idim2*6, "MPI_REAL", 0, "GRID", istatus)
    call rpn_comm_bcast(somgt2, idim2*5, "MPI_REAL", 0, "GRID", istatus)
    call rpn_comm_bcast(sgt2, idim2*5, "MPI_REAL", 0, "GRID", istatus)
    call rpn_comm_bcast(sabst2, idim2*9, "MPI_REAL", 0, "GRID", istatus)
#endif

end subroutine rdconf
