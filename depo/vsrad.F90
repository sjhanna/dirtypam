!> \file
!> \brief Dry deposition velocity
!!
!! @authors X. Ma, K. von Salzen, and M. Lazare
!
!-----------------------------------------------------------------------
subroutine vsrad(radw,radd,stick,t,p,rhoa,flnd,fcan, &
                       fcs,fgs,fc,fg,zspd,cdml,cdmnl, &
                       vdp,grav,ilga,leva,isec,ican)
  !
  use sdparm, only : akb,dnh2o,drydn0,ypi
  use fpdef,  only : r8
  !
  implicit none
  !
  integer, intent(in) :: ilga !< Number of grid points in the horizontal direction
  integer, intent(in) :: leva !< Number of grid points in the vertical direction
  integer, intent(in) :: isec !< Number of separate aerosol tracers
  real, intent(in), dimension(ilga,leva) :: t !< Air temperature \f$[K]\f$
  real, intent(in), dimension(ilga,leva) :: rhoa !< Air density \f$[kg/m^3]\f$
  real, intent(in), dimension(ilga,leva) :: p !< Air pressure \f$[Pa]\f$
  real, intent(in), dimension(ilga,leva,isec) :: stick !< Sticking probability
  real, intent(in), dimension(ilga,leva,isec) :: radw !< Dry radius
  real, intent(in), dimension(ilga,leva,isec) :: radd !< Wet radius
  real, intent(in) :: grav !< Gravtiational accelaration \f$[m/s^2]\f$
  !
  real, intent(in), dimension(ilga) :: fcs !< Fraction of canopy over snow on ground in a grid cell
  real, intent(in), dimension(ilga) :: fgs !< Fraction of snow on bare ground in a grid cell
  real, intent(in), dimension(ilga) :: fc !< Fraction of snow-free canopy in a grid cell
  real, intent(in), dimension(ilga) :: fg !< Fraction of snow-free bare ground in a grid cell
  real, intent(in), dimension(ilga) :: cdml !< Surface drag coefficient over land (dimensionless)
  real, intent(in), dimension(ilga) :: cdmnl !< Surface drag coefficient over water, ice, and open ocean (dimensionless)
  real, intent(in), dimension(ilga) :: flnd !< Fraction of land in a grid cell
  real, intent(in), dimension(ilga) :: zspd !< Surface wind speed \f$[m/sec]\f$
  real, intent(in), dimension(ilga,ican+1) :: fcan !<  Fraction of canopy type
  real, intent(out), dimension(ilga,isec) :: vdp !< Dry deposition velocity
  !
  !     internal work variables
  !
  real, allocatable, dimension(:,:) :: amu !<
  real, allocatable, dimension(:,:) :: amfp !<
  real, allocatable, dimension(:,:) :: term1 !<
  real, allocatable, dimension(:,:) :: anu !<
  real, allocatable, dimension(:,:,:) :: wdn !<
  real, allocatable, dimension(:,:,:) :: ddn !<
  real(r8) :: rssl !<
  real(r8) :: r1l !<
  real(r8) :: rebl !<
  real(r8) :: reiml !<
  real(r8) :: reinl !<
  real(r8) :: ustal !<
  real(r8) :: stl !<
  real(r8) :: aa !<
  real(r8) :: sc !<
  real(r8) :: alfal !<
  real(r8) :: raal !<
  real(r8) :: cfac !<
  real(r8) :: tmp1 !<
  real(r8) :: tmp2 !<
  real(r8) :: fgr !<
  real(r8) :: fgr3i !<
  real(r8) :: pdepv !<
  real(r8) :: rssnl !<
  real(r8) :: r1nl !<
  real(r8) :: rebnl !<
  real(r8) :: reimnl !<
  real(r8) :: reinnl !<
  real(r8) :: ustanl !<
  real(r8) :: stnl !<
  real(r8) :: alfanl !<
  real(r8) :: raanl !<
  integer :: is !<
  integer :: il !<
  integer :: ican !<
  integer :: l !<
  real :: a1 !<
  real :: a2 !<
  real :: a3 !<
  real :: a4 !<
  real :: al1 !<
  real :: al2 !<
  real :: al3 !<
  real :: al4 !<
  real :: c1 !<
  real :: c2 !<
  real :: c3 !<
  real :: c4 !<
  real :: cl1 !<
  real :: cl2 !<
  real :: cl3 !<
  real :: cl4 !<
  real :: amob !<
  real :: pdiff !<
  real :: vdpl !<
  real :: vdpnl !<
  !
  !-----------------------------------------------------------------------
  !     * allocate work arrays.
  !
  allocate(amu  (ilga,leva))
  allocate(amfp (ilga,leva))
  allocate(term1(ilga,leva))
  allocate(anu  (ilga,leva))
  !
  allocate(wdn  (ilga,leva,isec))
  allocate(ddn  (ilga,leva,isec))
  !
  !-----------------------------------------------------------------------
  !     * surface resistence (rss)
  !
  !     * dynamic viscosity of air.
  !
  term1=sqrt(t)
  term1=term1*t
  amu=145.8*1.e-8*term1/(t+110.4)
  !
  !     * kinematic viscosity of air
  !
  anu=amu/rhoa
  !
  !     * mean free path (k.v. beard, 1976) and cunningham slip
  !     * correction factor.
  !
  term1=sqrt(t/293.15)
  amfp=6.54e-8*(amu/1.818e-5)*(1.013e5/p)*term1
  !
  do is=1,isec
    do l=1,leva
      do il=1,ilga
        ddn(il,l,is)=drydn0
        fgr=radw(il,l,is)/radd(il,l,is)
        fgr3i=1./fgr**3
        wdn(il,l,is)=dnh2o*(1.-fgr3i)+ddn(il,l,is)*fgr3i
      end do
    end do
  end do
  !
  vdp=0.
  !------------------------------------------------------------------
  a1 = 2.0*1e-3                    !! canopy over snow
  a2 = 0.6*1e-3                    !! snow covered ground
  a3 = 5.0*1e-3                    !! canopy
  a4 = 8.0*1e-3                    !! ground
  !
  !     canopy 1: tall coniferous. &
  !     canopy 2: tall broadleaf. &
  !     canopy 3: arable and crops. &
  !     canopy 4: grass, swamp and tundra (i.e. other).
  !
  c1 = 2.6*1e-3                    !! TALL CONIFEROUS
  c2 = 6.0*1e-3                    !! TALL BROADLEAF
  c3 = 3.2*1e-3                    !! ARABLE AND CROPS
  c4 = 6.6*1e-3                    !! GRASS, SWAMP AND TUNDRA (I.E. OTHER)
  !
  al1 = 0.5                    !! canopy over snow
  al2 = 0.5                    !! snow covered ground
  al3 = 999.9                  !! canopy
  al4 = 15.0                   !! ground
  !
  cl1 = 1.1                    !! TALL CONIFEROUS
  cl2 = 0.7                    !! TALL BROADLEAF
  cl3 = 1.2                    !! ARABLE AND CROPS
  cl4 = 10.0                   !! GRASS, SWAMP AND TUNDRA (I.E. OTHER)
  !
  l=leva
  do is=1,isec
    do il=1,ilga
      !
      !       * cunninghan factor
      !
      tmp1=amfp(il,l)/radw(il,l,is)
      tmp2=radw(il,l,is)/amfp(il,l)
      tmp2=exp(-1.1_r8*tmp2)
      cfac=1._r8+tmp1*(1.257_r8+0.4_r8*tmp2)
      !
      !       * stokes friction and diffusion coefficients.
      !
      amob=6.*ypi*amu(il,l)*radw(il,l,is)/cfac
      pdiff=akb*t(il,l)/amob
      sc   =anu(il,l)/pdiff
     !
      !       * surface resistance (zhang et al., 2001,atmospheric environement)
      !
      !       * pdepv: gravatational settling velocities
      !       * eb   : collection efficiency from brownian diffution
      !       * eim  : collection efficiency from impaction
      !       * ein  : collection efficiency from interception
      !
      tmp1=radw(il,l,is)
      pdepv=2.0_r8*wdn(il,l,is)*tmp1**2*grav*cfac/(9.0_r8*amu(il,l))
      vdpl=0.
      vdpnl=0.
      if (flnd(il)>0.) then
        ustal  = sqrt(cdml(il))*zspd(il)
        raal   = 1.0_r8/(cdml(il)*zspd(il))
        a3     = fcan(il,1)*c1+fcan(il,2)*c2+fcan(il,3)*c3 &
                   +fcan(il,4)*c4
        al3    = fcan(il,1)*cl1+fcan(il,2)*cl2+fcan(il,3)*cl3 &
                   +fcan(il,4)*cl4
        aa     = fcs(il)*a1 +fgs(il)*a2 +fc(il)*a3 +fg(il)*a4
        alfal  = fcs(il)*al1+fgs(il)*al2+fc(il)*al3+fg(il)*al4
        stl    = pdepv*ustal/(grav*aa)
        !         alfal  = 0.8
      !***    original from Zhang (2001)
        !rebl   = sc**(-2.0_r8/3.0_r8)
        !reiml  = ( stl/(stl+alfal))**2
        !reiml  = min(reiml,0.8_r8)
        !reinl  = 0.5_r8*(2.0_r8*radw(il,l,is)/aa)**2
        !reinl  = min(reinl,0.6_r8)
      !***    Changes According to Emerson et al. (2020)
        rebl   = 0.2*sc**(-2.0_r8/3.0_r8)
        reiml  = 0.4*( stl/(stl+alfal))**1.7
        reinl  = 2.5_r8*(2.0_r8*radw(il,l,is)/aa)**0.8
      !***        
        r1l    = max(stick(il,l,is)*max(0.5_r8,exp(-sqrt(stl))) &
                   ,0.001_r8)
        rssl   = 1.0_r8/(3._r8*ustal*(rebl+reiml+reinl)*r1l)
        vdpl   = 1.0/(raal+rssl)
      end if
      if (flnd(il)<1.) then
        ustanl = sqrt(cdmnl(il))*zspd(il)
        raanl  = 1.0_r8/(cdmnl(il)*zspd(il))
        stnl   = pdepv*ustanl**2/(grav*anu(il,l))
        alfanl = 100.0_r8
      !***    original from Zhang et al. (2001)
        !rebnl  = 1._r8/sqrt(sc)
        !reimnl = ( stnl/(stnl+alfanl))**2
        !reimnl = min(reimnl,0.8_r8)
      !***    Changes according to Emerson (2020)
        rebnl  = 0.2*sc**(-2.0_r8/3.0_r8)
        reimnl = 0.4*( stnl/(stnl+alfanl))**1.7
      !***
        reinnl = 0._r8
        r1nl   = max(stick(il,l,is)*max(0.5_r8,exp(-sqrt(stnl))) &
                   ,0.001_r8)
        rssnl  = 1.0_r8/(3._r8*ustanl*(rebnl+reimnl+reinnl)*r1nl)
        vdpnl  = 1.0/(raanl+rssnl)
      end if
      !
      vdp(il,is) = flnd(il)*vdpl + (1.-flnd(il))*vdpnl
    end do
  end do
  !------------------------------------------------------------------
  !     * deallocate work arrays.
  !
  deallocate(amu)
  deallocate(amfp)
  deallocate(term1)
  deallocate(anu)
  !
  deallocate(wdn)
  deallocate(ddn)
  !
end subroutine vsrad
!> \file
!! \subsection ssec_details Details
!! Dry deposition velocity :
!! \f{equation*}{V_{s} = 1/(R_{a} + R_{s})\f}
