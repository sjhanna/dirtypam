!> \file
!> \brief Diagnosis of cloud-condensation nuclei (CCN) concentration based
!>       on dry size distribution and supersaturation.
!!
!! @author K. von Salzen
!
!-----------------------------------------------------------------------
subroutine cncdnca(cecdnc,cerci,cicdnc,circi,cewetrb,cerc,cesc, &
                         ciwetrb,circ,cisc,pen0,pephi0,pepsi,pephis0, &
                         pedphi0,pin0,piphi0,pipsi,piphis0,pidphi0,sv, &
                         ilga,leva)
  !
  use sdparm, only : aextf,ina,isaext,isaint,iswext,iswint, &
                         kext,kint, &
                         pedryr,peismax,peismin,peismxk,pidryr, &
                         r0,sextf,sintf,yna,ysec,ytiny
  use sdcode, only : sdint0
  use scparm, only : cedryrb,ceis,cekx,cidryrb,ciis,isextb,isintb
  !
  implicit none
  !
  integer, intent(in) :: ilga !< Number of grid points in the horizontal direction
  integer, intent(in) :: leva !< Number of grid points in the vertical direction
  real, intent(in), dimension(ilga,leva) :: sv !< Vertical profile of supersaturation
  real, intent(in), dimension(ilga,leva,isextb) :: cewetrb !< Radius for wet externally mixed aerosol particles
  real, intent(in), dimension(ilga,leva,isextb) :: cerc !< Critical particle radius for externally mixed aerosols
  real, intent(in), dimension(ilga,leva,isextb) :: cesc !< Critical particle supersaturation for externally mixed aerosols
  real, intent(in), dimension(ilga,leva,isintb) :: ciwetrb !< Radius for wet internally mixed aerosol particles
  real, intent(in), dimension(ilga,leva,isintb) :: circ !< Critical particle radius for internally mixed aerosols
  real, intent(in), dimension(ilga,leva,isintb) :: cisc !< Critical particle supersaturation for internally mixed aerosols
  real, intent(in), dimension(ilga,leva,isaext) :: pen0 !< 1st PLA size distribution parameter (\f$n_{0,i}\f$,amplitude) ext. mixture
  real, intent(in), dimension(ilga,leva,isaext) :: pephi0 !< 3rd pla size distribution parameter (\f$\phi_{0,i}\f$,width), ext. mixture
  real, intent(in), dimension(ilga,leva,isaext) :: pepsi !< 2nd pla size distribution parameter (\f$\psi_{i}\f$, mode size) ext. mixture
  real, intent(in), dimension(ilga,leva,isaext) :: pephis0 !< Dry particle size ln(Rp /R0) at the boundaries of
  !< the size sections for externally mixed aerosol
  real, intent(in), dimension(ilga,leva,isaext) :: pedphi0 !< Dry particle radius in the centres of the size
  !< sections for externally mixed aerosol \f$[m]\f$
  real, intent(in), dimension(ilga,leva,isaint) :: pin0 !< 1st PLA size distribution parameter (\f$n_{0,i}\f$, amplitude) int. mixture
  real, intent(in), dimension(ilga,leva,isaint) :: piphi0 !< 3rd pla size distribution parameter (\f$\phi_{0,i}\f$, width), int. mixture
  real, intent(in), dimension(ilga,leva,isaint) :: pipsi !< 2nd pla size distribution parameter (\f$\psi_{i}\f$, mode size) int. mixture
  real, intent(in), dimension(ilga,leva,isaint) :: piphis0 !< Dry particle size ln(Rp /R0) at the boundaries of
  !< the size sections for internally mixed aerosol
  real, intent(in), dimension(ilga,leva,isaint) :: pidphi0 !< Dry particle radius in the centres of the size
  !< sections for internally mixed aerosol \f$[m]\f$
  real, intent(out), dimension(ilga,leva) :: cicdnc !< Cloud droplet number concentration from integration of number
  !< size distribution. For internally mixed aerosols
  real, intent(out), dimension(ilga,leva) :: circi !< Critical radius for externally mixed aerosol that corresponds to actual
  !< supersaturation
  real, intent(out), dimension(ilga,leva,kext) :: cecdnc !< Cloud droplet number concentration from integration of number
  !< size distribution. For externally mixed aerosols.
  real, intent(out), dimension(ilga,leva,kext) :: cerci !< Critical radius for externally mixed aerosol that corresponds to actual
  !< supersaturation

  !
  !     internal work variables
  !
  logical, allocatable, dimension(:,:,:) :: tiact !<
  logical, allocatable, dimension(:,:,:) :: teact !<
  logical, allocatable, dimension(:,:,:) :: kfrst !<
  integer, allocatable, dimension(:,:) :: ciisi !<
  integer, allocatable, dimension(:,:,:) :: ceisi !<
  real, allocatable, dimension(:,:,:) :: tidphis !<
  real, allocatable, dimension(:,:,:) :: tiphiss !<
  real, allocatable, dimension(:,:,:) :: tisdf !<
  real, allocatable, dimension(:,:,:) :: tinum !<
  real, allocatable, dimension(:,:,:) :: tipn0 !<
  real, allocatable, dimension(:,:,:) :: tiphi0 !<
  real, allocatable, dimension(:,:,:) :: tipsi !<
  real, allocatable, dimension(:,:,:) :: tedphis !<
  real, allocatable, dimension(:,:,:) :: tephiss !<
  real, allocatable, dimension(:,:,:) :: tesdf !<
  real, allocatable, dimension(:,:,:) :: tenum !<
  real, allocatable, dimension(:,:,:) :: tepn0 !<
  real, allocatable, dimension(:,:,:) :: tephi0 !<
  real, allocatable, dimension(:,:,:) :: tepsi !<
  integer :: il !<
  integer :: is !<
  integer :: l !<
  integer :: kx !<
  integer :: ismax !<
  integer :: ismin !<
  integer :: ism !<
  integer :: isi !<
  integer :: isx !<
  integer :: isc !<
  real :: phisrt !<
  real :: phisst !<
  real :: rmax !<
  real :: rmin !<
  real :: rcit !<
  real :: grwfl !<
  real :: grwft !<
  real :: grwfh !<
  real :: dphist !<
  !
  !-----------------------------------------------------------------------
  !     * allocate work arrays.
  !
  if (kext > 0) then
    allocate(kfrst(ilga,leva,kext))
    allocate(ceisi(ilga,leva,kext))
  end if
  if (iswext > 0) then
    allocate(tepn0  (ilga,leva,iswext))
    allocate(tephi0 (ilga,leva,iswext))
    allocate(tepsi  (ilga,leva,iswext))
    allocate(tedphis(ilga,leva,iswext))
    allocate(tephiss(ilga,leva,iswext))
    allocate(tesdf  (ilga,leva,iswext))
    allocate(tenum  (ilga,leva,iswext))
    allocate(teact  (ilga,leva,iswext))
  end if
  if (kint > 0) then
    allocate(ciisi(ilga,leva))
  end if
  if (iswint > 0) then
    allocate(tipn0  (ilga,leva,iswint))
    allocate(tiphi0 (ilga,leva,iswint))
    allocate(tipsi  (ilga,leva,iswint))
    allocate(tidphis(ilga,leva,iswint))
    allocate(tiphiss(ilga,leva,iswint))
    allocate(tisdf  (ilga,leva,iswint))
    allocate(tinum  (ilga,leva,iswint))
    allocate(tiact  (ilga,leva,iswint))
  end if
  !
  !-----------------------------------------------------------------------
  !     * lineraly interpolate critical radii between section boundaries
  !     * for each externally mixed aerosol type to find matching critical
  !     * radius that corresponds to actual supersaturation.
  !
  if (isextb > 0) then
    ceisi=ina
    do kx=1,kext
      ismax=peismxk(kx)
      rmax=pedryr(ismax)%vr
      cerci(:,:,kx)=rmax
    end do
    kfrst=.true.
    teact=.false.
    do is=1,isextb-1
      if (cekx(is) == cekx(is+1) ) then
        ism=sextf%iswet(ceis(is))%ism
        ismin=peismin(ism)
        ismax=peismax(ism)
        rmin=pedryr(ismin)%vl
        rmax=pedryr(ismax)%vr
        do l=1,leva
          do il=1,ilga
            if (abs(cesc(il,l,is)-yna) > ytiny &
                .and. abs(cesc(il,l,is+1)-yna) > ytiny &
                .and. sv(il,l) > ytiny) then
              !
              !               * set activation size to size of smallest particle
              !               * if supersaturation large enough to activate these
              !               * particles.
              !
              if (kfrst(il,l,cekx(is)) ) then
                if (sv(il,l) > cesc(il,l,is) ) then
                  ceisi(il,l,cekx(is))=ceis(is)
                  cerci(il,l,cekx(is))=rmin
                end if
                kfrst(il,l,cekx(is))=.false.
              end if
              !
              !               * activation size from linear interpolation between
              !               * boundaries of each sub-section.
              !
              if (sv(il,l) >= cesc(il,l,is+1) &
                  .and. sv(il,l) < cesc(il,l,is) ) then
                rcit=cerc(il,l,is)+(sv(il,l)-cesc(il,l,is)) &
                                    *(cerc(il,l,is+1)-cerc(il,l,is)) &
                                    /(cesc(il,l,is+1)-cesc(il,l,is))
              else
                rcit=yna
              end if
              if (  (rcit>=cerc(il,l,is) .and. rcit<=cerc(il,l,is+1)) &
                  .or. (rcit<=cerc(il,l,is) .and. rcit>=cerc(il,l,is+1)) &
                  ) then
                ceisi(il,l,cekx(is))=ceis(is)
                !
                !                 * growth factor for first activated particle.
                !
                grwfl=cerc(il,l,is)/cedryrb(is)
                grwfh=cerc(il,l,is+1)/cedryrb(is+1)
                grwft=grwfl+(sv(il,l)-cesc(il,l,is))*(grwfh-grwfl) &
                                       /(cesc(il,l,is+1)-cesc(il,l,is))
                cerci(il,l,cekx(is))=rcit/grwft
              end if
              !
              !               * check if condensate present.
              !
              if (ysec*cewetrb(il,l,is)>cedryrb(is) &
                  .or. ysec*cewetrb(il,l,is+1)>cedryrb(is+1)  ) then
                teact(il,l,ceis(is))=.true.
              end if
            end if
          end do
        end do
      end if
    end do
  end if
  !
  !     * lineraly interpolate critical radii between section boundaries
  !     * for each internally mixed aerosol type to find matching critical
  !     * radius that corresponds to actual supersaturation.
  !
  if (isintb > 0) then
    ciisi=ina
    rmin=pidryr(1)%vl
    rmax=pidryr(isaint)%vr
    circi=rmax
    !
    !       * set activation size to size of smallest particle
    !       * if supersaturation large enough to activate these
    !       * particles.
    !
    is=1
    do l=1,leva
      do il=1,ilga
        if (abs(cisc(il,l,is)-yna) > ytiny .and. sv(il,l) > ytiny &
            ) then
          if (sv(il,l) > cisc(il,l,is) ) then
            ciisi(il,l)=ciis(is)
            circi(il,l)=rmin
          end if
        end if
      end do
    end do
    tiact=.false.
    do is=1,isintb-1
      do l=1,leva
        do il=1,ilga
          if (abs(cisc(il,l,is)-yna) > ytiny &
              .and. abs(cisc(il,l,is+1)-yna) > ytiny &
              .and. sv(il,l) > ytiny) then
            !
            !           * activation size from linear interpolation between
            !           * boundaries of each sub-section.
            !
            if (sv(il,l) >= cisc(il,l,is+1) &
                .and. sv(il,l) < cisc(il,l,is) ) then
              rcit=circ(il,l,is)+(sv(il,l)-cisc(il,l,is)) &
                                *(circ(il,l,is+1)-circ(il,l,is)) &
                                /(cisc(il,l,is+1)-cisc(il,l,is))
            else
              rcit=yna
            end if
            if (  (rcit>=circ(il,l,is) .and. rcit<=circ(il,l,is+1)) &
                .or. (rcit<=circ(il,l,is) .and. rcit>=circ(il,l,is+1)) &
                ) then
              ciisi(il,l)=ciis(is)
              !
              !             * growth factor for first activated particle.
              !
              grwfl=circ(il,l,is)/cidryrb(is)
              grwfh=circ(il,l,is+1)/cidryrb(is+1)
              grwft=grwfl+(sv(il,l)-cisc(il,l,is))*(grwfh-grwfl) &
                                       /(cisc(il,l,is+1)-cisc(il,l,is))
              circi(il,l)=rcit/grwft
            end if
            !
            !           * check if condensate present.
            !
            if (ysec*ciwetrb(il,l,is)>cidryrb(is) &
                .or. ysec*ciwetrb(il,l,is+1)>cidryrb(is+1)      ) then
              tiact(il,l,ciis(is))=.true.
            end if
          end if
        end do
      end do
    end do
  end if
  !
  !-----------------------------------------------------------------------
  !     * pla parameters for externally mixed aerosol types.
  !
  do is=1,iswext
    ism=sextf%iswet(is)%ism
    tepn0  (:,:,is)=pen0   (:,:,ism)
    tephi0 (:,:,is)=pephi0 (:,:,ism)
    tepsi  (:,:,is)=pepsi  (:,:,ism)
    tedphis(:,:,is)=pedphi0(:,:,ism)
    tephiss(:,:,is)=pephis0(:,:,ism)
  end do
  !
  !     * pla parameters for internally mixed aerosol types.
  !
  do is=1,iswint
    ism=sintf%iswet(is)%isi
    tipn0  (:,:,is)=pin0   (:,:,ism)
    tiphi0 (:,:,is)=piphi0 (:,:,ism)
    tipsi  (:,:,is)=pipsi  (:,:,ism)
    tidphis(:,:,is)=pidphi0(:,:,ism)
    tiphiss(:,:,is)=piphis0(:,:,ism)
  end do
  !
  !     * copy size information for activation size into size information
  !     * arrays for externally mixed aerosol.
  !
  if (iswext > 0) then
    do kx=1,kext
      do l=1,leva
        do il=1,ilga
          is=ceisi(il,l,kx)
          if (is /= ina) then
            phisrt=tephiss(il,l,is)+tedphis(il,l,is)
            phisst=min(max(log(cerci(il,l,kx)/r0),tephiss(il,l,is)), &
                                                                phisrt)
            dphist=phisrt-phisst
            tephiss(il,l,is)=phisst
            tedphis(il,l,is)=dphist
          end if
        end do
      end do
    end do
  end if
  !
  !     * copy size information for activation size into size information
  !     * arrays for internally mixed aerosol.
  !
  if (iswint > 0) then
    do l=1,leva
      do il=1,ilga
        is=ciisi(il,l)
        if (is /= ina) then
          phisrt=tiphiss(il,l,is)+tidphis(il,l,is)
          phisst=min(max(log(circi(il,l)/r0),tiphiss(il,l,is)), &
                                                                phisrt)
          dphist=phisrt-phisst
          tiphiss(il,l,is)=phisst
          tidphis(il,l,is)=dphist
        end if
      end do
    end do
  end if
  !
  !     * obtain number concentrations from integration of size disribution
  !     * for externally mixed aerosols.
  !
  if (iswext > 0) then
    tesdf=sdint0(tephi0,tepsi,tephiss,tedphis,ilga,leva,iswext)
    where (abs(tepsi-yna) > ytiny)
      tenum=tepn0*tesdf
    else where
      tenum=0.
    end where
  end if
  !
  !     * obtain number concentrations from integration of size disribution
  !     * for internally mixed aerosols.
  !
  if (iswint > 0) then
    tisdf=sdint0(tiphi0,tipsi,tiphiss,tidphis,ilga,leva,iswint)
    where (abs(tipsi-yna) > ytiny)
      tinum=tipn0*tisdf
    else where
      tinum=0.
    end where
  end if
  !
  !-----------------------------------------------------------------------
  !     * cloud droplet number concentration from integration of number
  !     * size distribution from critical size to size of largest particle
  !     * in the size distribution. for externally mixed aerosols.
  !
  if (kext > 0) then
    cecdnc=0.
  end if
  isi=0
  do kx=1,kext
    if (     (abs(aextf%tp(kx)%nuio-yna) > ytiny &
        .and. aextf%tp(kx)%nuio  > ytiny) &
        .or. (abs(aextf%tp(kx)%kappa-yna) > ytiny &
        .and. aextf%tp(kx)%kappa > ytiny) ) then
      do is=1,aextf%tp(kx)%isec
        isi=isi+1
        do l=1,leva
          do il=1,ilga
            isx=ceisi(il,l,kx)
            if (isx /= ina) then
              isc=sextf%iswet(isx)%isi
              if (is >= isc .and. teact(il,l,isi) ) then
                cecdnc(il,l,kx)=cecdnc(il,l,kx)+tenum(il,l,isi)
              end if
            end if
          end do
        end do
      end do
    end if
  end do
  !
  !     * cloud droplet number concentration from integration of number
  !     * size distribution. for internally mixed aerosols.
  !
  if (kint > 0) then
    cicdnc=0.
  end if
  do is=1,iswint
    do l=1,leva
      do il=1,ilga
        isx=ciisi(il,l)
        if (isx /= ina) then
          if (is >= isx .and. tiact(il,l,is) ) then
            cicdnc(il,l)=cicdnc(il,l)+tinum(il,l,is)
          end if
        end if
      end do
    end do
  end do
  !
  !-----------------------------------------------------------------------
  !     * deallocate work arrays.
  !
  if (kext > 0) then
    deallocate(kfrst)
    deallocate(ceisi)
  end if
  if (iswext > 0) then
    deallocate(tepn0)
    deallocate(tephi0)
    deallocate(tepsi)
    deallocate(tedphis)
    deallocate(tephiss)
    deallocate(tesdf)
    deallocate(tenum)
    deallocate(teact)
  end if
  if (kint > 0) then
    deallocate(ciisi)
  end if
  if (iswint > 0) then
    deallocate(tipn0)
    deallocate(tiphi0)
    deallocate(tipsi)
    deallocate(tidphis)
    deallocate(tiphiss)
    deallocate(tisdf)
    deallocate(tinum)
    deallocate(tiact)
  end if
  !
end subroutine cncdnca
