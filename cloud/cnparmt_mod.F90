!> \file
!> \brief Basic temporary work parameters for CCN calculations, relevant
!>       to tabulated growth data.
!!
!! @author K. von Salzen
!
!-----------------------------------------------------------------------
module cnparmt
  !
  implicit none
  !
  type rego
    integer :: ibt !<
    integer :: irt !<
    real, allocatable, dimension(:,:) :: bs !<
    real, allocatable, dimension(:,:,:) :: xp !<
    real, allocatable, dimension(:,:,:) :: tp !<
  end type rego
  type(rego), allocatable, dimension(:) :: rgpno
  type pntdpt
    real, allocatable, dimension(:) :: pntl !<
    real, allocatable, dimension(:) :: derl !<
    real, allocatable, dimension(:) :: pntu !<
    real, allocatable, dimension(:) :: deru !<
  end type pntdpt
  type(pntdpt), allocatable, dimension(:) :: rgbt
  type regt
    logical, allocatable, dimension(:,:,:) :: ichkb !<
    integer, allocatable, dimension(:,:,:) :: iplnka !<
    integer, allocatable, dimension(:,:,:) :: iplnkb !<
    integer, allocatable, dimension(:,:,:) :: islnka !<
    integer, allocatable, dimension(:,:,:) :: islnkb !<
    integer, allocatable, dimension(:,:) :: ipl !<
    integer, allocatable, dimension(:,:) :: ipu !<
    integer, allocatable, dimension(:,:) :: ipd !<
    integer, allocatable, dimension(:,:) :: ipm !<
    integer, allocatable, dimension(:,:) :: iplnki !<
    integer, allocatable, dimension(:,:) :: ipnl !<
  end type regt
  type(regt), allocatable, dimension(:) :: itmp
  !
end module cnparmt
